(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.knowledgeActivity = {
    attach: function (context, settings) {
      var $activity = once('knowledge-activity', '.knowledge-activity', context);
      $('.knowledge-activity', context).map(function() {
        var id = $(this).prop('id');

        /* global Chart:object */
        if ($activity.length && settings.knowledge_activity[id] && Chart) {
          var default_activity_chart_height = 500;
          var activity_chart_height = 16 *
            settings.knowledge_activity[id].labels.length;

          if (activity_chart_height < default_activity_chart_height) {
            activity_chart_height = default_activity_chart_height;
          }
          var $canvas =
            $(`<canvas width="500" height="${activity_chart_height}"></canvas>`);
          $(this).append($canvas);

          new Chart($canvas, {
            type: 'bar',
            data: settings.knowledge_activity[id],
            options: {
              indexAxis: 'y'
            }
          });
        }

      });


    }
  };

}(jQuery, Drupal));
