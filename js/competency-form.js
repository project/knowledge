/**
 * @file
 * Defines JavaScript behaviors for the node module.
 */

(function ($, Drupal) {

  /**
   * Update the summary of a vertical tab.
   */
  function updateSummary(element) {
    const total = $(element).find('input[type="checkbox"]').length;
    const correct = $(element).find('input[type="checkbox"]:checked').length;
    var summary = Drupal.t('@correct of @total', {'@correct': correct, '@total': total});

    if (correct > 0) {
      const percent = Math.round((correct / total) * 100);
      summary = Drupal.t('@correct of @total <span class="pull-right">@percent%</span>', {'@correct': correct, '@total': total, '@percent': percent});
    }


    $(element).drupalSetSummary(summary);
  }

  /**
   * Behaviors for tabs in the node edit form.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches summary behavior for tabs in the node edit form.
   */
  Drupal.behaviors.knowledgeCompetencySummaries = {
    attach(context) {

      $(context).find('details.vertical-tabs__item').each(function () {
        updateSummary(this);
      });

      once('knowledge-competency-summaries', 'details.vertical-tabs__item input[type="checkbox"]', context).forEach(function (element) {
        $(element).on('change', function () {
          // Your code to execute when the checkbox changes state
          const parent =$(element).parents('details.vertical-tabs__item');
          updateSummary(parent);
        });
      });

    },
  };
})(jQuery, Drupal);
