/**
 * @file
 * Attaches behaviors for the Knowledge module's "by-viewer" class.
 */

(function ($, Drupal, drupalSettings) {
  /**
   * Add 'by-viewer' class to knowledge written by the current user.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.knowledgeByViewer = {
    attach(context) {
      const currentUserID = parseInt(drupalSettings.user.uid, 10);
      $('[data-knowledge-user-id]')
        .filter(function () {
          return (
            parseInt(this.getAttribute('data-knowledge-user-id'), 10) ===
            currentUserID
          );
        })
        .addClass('by-viewer');
    },
  };
})(jQuery, Drupal, drupalSettings);
