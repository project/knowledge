/**
 * @file
 * Attaches behaviors for the Knowledge module's "X new knowledge" link.
 *
 * May only be loaded for authenticated users, with the History module
 * installed.
 */

(function ($, Drupal, drupalSettings) {
  /**
   * Hides a "new knowledge" element.
   *
   * @param {jQuery} $placeholder
   *   The placeholder element of the new knowledge link.
   *
   * @return {jQuery}
   *   The placeholder element passed in as a parameter.
   */
  function hide($placeholder) {
    return (
      $placeholder
        // Find the parent <li>.
        .closest('.knowledge-new-knowledge')
        // Find the preceding <li>, if any, and give it the 'last' class.
        .prev()
        .addClass('last')
        // Go back to the parent <li> and hide it.
        .end()
        .hide()
    );
  }

  /**
   * Removes a "new knowledge" element.
   *
   * @param {jQuery} $placeholder
   *   The placeholder element of the new knowledge link.
   */
  function remove($placeholder) {
    hide($placeholder).remove();
  }

  /**
   * Shows a "new knowledge" element.
   *
   * @param {jQuery} $placeholder
   *   The placeholder element of the new knowledge link.
   *
   * @return {jQuery}
   *   The placeholder element passed in as a parameter.
   */
  function show($placeholder) {
    return (
      $placeholder
        // Find the parent <li>.
        .closest('.knowledge-new-knowledge')
        // Find the preceding <li>, if any, and remove its 'last' class, if any.
        .prev()
        .removeClass('last')
        // Go back to the parent <li> and show it.
        .end()
        .show()
    );
  }

  /**
   * Processes new knowledge links and adds appropriate text in relevant cases.
   *
   * @param {Array.<Element>} placeholders
   *   The placeholder elements of the current page.
   */
  function processNodeNewKnowledgeLinks(placeholders) {
    // Figure out which placeholders need the "x new knowledge" links.
    const $placeholdersToUpdate = {};
    let fieldName = 'knowledge';
    let $placeholder;
    placeholders.forEach((placeholder) => {
      $placeholder = $(placeholder);
      const timestamp = parseInt(
        $placeholder.attr('data-history-node-last-knowledge-timestamp'),
        10,
      );
      fieldName = $placeholder.attr('data-history-node-field-name');
      const nodeID = $placeholder
        .closest('[data-history-node-id]')
        .attr('data-history-node-id');
      const lastViewTimestamp = Drupal.history.getLastRead(nodeID);

      // Queue this placeholder's "X new knowledge" link to be downloaded from
      // the server.
      if (timestamp > lastViewTimestamp) {
        $placeholdersToUpdate[nodeID] = $placeholder;
      }
      // No "X new knowledge" link necessary; remove it from the DOM.
      else {
        remove($placeholder);
      }
    });

    // Perform an AJAX request to retrieve node view timestamps.
    const nodeIDs = Object.keys($placeholdersToUpdate);
    if (nodeIDs.length === 0) {
      return;
    }

    /**
     * Renders the "X new knowledge" links.
     *
     * Either use the data embedded in the page or perform an AJAX request to
     * retrieve the same data.
     *
     * @param {object} results
     *   Data about new knowledge links indexed by nodeID.
     */
    function render(results) {
      Object.keys(results || {}).forEach((nodeID) => {
        if ($placeholdersToUpdate.hasOwnProperty(nodeID)) {
          const $placeholderItem = $placeholdersToUpdate[nodeID];
          const result = results[nodeID];
          $placeholderItem[0].textContent = Drupal.formatPlural(
            result.new_total_count,
            '1 new knowledge',
            '@count new knowledge',
          );
          $placeholderItem
            .attr('href', result.first_new_knowledge_link)
            .removeClass('hidden');
          show($placeholderItem);
        }
      });
    }

    if (drupalSettings.knowledge && drupalSettings.knowledge.newKnowledgeLinks) {
      render(drupalSettings.knowledge.newKnowledgeLinks.node[fieldName]);
    } else {
      $.ajax({
        url: Drupal.url('knowledge/render_new_knowledge_node_links'),
        type: 'POST',
        data: { 'node_ids[]': nodeIDs, field_name: fieldName },
        dataType: 'json',
        success: render,
      });
    }
  }

  /**
   * Render "X new knowledge" links wherever necessary.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches new knowledge links behavior.
   */
  Drupal.behaviors.nodeNewKnowledgeLink = {
    attach(context) {
      // Collect all "X new knowledge" node link placeholders (and their
      // corresponding node IDs) newer than 30 days ago that have not already
      // been read after their last knowledge timestamp.
      const nodeIDs = [];
      const placeholders = once(
        'history',
        '[data-history-node-last-knowledge-timestamp]',
        context,
      ).filter((placeholder) => {
        const $placeholder = $(placeholder);
        const lastKnowledgeTimestamp = parseInt(
          $placeholder.attr('data-history-node-last-knowledge-timestamp'),
          10,
        );
        const nodeID = $placeholder
          .closest('[data-history-node-id]')
          .attr('data-history-node-id');
        if (Drupal.history.needsServerCheck(nodeID, lastKnowledgeTimestamp)) {
          nodeIDs.push(nodeID);
          // Hide this placeholder link until it is certain we'll need it.
          hide($placeholder);
          return true;
        }

        // Remove this placeholder link from the DOM because we won't need it.
        remove($placeholder);
        return false;
      });

      if (placeholders.length === 0) {
        return;
      }

      // Perform an AJAX request to retrieve node read timestamps.
      Drupal.history.fetchTimestamps(nodeIDs, () => {
        processNodeNewKnowledgeLinks(placeholders);
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
