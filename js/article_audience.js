(function ($, Drupal) {
    'use strict';

    Drupal.behaviors.knowledgeActivityAudience = {
        attach: function (context, settings) {
            var $activity = once('knowledge-article-audience', '.knowledge-article-audience', context);
            $('.knowledge-article-audience', context).map(function () {
                var id = $(this).prop('id');

                /* global Chart:object */
                if ($activity.length && settings.knowledge_article_audience[id] && Chart) {
                    var default_activity_chart_height = 500;
                    var activity_chart_height = 16 *
                        settings.knowledge_article_audience[id].labels.length;

                    if (activity_chart_height < default_activity_chart_height) {
                        activity_chart_height = default_activity_chart_height;
                    }
                    var $canvas =
                        $(`<canvas width="500" height="${activity_chart_height}"></canvas>`);
                    $(this).append($canvas);

                    new Chart($canvas, {
                        type: 'pie',
                        data: settings.knowledge_article_audience[id],
                        options: {
                        }
                    });
                }

            });


        }
    };

}(jQuery, Drupal));
