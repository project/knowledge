(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.knowledgeActivity = {
    attach: function (context, settings) {
      var $activity = once('competency-report', '.competency-report', context);
      $('.competency-report', context).map(function() {
        var id = $(this).prop('id');

        /* global Chart:object */
        console.log(settings.competency_report[id])
        if ($activity.length && settings.competency_report[id] && Chart) {
          var default_activity_chart_height = 300;
          var activity_chart_height = 300; //16 *
            // settings.competency_report[id].labels.length;

          if (activity_chart_height < default_activity_chart_height) {
            activity_chart_height = default_activity_chart_height;
          }
          var $canvas =
            $(`<canvas width="500" height="${activity_chart_height}"></canvas>`);
          $(this).append($canvas);

          new Chart($canvas, {
            type: 'bar',
            data: settings.competency_report[id],
            options: {
              indexAxis: 'y',
              plugins: {
                legend: {
                  display: false
                },
              }
            }
          });
        }

      });


    }
  };

}(jQuery, Drupal));
