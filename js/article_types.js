(function ($, Drupal) {
    'use strict';

    Drupal.behaviors.knowledgeActivityTypes = {
        attach: function (context, settings) {
            var $activity = once('knowledge-article-types', '.knowledge-article-types', context);
            $('.knowledge-article-types', context).map(function () {
                var id = $(this).prop('id');

                /* global Chart:object */
                if ($activity.length && settings.knowledge_article_type[id] && Chart) {
                    var default_activity_chart_height = 500;
                    var activity_chart_height = 16 *
                        settings.knowledge_article_type[id].labels.length;

                    if (activity_chart_height < default_activity_chart_height) {
                        activity_chart_height = default_activity_chart_height;
                    }
                    var $canvas =
                        $(`<canvas width="500" height="${activity_chart_height}"></canvas>`);
                    $(this).append($canvas);

                    new Chart($canvas, {
                        type: 'pie',
                        data: settings.knowledge_article_type[id],
                        options: {
                        }
                    });
                }

            });


        }
    };

}(jQuery, Drupal));
