(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.knowledgeAdherenceForm = {
    attach: function (context, settings) {
      var getDisposition = function(context) {
        var disposition = 'Pending';
        var is_linked = $(':input[name="linked[0][value]"]:checked', context).val();
        var is_reuse = $(':input[name="reused[0][value]"]:checked', context).val();
        var is_accurate = $(':input[name="accurate[0][value]"]:checked', context).val();
        var is_improve = $(':input[name="improve[0][value]"]:checked', context).val();
        var is_required = $(':input[name="required[0][value]"]:checked', context).val();
        var is_found = $(':input[name="found[0][value]"]:checked', context).val();

        if (is_linked === '1') {
          if (is_reuse === '1') {
            if (is_accurate === '1') {
              if (is_improve === '1') {
                disposition = 'Improve loss';
              }
              if (is_improve === '0') {
                disposition = 'Reuse';
              }
            }
            else if (is_accurate === '0') {
              disposition = 'Inaccurate Reuse';
            }
          }
          else if (is_reuse === '0') {
            disposition = 'Created';
          }
        }
        else if (is_linked === '0') {
          if (is_required === '1') {
            if (is_found === '1') {
              disposition = 'Reuse Loss';
            }
            else if (is_found === '0') {
              disposition = 'Create Loss';
            }
          }
          else if (is_required === '0') {
            disposition = 'No Link Required';
          }
        }


        return disposition;
      }

      $('#knowledge-adherence-add-form').ready(function(){
        var name = $(this).prop('name');
        var checked = $(this).val();

        var disposition = getDisposition(context);
        $('#edit-incident-disposition', context).text(disposition);
        var is_pending = (disposition === 'Pending');

        if (name === 'found[0][value]' && checked === '1') {
          var article = $('[data-drupal-selector="edit-article-id-0-target-id"]', context).val();
          $('[data-drupal-selector="edit-submit"]', context).prop('disabled', 0 === article.length);
        } else {
          $('[data-drupal-selector="edit-submit"]', context).prop('disabled', is_pending);
        }
        $('[data-drupal-selector="edit-disposition"]', context).val(disposition);


      });
      $('#knowledge-adherence-edit-form :input, #knowledge-adherence-add-form :input', context).change(function(){
        var name = $(this).prop('name');
        var checked = $(this).val();

        var disposition = getDisposition(context);
        $('#edit-incident-disposition', context).text(disposition);
        var is_pending = (disposition === 'Pending');

        if (name === 'found[0][value]' && checked === '1') {
          var article = $('[data-drupal-selector="edit-article-id-0-target-id"]', context).val();
          $('[data-drupal-selector="edit-submit"]', context).prop('disabled', 0 === article.length);
        } else {
          $('[data-drupal-selector="edit-submit"]', context).prop('disabled', is_pending);
        }
        $('[data-drupal-selector="edit-disposition"]', context).val(disposition);

      });


    }
  };

}(jQuery, Drupal));