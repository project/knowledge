<?php

namespace Drupal\knowledge_base_field_test\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;

/**
 * Defines a test entity class for knowledge as a base field.
 *
 * @ContentEntityType(
 *   id = "knowledge_test_base_field",
 *   label = @Translation("Test knowledge - base field"),
 *   base_table = "knowledge_test_base_field",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type"
 *   },
 * )
 */
class KnowledgeTestBaseField extends EntityTest {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['test_knowledge'] = BaseFieldDefinition::create('knowledge')
      ->setLabel(t('A knowledge field'))
      ->setSetting('knowledge_type', 'test_knowledge_type')
      ->setDefaultValue([
        'status' => KnowledgeItemInterface::OPEN,
      ]);

    return $fields;
  }

}
