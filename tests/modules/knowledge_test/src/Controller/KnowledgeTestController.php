<?php

namespace Drupal\knowledge_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\knowledge\KnowledgeInterface;

/**
 * Controller for the knowledge_test.module.
 */
class KnowledgeTestController extends ControllerBase {

  /**
   * Provides a knowledge report.
   */
  public function knowledgeReport(KnowledgeInterface $knowledge) {
    return ['#markup' => $this->t('Report for a knowledge')];
  }

}
