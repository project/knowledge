<?php

/**
 * @file
 * Dummy module implementing knowledge related hooks to test API interaction.
 */

use Drupal\Core\Url;
use Drupal\knowledge\KnowledgeInterface;

/**
 * Implements hook_entity_type_alter().
 */
function knowledge_test_entity_type_alter(array &$entity_types) {
  /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
  if (\Drupal::languageManager()->isMultilingual()) {
    // Enable language handling for knowledge fields.
    $translation = $entity_types['knowledge']->get('translation');
    $translation['knowledge_test'] = TRUE;
    $entity_types['knowledge']->set('translation', $translation);
  }
}

/**
 * Implements hook_knowledge_links_alter().
 */
function knowledge_test_knowledge_links_alter(array &$links, KnowledgeInterface &$entity, array &$context) {
  // Allow tests to enable or disable this alter hook.
  if (!\Drupal::state()->get('knowledge_test_links_alter_enabled', FALSE)) {
    return;
  }

  $links['knowledge_test'] = [
    '#theme' => 'links__knowledge__knowledge_test',
    '#attributes' => ['class' => ['links', 'inline']],
    '#links' => [
      'knowledge-report' => [
        'title' => t('Report'),
        'url' => Url::fromRoute('knowledge_test.report', ['knowledge' => $entity->id()], ['query' => ['token' => \Drupal::getContainer()->get('csrf_token')->get("knowledge/{$entity->id()}/report")]]),
      ],
    ],
  ];
}
