<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Language\LanguageInterface;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\user\RoleInterface;

/**
 * Tests making knowledge base fields' displays configurable.
 *
 * @group knowledge
 */
class KnowledgeDisplayConfigurableTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Allow anonymous users to see knowledge.
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge',
      'access content',
    ]);
  }

  /**
   * Sets base fields to configurable display and check settings are respected.
   */
  public function testDisplayConfigurable() {
    // Add a knowledge.
    $nid = $this->node->id();
    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = Knowledge::create([
      'entity_id' => $nid,
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'uid' => $this->webUser->id(),
      'status' => KnowledgeInterface::PUBLISHED,
      'language' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $knowledge->save();
    $assert = $this->assertSession();

    // Check the knowledge author with Drupal default non-configurable display.
    $this->drupalGet('node/' . $nid);
    $assert->elementExists('css', 'span.knowledge__author');

    // Enable module to make base fields' displays configurable.
    \Drupal::service('module_installer')->install(['knowledge_display_configurable_test']);

    // Configure display.
    $display = EntityViewDisplay::load('knowledge.knowledge.default');
    $display->setComponent('uid',
      [
        'type' => 'entity_reference_label',
        'label' => 'above',
        'settings' => ['link' => FALSE],
      ])
      ->save();
    // Recheck the knowledge author with configurable display.
    $this->drupalGet('node/' . $nid);
    $assert->elementExists('css', '.field--name-uid .field__item');

    // Remove from display.
    $display->removeComponent('uid')
      ->removeComponent('created')
      ->save();

    $this->drupalGet('node/' . $this->node->id());
    $assert->elementNotExists('css', '.field--name-uid .field__item');
  }

}
