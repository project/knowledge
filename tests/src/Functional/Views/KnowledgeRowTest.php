<?php

namespace Drupal\Tests\knowledge\Functional\Views;

/**
 * Tests the knowledge row plugin.
 *
 * @group knowledge
 */
class KnowledgeRowTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_knowledge_row'];

  /**
   * Tests knowledge row.
   */
  public function testKnowledgeRow() {
    $this->drupalGet('test-knowledge-row');

    $result = $this->xpath('//article[contains(@class, "knowledge")]');
    $this->assertCount(1, $result, 'One rendered knowledge found.');
  }

}
