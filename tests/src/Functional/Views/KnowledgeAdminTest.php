<?php

namespace Drupal\Tests\knowledge\Functional\Views;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Tests\knowledge\Functional\KnowledgeTestBase as KnowledgeBrowserTestBase;
use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\user\RoleInterface;
use Drupal\views\Views;

/**
 * Tests knowledge approval functionality.
 *
 * @group knowledge
 */
class KnowledgeAdminTest extends KnowledgeBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::service('module_installer')->install(['views']);
    $view = Views::getView('knowledge');
    $view->storage->enable()->save();
    \Drupal::service('router.builder')->rebuildIfNeeded();
  }

  /**
   * Tests knowledge approval functionality through admin/content/knowledge.
   */
  public function testApprovalAdminInterface() {
    // Set anonymous knowledge to require approval.
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => TRUE,
      'post knowledge' => TRUE,
      'skip knowledge approval' => FALSE,
    ]);
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalLogin($this->adminUser);
    // Ensure that doesn't require contact info.
    $this->setKnowledgeAnonymous('0');

    // Test that the knowledge page loads correctly when there are no knowledge.
    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->pageTextContains('No knowledge available.');

    // Assert the expose filters on the admin page.
    $this->assertSession()->fieldExists('subject');
    $this->assertSession()->fieldExists('author_name');
    $this->assertSession()->fieldExists('langcode');

    $this->drupalLogout();

    // Post anonymous knowledge without contact info.
    $body = $this->getRandomGenerator()->sentences(4);
    $subject = Unicode::truncate(trim(Html::decodeEntities(strip_tags($body))), 29, TRUE, TRUE);
    $author_name = $this->randomMachineName();
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->submitForm([
      'name' => $author_name,
      'knowledge_body[0][value]' => $body,
    ], 'Save');
    $this->assertSession()->pageTextContains('Your knowledge has been queued for review by site administrators and will be published after approval.');

    // Get unapproved knowledge id.
    $this->drupalLogin($this->adminUser);
    $anonymous_knowledge4 = $this->getUnapprovedKnowledge($subject);
    $anonymous_knowledge4 = Knowledge::create([
      'kid' => $anonymous_knowledge4,
      'subject' => $subject,
      'knowledge_body' => $body,
      'entity_id' => $this->node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ]);
    $this->drupalLogout();

    $this->assertFalse($this->knowledgeExists($anonymous_knowledge4), 'Anonymous knowledge was not published.');

    // Approve knowledge.
    $this->drupalLogin($this->adminUser);
    $edit = [];
    $edit['action'] = 'knowledge_publish_action';
    $edit['knowledge_bulk_form[0]'] = $anonymous_knowledge4->id();
    $this->drupalGet('admin/content/knowledge/approval');
    $this->submitForm($edit, 'Apply to selected items');

    $this->assertSession()->pageTextContains('Publish knowledge was applied to 1 item.');
    $this->drupalLogout();

    $this->drupalGet('node/' . $this->node->id());
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge4), 'Anonymous knowledge visible.');

    // Post 2 anonymous knowledge without contact info.
    $knowledge_links[] = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Publish multiple knowledge in one operation.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content/knowledge/approval');
    $this->assertSession()->pageTextContains('Unapproved knowledge (2)');

    // Assert the expose filters on the admin page.
    $this->assertSession()->fieldExists('subject');
    $this->assertSession()->fieldExists('author_name');
    $this->assertSession()->fieldExists('langcode');

    $edit = [
      "action" => 'knowledge_publish_action',
      "knowledge_bulk_form[1]" => $knowledge_links[0]->id(),
      "knowledge_bulk_form[0]" => $knowledge_links[1]->id(),
    ];
    $this->submitForm($edit, 'Apply to selected items');
    $this->assertSession()->pageTextContains('Unapproved knowledge (0)');

    // Test message when no knowledge selected.
    $this->drupalGet('admin/content/knowledge');
    $this->submitForm([], 'Apply to selected items');
    $this->assertSession()->pageTextContains('Select one or more knowledge to perform the update on.');

    // Test that knowledge listing shows the correct subject link.
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table/tbody/tr/td/a[contains(@href, :href) and contains(@title, :title) and text()=:text]', [
      ':href' => $knowledge_links[0]->permalink()->toString(),
      ':title' => Unicode::truncate($knowledge_links[0]->get('knowledge_body')->value, 128),
      ':text' => $knowledge_links[0]->label(),
    ]));

    // Verify that anonymous author name is displayed correctly.
    $this->assertSession()->pageTextContains($author_name . ' (not verified)');

    // Test that knowledge listing shows the correct subject link.
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table/tbody/tr/td/a[contains(@href, :href) and contains(@title, :title) and text()=:text]', [
      ':href' => $anonymous_knowledge4->permalink()->toString(),
      ':title' => Unicode::truncate($body, 128),
      ':text' => $subject,
    ]));

    // Verify that anonymous author name is displayed correctly.
    $this->assertSession()->pageTextContains($author_name . ' (not verified)');

    // Delete multiple knowledge in one operation.
    $edit = [
      'action' => 'knowledge_delete_action',
      "knowledge_bulk_form[1]" => $knowledge_links[0]->id(),
      "knowledge_bulk_form[0]" => $knowledge_links[1]->id(),
      "knowledge_bulk_form[2]" => $anonymous_knowledge4->id(),
    ];
    $this->submitForm($edit, 'Apply to selected items');
    $this->assertSession()->pageTextContains('Are you sure you want to delete these knowledge and all their children?');
    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains('No knowledge available.');

    // Make sure the label of unpublished node is not visible on listing page.
    $this->drupalGet('admin/content/knowledge');
    $this->postKnowledge($this->node, $this->randomMachineName());
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content/knowledge');
    // Verify that knowledge admin can see the title of a published node.
    $this->assertSession()->pageTextContains(Html::escape($this->node->label()));
    $this->node->setUnpublished()->save();
    $this->assertFalse($this->node->isPublished(), 'Node is unpublished now.');
    $this->drupalGet('admin/content/knowledge');
    // Verify that knowledge admin cannot see the title of an unpublished node.
    $this->assertSession()->pageTextNotContains(Html::escape($this->node->label()));
    $this->drupalLogout();
    $node_access_user = $this->drupalCreateUser([
      'administer knowledge',
      'bypass node access',
    ]);
    $this->drupalLogin($node_access_user);
    $this->drupalGet('admin/content/knowledge');
    // Verify that knowledge admin with bypass node access permissions can still
    // see the title of a published node.
    $this->assertSession()->pageTextContains(Html::escape($this->node->label()));
  }

  /**
   * Tests linked entity label of admin view.
   */
  public function testKnowledgeedEntityLabel() {
    \Drupal::service('module_installer')->install(['block_content']);
    \Drupal::service('router.builder')->rebuildIfNeeded();
    $bundle = BlockContentType::create([
      'id' => 'basic',
      'label' => 'basic',
      'revision' => FALSE,
    ]);
    $bundle->save();
    $block_content = BlockContent::create([
      'type' => 'basic',
      'label' => 'Some block title',
      'info' => 'Test block',
    ]);
    $block_content->save();

    // Create knowledge field on block_content.
    $this->addDefaultKnowledgeField('block_content', 'basic', 'block_knowledge', KnowledgeItemInterface::OPEN, 'block_knowledge');
    $this->drupalLogin($this->webUser);
    // Post a knowledge to node.
    $node_knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    // Post a knowledge to block content.
    $block_content_knowledge = $this->postKnowledge($block_content, $this->randomMachineName(), $this->randomMachineName(), TRUE, 'block_knowledge');
    $this->drupalLogout();
    // Login as admin to test the admin knowledge page.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content/knowledge');

    // Test that knowledge listing links to knowledge author.
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table/tbody/tr[1]/td/a[contains(@href, :href) and text()=:text]', [
      ':href' => $this->webUser->toUrl()->toString(),
      ':text' => $this->webUser->label(),
    ]));
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table/tbody/tr[2]/td/a[contains(@href, :href) and text()=:text]', [
      ':href' => $this->webUser->toUrl()->toString(),
      ':text' => $this->webUser->label(),
    ]));

    // Admin page contains label of both entities.
    $this->assertSession()->pageTextContains(Html::escape($this->node->label()));
    $this->assertSession()->pageTextContains(Html::escape($block_content->label()));
    // Admin page contains subject of both entities.
    $this->assertSession()->pageTextContains(Html::escape($node_knowledge->label()));
    $this->assertSession()->pageTextContains(Html::escape($block_content_knowledge->label()));
  }

}
