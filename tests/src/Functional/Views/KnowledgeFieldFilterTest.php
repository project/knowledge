<?php

namespace Drupal\Tests\knowledge\Functional\Views;

use Drupal\knowledge\Entity\Knowledge;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests knowledge field filters with translations.
 *
 * @group knowledge
 */
class KnowledgeFieldFilterTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_field_filters'];

  /**
   * List of knowledge titles by language.
   *
   * @var array
   */
  public $knowledgeTitles = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['knowledge_test_views']): void {
    parent::setUp($import_test_views, $modules);
    $this->drupalLogin($this->drupalCreateUser(['access knowledge']));

    // Add two new languages.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    ConfigurableLanguage::createFromLangcode('es')->save();

    // Set up knowledge titles.
    $this->knowledgeTitles = [
      'en' => 'Food in Paris',
      'es' => 'Comida en Paris',
      'fr' => 'Nourriture en Paris',
    ];

    // Create a new knowledge. Using the one created earlier will not work,
    // as it predates the language set-up.
    $knowledge = [
      'uid' => $this->loggedInUser->id(),
      'entity_id' => $this->nodeUserKnowledgeed->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'kid' => '',
      'node_type' => '',
    ];
    $this->knowledge = Knowledge::create($knowledge);

    // Add field values and translate the knowledge.
    $this->knowledge->subject->value = $this->knowledgeTitles['en'];
    $this->knowledge->knowledge_body->value = $this->knowledgeTitles['en'];
    $this->knowledge->langcode = 'en';
    $this->knowledge->save();

  }

  /**
   * Tests body and title filters.
   */
  public function testFilters() {
    // Test the title filter page, which filters for title contains 'Comida'.
    // Should show just the Spanish translation, once.
    $this->assertPageCounts('test-title-filter', [
      'es' => 1,
      'fr' => 0,
      'en' => 0,
    ], 'Comida title filter');

    // Test the body filter page, which filters for body contains 'Comida'.
    // Should show just the Spanish translation, once.
    $this->assertPageCounts('test-body-filter', [
      'es' => 1,
      'fr' => 0,
      'en' => 0,
    ], 'Comida body filter');

    // Test the title Paris filter page, which filters for title contains
    // 'Paris'. Should show each translation once.
    $this->assertPageCounts('test-title-paris', [
      'es' => 1,
      'fr' => 1,
      'en' => 1,
    ], 'Paris title filter');

    // Test the body Paris filter page, which filters for body contains
    // 'Paris'. Should show each translation once.
    $this->assertPageCounts('test-body-paris', [
      'es' => 1,
      'fr' => 1,
      'en' => 1,
    ], 'Paris body filter');
  }

  /**
   * Asserts that the given knowledge translation counts are correct.
   *
   * @param string $path
   *   Path of the page to test.
   * @param array $counts
   *   Array whose keys are languages, and values are the number of times
   *   that translation should be shown on the given page.
   * @param string $message
   *   Message suffix to display.
   *
   * @internal
   */
  protected function assertPageCounts(string $path, array $counts, string $message): void {
    // Get the text of the page.
    $this->drupalGet($path);
    $text = $this->getTextContent();

    // Check the counts. Note that the title and body are both shown on the
    // page, and they are the same. So the title/body string should appear on
    // the page twice as many times as the input count.
    foreach ($counts as $langcode => $count) {
      $this->assertEquals(2 * $count, substr_count($text, $this->knowledgeTitles[$langcode]), 'Translation ' . $langcode . ' has count ' . $count . ' with ' . $message);
    }
  }

}
