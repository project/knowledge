<?php

namespace Drupal\Tests\knowledge\Functional\Views;

/**
 * Tests the knowledge rss row plugin.
 *
 * @group knowledge
 * @see \Drupal\knowledge\Plugin\views\row\Rss
 */
class RowRssTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_knowledge_rss'];

  /**
   * Tests knowledge rss output.
   */
  public function testRssRow() {
    $this->drupalGet('test-knowledge-rss');

    // Because the response is XML we can't use the page which depends on an
    // HTML tag being present.
    $result = $this->getSession()->getDriver()->find('//item');
    $this->assertCount(1, $result, 'Just one knowledge was found in the rss output.');

    $this->assertEquals(gmdate('r', $this->knowledge->getCreatedTime()), $result[0]->find('xpath', '//pubDate')->getHtml(), 'The right pubDate appears in the rss output.');
  }

}
