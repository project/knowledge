<?php

namespace Drupal\Tests\knowledge\Functional\Views;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Tests\KnowledgeTestTrait;

/**
 * Provides setup and helper methods for knowledge views tests.
 */
abstract class KnowledgeTestBase extends ViewTestBase {

  use KnowledgeTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'knowledge', 'knowledge_test_views'];

  /**
   * A normal user with permission to post knowledge (without approval).
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * A second normal user that will author a node for $account to knowledge on.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account2;

  /**
   * Stores a node posted by the user created as $account.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodeUserPosted;

  /**
   * Stores a node posted by the user created as $account2.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodeUserKnowledgeed;

  /**
   * Stores a knowledge used by the tests.
   *
   * @var \Drupal\knowledge\Entity\Knowledge
   */
  protected $knowledge;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['knowledge_test_views']): void {
    parent::setUp($import_test_views, $modules);

    // Add two users, create a node with the user1 as author and another node
    // with user2 as author. For the second node add a knowledge from user1.
    $this->account = $this->drupalCreateUser(['skip knowledge approval']);
    $this->account2 = $this->drupalCreateUser();
    $this->drupalLogin($this->account);

    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => new TranslatableMarkup('Basic page'),
    ]);
    $this->addDefaultKnowledgeField('node', 'page');

    $this->nodeUserPosted = $this->drupalCreateNode();
    $this->nodeUserKnowledgeed = $this->drupalCreateNode(['uid' => $this->account2->id()]);

    $knowledge = [
      'uid' => $this->loggedInUser->id(),
      'entity_id' => $this->nodeUserKnowledgeed->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'subject' => 'How much wood would a woodchuck chuck',
      'kid' => '',
      'mail' => 'someone@example.com',
    ];
    $this->knowledge = Knowledge::create($knowledge);
    $this->knowledge->save();
  }

}
