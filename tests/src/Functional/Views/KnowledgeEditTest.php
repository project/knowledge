<?php

namespace Drupal\Tests\knowledge\Functional\Views;

use Drupal\Tests\knowledge\Functional\KnowledgeTestBase as KnowledgeBrowserTestBase;

/**
 * Tests knowledge edit functionality.
 *
 * @group knowledge
 */
class KnowledgeEditTest extends KnowledgeBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Tests knowledge label in admin view.
   */
  public function testKnowledgeEdit() {
    $this->drupalLogin($this->adminUser);
    // Post a knowledge to node.
    $node_knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->pageTextContains($this->adminUser->label());
    $this->drupalGet($node_knowledge->toUrl('edit-form'));
    $edit = [
      'knowledge_body[0][value]' => $this->randomMachineName(),
    ];
    $this->submitForm($edit, 'Save');
    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->pageTextContains($this->adminUser->label());
  }

}
