<?php

namespace Drupal\Tests\knowledge\Functional\Views;

/**
 * Tests knowledge operations.
 *
 * @group knowledge
 */
class KnowledgeOperationsTest extends KnowledgeTestBase {

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_knowledge_operations'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the operations field plugin.
   */
  public function testKnowledgeOperations() {
    $admin_account = $this->drupalCreateUser(['administer knowledge']);
    $this->drupalLogin($admin_account);
    $this->drupalGet('test-knowledge-operations');
    $this->assertSession()->statusCodeEquals(200);
    // Assert Edit operation is present.
    $this->assertSession()->elementsCount('xpath', '//td[contains(@class, "views-field-operations")]//li/a[text() = "Edit"]', 1);
    // Assert Delete operation is present.
    $this->assertSession()->elementsCount('xpath', '//td[contains(@class, "views-field-operations")]//li/a[text() = "Delete"]', 1);
  }

}
