<?php

namespace Drupal\Tests\knowledge\Functional\Views;

/**
 * Tests knowledge on nodes.
 *
 * @group knowledge
 */
class NodeKnowledgesTest extends KnowledgeTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['history'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_new_knowledge', 'test_total_count'];

  /**
   * Tests the new knowledge field plugin.
   */
  public function testNewKnowledges() {
    $this->drupalGet('test-new-knowledge');
    $this->assertSession()->statusCodeEquals(200);
    $new_knowledge = $this->cssSelect(".views-field-new-knowledge a:contains('1')");
    $this->assertCount(1, $new_knowledge, 'Found the number of new knowledge for a certain node.');
  }

  /**
   * Test the knowledge count field.
   */
  public function testKnowledgeCount() {
    $this->drupalGet('test-knowledge-count');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertCount(2, $this->cssSelect('.views-row'));
    $total_count_with_knowledge = $this->cssSelect(".views-field-knowledge-count span:contains('1')");
    $this->assertCount(1, $total_count_with_knowledge);
    $total_count_without_knowledge = $this->cssSelect(".views-field-knowledge-count span:contains('0')");
    $this->assertCount(1, $total_count_without_knowledge);

    // Create a content type with no knowledge field, and add a node.
    $this->drupalCreateContentType([
      'type' => 'no_knowledge',
      'name' => 'No knowledge page',
    ]);
    $this->nodeUserPosted = $this->drupalCreateNode(['type' => 'no_knowledge']);
    $this->drupalGet('test-knowledge-count');

    // Test that the node with no knowledge field is also shown.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertCount(3, $this->cssSelect('.views-row'));
    $total_count_with_knowledge = $this->cssSelect(".views-field-knowledge-count span:contains('1')");
    $this->assertCount(1, $total_count_with_knowledge);
    $total_count_without_knowledge = $this->cssSelect(".views-field-knowledge-count span:contains('0')");
    $this->assertCount(2, $total_count_without_knowledge);
  }

}
