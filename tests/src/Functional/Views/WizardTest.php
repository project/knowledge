<?php

namespace Drupal\Tests\knowledge\Functional\Views;

use Drupal\Tests\views\Functional\Wizard\WizardTestBase;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\views\Views;

/**
 * Tests the knowledge module integration into the wizard.
 *
 * @group knowledge
 * @see \Drupal\knowledge\Plugin\views\wizard\Knowledge
 */
class WizardTest extends WizardTestBase {

  use KnowledgeTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'knowledge'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = []): void {
    parent::setUp($import_test_views, $modules);
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    // Add knowledge field to page node type.
    $this->addDefaultKnowledgeField('node', 'page');
  }

  /**
   * Tests adding a view of knowledge.
   */
  public function testKnowledgeWizard() {
    $view = [];
    $view['label'] = $this->randomMachineName(16);
    $view['id'] = strtolower($this->randomMachineName(16));
    $view['show[wizard_key]'] = 'knowledge';
    $view['page[create]'] = TRUE;
    $view['page[path]'] = $this->randomMachineName(16);

    // Just triggering the saving should automatically choose a proper row
    // plugin.
    $this->drupalGet('admin/structure/views/add');
    $this->submitForm($view, 'Save and edit');
    // Verify that the view saving was successful and the browser got redirected
    // to the edit page.
    $this->assertSession()->addressEquals('admin/structure/views/view/' . $view['id']);

    // If we update the type first we should get a selection of knowledge valid
    // row plugins as the select field.
    $this->drupalGet('admin/structure/views/add');
    $this->drupalGet('admin/structure/views/add');
    $this->submitForm($view, 'Update "of type" choice');

    // Check for available options of the row plugin.
    $expected_options = ['entity:knowledge', 'fields'];
    $items = $this->getSession()->getPage()->findField('page[style][row_plugin]')->findAll('xpath', 'option');
    $actual_options = [];
    foreach ($items as $item) {
      $actual_options[] = $item->getValue();
    }
    $this->assertEquals($expected_options, $actual_options);

    $view['id'] = strtolower($this->randomMachineName(16));
    $this->submitForm($view, 'Save and edit');
    // Verify that the view saving was successful and the browser got redirected
    // to the edit page.
    $this->assertSession()->addressEquals('admin/structure/views/view/' . $view['id']);

    $user = $this->drupalCreateUser(['access knowledge']);
    $this->drupalLogin($user);

    $view = Views::getView($view['id']);
    $view->initHandlers();
    $row = $view->display_handler->getOption('row');
    $this->assertEquals('entity:knowledge', $row['type']);

    // Check for the default filters.
    $this->assertEquals('knowledge', $view->filter['status']->table);
    $this->assertEquals('status', $view->filter['status']->field);
    $this->assertEquals('1', $view->filter['status']->value);
    $this->assertEquals('node_field_data', $view->filter['status_node']->table);
    $this->assertEquals('status', $view->filter['status_node']->field);
    $this->assertEquals('1', $view->filter['status_node']->value);

    // Check for the default fields.
    $this->assertEquals('knowledge', $view->field['subject']->table);
    $this->assertEquals('subject', $view->field['subject']->field);
  }

}
