<?php

namespace Drupal\Tests\knowledge\Functional\Views;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\views\Views;

/**
 * Tests results for the Recent Knowledge view shipped with the module.
 *
 * @group knowledge
 */
class DefaultViewRecentKnowledgesTest extends ViewTestBase {

  use KnowledgeTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'knowledge', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Number of results for the Default display.
   *
   * @var int
   */
  protected $defaultDisplayResults = 5;

  /**
   * Number of results for the Block display.
   *
   * @var int
   */
  protected $blockDisplayResults = 5;

  /**
   * Number of results for the Page display.
   *
   * @var int
   */
  protected $pageDisplayResults = 5;

  /**
   * Will hold the knowledge created for testing.
   *
   * @var array
   */
  protected $knowledgeCreated = [];

  /**
   * Contains the node object used for knowledge of this test.
   *
   * @var \Drupal\node\NodeInterface
   */
  public $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = []): void {
    parent::setUp($import_test_views, $modules);

    // Create a new content type.
    $content_type = $this->drupalCreateContentType();

    // Add a node of the new content type.
    $node_data = [
      'type' => $content_type->id(),
    ];

    $this->addDefaultKnowledgeField('node', $content_type->id());
    $this->node = $this->drupalCreateNode($node_data);

    // Force a flush of the in-memory storage.
    $this->container->get('views.views_data')->clear();

    // Create some knowledge and attach them to the created node.
    for ($i = 0; $i < $this->defaultDisplayResults; $i++) {
      /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
      $knowledge = Knowledge::create([
        'status' => KnowledgeInterface::PUBLISHED,
        'field_name' => 'knowledge',
        'entity_type' => 'node',
        'entity_id' => $this->node->id(),
      ]);
      $knowledge->setOwnerId(0);
      $knowledge->knowledge_body->value = 'Test body ' . $i;
      $knowledge->knowledge_body->format = 'full_html';

      // Ensure knowledge are sorted in ascending order.
      $time = \Drupal::time()->getRequestTime() + ($this->defaultDisplayResults - $i);
      $knowledge->setCreatedTime($time);
      $knowledge->changed->value = $time;

      $knowledge->save();
    }

    // Store all the nodes just created to access their properties on the tests.
    $this->knowledgeCreated = Knowledge::loadMultiple();

    // Sort created knowledge in descending order.
    ksort($this->knowledgeCreated, SORT_NUMERIC);
  }

  /**
   * Tests the block defined by the knowledge_recent view.
   */
  public function testBlockDisplay() {
    $user = $this->drupalCreateUser(['access knowledge']);
    $this->drupalLogin($user);

    $view = Views::getView('knowledge_recent');
    $view->setDisplay('block_1');
    $this->executeView($view);

    $map = [
      'subject' => 'subject',
      'kid' => 'kid',
      'knowledge_field_data_created' => 'created',
    ];
    $expected_result = [];
    foreach (array_values($this->knowledgeCreated) as $key => $knowledge) {
      $expected_result[$key]['subject'] = $knowledge->label();
      $expected_result[$key]['kid'] = $knowledge->id();
      $expected_result[$key]['created'] = $knowledge->getCreatedTime();
    }
    $this->assertIdenticalResultset($view, $expected_result, $map);

    // Check the number of results given by the display is the expected.
    $this->assertCount($this->blockDisplayResults, $view->result,
      new FormattableMarkup('There are exactly @results knowledge. Expected @expected', [
        '@results' => count($view->result),
        '@expected' => $this->blockDisplayResults,
      ])
    );
  }

}
