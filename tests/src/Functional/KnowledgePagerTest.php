<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\knowledge\KnowledgeManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Tests paging of knowledge and their settings.
 *
 * @group knowledge
 */
class KnowledgePagerTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Confirms knowledge paging works correctly with flat and threaded knowledge.
   */
  public function testKnowledgePaging() {
    $this->drupalLogin($this->adminUser);

    // Set knowledge variables.
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgePreview(DRUPAL_DISABLED);

    // Create a node and three knowledge.
    $node = $this->drupalCreateNode(['type' => 'article', 'promote' => 1]);
    $knowledge_links = [];
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT, 'Knowledge paging changed.');

    // Set knowledge to one per page so that we are able to test paging without
    // needing to insert large numbers of knowledge.
    $this->setKnowledgesPerPage(1);

    // Check the first page of the node, and confirm the correct knowledge are
    // shown.
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->pageTextContains('next');
    $this->assertTrue($this->knowledgeExists($knowledge_links[0]), 'Knowledge 1 appears on page 1.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[1]), 'Knowledge 2 does not appear on page 1.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[2]), 'Knowledge 3 does not appear on page 1.');

    // Check the second page.
    $this->drupalGet('node/' . $node->id(), ['query' => ['page' => 1]]);
    $this->assertTrue($this->knowledgeExists($knowledge_links[1]), 'Knowledge 2 appears on page 2.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[0]), 'Knowledge 1 does not appear on page 2.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[2]), 'Knowledge 3 does not appear on page 2.');

    // Check the third page.
    $this->drupalGet('node/' . $node->id(), ['query' => ['page' => 2]]);
    $this->assertTrue($this->knowledgeExists($knowledge_links[2]), 'Knowledge 3 appears on page 3.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[0]), 'Knowledge 1 does not appear on page 3.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[1]), 'Knowledge 2 does not appear on page 3.');

    // Post a reply to the oldest knowledge and test again.
    $oldest_knowledge = reset($knowledge_links);
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $oldest_knowledge->id());
    $reply = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    $this->setKnowledgesPerPage(2);
    // We are still in flat view - the replies should not be on the first page,
    // even though they are replies to the oldest knowledge.
    $this->drupalGet('node/' . $node->id(), ['query' => ['page' => 0]]);
    $this->assertFalse($this->knowledgeExists($reply, TRUE), 'In flat mode, reply does not appear on page 1.');

    // If we switch to threaded mode, the replies on the oldest knowledge
    // should be bumped to the first page and knowledge 6 should be bumped
    // to the second page.
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Switched to threaded mode.');
    $this->drupalGet('node/' . $node->id(), ['query' => ['page' => 0]]);
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'In threaded mode, reply appears on page 1.');
    $this->assertFalse($this->knowledgeExists($knowledge_links[1]), 'In threaded mode, knowledge 2 has been bumped off of page 1.');

    // If (# replies > # knowledge per page) in threaded expanded view,
    // the overage should be bumped.
    $reply2 = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $this->drupalGet('node/' . $node->id(), ['query' => ['page' => 0]]);
    $this->assertFalse($this->knowledgeExists($reply2, TRUE), 'In threaded mode where # replies > # knowledge per page, the newest reply does not appear on page 1.');

    // Test that the page build process does not somehow generate errors when
    // # knowledge per page is set to 0.
    $this->setKnowledgesPerPage(0);
    $this->drupalGet('node/' . $node->id(), ['query' => ['page' => 0]]);
    $this->assertFalse($this->knowledgeExists($reply2, TRUE), 'Threaded mode works correctly when knowledge per page is 0.');

    $this->drupalLogout();
  }

  /**
   * Confirms knowledge paging works correctly with flat and threaded knowledge.
   */
  public function testKnowledgePermalink() {
    $this->drupalLogin($this->adminUser);

    // Set knowledge variables.
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgePreview(DRUPAL_DISABLED);

    // Create a node and three knowledge.
    $node = $this->drupalCreateNode(['type' => 'article', 'promote' => 1]);
    $knowledge_links = [];
    $knowledge_links[] = $this->postKnowledge($node, 'knowledge 1: ' . $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, 'knowledge 2: ' . $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, 'knowledge 3: ' . $this->randomMachineName(), $this->randomMachineName(), TRUE);

    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT, 'Knowledge paging changed.');

    // Set knowledge to one per page so that we are able to test paging without
    // needing to insert large numbers of knowledge.
    $this->setKnowledgesPerPage(1);

    // Navigate to each knowledge permalink as anonymous and assert it appears
    // on the page.
    foreach ($knowledge_links as $index => $knowledge) {
      $this->drupalGet($knowledge->toUrl());
      $this->assertTrue($this->knowledgeExists($knowledge), sprintf('Knowledge %d appears on page %d.', $index + 1, $index + 1));
    }
  }

  /**
   * Tests knowledge ordering and threading.
   */
  public function testKnowledgeOrderingThreading() {
    $this->drupalLogin($this->adminUser);

    // Set knowledge variables.
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgePreview(DRUPAL_DISABLED);

    // Display all the knowledge on the same page.
    $this->setKnowledgesPerPage(1000);

    // Create a node and three knowledge.
    $node = $this->drupalCreateNode(['type' => 'article', 'promote' => 1]);
    $knowledge_links = [];
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the second knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[1]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the first knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[0]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the last knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[2]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the second knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[3]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT, 'Knowledge paging changed.');

    $expected_order = [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
    ];
    $this->drupalGet('node/' . $node->id());
    $this->assertKnowledgeOrder($knowledge_links, $expected_order);

    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Switched to threaded mode.');

    $expected_order = [
      0,
      4,
      1,
      3,
      6,
      2,
      5,
    ];
    $this->drupalGet('node/' . $node->id());
    $this->assertKnowledgeOrder($knowledge_links, $expected_order);
  }

  /**
   * Asserts that the knowledge are displayed in the correct order.
   *
   * @param \Drupal\knowledge\KnowledgeInterface[] $knowledge_links
   *   An array of knowledge, must be of the type KnowledgeInterface.
   * @param array $expected_order
   *   An array of keys from $knowledge_links describing the expected order.
   *
   * @internal
   */
  public function assertKnowledgeOrder(array $knowledge_links, array $expected_order): void {
    $expected_kids = [];

    // First, rekey the expected order by kid.
    foreach ($expected_order as $key) {
      $expected_kids[] = $knowledge_links[$key]->id();
    }

    $knowledge_anchors = $this->xpath('//article[starts-with(@id,"knowledge-")]');
    $result_order = [];
    foreach ($knowledge_anchors as $anchor) {
      $result_order[] = substr($anchor->getAttribute('id'), 8);
    }
    $this->assertEquals($expected_kids, $result_order, new FormattableMarkup('Knowledge order: expected @expected, returned @returned.', [
      '@expected' => implode(',', $expected_kids),
      '@returned' => implode(',', $result_order),
    ]));
  }

  /**
   * Tests calculation of first page with new knowledge.
   */
  public function testKnowledgeNewPageIndicator() {
    $this->drupalLogin($this->adminUser);

    // Set knowledge variables.
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgePreview(DRUPAL_DISABLED);

    // Set knowledge to one per page so that we are able to test paging without
    // needing to insert large numbers of knowledge.
    $this->setKnowledgesPerPage(1);

    // Create a node and three knowledge.
    $node = $this->drupalCreateNode(['type' => 'article', 'promote' => 1]);
    $knowledge_links = [];
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the second knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[1]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the first knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[0]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the last knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $knowledge_links[2]->id());
    $knowledge_links[] = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT, 'Knowledge paging changed.');

    $expected_pages = [
      // Page of knowledge 5.
      1 => 5,
      // Page of knowledge 4.
      2 => 4,
      // Page of knowledge 3.
      3 => 3,
      // Page of knowledge 2.
      4 => 2,
      // Page of knowledge 1.
      5 => 1,
      // Page of knowledge 0.
      6 => 0,
    ];

    $node = Node::load($node->id());
    foreach ($expected_pages as $new_replies => $expected_page) {
      $returned_page = \Drupal::entityTypeManager()->getStorage('knowledge')
        ->getNewKnowledgePageNumber($node->get('knowledge')->total_count, $new_replies, $node, 'knowledge');
      $this->assertSame($expected_page, $returned_page, new FormattableMarkup('Flat mode, @new replies: expected page @expected, returned page @returned.', [
        '@new' => $new_replies,
        '@expected' => $expected_page,
        '@returned' => $returned_page,
      ]));
    }

    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Switched to threaded mode.');

    $expected_pages = [
      // Page of knowledge 5.
      1 => 5,
      // Page of knowledge 4.
      2 => 1,
      // Page of knowledge 4.
      3 => 1,
      // Page of knowledge 4.
      4 => 1,
      // Page of knowledge 4.
      5 => 1,
      // Page of knowledge 0.
      6 => 0,
    ];

    \Drupal::entityTypeManager()->getStorage('node')->resetCache([$node->id()]);
    $node = Node::load($node->id());
    foreach ($expected_pages as $new_replies => $expected_page) {
      $returned_page = \Drupal::entityTypeManager()->getStorage('knowledge')
        ->getNewKnowledgePageNumber($node->get('knowledge')->total_count, $new_replies, $node, 'knowledge');
      $this->assertEquals($expected_page, $returned_page, new FormattableMarkup('Threaded mode, @new replies: expected page @expected, returned page @returned.', [
        '@new' => $new_replies,
        '@expected' => $expected_page,
        '@returned' => $returned_page,
      ]));
    }
  }

  /**
   * Confirms knowledge paging works correctly with two pagers.
   */
  public function testTwoPagers() {
    // Add another field to article content-type.
    $this->addDefaultKnowledgeField('node', 'article', 'knowledge_2');
    // Set default to display knowledge list with unique pager id.
    \Drupal::service('entity_display.repository')
      ->getViewDisplay('node', 'article')
      ->setComponent('knowledge_2', [
        'label' => 'hidden',
        'type' => 'knowledge_default',
        'weight' => 30,
        'settings' => [
          'pager_id' => 1,
          'view_mode' => 'default',
        ],
      ])
      ->save();

    // Make sure pager appears in formatter summary and settings form.
    $account = $this->drupalCreateUser(['administer node display']);
    $this->drupalLogin($account);
    $this->drupalGet('admin/structure/types/manage/article/display');
    // No summary for standard pager.
    $this->assertSession()->pageTextNotContains('Pager ID: 0');
    $this->assertSession()->pageTextContains('Pager ID: 1');
    $this->submitForm([], 'knowledge_settings_edit');
    // Change default pager to 2.
    $this->submitForm(['fields[knowledge][settings_edit_form][settings][pager_id]' => 2], 'Save');
    $this->assertSession()->pageTextContains('Pager ID: 2');
    // Revert the changes.
    $this->submitForm([], 'knowledge_settings_edit');
    $this->submitForm(['fields[knowledge][settings_edit_form][settings][pager_id]' => 0], 'Save');
    // No summary for standard pager.
    $this->assertSession()->pageTextNotContains('Pager ID: 0');

    $this->drupalLogin($this->adminUser);

    // Add a new node with both knowledge fields open.
    $node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'uid' => $this->webUser->id(),
    ]);
    // Set knowledge options.
    $knowledge_links = [];
    foreach (['knowledge', 'knowledge_2'] as $field_name) {
      $this->setKnowledgeForm(TRUE, $field_name);
      $this->setKnowledgePreview(DRUPAL_OPTIONAL, $field_name);
      $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT, 'Knowledge paging changed.', $field_name);

      // Set knowledge to one per page so that we are able to test
      // paging without needing to insert large numbers of knowledge.
      $this->setKnowledgesPerPage(1, $field_name);
      for ($i = 1; $i <= 4; $i++) {
        $knowledge = "Knowledge $i on field $field_name";
        $knowledge_links[] = $this->postKnowledge($node, $knowledge, $knowledge, TRUE, $field_name);
      }
    }

    // Check the first page of the node, and confirm the correct knowledge are
    // shown.
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->pageTextContains('next');
    $this->assertSession()->pageTextContains('Knowledge 1 on field knowledge');
    $this->assertSession()->pageTextContains('Knowledge 1 on field knowledge_2');
    // Navigate to next page of field 1.
    $this->clickLinkWithXpath('//h3/a[normalize-space(text())=:label]/ancestor::section[1]//a[@rel="next"]', [':label' => 'Knowledge 1 on field knowledge']);
    // Check only one pager updated.
    $this->assertSession()->pageTextContains('Knowledge 2 on field knowledge');
    $this->assertSession()->pageTextContains('Knowledge 1 on field knowledge_2');
    // Return to page 1.
    $this->drupalGet('node/' . $node->id());
    // Navigate to next page of field 2.
    $this->clickLinkWithXpath('//h3/a[normalize-space(text())=:label]/ancestor::section[1]//a[@rel="next"]', [':label' => 'Knowledge 1 on field knowledge_2']);
    // Check only one pager updated.
    $this->assertSession()->pageTextContains('Knowledge 1 on field knowledge');
    $this->assertSession()->pageTextContains('Knowledge 2 on field knowledge_2');
    // Navigate to next page of field 1.
    $this->clickLinkWithXpath('//h3/a[normalize-space(text())=:label]/ancestor::section[1]//a[@rel="next"]', [':label' => 'Knowledge 1 on field knowledge']);
    // Check only one pager updated.
    $this->assertSession()->pageTextContains('Knowledge 2 on field knowledge');
    $this->assertSession()->pageTextContains('Knowledge 2 on field knowledge_2');
  }

  /**
   * Follows a link found at a give xpath query.
   *
   * Will click the first link found with the given xpath query by default,
   * or a later one if an index is given.
   *
   * If the link is discovered and clicked, the test passes. Fail otherwise.
   *
   * @param string $xpath
   *   Xpath query that targets an anchor tag, or set of anchor tags.
   * @param array $arguments
   *   An array of arguments with keys in the form ':name' matching the
   *   placeholders in the query. The values may be either strings or numeric
   *   values.
   * @param int $index
   *   Link position counting from zero.
   *
   * @return string|false
   *   Page contents on success, or FALSE on failure.
   *
   * @see \Drupal\Tests\UiHelperTrait::clickLink()
   */
  protected function clickLinkWithXpath(string $xpath, array $arguments = [], int $index = 0) {
    $url_before = $this->getUrl();
    $urls = $this->xpath($xpath, $arguments);
    if (isset($urls[$index])) {
      $url_target = $this->getAbsoluteUrl($urls[$index]->getAttribute('href'));
      return $this->drupalGet($url_target);
    }
    $this->fail(new FormattableMarkup('Link %label does not exist on @url_before', [
      '%label' => $xpath,
      '@url_before' => $url_before,
    ]));
    return FALSE;
  }

}
