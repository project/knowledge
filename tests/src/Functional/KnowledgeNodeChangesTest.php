<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\Knowledge;

/**
 * Tests that knowledge behave correctly when the node is changed.
 *
 * @group knowledge
 */
class KnowledgeNodeChangesTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that knowledge is deleted with the node.
   */
  public function testNodeDeletion() {
    $this->drupalLogin($this->webUser);
    $knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName());
    $this->assertInstanceOf(Knowledge::class, $knowledge);
    $this->node->delete();
    $this->assertNull(Knowledge::load($knowledge->id()), 'The knowledge could not be loaded after the node was deleted.');
    // Make sure the knowledge field storage and all its fields are deleted when
    // the node type is deleted.
    $this->assertNotNull(FieldStorageConfig::load('node.knowledge'), 'Knowledge field storage exists');
    $this->assertNotNull(FieldConfig::load('node.article.knowledge'), 'Knowledge field exists');
    // Delete the node type.
    $this->node->get('type')->entity->delete();
    $this->assertNull(FieldStorageConfig::load('node.knowledge'), 'Knowledge field storage deleted');
    $this->assertNull(FieldConfig::load('node.article.knowledge'), 'Knowledge field deleted');
  }

}
