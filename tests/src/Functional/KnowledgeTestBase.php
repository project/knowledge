<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\node\Entity\NodeType;

/**
 * Provides setup and helper methods for knowledge tests.
 */
abstract class KnowledgeTestBase extends BrowserTestBase {

  use KnowledgeTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'block',
    'knowledge',
    'knowledge_field',
    'node',
    'history',
    'field_ui',
    'datetime',
  ];

  /**
   * An administrative user with permission to configure knowledge settings.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A normal user with permission to post knowledge.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * A test node to which knowledge will be posted.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    // Create an article content type only if it does not yet exist, so that
    // child classes may specify the standard profile.
    $types = NodeType::loadMultiple();
    if (empty($types['article'])) {
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => new TranslatableMarkup('Article'),
      ]);
    }

    // Create two test users.
    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer knowledge',
      'administer knowledge types',
      'administer knowledge fields',
      'administer knowledge display',
      'skip knowledge approval',
      'post knowledge',
      'access knowledge',
      // Usernames aren't shown in knowledge edit form autocomplete unless this
      // permission is granted.
      'access user profiles',
      'access content',
    ]);
    $this->webUser = $this->drupalCreateUser([
      'access knowledge',
      'post knowledge',
      'create article content',
      'edit own knowledge',
      'skip knowledge approval',
      'access content',
    ]);

    // Create knowledge field on article.
    $this->addDefaultKnowledgeField('node', 'article');

    // Create a test node authored by the web user.
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'uid' => $this->webUser->id(),
    ]);
    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Posts a knowledge.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   Node to post knowledge on or NULL to post to the previously loaded page.
   * @param string $knowledge
   *   Knowledge body.
   * @param string $subject
   *   Knowledge subject.
   * @param string $contact
   *   Set to NULL for no contact info, TRUE to ignore success checking, and
   *   array of values to set contact info.
   * @param string $field_name
   *   (optional) Field name through which the knowledge should be posted.
   *   Defaults to 'knowledge'.
   *
   * @return \Drupal\knowledge\KnowledgeInterface|null
   *   The posted knowledge or NULL when posted knowledge was not found.
   */
  public function postKnowledge($entity, $knowledge, $subject = '', $contact = NULL, $field_name = 'knowledge') {
    $edit = [];
    $edit['knowledge_body[0][value]'] = $knowledge;

    if ($entity !== NULL) {
      /** @var \Drupal\field\FieldConfigInterface $field */
      $field = FieldConfig::loadByName($entity->getEntityTypeId(), $entity->bundle(), $field_name);
    }
    else {
      /** @var \Drupal\field\FieldConfigInterface $field */
      $field = FieldConfig::loadByName('node', 'article', $field_name);
    }
    $preview_mode = $field->getSetting('preview');

    // Must get the page before we test for fields.
    if ($entity !== NULL) {
      $this->drupalGet('knowledge/reply/' . $entity->getEntityTypeId() . '/' . $entity->id() . '/' . $field_name);
    }

    // Determine the visibility of subject form field.
    $display_repository = $this->container->get('entity_display.repository');
    if ($display_repository->getFormDisplay('knowledge', 'knowledge')->getComponent('subject')) {
      // Subject input allowed.
      $edit['subject[0][value]'] = $subject;
    }
    else {
      $this->assertSession()->fieldNotExists('subject[0][value]');
    }

    if ($contact !== NULL && is_array($contact)) {
      $edit += $contact;
    }
    switch ($preview_mode) {
      case DRUPAL_REQUIRED:
        // Preview required so no save button should be found.
        $this->assertSession()->buttonNotExists('Save');
        $this->submitForm($edit, 'Preview');
        // Don't break here so that we can test post-preview field presence and
        // function below.
      case DRUPAL_OPTIONAL:
        $this->assertSession()->buttonExists('Preview');
        $this->assertSession()->buttonExists('Save');
        $this->submitForm($edit, 'Save');
        break;

      case DRUPAL_DISABLED:
        $this->assertSession()->buttonNotExists('Preview');
        $this->assertSession()->buttonExists('Save');
        $this->submitForm($edit, 'Save');
        break;
    }
    $match = [];
    // Get knowledge ID.
    preg_match('/#knowledge-([0-9]+)/', $this->getURL(), $match);

    // Get knowledge.
    if ($contact !== TRUE) {
      // If true then attempting to find error message.
      if ($subject) {
        $this->assertSession()->pageTextContains($subject);
      }
      $this->assertSession()->pageTextContains($knowledge);
      // Check the knowledge ID was extracted.
      $this->assertArrayHasKey(1, $match);
    }

    if (isset($match[1])) {
      \Drupal::entityTypeManager()->getStorage('knowledge')->resetCache([$match[1]]);
      return Knowledge::load($match[1]);
    }
  }

  /**
   * Checks current page for specified knowledge.
   *
   * @param \Drupal\knowledge\KnowledgeInterface $knowledge
   *   The knowledge object.
   * @param bool $reply
   *   Boolean indicating whether the knowledge is a reply to another knowledge.
   *
   * @return bool
   *   Boolean indicating whether the knowledge was found.
   */
  public function knowledgeExists(?KnowledgeInterface $knowledge = NULL, $reply = FALSE) {
    if ($knowledge) {
      $knowledge_element = $this->cssSelect(($reply ? '.indented ' : '') . 'article#knowledge-' . $knowledge->id());
      if (empty($knowledge_element)) {
        return FALSE;
      }

      $knowledge_title = $knowledge_element[0]->find('xpath', 'div/h3/a');
      if (empty($knowledge_title) || $knowledge_title->getText() !== $knowledge->label()) {
        return FALSE;
      }

      $knowledge_body = $knowledge_element[0]->find('xpath', 'div/div/p');
      if (empty($knowledge_body) || $knowledge_body->getText() !== $knowledge->knowledge_body->value) {
        return FALSE;
      }

      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Deletes a knowledge.
   *
   * @param \Drupal\knowledge\KnowledgeInterface $knowledge
   *   Knowledge to delete.
   */
  public function deleteKnowledge(KnowledgeInterface $knowledge) {
    $this->drupalGet('knowledge/' . $knowledge->id() . '/delete');
    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains('The knowledge and all its replies have been deleted.');
  }

  /**
   * Sets the value governing whether the subject field should be enabled.
   *
   * @param bool $enabled
   *   Boolean specifying whether the subject field should be enabled.
   */
  public function setKnowledgeSubject($enabled) {
    $form_display = $this->container->get('entity_display.repository')
      ->getFormDisplay('knowledge', 'knowledge');

    if ($enabled) {
      $form_display->setComponent('subject', [
        'type' => 'string_textfield',
      ]);
    }
    else {
      $form_display->removeComponent('subject');
    }
    $form_display->save();
  }

  /**
   * Sets the value governing the previewing mode for the knowledge form.
   *
   * @param int $mode
   *   The preview mode: DRUPAL_DISABLED, DRUPAL_OPTIONAL or DRUPAL_REQUIRED.
   * @param string $field_name
   *   (optional) Field name through which the knowledge should be posted.
   *   Defaults to 'knowledge'.
   */
  public function setKnowledgePreview($mode, $field_name = 'knowledge') {
    switch ($mode) {
      case DRUPAL_DISABLED:
        $mode_text = 'disabled';
        break;

      case DRUPAL_OPTIONAL:
        $mode_text = 'optional';
        break;

      case DRUPAL_REQUIRED:
      default:
        $mode_text = 'required';
        break;
    }
    $this->setKnowledgeSettings('preview', $mode, new FormattableMarkup('Knowledge preview @mode_text.', ['@mode_text' => $mode_text]), $field_name);
  }

  /**
   * Sets the value governing whether the knowledge form is on its own page.
   *
   * @param bool $enabled
   *   TRUE if the knowledge form should be displayed on the same page as the
   *   knowledge; FALSE if it should be displayed on its own page.
   * @param string $field_name
   *   (optional) Field name through which the knowledge should be posted.
   *   Defaults to 'knowledge'.
   */
  public function setKnowledgeForm($enabled, $field_name = 'knowledge') {
    $this->setKnowledgeSettings('form_location', ($enabled ? KnowledgeItemInterface::FORM_BELOW : KnowledgeItemInterface::FORM_SEPARATE_PAGE), 'Knowledge controls ' . ($enabled ? 'enabled' : 'disabled') . '.', $field_name);
  }

  /**
   * Sets the value governing restrictions on anonymous knowledge.
   *
   * @param int $level
   *   The level of the contact information allowed for anonymous knowledge:
   *   - 0: No contact information allowed.
   *   - 1: Contact information allowed but not required.
   *   - 2: Contact information required.
   */
  public function setKnowledgeAnonymous($level) {
    $this->setKnowledgeSettings('anonymous', $level, new FormattableMarkup('Anonymous knowledge linking set to level @level.', ['@level' => $level]));
  }

  /**
   * Sets the value specifying the default amount of knowledge per page.
   *
   * @param int $number
   *   Knowledge per page value.
   * @param string $field_name
   *   (optional) Field name through which the knowledge should be posted.
   *   Defaults to 'knowledge'.
   */
  public function setKnowledgesPerPage($number, $field_name = 'knowledge') {
    $this->setKnowledgeSettings('per_page', $number, new FormattableMarkup('Amount of knowledge per page set to @number.', ['@number' => $number]), $field_name);
  }

  /**
   * Sets a knowledge settings variable for the article content type.
   *
   * @param string $name
   *   Name of variable.
   * @param string $value
   *   Value of variable.
   * @param string $message
   *   Status message to display.
   * @param string $field_name
   *   (optional) Field name through which the knowledge should be posted.
   *   Defaults to 'knowledge'.
   */
  public function setKnowledgeSettings($name, $value, $message, $field_name = 'knowledge') {
    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = FieldConfig::loadByName('node', 'article', $field_name);
    $field->setSetting($name, $value);
    $field->save();
  }

  /**
   * Checks whether the knowledgeer's contact information is displayed.
   *
   * @return bool
   *   Contact info is available.
   */
  public function knowledgeContactInfoAvailable() {
    return (bool) preg_match('/(input).*?(name="name").*?(input).*?(name="mail")/s', $this->getSession()->getPage()->getContent());
  }

  /**
   * Performs the specified operation on the specified knowledge.
   *
   * @param \Drupal\knowledge\KnowledgeInterface $knowledge
   *   Knowledge to perform operation on.
   * @param string $operation
   *   Operation to perform.
   * @param bool $approval
   *   Operation is found on approval page.
   */
  public function performKnowledgeOperation(KnowledgeInterface $knowledge, $operation, $approval = FALSE) {
    $edit = [];
    $edit['operation'] = $operation;
    $edit['knowledge[' . $knowledge->id() . ']'] = TRUE;
    $this->drupalGet('admin/content/knowledge' . ($approval ? '/approval' : ''));
    $this->submitForm($edit, 'Update');

    if ($operation == 'delete') {
      $this->submitForm([], 'Delete');
      $this->assertSession()->pageTextContains('Deleted 1 knowledge.');
    }
    else {
      $this->assertSession()->pageTextContains('The update has been performed.');
    }
  }

  /**
   * Gets the knowledge ID for an unapproved knowledge.
   *
   * @param string $subject
   *   Knowledge subject to find.
   *
   * @return int
   *   Knowledge id.
   */
  public function getUnapprovedKnowledge($subject) {
    $this->drupalGet('admin/content/knowledge/approval');
    preg_match('/href="(.*?)#knowledge-([^"]+)"(.*?)>(' . $subject . ')/', $this->getSession()->getPage()->getContent(), $match);

    return $match[2];
  }

  /**
   * Creates a knowledge type (bundle).
   *
   * @param string $label
   *   The knowledge type label.
   *
   * @return \Drupal\knowledge\Entity\KnowledgeType
   *   Created knowledge type.
   */
  protected function createKnowledgeType($label) {
    $bundle = KnowledgeType::create([
      'id' => $label,
      'label' => $label,
      'description' => '',
      'target_entity_type_id' => 'node',
    ]);
    $bundle->save();
    return $bundle;
  }

}
