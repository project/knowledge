<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeManagerInterface;
use Drupal\user\RoleInterface;

/**
 * Tests knowledge statistics on nodes.
 *
 * @group knowledge
 */
class KnowledgeStatisticsTest extends KnowledgeTestBase {

  /**
   * A secondary user for posting knowledge.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser2;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a second user to post knowledge.
    $this->webUser2 = $this->drupalCreateUser([
      'post knowledge',
      'create article content',
      'edit own knowledge',
      'post knowledge',
      'skip knowledge approval',
      'access knowledge',
      'access content',
    ]);
  }

  /**
   * Tests the node knowledge statistics.
   */
  public function testKnowledgeNodeKnowledgeStatistics() {
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    // Set knowledge to have subject and preview disabled.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgePreview(DRUPAL_DISABLED);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(FALSE);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');
    $this->drupalLogout();

    // Checks the initial values of node knowledge statistics with no knowledge.
    $node = $node_storage->load($this->node->id());
    $this->assertEquals($this->node->getCreatedTime(), $node->get('knowledge')->last_knowledge_timestamp, 'The initial value of node last_knowledge_timestamp is the node created date.');
    $this->assertEquals($this->webUser->id(), $node->get('knowledge')->last_knowledge_uid, 'The initial value of node last_knowledge_uid is the node uid.');
    $this->assertEquals(0, $node->get('knowledge')->total_count, 'The initial value of node total_count is zero.');

    // Post knowledge #1 as web_user2.
    $this->drupalLogin($this->webUser2);
    $knowledge_text = $this->randomMachineName();
    $this->postKnowledge($this->node, $knowledge_text);

    // Checks the new values of node knowledge statistics with knowledge #1.
    // The node cache needs to be reset before reload.
    $node_storage->resetCache([$this->node->id()]);
    $node = $node_storage->load($this->node->id());
    $this->assertEquals($this->webUser2->id(), $node->get('knowledge')->last_knowledge_uid, 'The value of node last_knowledge_uid is the knowledge #1 uid.');
    $this->assertEquals(1, $node->get('knowledge')->total_count, 'The value of node total_count is 1.');

    // Prepare for anonymous knowledge submission (knowledge approval enabled).
    $this->drupalLogin($this->adminUser);
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => TRUE,
      'post knowledge' => TRUE,
      'skip knowledge approval' => FALSE,
    ]);
    // Ensure that the poster can leave some contact info.
    $this->setKnowledgeAnonymous('1');
    $this->drupalLogout();

    // Post knowledge #2 as anonymous (knowledge approval enabled).
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $anonymous_knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), '', TRUE);

    // Checks the new values of node knowledge statistics with knowledge #2 and
    // ensure they haven't changed since the knowledge has not been moderated.
    // The node needs to be reloaded with the cache reset.
    $node_storage->resetCache([$this->node->id()]);
    $node = $node_storage->load($this->node->id());
    $this->assertEquals($this->webUser2->id(), $node->get('knowledge')->last_knowledge_uid, 'The value of node last_knowledge_uid is still the knowledge #1 uid.');
    $this->assertEquals(1, $node->get('knowledge')->total_count, 'The value of node total_count is still 1.');

    // Prepare for anonymous knowledge submission (no approval required).
    $this->drupalLogin($this->adminUser);
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => TRUE,
      'post knowledge' => TRUE,
      'skip knowledge approval' => TRUE,
    ]);
    $this->drupalLogout();

    // Post knowledge #3 as anonymous.
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $anonymous_knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), '', ['name' => $this->randomMachineName()]);
    Knowledge::load($anonymous_knowledge->id());

    // Checks the new values of node knowledge statistics with knowledge #3.
    // The node needs to be reloaded with the cache reset.
    $node_storage->resetCache([$this->node->id()]);
    $node = $node_storage->load($this->node->id());
    $this->assertEquals(0, $node->get('knowledge')->last_knowledge_uid, 'The value of node last_knowledge_uid is zero.');
    $this->assertEquals(2, $node->get('knowledge')->total_count, 'The value of node total_count is 2.');
  }

}
