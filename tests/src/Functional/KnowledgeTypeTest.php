<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\node\Entity\Node;

/**
 * Ensures that knowledge type functions work correctly.
 *
 * @group knowledge
 */
class KnowledgeTypeTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'administer knowledge',
    'administer knowledge fields',
    'administer knowledge types',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Tests creating a knowledge type programmatically and via a form.
   */
  public function testKnowledgeTypeCreation() {
    // Create a knowledge type programmatically.
    $type = $this->createKnowledgeType('other');

    $knowledge_type = KnowledgeType::load('other');
    $this->assertInstanceOf(KnowledgeType::class, $knowledge_type);

    // Log in a test user.
    $this->drupalLogin($this->adminUser);

    // Ensure that the new knowledge type admin page can be accessed.
    $this->drupalGet('admin/structure/knowledge/link/manage/' . $type->id());
    $this->assertSession()->statusCodeEquals(200);

    // Create a knowledge type via the user interface.
    $edit = [
      'id' => 'foo',
      'label' => 'title for foo',
      'description' => '',
      'target_entity_type_id' => 'node',
    ];
    $this->drupalGet('admin/structure/knowledge/link/types/add');
    $this->submitForm($edit, 'Save');
    $knowledge_type = KnowledgeType::load('foo');
    $this->assertInstanceOf(KnowledgeType::class, $knowledge_type);

    // Check that the knowledge type was created in site default language.
    $default_langcode = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $this->assertEquals($default_langcode, $knowledge_type->language()->getId());

    // Edit the knowledge-type and ensure that we cannot change the entity-type.
    $this->drupalGet('admin/structure/knowledge/link/manage/foo');
    $this->assertSession()->fieldNotExists('target_entity_type_id');
    $this->assertSession()->pageTextContains('Target entity type');
    // Save the form and ensure the entity-type value is preserved even though
    // the field isn't present.
    $this->submitForm([], 'Save');
    \Drupal::entityTypeManager()->getStorage('knowledge_type')->resetCache(['foo']);
    $knowledge_type = KnowledgeType::load('foo');
    $this->assertEquals('node', $knowledge_type->getTargetEntityTypeId());
  }

  /**
   * Tests editing a knowledge type using the UI.
   */
  public function testKnowledgeTypeEditing() {
    $this->drupalLogin($this->adminUser);
    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = FieldConfig::loadByName('knowledge', 'knowledge', 'knowledge_body');
    $this->assertEquals('Knowledge', $field->getLabel(), 'Knowledge body field was found.');

    // Change the knowledge type name.
    $this->drupalGet('admin/structure/knowledge/link');
    $edit = [
      'label' => 'Bar',
    ];
    $this->drupalGet('admin/structure/knowledge/link/manage/knowledge');
    $this->submitForm($edit, 'Save');

    $this->drupalGet('admin/structure/knowledge');
    $this->assertSession()->pageTextContains('Bar');
    $this->clickLink('Manage fields');
    // Verify that the original machine name was used in the URL.
    $this->assertSession()->addressEquals(Url::fromRoute('entity.knowledge.field_ui_fields', ['knowledge_type' => 'knowledge']));
    $this->assertCount(1, $this->cssSelect('tr#knowledge-body'), 'Body field exists.');

    // Remove the body field.
    $this->drupalGet('admin/structure/knowledge/link/manage/knowledge/fields/knowledge.knowledge.knowledge_body/delete');
    $this->submitForm([], 'Delete');
    // Resave the settings for this type.
    $this->drupalGet('admin/structure/knowledge/link/manage/knowledge');
    $this->submitForm([], 'Save');
    // Check that the body field doesn't exist.
    $this->drupalGet('admin/structure/knowledge/link/manage/knowledge/fields');
    $this->assertCount(0, $this->cssSelect('tr#knowledge-body'), 'Body field does not exist.');
  }

  /**
   * Tests deleting a knowledge type that still has content.
   */
  public function testKnowledgeTypeDeletion() {
    // Create a knowledge type programmatically.
    $type = $this->createKnowledgeType('foo');
    $this->drupalCreateContentType(['type' => 'page']);
    $this->addDefaultKnowledgeField('node', 'page', 'foo', KnowledgeItemInterface::OPEN, 'foo');
    $field_storage = FieldStorageConfig::loadByName('node', 'foo');

    $this->drupalLogin($this->adminUser);

    // Create a node.
    $node = Node::create([
      'type' => 'page',
      'title' => 'foo',
    ]);
    $node->save();

    // Add a new knowledge of this type.
    $knowledge = Knowledge::create([
      'knowledge_type' => 'foo',
      'entity_type' => 'node',
      'field_name' => 'foo',
      'entity_id' => $node->id(),
    ]);
    $knowledge->save();

    // Attempt to delete the knowledge type, which should not be allowed.
    $this->drupalGet('admin/structure/knowledge/link/manage/' . $type->id() . '/delete');
    $this->assertSession()->pageTextContains($type->label() . ' is used by 1 knowledge on your site. You can not remove this knowledge type until you have removed all of the ' . $type->label() . ' knowledge.');
    $this->assertSession()->pageTextContains('foo is used by the node.foo field on your site. You can not remove this knowledge type until you have removed the field.');
    $this->assertSession()->pageTextNotContains('This action cannot be undone.');

    // Delete the knowledge and the field.
    $knowledge->delete();
    $field_storage->delete();
    // Attempt to delete the knowledge type, which should now be allowed.
    $this->drupalGet('admin/structure/knowledge/link/manage/' . $type->id() . '/delete');
    $this->assertSession()->pageTextContains('Are you sure you want to delete the knowledge type ' . $type->id() . '?');
    $this->assertSession()->pageTextContains('This action cannot be undone.');

    // Test exception thrown when re-using an existing knowledge type.
    try {
      $this->addDefaultKnowledgeField('knowledge', 'knowledge', 'bar');
      $this->fail('Exception not thrown.');
    }
    catch (\InvalidArgumentException $e) {
      // Expected exception; just continue testing.
    }

    // Delete the knowledge type.
    $this->drupalGet('admin/structure/knowledge/link/manage/' . $type->id() . '/delete');
    $this->submitForm([], 'Delete');
    $this->assertNull(KnowledgeType::load($type->id()), 'Knowledge type deleted.');
    $this->assertSession()->pageTextContains('The knowledge type ' . $type->label() . ' has been deleted.');
  }

}
