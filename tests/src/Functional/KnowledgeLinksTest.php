<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\KnowledgeManagerInterface;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\user\RoleInterface;

/**
 * Basic knowledge links tests to ensure markup present.
 *
 * @group knowledge
 */
class KnowledgeLinksTest extends KnowledgeTestBase {

  /**
   * Knowledge being tested.
   *
   * @var \Drupal\knowledge\KnowledgeInterface
   */
  protected $knowledge;

  /**
   * Seen knowledge, array of knowledge IDs.
   *
   * @var array
   */
  protected $seen = [];

  /**
   * Use the main node listing to test rendering on teasers.
   *
   * @var array
   *
   * @todo Remove this dependency.
   */
  protected static $modules = ['views'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that knowledge links are output and can be hidden.
   */
  public function testKnowledgeLinks() {
    // Bartik theme alters knowledge links, so use a different theme.
    \Drupal::service('theme_installer')->install(['stark']);
    $this->config('system.theme')
      ->set('default', 'stark')
      ->save();

    // Remove additional user permissions from $this->webUser added by setUp(),
    // since this test is limited to anonymous and authenticated roles only.
    $roles = $this->webUser->getRoles();
    \Drupal::entityTypeManager()->getStorage('user_role')->load(reset($roles))->delete();

    // Create a knowledge via CRUD API functionality, since
    // $this->postKnowledge() relies on actual user permissions.
    $knowledge = Knowledge::create([
      'kid' => NULL,
      'entity_id' => $this->node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'uid' => 0,
      'status' => KnowledgeInterface::PUBLISHED,
      'subject' => $this->randomMachineName(),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'knowledge_body' => [['value' => $this->randomMachineName()]],
    ]);
    $knowledge->save();
    $this->knowledge = $knowledge;

    // Tests that reply link is not visible when threading is disabled.
    $this->drupalLogin($this->webUser);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT, 'Knowledge paging changed.');
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->linkNotExists('Reply');
    // Tests that reply link is visible when threading is enabled.
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->linkExists('Reply');

    // Change knowledge settings.
    $this->setKnowledgeSettings('form_location', KnowledgeItemInterface::FORM_BELOW, 'Set knowledge form location');
    $this->setKnowledgeAnonymous(TRUE);
    $this->node->knowledge = KnowledgeItemInterface::OPEN;
    $this->node->save();

    // Change user permissions.
    $perms = [
      'access knowledge' => 1,
      'post knowledge' => 1,
      'skip knowledge approval' => 1,
      'edit own knowledge' => 1,
    ];
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, $perms);

    $nid = $this->node->id();

    // Assert basic link is output, actual functionality is unit-tested in
    // \Drupal\knowledge\Tests\KnowledgeLinkBuilderTest.
    foreach (['node', "node/$nid"] as $path) {
      $this->drupalGet($path);

      // In teaser view, a link containing the knowledge count is always
      // expected.
      if ($path == 'node') {
        $this->assertSession()->linkExists('1 knowledge');
      }
      $this->assertSession()->linkExists('Add new knowledge');
    }

    $display_repository = $this->container->get('entity_display.repository');

    // Change weight to make links go before knowledge body.
    $display_repository->getViewDisplay('knowledge', 'knowledge')
      ->setComponent('links', ['weight' => -100])
      ->save();
    $this->drupalGet($this->node->toUrl());
    $element = $this->cssSelect('article.js-knowledge > div');
    // Get last child element.
    $element = end($element);
    $this->assertSame('div', $element->getTagName(), 'Last element is knowledge body.');

    // Change weight to make links go after knowledge body.
    $display_repository->getViewDisplay('knowledge', 'knowledge')
      ->setComponent('links', ['weight' => 100])
      ->save();
    $this->drupalGet($this->node->toUrl());
    $element = $this->cssSelect('article.js-knowledge > div');
    // Get last child element.
    $element = end($element);
    $this->assertNotEmpty($element->find('css', 'ul.links'), 'Last element is knowledge links.');

    // Make sure we can hide node links.
    $display_repository->getViewDisplay('node', $this->node->bundle())
      ->removeComponent('links')
      ->save();
    $this->drupalGet($this->node->toUrl());
    $this->assertSession()->linkNotExists('1 knowledge');
    $this->assertSession()->linkNotExists('Add new knowledge');

    // Visit the full node, make sure there are links for the knowledge.
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains($knowledge->label());
    $this->assertSession()->linkExists('Reply');

    // Make sure we can hide knowledge links.
    $display_repository->getViewDisplay('knowledge', 'knowledge')
      ->removeComponent('links')
      ->save();
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains($knowledge->label());
    $this->assertSession()->linkNotExists('Reply');
  }

}
