<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\system\Functional\Entity\EntityWithUriCacheTagsTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\KnowledgeManagerInterface;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests the Knowledge entity's cache tags.
 *
 * @group knowledge
 */
class KnowledgeCacheTagsTest extends EntityWithUriCacheTagsTestBase {

  use KnowledgeTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['knowledge'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A test entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entityTestCamelId;

  /**
   * Another test entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entityTestHippopotamusId;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Give anonymous users permission to view knowledge, so that we can verify
    // the cache tags of cached versions of knowledge pages.
    $user_role = Role::load(RoleInterface::ANONYMOUS_ID);
    $user_role->grantPermission('access knowledge');
    $user_role->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function createEntity() {
    // Create a "bar" bundle for the "entity_test" entity type and create.
    $bundle = 'bar';
    entity_test_create_bundle($bundle, NULL, 'entity_test');

    // Create a knowledge field on this bundle.
    $this->addDefaultKnowledgeField('entity_test', 'bar', 'knowledge');

    // Display knowledge in a flat list; threaded knowledge are not render
    // cached.
    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = FieldConfig::loadByName('entity_test', 'bar', 'knowledge');
    $field->setSetting('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT);
    $field->save();

    // Create a "CamelIds" test entity that the knowledge will be assigned to.
    $this->entityTestCamelId = EntityTest::create([
      'name' => 'CamelIds',
      'type' => 'bar',
    ]);
    $this->entityTestCamelId->save();

    // Create a "Llama" knowledge.
    $knowledge = Knowledge::create([
      'subject' => 'Llama',
      'entity_id' => $this->entityTestCamelId->id(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'status' => KnowledgeInterface::PUBLISHED,
    ]);
    $knowledge->save();

    return $knowledge;
  }

  /**
   * Tests that knowledge invalidate the cache tag of their host entity.
   */
  public function testKnowledgeEntity() {
    $this->verifyPageCache($this->entityTestCamelId->toUrl(), 'MISS');
    $this->verifyPageCache($this->entityTestCamelId->toUrl(), 'HIT');

    // Create a "Hippopotamus" knowledge.
    $this->entityTestHippopotamusId = EntityTest::create([
      'name' => 'Hippopotamus',
      'type' => 'bar',
    ]);
    $this->entityTestHippopotamusId->save();

    $this->verifyPageCache($this->entityTestHippopotamusId->toUrl(), 'MISS');
    $this->verifyPageCache($this->entityTestHippopotamusId->toUrl(), 'HIT');

    $hippo_knowledge = Knowledge::create([
      'subject' => 'Hippopotamus',
      'entity_id' => $this->entityTestHippopotamusId->id(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'status' => KnowledgeInterface::PUBLISHED,
    ]);
    $hippo_knowledge->save();

    // Ensure that a new knowledge only invalidates the linked entity.
    $this->verifyPageCache($this->entityTestCamelId->toUrl(), 'HIT');
    $this->verifyPageCache($this->entityTestHippopotamusId->toUrl(), 'MISS');
    $this->assertSession()->pageTextContains($hippo_knowledge->label());

    // Ensure that updating an existing knowledge only invalidates the knowledge
    // linked entity.
    $this->entity->save();
    $this->verifyPageCache($this->entityTestCamelId->toUrl(), 'MISS');
    $this->verifyPageCache($this->entityTestHippopotamusId->toUrl(), 'HIT');
  }

  /**
   * {@inheritdoc}
   */
  protected function getAdditionalCacheContextsForEntity(EntityInterface $entity) {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * Each knowledge must have a knowledge body, which always has a text format.
   */
  protected function getAdditionalCacheTagsForEntity(EntityInterface $entity) {
    /** @var \Drupal\knowledge\KnowledgeInterface $entity */
    return [
      'config:filter.format.plain_text',
      'user:' . $entity->getOwnerId(),
      'user_view',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultCacheContexts() {
    return [
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'theme',
      'user.permissions',
    ];
  }

}
