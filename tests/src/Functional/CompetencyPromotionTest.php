<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests knowledge administration and preview access.
 *
 * @group knowledge
 */
class CompetencyPromotionTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'knowledge',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The learner user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $learner;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->learner = $this->drupalCreateUser([
      'access knowledge',
      'access content',
      'view own knowledge_competency',
    ]);

  }

  /**
   * Tests role advancement, auto-promotion, current role.
   */
  public function testCompetencyAutoSelf() {
    $learner_id = $this->learner->id();
    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $competency_url = "user/$learner_id/competency";
    $edit_url = "user/$learner_id/edit";
    // The coach is not the learner's coach yet.
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(403);

    // The coach is the learner's coach.
    $this->learner->knowledge_coach->target_id = $coach->id();
    $this->learner->save();

    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Candidate');
    $this->assertSession()->pageTextContains('Contributor');
    $this->assertSession()->pageTextContains('Publisher');

    $this->doCandidate();

    $this->assertSession()->pageTextContains('was promoted to Candidate');

    $this->doContributor();

    $this->assertSession()->pageTextContains('was promoted to Contributor');

    $this->doPublisher();

    $this->assertSession()->pageTextContains('was promoted to Publisher');

  }

  /**
   * Tests role advancement, auto-promotion, next role.
   */
  public function testCompetencyAutoNext() {
    $config = \Drupal::service('config.factory')->getEditable('knowledge.competency.settings');
    $settings = $config->get('roles');
    foreach ($settings as $index => $value) {
      $settings[$index]['promote'] = 'next';
      if ($value['role'] == 'publisher') {
        $settings[$index]['promote'] = '_none';
        $settings[$index]['action'] = '_none';
      }
    }
    $config->set('roles', $settings)->save();

    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $learner_id = $this->learner->id();
    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $competency_url = "user/$learner_id/competency";
    $edit_url = "user/$learner_id/edit";
    // The coach is not the learner's coach yet.
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(403);

    // The coach is the learner's coach.
    $this->learner->knowledge_coach->target_id = $coach->id();
    $this->learner->save();

    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Candidate');
    $this->assertSession()->pageTextContains('Contributor');
    $this->assertSession()->pageTextContains('Publisher');

    $this->doCandidate();

    $this->assertSession()->pageTextContains('was promoted to Contributor');

    $this->doContributor();

    $this->assertSession()->pageTextContains('was promoted to Publisher');

    $this->doPublisher();

  }

  /**
   * Tests role advancement, leader approval, current role.
   */
  public function testCompetencyLeaderSelf() {
    $config = \Drupal::service('config.factory')->getEditable('knowledge.competency.settings');
    $settings = $config->get('roles');
    foreach ($settings as $index => $value) {
      $settings[$index]['action'] = 'leader';
    }
    $config->set('roles', $settings)->save();

    $leader = $this->drupalCreateUser([
      'view follower knowledge_competency',
    ]);
    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $learner_id = $this->learner->id();
    $competency_url = "user/$learner_id/competency";

    // The coach is the learner's coach.
    $this->learner->knowledge_coach->target_id = $coach->id();
    $this->learner->knowledge_leader->target_id = $leader->id();
    $this->learner->save();

    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Candidate');
    $this->assertSession()->pageTextContains('Contributor');
    $this->assertSession()->pageTextContains('Publisher');

    $this->doCandidate();

    $this->drupalLogin($leader);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);

    $this->drupalGet($competency_url . '/approve');
    $assert->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Candidate');
    $this->submitForm([], 'Promote');

    $this->assertSession()->pageTextContains('was promoted to Candidate');

    $this->drupalLogin($coach);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->doContributor();

    $this->drupalLogin($leader);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);

    $this->drupalGet($competency_url . '/approve');
    $assert->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Contributor');
    $this->submitForm([], 'Promote');

    $this->assertSession()->pageTextContains('was promoted to Contributor');

    $this->drupalLogin($coach);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->doPublisher();

    $this->drupalLogin($leader);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);

    $this->drupalGet($competency_url . '/approve');
    $assert->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Publisher');
    $this->submitForm([], 'Promote');

    $this->assertSession()->pageTextContains('was promoted to Publisher');

  }

  /**
   * Tests role advancement, leader approval, current role.
   */
  public function testCompetencyLeaderNext() {
    $config = \Drupal::service('config.factory')->getEditable('knowledge.competency.settings');
    $settings = $config->get('roles');
    foreach ($settings as $index => $value) {
      $settings[$index]['action'] = 'leader';
      $settings[$index]['promote'] = 'next';
      if ($value['role'] == 'knowledge_publisher') {
        $settings[$index]['promote'] = '_none';
        $settings[$index]['action'] = '_none';
      }
    }
    $config->set('roles', $settings)->save();

    $leader = $this->drupalCreateUser([
      'view follower knowledge_competency',
    ]);
    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $learner_id = $this->learner->id();
    $competency_url = "user/$learner_id/competency";

    // The coach is the learner's coach.
    $this->learner->knowledge_coach->target_id = $coach->id();
    $this->learner->knowledge_leader->target_id = $leader->id();
    $this->learner->save();

    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Candidate');
    $this->assertSession()->pageTextContains('Contributor');
    $this->assertSession()->pageTextContains('Publisher');

    $this->doCandidate();

    $this->drupalLogin($leader);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);

    $this->drupalGet($competency_url . '/approve');
    $assert->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Contributor');
    $this->submitForm([], 'Promote');

    $this->assertSession()->pageTextContains('was promoted to Contributor');

    $this->drupalLogin($coach);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->doContributor();

    $this->drupalLogin($leader);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);

    $this->drupalGet($competency_url . '/approve');
    $assert->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Publisher');
    $this->submitForm([], 'Promote');

    $this->assertSession()->pageTextContains('was promoted to Publisher');

    $this->drupalLogin($coach);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->doPublisher();

    $this->drupalLogin($leader);
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);

    $this->drupalGet($competency_url . '/approve');
    $assert->statusCodeEquals(404);

    // $this->assertSession()->pageTextContains('Publisher');
    // $this->submitForm([], 'Promote');
    // $this->assertSession()->pageTextContains('was promoted to Publisher');
  }

  /**
   * Tests role advancement, _none, on role.
   */
  public function testCompetencyNone() {
    $config = \Drupal::service('config.factory')->getEditable('knowledge.competency.settings');
    $settings = $config->get('roles');
    foreach ($settings as $index => $value) {
      $settings[$index]['action'] = '_none';
      $settings[$index]['promote'] = '_none';
    }
    $config->set('roles', $settings)->save();

    $learner_id = $this->learner->id();
    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $competency_url = "user/$learner_id/competency";
    $edit_url = "user/$learner_id/edit";
    // The coach is not the learner's coach yet.
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(403);

    // The coach is the learner's coach.
    $this->learner->knowledge_coach->target_id = $coach->id();
    $this->learner->save();

    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Candidate');
    $this->assertSession()->pageTextContains('Contributor');
    $this->assertSession()->pageTextContains('Publisher');

    $this->doCandidate();

    $this->assertSession()->pageTextNotContains('was promoted to Candidate');

    $this->doContributor();

    $this->assertSession()->pageTextNotContains('was promoted to Contributor');

    $this->doPublisher();

    $this->assertSession()->pageTextNotContains('was promoted to Publisher');

  }

  /**
   * Tests role advancement, auto-promotion, current role.
   */
  public function testCompetencyAutoSelfAll() {
    $learner_id = $this->learner->id();
    $coach = $this->drupalCreateUser([
      'edit learner knowledge_competency',
    ]);
    $this->drupalLogin($coach);

    $assert = $this->assertSession();

    $competency_url = "user/$learner_id/competency";
    $edit_url = "user/$learner_id/edit";
    // The coach is not the learner's coach yet.
    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(403);

    // The coach is the learner's coach.
    $this->learner->knowledge_coach->target_id = $coach->id();
    $this->learner->save();

    $this->drupalGet($competency_url);
    $assert->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Candidate');
    $this->assertSession()->pageTextContains('Contributor');
    $this->assertSession()->pageTextContains('Publisher');

    $this->doPublisher();

    $this->assertSession()->pageTextContains('was promoted to Publisher');

  }

  /**
   * Submit the form with Candidate values.
   */
  protected function doCandidate() {
    $this->submitForm([
      'edit-field-search-it-value' => 1,
      'edit-field-link-it-value' => 1,
      'edit-field-flag-it-value' => 1,
    ], 'Save');
  }

  /**
   * Submit the form with Contributor values.
   */
  protected function doContributor() {
    $this->submitForm([
      'edit-field-search-it-value' => 1,
      'edit-field-link-it-value' => 1,
      'edit-field-flag-it-value' => 1,

      'edit-field-business-value' => 1,
      'edit-field-documents-request-value' => 1,
      'edit-field-fix-it-value' => 1,
      'edit-field-reuse-value' => 1,
      'edit-field-kcs-article-elements-value' => 1,
      'edit-field-structure-value' => 1,
      'edit-field-complete-thoughts-value' => 1,
      'edit-field-one-value' => 1,
      'edit-field-includes-context-value' => 1,
      'edit-field-update-or-create-value' => 1,
      'edit-field-solve-loop-value' => 1,

    ], 'Save');
  }

  /**
   * Submit the form with publisher values.
   */
  protected function doPublisher() {
    $this->submitForm([
      'edit-field-search-it-value' => 1,
      'edit-field-link-it-value' => 1,
      'edit-field-flag-it-value' => 1,

      'edit-field-business-value' => 1,
      'edit-field-documents-request-value' => 1,
      'edit-field-fix-it-value' => 1,
      'edit-field-reuse-value' => 1,
      'edit-field-kcs-article-elements-value' => 1,
      'edit-field-structure-value' => 1,
      'edit-field-complete-thoughts-value' => 1,
      'edit-field-one-value' => 1,
      'edit-field-includes-context-value' => 1,
      'edit-field-update-or-create-value' => 1,
      'edit-field-solve-loop-value' => 1,

      'edit-field-capture-context-value' => 1,
      'edit-field-audience-value' => 1,
      'edit-field-content-standard-value' => 1,
      'edit-field-sufficient-to-solve-value' => 1,
      'edit-field-relevant-value' => 1,
      'edit-field-iterative-search-value' => 1,
      'edit-field-improve-value' => 1,
      'edit-field-process-adherence-value' => 1,
      'edit-field-confidence-value' => 1,
      'edit-field-capture-in-the-moment-value' => 1,
      'edit-field-collaborate-value' => 1,

    ], 'Save');
  }

}
