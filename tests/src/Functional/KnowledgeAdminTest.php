<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Component\Utility\Html;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\user\RoleInterface;

/**
 * Tests knowledge approval functionality.
 *
 * @group knowledge
 */
class KnowledgeAdminTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests knowledge approval functionality through admin/content/knowledge.
   */
  public function testApprovalAdminInterface() {
    // Set anonymous knowledge to require approval.
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => TRUE,
      'post knowledge' => TRUE,
      'skip knowledge approval' => FALSE,
    ]);
    $this->drupalLogin($this->adminUser);
    // Ensure that doesn't require contact info.
    $this->setKnowledgeAnonymous('0');

    // Test that the knowledge page loads correctly when there are no knowledge.
    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->pageTextContains('No knowledge available.');

    $this->drupalLogout();

    // Post anonymous knowledge without contact info.
    $subject = $this->randomMachineName();
    $body = $this->randomMachineName();
    // Set $contact to true so that it won't check for id and message.
    $this->postKnowledge($this->node, $body, $subject, TRUE);
    $this->assertSession()->pageTextContains('Your knowledge has been queued for review by site administrators and will be published after approval.');

    // Get unapproved knowledge id.
    $this->drupalLogin($this->adminUser);
    $anonymous_knowledge4 = $this->getUnapprovedKnowledge($subject);
    $anonymous_knowledge4 = Knowledge::create([
      'kid' => $anonymous_knowledge4,
      'subject' => $subject,
      'entity_id' => $this->node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ]);
    $this->drupalLogout();

    $this->assertFalse($this->knowledgeExists($anonymous_knowledge4), 'Anonymous knowledge was not published.');

    // Approve knowledge.
    $this->drupalLogin($this->adminUser);
    $this->performKnowledgeOperation($anonymous_knowledge4, 'publish', TRUE);
    $this->drupalLogout();

    $this->drupalGet('node/' . $this->node->id());
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge4), 'Anonymous knowledge visible.');

    // Post 2 anonymous knowledge without contact info.
    $knowledge_links[] = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $knowledge_links[] = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Publish multiple knowledge in one operation.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content/knowledge/approval');
    $this->assertSession()->pageTextContains('Unapproved knowledge (2)');
    $edit = [
      "knowledge[{$knowledge_links[0]->id()}]" => 1,
      "knowledge[{$knowledge_links[1]->id()}]" => 1,
    ];
    $this->submitForm($edit, 'Update');
    $this->assertSession()->pageTextContains('Unapproved knowledge (0)');

    // Delete multiple knowledge in one operation.
    $edit = [
      'operation' => 'delete',
      "knowledge[{$knowledge_links[0]->id()}]" => 1,
      "knowledge[{$knowledge_links[1]->id()}]" => 1,
      "knowledge[{$anonymous_knowledge4->id()}]" => 1,
    ];
    $this->submitForm($edit, 'Update');
    $this->assertSession()->pageTextContains('Are you sure you want to delete these knowledge and all their children?');
    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains('No knowledge available.');
    // Test message when no knowledge selected.
    $edit = [
      'operation' => 'delete',
    ];
    $this->submitForm($edit, 'Update');
    $this->assertSession()->pageTextContains('Select one or more knowledge to perform the update on.');

    // Make sure the label of unpublished node is not visible on listing page.
    $this->drupalGet('admin/content/knowledge');
    $this->postKnowledge($this->node, $this->randomMachineName());
    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->pageTextContains(Html::escape($this->node->label()));
    $this->node->setUnpublished()->save();
    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->pageTextNotContains(Html::escape($this->node->label()));
  }

  /**
   * Tests knowledge approval functionality through the node interface.
   */
  public function testApprovalNodeInterface() {
    // Set anonymous knowledge to require approval.
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => TRUE,
      'post knowledge' => TRUE,
      'skip knowledge approval' => FALSE,
    ]);
    $this->drupalLogin($this->adminUser);
    // Ensure that doesn't require contact info.
    $this->setKnowledgeAnonymous('0');
    $this->drupalLogout();

    // Post anonymous knowledge without contact info.
    $subject = $this->randomMachineName();
    $body = $this->randomMachineName();
    // Set $contact to true so that it won't check for id and message.
    $this->postKnowledge($this->node, $body, $subject, TRUE);
    $this->assertSession()->pageTextContains('Your knowledge has been queued for review by site administrators and will be published after approval.');

    // Get unapproved knowledge id.
    $this->drupalLogin($this->adminUser);
    $anonymous_knowledge4 = $this->getUnapprovedKnowledge($subject);
    $anonymous_knowledge4 = Knowledge::create([
      'kid' => $anonymous_knowledge4,
      'subject' => $subject,
      'entity_id' => $this->node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ]);
    $this->drupalLogout();

    $this->assertFalse($this->knowledgeExists($anonymous_knowledge4), 'Anonymous knowledge was not published.');

    // Ensure knowledge cannot be approved without a valid token.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('knowledge/1/approve');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('knowledge/1/approve', ['query' => ['token' => 'forged']]);
    $this->assertSession()->statusCodeEquals(403);

    // Approve knowledge.
    $this->drupalGet('knowledge/1/edit');
    $this->assertSession()->checkboxChecked('edit-status-0');
    $this->drupalGet('node/' . $this->node->id());
    $this->clickLink('Approve');
    $this->drupalLogout();

    $this->drupalGet('node/' . $this->node->id());
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge4), 'Anonymous knowledge visible.');
  }

  /**
   * Tests knowledge bundle admin.
   */
  public function testKnowledgeAdmin() {
    // Login.
    $this->drupalLogin($this->adminUser);
    // Browse to knowledge bundle overview.
    $this->drupalGet('admin/structure/knowledge/link');
    $this->assertSession()->statusCodeEquals(200);
    // Make sure titles visible.
    $this->assertSession()->pageTextContains('Knowledge type');
    $this->assertSession()->pageTextContains('Description');
    // Make sure the description is present.
    $this->assertSession()->pageTextContains('Default knowledge field');
    // Manage fields.
    $this->clickLink('Manage fields');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests editing a knowledge as an admin.
   */
  public function testEditKnowledge() {
    // Enable anonymous user knowledge.
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge',
      'post knowledge',
      'skip knowledge approval',
    ]);

    // Log in as a web user.
    $this->drupalLogin($this->webUser);
    // Post a knowledge.
    $knowledge = $this->postKnowledge($this->node, $this->randomMachineName());

    $this->drupalLogout();

    // Post anonymous knowledge.
    $this->drupalLogin($this->adminUser);
    // Ensure that we need email id before posting knowledge.
    $this->setKnowledgeAnonymous('2');
    $this->drupalLogout();

    // Post knowledge with contact info (required).
    $anonymous_knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), []);

    // Log in as an admin user.
    $this->drupalLogin($this->adminUser);

    // Make sure the knowledge field is not visible when
    // the knowledge was posted by an authenticated user.
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->assertSession()->fieldNotExists('edit-mail');

    // Make sure the knowledge field is visible when
    // the knowledge was posted by an anonymous user.
    $this->drupalGet('knowledge/' . $anonymous_knowledge->id() . '/edit');

  }

}
