<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\KnowledgeType;

/**
 * Tests fields on knowledge.
 *
 * @group knowledge
 */
class KnowledgeFieldsTest extends KnowledgeTestBase {

  /**
   * Install the field UI.
   *
   * @var array
   */
  protected static $modules = ['field_ui'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that you can remove a knowledge field.
   */
  public function testKnowledgeFieldDelete() {
    $this->drupalCreateContentType(['type' => 'test_node_type']);
    $this->addDefaultKnowledgeField('node', 'test_node_type');
    // We want to test the handling of removing the primary knowledge field, so
    // we ensure there is at least one other knowledge field attached to a
    // node type so that knowledge_entity_load() runs for nodes.
    $this->addDefaultKnowledgeField('node', 'test_node_type', 'knowledge2');

    // Create a sample node.
    $node = $this->drupalCreateNode([
      'title' => 'Baloney',
      'type' => 'test_node_type',
    ]);

    $this->drupalLogin($this->webUser);

    $this->drupalGet('node/' . $node->nid->value);
    $elements = $this->cssSelect('.knowledge-form');
    $this->assertCount(2, $elements, 'There are two knowledge fields on the node.');

    // Delete the first knowledge field.
    FieldStorageConfig::loadByName('node', 'knowledge')->delete();
    $this->drupalGet('node/' . $node->nid->value);
    $elements = $this->cssSelect('.knowledge-form');
    $this->assertCount(1, $elements, 'There is one knowledge field on the node.');
  }

  /**
   * Tests link building with non-default knowledge field names.
   */
  public function testKnowledgeFieldLinksNonDefaultName() {
    $this->drupalCreateContentType(['type' => 'test_node_type']);
    $this->addDefaultKnowledgeField('node', 'test_node_type', 'knowledge2');

    $web_user2 = $this->drupalCreateUser([
      'access knowledge',
      'post knowledge',
      'create article content',
      'edit own knowledge',
      'skip knowledge approval',
      'access content',
    ]);

    // Create a sample node.
    $node = $this->drupalCreateNode([
      'title' => 'Baloney',
      'type' => 'test_node_type',
    ]);

    // Go to the node first so that webuser2 see new knowledge.
    $this->drupalLogin($web_user2);
    $this->drupalGet($node->toUrl());
    $this->drupalLogout();

    // Test that buildKnowledgeedEntityLinks() does not break when the
    // 'knowledge' field does not exist. Requires at least one knowledge.
    $this->drupalLogin($this->webUser);
    $this->postKnowledge($node, 'Here is a knowledge', '', NULL, 'knowledge2');
    $this->drupalLogout();

    $this->drupalLogin($web_user2);

    // We want to check the attached drupalSettings of
    // \Drupal\knowledge\KnowledgeLinkBuilder::buildKnowledgeedEntityLinks.
    // Therefore we need a node listing, let's use views for that.
    $this->container->get('module_installer')->install(['views'], TRUE);
    $this->drupalGet('node');

    $link_info = $this->getDrupalSettings()['knowledge']['newKnowledgesLinks']['node']['knowledge2']['2'];
    $this->assertSame(1, $link_info['new_total_count']);
    $this->assertSame($node->toUrl('canonical', ['fragment' => 'new'])->toString(), $link_info['first_new_knowledge_link']);
  }

  /**
   * Tests creating a knowledge field through the interface.
   */
  public function testKnowledgeFieldCreate() {
    // Create user who can administer user fields.
    $user = $this->drupalCreateUser([
      'administer user fields',
    ]);
    $this->drupalLogin($user);

    // Create knowledge field in account settings.
    $edit = [
      'new_storage_type' => 'knowledge',
      'label' => 'User knowledge',
      'field_name' => 'user_knowledge',
    ];
    $this->drupalGet('admin/config/people/accounts/fields/add-field');
    $this->submitForm($edit, 'Save and continue');

    // Try to save the knowledge field without selecting a knowledge type.
    $edit = [];
    $this->drupalGet('admin/config/people/accounts/fields/user.user.field_user_knowledge/storage');
    $this->submitForm($edit, 'Save field settings');
    // We should get an error message.
    $this->assertSession()->pageTextContains('An illegal choice has been detected. Please contact the site administrator.');

    // Create a knowledge type for users.
    $bundle = KnowledgeType::create([
      'id' => 'user_knowledge_type',
      'label' => 'user_knowledge_type',
      'description' => '',
      'target_entity_type_id' => 'user',
    ]);
    $bundle->save();

    // Select a knowledge type and try to save again.
    $edit = [
      'settings[knowledge_type]' => 'user_knowledge_type',
    ];
    $this->drupalGet('admin/config/people/accounts/fields/user.user.field_user_knowledge/storage');
    $this->submitForm($edit, 'Save field settings');
    // We shouldn't get an error message.
    $this->assertSession()->pageTextNotContains('An illegal choice has been detected. Please contact the site administrator.');
  }

  /**
   * Tests that knowledge module works when installed after a content module.
   */
  public function testKnowledgeInstallAfterContentModule() {
    // Create a user to do module administration.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer modules',
    ]);
    $this->drupalLogin($this->adminUser);

    // Drop default knowledge field added in KnowledgeTestBase::setUp().
    FieldStorageConfig::loadByName('node', 'knowledge')->delete();
    if ($field_storage = FieldStorageConfig::loadByName('node', 'knowledge_forum')) {
      $field_storage->delete();
    }

    // Purge field data now to allow knowledge module to be uninstalled once the
    // field has been deleted.
    field_purge_batch(10);

    // Uninstall the knowledge module.
    $edit = [];
    $edit['uninstall[knowledge]'] = TRUE;
    $this->drupalGet('admin/modules/uninstall');
    $this->submitForm($edit, 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->rebuildContainer();
    $this->assertFalse($this->container->get('module_handler')->moduleExists('knowledge'), 'Knowledge module uninstalled.');

    // Install core content type module (book).
    $edit = [];
    $edit['modules[book][enable]'] = 'book';
    $this->drupalGet('admin/modules');
    $this->submitForm($edit, 'Install');

    // Now install the knowledge module.
    $edit = [];
    $edit['modules[knowledge][enable]'] = 'knowledge';
    $this->drupalGet('admin/modules');
    $this->submitForm($edit, 'Install');
    $this->rebuildContainer();
    $this->assertTrue($this->container->get('module_handler')->moduleExists('knowledge'), 'Knowledge module enabled.');

    // Create nodes of each type.
    $this->addDefaultKnowledgeField('node', 'book');
    $book_node = $this->drupalCreateNode(['type' => 'book']);

    $this->drupalLogout();

    // Try to post a knowledge on each node. A failure will be triggered if the
    // knowledge body is missing on one of these forms, due to postKnowledge()
    // asserting that the body is actually posted correctly.
    $this->webUser = $this->drupalCreateUser([
      'access content',
      'access knowledge',
      'post knowledge',
      'skip knowledge approval',
    ]);
    $this->drupalLogin($this->webUser);
    $this->postKnowledge($book_node, $this->randomMachineName(), $this->randomMachineName());
  }

}
