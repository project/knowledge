<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\user\RoleInterface;

/**
 * Tests knowledge block functionality.
 *
 * @group knowledge
 */
class KnowledgeBlockTest extends KnowledgeTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['block', 'views'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Update admin user to have the 'administer blocks' permission.
    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer knowledge',
      'skip knowledge approval',
      'post knowledge',
      'access knowledge',
      'access content',
      'administer blocks',
    ]);
  }

  /**
   * Tests the recent knowledge block.
   */
  public function testRecentKnowledgeBlock() {
    $this->drupalLogin($this->adminUser);
    $this->drupalPlaceBlock('views_block:knowledge_recent-block_1');

    // Add some test knowledge, with and without subjects. Because the 10 newest
    // knowledge should be shown by the block, we create 11 to test that
    // behavior below.
    $timestamp = \Drupal::time()->getRequestTime();
    for ($i = 0; $i < 11; ++$i) {
      $subject = ($i % 2) ? $this->randomMachineName() : '';
      $knowledge_links[$i] = $this->postKnowledge($this->node, $this->randomMachineName(), $subject);
      $knowledge_links[$i]->created->value = $timestamp--;
      $knowledge_links[$i]->save();
    }

    // Test that a user without the 'access knowledge' permission cannot see the
    // block.
    $this->drupalLogout();
    user_role_revoke_permissions(RoleInterface::ANONYMOUS_ID, ['access knowledge']);
    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains('Recent knowledge');
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access knowledge']);

    // Test that a user with the 'access knowledge' permission can see the
    // block.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('');
    $this->assertSession()->pageTextContains('Recent knowledge');

    // Test the only the 10 latest knowledge are shown and in the proper order.
    $this->assertSession()->pageTextNotContains($knowledge_links[10]->label());
    $position = 0;
    for ($i = 0; $i < 10; $i++) {
      $this->assertSession()->pageTextContains($knowledge_links[$i]->label());
      if ($i > 1) {
        $previous_position = $position;
        $position = strpos($this->getSession()->getPage()->getContent(), $knowledge_links[$i]->label());
        $this->assertGreaterThan($previous_position, $position, sprintf('Knowledge %d does not appear after knowledge %d', 10 - $i, 11 - $i));
      }
      $position = strpos($this->getSession()->getPage()->getContent(), $knowledge_links[$i]->label());
    }

    // Test that links to knowledge work when knowledge are across pages.
    $this->setKnowledgesPerPage(1);

    for ($i = 0; $i < 10; $i++) {
      $this->clickLink($knowledge_links[$i]->label());
      $this->assertSession()->pageTextContains($knowledge_links[$i]->label());
      $this->assertSession()->responseContains('<link rel="canonical"');
    }
  }

}
