<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\node\Entity\NodeType;

/**
 * Tests knowledge administration and preview access.
 *
 * @group knowledge
 */
class KnowledgeAccessTest extends BrowserTestBase {

  use KnowledgeTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'knowledge',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Node for knowledge linking.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $unpublishedNode;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $node_type = NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ]);
    $node_type->save();
    $node_author = $this->drupalCreateUser([
      'create article content',
      'access knowledge',
    ]);

    $this->drupalLogin($this->drupalCreateUser([
      'edit own knowledge',
      'skip knowledge approval',
      'post knowledge',
      'access knowledge',
      'access content',
    ]));

    $this->addDefaultKnowledgeField('node', 'article');
    $this->unpublishedNode = $this->createNode([
      'title' => 'This is unpublished',
      'uid' => $node_author->id(),
      'status' => 0,
      'type' => 'article',
    ]);
    $this->unpublishedNode->save();
  }

  /**
   * Tests knowledge linking disabled for access-blocked entities.
   */
  public function testCannotKnowledgeOnEntitiesYouCannotView() {
    $assert = $this->assertSession();

    $knowledge_url = 'knowledge/reply/node/' . $this->unpublishedNode->id() . '/knowledge';

    // Knowledge linking on an unpublished node results in access denied.
    $this->drupalGet($knowledge_url);
    $assert->statusCodeEquals(403);

    // Publishing the node grants access.
    $this->unpublishedNode->setPublished()->save();
    $this->drupalGet($knowledge_url);
    $assert->statusCodeEquals(200);
  }

  /**
   * Tests cannot view knowledge reply form on entities you cannot view.
   */
  public function testCannotViewKnowledgeReplyFormOnEntitiesYouCannotView() {
    $assert = $this->assertSession();

    // Create a knowledge on an unpublished node.
    $knowledge = Knowledge::create([
      'entity_type' => 'node',
      'name' => 'Tony',
      'subject' => 'Knowledge on unpublished node',
      'entity_id' => $this->unpublishedNode->id(),
      'knowledge_type' => 'knowledge',
      'field_name' => 'knowledge',
      'uid' => $this->unpublishedNode->getOwnerId(),
      'status' => 1,
    ]);
    $knowledge->save();

    $knowledge_url = 'knowledge/reply/node/' . $this->unpublishedNode->id() . '/knowledge/' . $knowledge->id();

    // Replying to a knowledge on an unpublished node results in access denied.
    $this->drupalGet($knowledge_url);
    $assert->statusCodeEquals(403);

    // Publishing the node grants access.
    $this->unpublishedNode->setPublished()->save();
    $this->drupalGet($knowledge_url);
    $assert->statusCodeEquals(200);
  }

}
