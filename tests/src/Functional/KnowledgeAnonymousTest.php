<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\knowledge\KnowledgeInterface;
use Drupal\user\RoleInterface;

/**
 * Tests anonymous knowledge linking.
 *
 * @group knowledge
 */
class KnowledgeAnonymousTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable anonymous and authenticated user knowledge.
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge',
      'post knowledge',
      'skip knowledge approval',
    ]);
    user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, [
      'access knowledge',
      'post knowledge',
      'skip knowledge approval',
    ]);
  }

  /**
   * Tests anonymous knowledge functionality.
   */
  public function testAnonymous() {
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeAnonymous(KnowledgeInterface::ANONYMOUS_MAYNOT_CONTACT);
    $this->drupalLogout();

    // Preview knowledge (with `skip knowledge approval` permission).
    $edit = [];
    $title = 'knowledge title with skip knowledge approval';
    $body = 'knowledge body with skip knowledge approval';
    $edit['subject[0][value]'] = $title;

    $this->drupalGet($this->node->toUrl());
    $this->submitForm($edit, 'Preview');
    // Cannot use assertRaw here since both title and body are in the form.
    $preview = (string) $this->cssSelect('[data-drupal-selector="edit-knowledge-preview"]')[0]->getHtml();
    $this->assertStringContainsString($title, $preview, 'Anonymous user can preview knowledge title.');
    $this->assertStringContainsString($body, $preview, 'Anonymous user can preview knowledge body.');

    // Preview knowledge (without `skip knowledge approval` permission).
    user_role_revoke_permissions(RoleInterface::ANONYMOUS_ID, ['skip knowledge approval']);
    $edit = [];
    $title = 'knowledge title without skip knowledge approval';
    $body = 'knowledge body without skip knowledge approval';
    $edit['subject[0][value]'] = $title;
    $this->drupalGet($this->node->toUrl());
    $this->submitForm($edit, 'Preview');
    // Cannot use assertRaw here since both title and body are in the form.
    $preview = (string) $this->cssSelect('[data-drupal-selector="edit-knowledge-preview"]')[0]->getHtml();
    $this->assertStringContainsString($title, $preview, 'Anonymous user can preview knowledge title.');
    $this->assertStringContainsString($body, $preview, 'Anonymous user can preview knowledge body.');
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['skip knowledge approval']);

    // Post anonymous knowledge without contact info.
    $anonymous_knowledge1 = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName());
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge1), 'Anonymous knowledge without contact info found.');

    // Ensure anonymous users cannot post in the name of registered users.
    $edit = [
      'name' => $this->adminUser->getAccountName(),
    ];
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('The name you used (' . $this->adminUser->getAccountName() . ') belongs to a registered user.');

    // Allow contact info.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeAnonymous(KnowledgeInterface::ANONYMOUS_MAY_CONTACT);

    // Attempt to edit anonymous knowledge.
    $this->drupalGet('knowledge/' . $anonymous_knowledge1->id() . '/edit');
    $edited_knowledge = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName());
    $this->assertTrue($this->knowledgeExists($edited_knowledge, FALSE), 'Modified reply found.');
    $this->drupalLogout();

    // Post anonymous knowledge with contact info (optional).
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->assertTrue($this->knowledgeContactInfoAvailable(), 'Contact information available.');

    // Check the presence of expected cache tags.
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'config:field.field.node.article.knowledge');
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'config:user.settings');

    $anonymous_knowledge2 = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName());
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge2), 'Anonymous knowledge with contact info (optional) found.');

    // Ensure anonymous users cannot post in the name of registered users.
    $edit = [
      'name' => $this->adminUser->getAccountName(),
      'subject[0][value]' => $this->randomMachineName(),
    ];
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('The name you used (' . $this->adminUser->getAccountName() . ') belongs to a registered user.');

    // Require contact info.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeAnonymous(KnowledgeInterface::ANONYMOUS_MUST_CONTACT);
    $this->drupalLogout();

    // Try to post knowledge with contact info (required).
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->assertTrue($this->knowledgeContactInfoAvailable(), 'Contact information available.');

    $anonymous_knowledge3 = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    // Name should have 'Anonymous' for value by default.
    $this->assertSession()->pageTextContains('Email field is required.');
    $this->assertFalse($this->knowledgeExists($anonymous_knowledge3), 'Anonymous knowledge with contact info (required) not found.');

    // Post knowledge with contact info (required).
    $author_name = $this->randomMachineName();
    $anonymous_knowledge3 = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), [
      'name' => $author_name,
    ]);
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge3), 'Anonymous knowledge with contact info (required) found.');

    // Make sure the user data appears correctly when editing the knowledge.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('knowledge/' . $anonymous_knowledge3->id() . '/edit');
    $this->assertSession()->responseContains($author_name);
    // Check the author field is empty (i.e. anonymous) when editing
    // the knowledge.
    $this->assertSession()->fieldValueEquals('uid', '');

    // Unpublish knowledge.
    $this->performKnowledgeOperation($anonymous_knowledge3, 'unpublish');

    $this->drupalGet('admin/content/knowledge/approval');
    $this->assertSession()->responseContains('knowledge[' . $anonymous_knowledge3->id() . ']');

    // Publish knowledge.
    $this->performKnowledgeOperation($anonymous_knowledge3, 'publish', TRUE);

    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->responseContains('knowledge[' . $anonymous_knowledge3->id() . ']');

    // Delete knowledge.
    $this->performKnowledgeOperation($anonymous_knowledge3, 'delete');

    $this->drupalGet('admin/content/knowledge');
    $this->assertSession()->responseNotContains('knowledge[' . $anonymous_knowledge3->id() . ']');
    $this->drupalLogout();

    // Knowledge 3 was deleted.
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $anonymous_knowledge3->id());
    $this->assertSession()->statusCodeEquals(403);

    // Reset.
    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => FALSE,
      'post knowledge' => FALSE,
      'skip knowledge approval' => FALSE,
    ]);

    // Attempt to view knowledge while disallowed.
    // NOTE: if authenticated user has permission to post knowledge, then a
    // "Login or register to post knowledge" type link may be shown.
    $this->drupalGet('node/' . $this->node->id());
    // Verify that knowledge were not displayed.
    $this->assertSession()->responseNotMatches('@<h2[^>]*>Knowledge</h2>@');
    $this->assertSession()->linkNotExists('Add new knowledge', 'Link to add knowledge was found.');

    // Attempt to view node-knowledge form while disallowed.
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->assertSession()->statusCodeEquals(403);

    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => TRUE,
      'post knowledge' => FALSE,
      'skip knowledge approval' => FALSE,
    ]);
    $this->drupalGet('node/' . $this->node->id());
    // Verify that the knowledge field title is displayed.
    $this->assertSession()->responseMatches('@<h2[^>]*>Knowledge</h2>@');
    $this->assertSession()->linkExists('Log in', 1, 'Link to login was found.');
    $this->assertSession()->linkExists('register', 1, 'Link to register was found.');

    user_role_change_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge' => FALSE,
      'post knowledge' => TRUE,
      'skip knowledge approval' => TRUE,
    ]);
    $this->drupalGet('node/' . $this->node->id());
    // Verify that knowledge were not displayed.
    $this->assertSession()->responseNotMatches('@<h2[^>]*>Knowledge</h2>@');
    $this->assertSession()->fieldValueEquals('subject[0][value]', '');

    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $anonymous_knowledge2->id());
    $this->assertSession()->statusCodeEquals(403);
  }

}
