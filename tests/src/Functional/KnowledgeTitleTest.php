<?php

namespace Drupal\Tests\knowledge\Functional;

/**
 * Tests to ensure that appropriate and accessible markup is created for titles.
 *
 * @group knowledge
 */
class KnowledgeTitleTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests markup for knowledge with empty titles.
   */
  public function testKnowledgeEmptyTitles() {
    // Create a node.
    $this->drupalLogin($this->webUser);
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'uid' => $this->webUser->id(),
    ]);

    // Post knowledge #1 and verify that h3 is rendered.
    $subject_text = "Test subject";
    $knowledge_text = "Test knowledge";
    $this->postKnowledge($this->node, $knowledge_text, $subject_text, TRUE);
    // Tests that markup is generated for the knowledge title.
    $regex_h3 = '|<h3[^>]*>.*?</h3>|';
    $this->assertSession()->responseMatches($regex_h3);

    // Installs module that sets knowledge title to an empty string.
    \Drupal::service('module_installer')->install(['knowledge_empty_title_test']);

    // Set knowledge to have a subject with preview disabled.
    $this->setKnowledgePreview(DRUPAL_DISABLED);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);

    // Create a new node.
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'uid' => $this->webUser->id(),
    ]);

    // Post another knowledge and verify that h3 is not rendered.
    $subject_text = $this->randomMachineName();
    $knowledge_text = $this->randomMachineName();
    $knowledge = $this->postKnowledge($this->node, $knowledge_text, $subject_text, TRUE);

    // The entity fields for name and mail have no meaning if the user is not
    // Anonymous.
    $this->assertNull($knowledge->name->value);
    $this->assertNull($knowledge->mail->value);

    // Confirm that the knowledge was created.
    $regex = '/<article(.*?)id="knowledge-' . $knowledge->id() . '"(.*?)';
    $regex .= $knowledge->knowledge_body->value . '(.*?)';
    $regex .= '/s';
    // Verify that the knowledge is created successfully.
    $this->assertSession()->responseMatches($regex);
    // Tests that markup is not generated for the knowledge title.
    $this->assertSession()->responseNotMatches($regex_h3);
    $this->assertSession()->pageTextNotContains($subject_text);
  }

  /**
   * Tests markup for knowledge with populated titles.
   */
  public function testKnowledgePopulatedTitles() {
    // Set knowledge to have a subject with preview disabled.
    $this->setKnowledgePreview(DRUPAL_DISABLED);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);

    // Create a node.
    $this->drupalLogin($this->webUser);
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'uid' => $this->webUser->id(),
    ]);

    // Post knowledge #1 and verify that title is rendered in h3.
    $subject_text = $this->randomMachineName();
    $knowledge_text = $this->randomMachineName();
    $knowledge1 = $this->postKnowledge($this->node, $knowledge_text, $subject_text, TRUE);

    // The entity fields for name and mail have no meaning if the user is not
    // Anonymous.
    $this->assertNull($knowledge1->name->value);
    $this->assertNull($knowledge1->mail->value);

    // Confirm that the knowledge was created.
    $this->assertTrue($this->knowledgeExists($knowledge1), 'Knowledge #1. Knowledge found.');
    // Tests that markup is created for knowledge with heading.
    $this->assertSession()->responseMatches('|<h3[^>]*><a[^>]*>' . $subject_text . '</a></h3>|');
    // Tests that the knowledge's title link is the permalink of the knowledge.
    $knowledge_permalink = $this->cssSelect('.permalink');
    $knowledge_permalink = $knowledge_permalink[0]->getAttribute('href');
    // Tests that the knowledge's title link contains the url fragment.
    $this->assertStringContainsString('#knowledge-' . $knowledge1->id(), $knowledge_permalink, "The knowledge's title link contains the url fragment.");
    $this->assertEquals($knowledge1->permalink()->toString(), $knowledge_permalink, "The knowledge's title has the correct link.");
  }

}
