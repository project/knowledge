<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\user\Entity\User;

/**
 * Generates text using placeholders.
 *
 * Dummy content to check knowledge token replacement.
 *
 * @group knowledge
 */
class KnowledgeTokenReplaceTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['taxonomy'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Creates a knowledge, then tests the tokens generated from it.
   */
  public function testKnowledgeTokenReplacement() {
    $token_service = \Drupal::token();
    $language_interface = \Drupal::languageManager()->getCurrentLanguage();
    $url_options = [
      'absolute' => TRUE,
      'language' => $language_interface,
    ];

    // Setup vocabulary.
    Vocabulary::create([
      'vid' => 'tags',
      'name' => 'Tags',
    ])->save();

    // Change the title of the admin user.
    $this->adminUser->name->value = 'This is a title with some special & > " stuff.';
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);

    // Set knowledge variables.
    $this->setKnowledgeSubject(TRUE);

    // Create a node and a knowledge.
    $node = $this->drupalCreateNode([
      'type' => 'article',
      'title' => '<script>alert("123")</script>',
    ]);
    $parent_knowledge = $this->postKnowledge($node, $this->randomMachineName(), $this->randomMachineName(), TRUE);

    // Post a reply to the knowledge.
    $this->drupalGet('knowledge/reply/node/' . $node->id() . '/knowledge/' . $parent_knowledge->id());
    $child_knowledge = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName());
    $knowledge = Knowledge::load($child_knowledge->id());

    // Generate and test tokens.
    $tests = [];
    $tests['[knowledge:kid]'] = $knowledge->id();
    $tests['[knowledge:author]'] = Html::escape($knowledge->getAuthorName());
    $tests['[knowledge:mail]'] = $this->adminUser->getEmail();
    $tests['[knowledge:langcode]'] = $knowledge->language()->getId();
    $tests['[knowledge:url]'] = $knowledge->toUrl('canonical', $url_options + ['fragment' => 'knowledge-' . $knowledge->id()])->toString();
    $tests['[knowledge:edit-url]'] = $knowledge->toUrl('edit-form', $url_options)->toString();
    $tests['[knowledge:created]'] = \Drupal::service('date.formatter')->format($knowledge->getCreatedTime(), 'medium', ['langcode' => $language_interface->getId()]);
    $tests['[knowledge:created:since]'] = \Drupal::service('date.formatter')->formatTimeDiffSince($knowledge->getCreatedTime(), ['langcode' => $language_interface->getId()]);
    $tests['[knowledge:changed:since]'] = \Drupal::service('date.formatter')->formatTimeDiffSince($knowledge->getChangedTimeAcrossTranslations(), ['langcode' => $language_interface->getId()]);
    $tests['[knowledge:parent:kid]'] = NULL;
    $tests['[knowledge:parent:title]'] = $parent_knowledge->label();
    $tests['[knowledge:entity]'] = Html::escape($node->getTitle());
    // Test node specific tokens.
    $tests['[knowledge:entity:nid]'] = $knowledge->getKnowledgeedEntityId();
    $tests['[knowledge:entity:title]'] = Html::escape($node->getTitle());
    $tests['[knowledge:author:uid]'] = $knowledge->getOwnerId();
    $tests['[knowledge:author:name]'] = Html::escape($this->adminUser->getDisplayName());

    $base_bubbleable_metadata = BubbleableMetadata::createFromObject($knowledge);
    $metadata_tests = [];
    $metadata_tests['[knowledge:kid]'] = $base_bubbleable_metadata;
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $bubbleable_metadata->addCacheableDependency($this->adminUser);
    $metadata_tests['[knowledge:author]'] = $bubbleable_metadata;
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $bubbleable_metadata->addCacheableDependency($this->adminUser);
    $metadata_tests['[knowledge:mail]'] = $bubbleable_metadata;
    $metadata_tests['[knowledge:title]'] = $base_bubbleable_metadata;
    $metadata_tests['[knowledge:body]'] = $base_bubbleable_metadata;
    $metadata_tests['[knowledge:langcode]'] = $base_bubbleable_metadata;
    $metadata_tests['[knowledge:url]'] = $base_bubbleable_metadata;
    $metadata_tests['[knowledge:edit-url]'] = $base_bubbleable_metadata;
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[knowledge:created]'] = $bubbleable_metadata->addCacheTags(['rendered']);
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[knowledge:created:since]'] = $bubbleable_metadata->setCacheMaxAge(0);
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[knowledge:changed:since]'] = $bubbleable_metadata->setCacheMaxAge(0);
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[knowledge:parent:kid]'] = $bubbleable_metadata->addCacheTags(['knowledge:1']);
    $metadata_tests['[knowledge:parent:title]'] = $bubbleable_metadata;
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[knowledge:entity]'] = $bubbleable_metadata->addCacheTags(['node:2']);
    // Test node specific tokens.
    $metadata_tests['[knowledge:entity:nid]'] = $bubbleable_metadata;
    $metadata_tests['[knowledge:entity:title]'] = $bubbleable_metadata;
    $bubbleable_metadata = clone $base_bubbleable_metadata;
    $metadata_tests['[knowledge:author:uid]'] = $bubbleable_metadata->addCacheTags(['user:2']);
    $metadata_tests['[knowledge:author:name]'] = $bubbleable_metadata;

    // Test to make sure that we generated something for each token.
    $this->assertNotContains(0, array_map('strlen', $tests), 'No empty tokens generated.');

    foreach ($tests as $input => $expected) {
      $bubbleable_metadata = new BubbleableMetadata();
      $output = $token_service->replace($input, ['knowledge' => $knowledge], ['langcode' => $language_interface->getId()], $bubbleable_metadata);
      $this->assertEquals($expected, $output, new FormattableMarkup('Knowledge token %token replaced.', ['%token' => $input]));
      $this->assertEquals($metadata_tests[$input], $bubbleable_metadata);
    }

    // Add knowledge field to user and term entities.
    $this->addDefaultKnowledgeField('user', 'user', 'knowledge', KnowledgeItemInterface::OPEN, 'knowledge_user');
    $this->addDefaultKnowledgeField('taxonomy_term', 'tags', 'knowledge', KnowledgeItemInterface::OPEN, 'knowledge_term');

    // Create a user and a knowledge.
    $user = User::create(['name' => 'alice']);
    $user->activate();
    $user->save();
    $this->postKnowledge($user, 'user body', 'user subject', TRUE);

    // Create a term and a knowledge.
    $term = Term::create([
      'vid' => 'tags',
      'name' => 'term',
    ]);
    $term->save();
    $this->postKnowledge($term, 'term body', 'term subject', TRUE);

    // Load node, user and term again so total_count gets computed.
    $node = Node::load($node->id());
    $user = User::load($user->id());
    $term = Term::load($term->id());

    // Generate knowledge tokens for node (it has 2 knowledge, both new),
    // user and term.
    $tests = [];
    $tests['[entity:knowledge-count]'] = 2;
    $tests['[entity:knowledge-count-new]'] = 2;
    $tests['[node:knowledge-count]'] = 2;
    $tests['[node:knowledge-count-new]'] = 2;
    $tests['[user:knowledge-count]'] = 1;
    $tests['[user:knowledge-count-new]'] = 1;
    $tests['[term:knowledge-count]'] = 1;
    $tests['[term:knowledge-count-new]'] = 1;

    foreach ($tests as $input => $expected) {
      $output = $token_service->replace($input, [
        'entity' => $node,
        'node' => $node,
        'user' => $user,
        'term' => $term,
      ], ['langcode' => $language_interface->getId()]);
      $this->assertEquals($expected, $output, new FormattableMarkup('Knowledge token %token replaced.', ['%token' => $input]));
    }
  }

}
