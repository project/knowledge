<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\Core\Url;
use Drupal\filter\Entity\FilterFormat;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeManagerInterface;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\user\RoleInterface;

/**
 * Tests knowledge user interfaces.
 *
 * @group knowledge
 */
class KnowledgeInterfaceTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Set up knowledge to have subject and preview disabled.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->adminUser);
    // Make sure that knowledge field title is not displayed when there's no
    // knowledge posted.
    $this->drupalGet($this->node->toUrl());
    $this->assertSession()->responseNotMatches('@<h2[^>]*>Knowledges</h2>@');

    // Set knowledge to have subject and preview disabled.
    $this->setKnowledgePreview(DRUPAL_DISABLED);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(FALSE);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');
    $this->drupalLogout();
  }

  /**
   * Tests the knowledge interface.
   */
  public function testKnowledgeInterface() {

    // Post knowledge #1 without subject or preview.
    $this->drupalLogin($this->webUser);
    $knowledge_text = $this->randomMachineName();
    $knowledge = $this->postKnowledge($this->node, $knowledge_text);
    $this->assertTrue($this->knowledgeExists($knowledge), 'Knowledge found.');

    // Test that using an invalid entity-type does not raise an error.
    $this->drupalGet('knowledge/reply/yeah-this-is-not-an-entity-type/' . $this->node->id() . '/knowledge/' . $knowledge->id());
    $this->assertSession()->statusCodeEquals(404);

    // Test the knowledge field title is displayed when there's knowledge.
    $this->drupalGet($this->node->toUrl());
    $this->assertSession()->responseMatches('@<h2[^>]*>Knowledge</h2>@');

    // Set knowledge to have subject and preview to required.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgePreview(DRUPAL_REQUIRED);
    $this->drupalLogout();

    // Create knowledge #2 that allows subject and requires preview.
    $this->drupalLogin($this->webUser);
    $subject_text = $this->randomMachineName();
    $knowledge_text = $this->randomMachineName();
    $knowledge = $this->postKnowledge($this->node, $knowledge_text, $subject_text, TRUE);
    $this->assertTrue($this->knowledgeExists($knowledge), 'Knowledge found.');

    // Knowledge as anonymous with preview required.
    $this->drupalLogout();
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, [
      'access content',
      'access knowledge',
      'post knowledge',
      'skip knowledge approval',
    ]);
    $anonymous_knowledge = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $this->assertTrue($this->knowledgeExists($anonymous_knowledge), 'Knowledge found.');
    $anonymous_knowledge->delete();

    // Check knowledge display.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains($subject_text);
    $this->assertSession()->pageTextContains($knowledge_text);
    $arguments = [
      ':link' => base_path() . 'knowledge/' . $knowledge->id() . '#knowledge-' . $knowledge->id(),
    ];
    $pattern_permalink = '//footer/a[contains(@href,:link) and text()="Permalink"]';
    $permalink = $this->xpath($pattern_permalink, $arguments);
    $this->assertNotEmpty($permalink, 'Permalink link found.');

    // Set knowledge to have subject and preview to optional.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgePreview(DRUPAL_OPTIONAL);

    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->assertSession()->titleEquals('Edit knowledge ' . $knowledge->label() . ' | Drupal');

    // Test changing the knowledge author to "Anonymous".
    $knowledge = $this->postKnowledge(NULL, $knowledge->knowledge_body->value, $knowledge->label(), ['uid' => '']);
    $this->assertSame('Anonymous', $knowledge->getAuthorName());
    $this->assertEquals(0, $knowledge->getOwnerId());

    // Test changing the knowledge author to an unverified user.
    $random_name = $this->randomMachineName();
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $knowledge = $this->postKnowledge(NULL, $knowledge->knowledge_body->value, $knowledge->label(), ['name' => $random_name]);
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains($random_name . ' (not verified)');

    // Test changing the knowledge author to a verified user.
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $knowledge = $this->postKnowledge(NULL, $knowledge->knowledge_body->value, $knowledge->label(), ['uid' => $this->webUser->getAccountName() . ' (' . $this->webUser->id() . ')']);
    $this->assertSame($this->webUser->getAccountName(), $knowledge->getAuthorName());
    $this->assertSame($this->webUser->id(), $knowledge->getOwnerId());

    $this->drupalLogout();

    // Reply to knowledge #2 creating knowledge #3 with optional preview and no
    // subject though field enabled.
    $this->drupalLogin($this->webUser);
    // Deliberately use the wrong url to test
    // \Drupal\knowledge\Controller\KnowledgeController::redirectNode().
    $this->drupalGet('knowledge/' . $this->node->id() . '/reply');
    // Verify we were correctly redirected.
    $this->assertSession()->addressEquals(Url::fromRoute('knowledge.reply', [
      'entity_type' => 'node',
      'entity' => $this->node->id(),
      'field_name' => 'knowledge',
    ]));
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $knowledge->id());
    $this->assertSession()->pageTextContains($subject_text);
    $this->assertSession()->pageTextContains($knowledge_text);
    $reply = $this->postKnowledge(NULL, $this->randomMachineName(), '', TRUE);
    $reply_loaded = Knowledge::load($reply->id());
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'Reply found.');
    // Check the thread of reply grows correctly.
    // Second reply to knowledge #2 creating knowledge #4.
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $knowledge->id());
    $this->assertSession()->pageTextContains($knowledge->label());
    $this->assertSession()->pageTextContains($knowledge->knowledge_body->value);
    $reply = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $reply_loaded = Knowledge::load($reply->id());
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'Second reply found.');

    // Reply to knowledge #4 creating knowledge #5.
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $reply_loaded->id());
    $this->assertSession()->pageTextContains($reply_loaded->label());
    $this->assertSession()->pageTextContains($reply_loaded->knowledge_body->value);
    $reply = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $reply_loaded = Knowledge::load($reply->id());
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'Second reply found.');

    // Edit reply.
    $this->drupalGet('knowledge/' . $reply->id() . '/edit');
    $reply = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'Modified reply found.');

    // Confirm a new knowledge is posted to the correct page.
    $this->setKnowledgesPerPage(2);
    $knowledge_new_page = $this->postKnowledge($this->node, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $this->assertTrue($this->knowledgeExists($knowledge_new_page), 'Page one exists. %s');
    $this->drupalGet('node/' . $this->node->id(), ['query' => ['page' => 2]]);
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'Page two exists. %s');
    $this->setKnowledgesPerPage(50);

    // Attempt to reply to an unpublished knowledge.
    $reply_loaded->setUnpublished();
    $reply_loaded->save();
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $reply_loaded->id());
    $this->assertSession()->statusCodeEquals(403);

    // Attempt to post to node with knowledge disabled.
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'knowledge' => [['status' => KnowledgeItemInterface::HIDDEN]],
    ]);
    $this->assertNotNull($this->node, 'Article node created.');
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->fieldNotExists('edit-knowledge');

    // Attempt to post to node with read-only knowledge.
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'knowledge' => [['status' => KnowledgeItemInterface::CLOSED]],
    ]);
    $this->assertNotNull($this->node, 'Article node created.');
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->fieldNotExists('edit-knowledge');

    // Attempt to post to node with knowledge enabled (check field names etc).
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'promote' => 1,
      'knowledge' => [['status' => KnowledgeItemInterface::OPEN]],
    ]);
    $this->assertNotNull($this->node, 'Article node created.');
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge');
    $this->assertSession()->pageTextNotContains('This discussion is closed');
    // Ensure that the knowledge body field exists.
    $this->assertSession()->fieldExists('edit-knowledge-body-0-value');

    // Delete knowledge and make sure that reply is also removed.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->deleteKnowledge($knowledge);
    $this->deleteKnowledge($knowledge_new_page);

    $this->drupalGet('node/' . $this->node->id());
    $this->assertFalse($this->knowledgeExists($knowledge), 'Knowledge not found.');
    $this->assertFalse($this->knowledgeExists($reply, TRUE), 'Reply not found.');

    // Enabled knowledge form on node page.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeForm(TRUE);
    $this->drupalLogout();

    // Submit knowledge through node form.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $this->node->id());
    $form_knowledge = $this->postKnowledge(NULL, $this->randomMachineName(), $this->randomMachineName(), TRUE);
    $this->assertTrue($this->knowledgeExists($form_knowledge), 'Form knowledge found.');

    // Disable knowledge form on node page.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgeForm(FALSE);
  }

  /**
   * Tests that the subject is automatically filled if disabled or left blank.
   *
   * When the subject field is blank or disabled, the first 29 characters of the
   * knowledge body are used for the subject. If this would break within a word,
   * then the break is put at the previous word boundary instead.
   */
  public function testAutoFilledSubject() {
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $this->node->id());

    // Break when there is a word boundary before 29 characters.
    $body_text = 'Lorem ipsum Lorem ipsum Loreming ipsum Lorem ipsum';
    $knowledge1 = $this->postKnowledge(NULL, $body_text, '', TRUE);
    $this->assertTrue($this->knowledgeExists($knowledge1), 'Form knowledge found.');
    $this->assertEquals('Lorem ipsum Lorem ipsum…', $knowledge1->label());

    // Break at 29 characters where there's no boundary before that.
    $body_text2 = 'LoremipsumloremipsumLoremingipsumLoremipsum';
    $knowledge2 = $this->postKnowledge(NULL, $body_text2, '', TRUE);
    $this->assertEquals('LoremipsumloremipsumLoreming…', $knowledge2->label());
  }

  /**
   * Tests that automatic subject is correctly created from HTML knowledge text.
   *
   * This is the same test as in KnowledgeInterfaceTest::testAutoFilledSubject()
   * with the additional check that HTML is stripped appropriately prior to
   * character-counting.
   */
  public function testAutoFilledHtmlSubject() {
    // Set up two default (i.e. filtered HTML) input formats, because then we
    // can select one of them. Then create a user that can use these formats,
    // log the user in, and then GET the node page on which to test the
    // knowledge.
    $filtered_html_format = FilterFormat::create([
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
    ]);
    $filtered_html_format->save();
    $full_html_format = FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
    ]);
    $full_html_format->save();
    $html_user = $this->drupalCreateUser([
      'access knowledge',
      'post knowledge',
      'edit own knowledge',
      'skip knowledge approval',
      'access content',
      $filtered_html_format->getPermissionName(),
      $full_html_format->getPermissionName(),
    ]);
    $this->drupalLogin($html_user);
    $this->drupalGet('node/' . $this->node->id());

    // HTML should not be included in the character count.
    $body_text1 = '<span></span><strong> </strong><span> </span><strong></strong>Hello World<br />';
    $edit1 = [
      'knowledge_body[0][value]' => $body_text1,
      'knowledge_body[0][format]' => 'filtered_html',
    ];
    $this->submitForm($edit1, 'Save');
    $this->assertEquals('Hello World', Knowledge::load(1)->label());

    // If there's nothing other than HTML, the subject should be '(No subject)'.
    $body_text2 = '<span></span><strong> </strong><span> </span><strong></strong> <br />';
    $edit2 = [
      'knowledge_body[0][value]' => $body_text2,
      'knowledge_body[0][format]' => 'filtered_html',
    ];
    $this->submitForm($edit2, 'Save');
    $this->assertEquals('(No subject)', Knowledge::load(2)->label());
  }

  /**
   * Tests the knowledge formatter configured with a custom knowledge view mode.
   */
  public function testViewMode() {
    $this->drupalLogin($this->webUser);
    $this->drupalGet($this->node->toUrl());
    $knowledge_text = $this->randomMachineName();
    // Post a knowledge.
    $this->postKnowledge($this->node, $knowledge_text);

    // Knowledge displayed in 'default' display mode found and has body text.
    $knowledge_element = $this->cssSelect('#knowledge-1');
    $this->assertNotEmpty($knowledge_element);
    $this->assertSession()->responseContains('<p>' . $knowledge_text . '</p>');

    // Create a new knowledge entity view mode.
    $mode = mb_strtolower($this->randomMachineName());
    EntityViewMode::create([
      'targetEntityType' => 'knowledge',
      'id' => "knowledge.$mode",
    ])->save();
    // Create the corresponding entity view display for article node-type. Note
    // that this new view display mode doesn't contain the knowledge body.
    EntityViewDisplay::create([
      'targetEntityType' => 'knowledge',
      'bundle' => 'knowledge',
      'mode' => $mode,
    ])->setStatus(TRUE)->save();

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $node_display */
    $node_display = EntityViewDisplay::load('node.article.default');
    $formatter = $node_display->getComponent('knowledge');
    // Change the node knowledge field formatter to use $mode mode instead of
    // 'default' mode.
    $formatter['settings']['view_mode'] = $mode;
    $node_display
      ->setComponent('knowledge', $formatter)
      ->save();

    // Reloading the node page to show the same node with its same knowledge but
    // with a different display mode.
    $this->drupalGet($this->node->toUrl());
    // The knowledge should exist but without the body text because we used
    // $mode mode this time.
    $knowledge_element = $this->cssSelect('#knowledge-1');
    $this->assertNotEmpty($knowledge_element);
    $this->assertSession()->responseNotContains('<p>' . $knowledge_text . '</p>');
  }

}
