<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeManagerInterface;

/**
 * Tests knowledge preview.
 *
 * @group knowledge
 */
class KnowledgePreviewTest extends KnowledgeTestBase {

  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }

  /**
   * The profile to install as a basis for testing.
   *
   * Using the standard profile to test user picture display in knowledge.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Tests knowledge preview.
   */
  public function testKnowledgePreview() {
    // As admin user, configure knowledge settings.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgePreview(DRUPAL_OPTIONAL);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');
    $this->drupalLogout();

    // Log in as web user.
    $this->drupalLogin($this->webUser);

    // Test escaping of the username on the preview form.
    \Drupal::service('module_installer')->install(['user_hooks_test']);
    \Drupal::state()->set('user_hooks_test_user_format_name_alter', TRUE);
    $edit = [];
    $edit['subject[0][value]'] = $this->randomMachineName(8);
    $edit['knowledge_body[0][value]'] = $this->randomMachineName(16);
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm($edit, 'Preview');
    $this->assertSession()->assertEscaped('<em>' . $this->webUser->id() . '</em>');

    \Drupal::state()->set('user_hooks_test_user_format_name_alter_safe', TRUE);
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm($edit, 'Preview');
    $this->assertInstanceOf(MarkupInterface::class, $this->webUser->getDisplayName());
    $this->assertSession()->assertNoEscaped('<em>' . $this->webUser->id() . '</em>');
    $this->assertSession()->responseContains('<em>' . $this->webUser->id() . '</em>');

    // Add a user picture.
    $image = current($this->drupalGetTestFiles('image'));
    $user_edit['files[user_picture_0]'] = \Drupal::service('file_system')->realpath($image->uri);
    $this->drupalGet('user/' . $this->webUser->id() . '/edit');
    $this->submitForm($user_edit, 'Save');

    // As the web user, fill in the knowledge form and preview the knowledge.
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm($edit, 'Preview');

    // Check that the preview is displaying the title and body.
    $this->assertSession()->titleEquals('Preview knowledge | Drupal');
    $this->assertSession()->pageTextContains($edit['subject[0][value]']);
    $this->assertSession()->pageTextContains($edit['knowledge_body[0][value]']);

    // Check that the title and body fields are displayed with
    // the correct values.
    $this->assertSession()->fieldValueEquals('subject[0][value]', $edit['subject[0][value]']);
    $this->assertSession()->fieldValueEquals('knowledge_body[0][value]', $edit['knowledge_body[0][value]']);

    // Check that the user picture is displayed.
    $this->assertSession()->elementExists('xpath', "//article[contains(@class, 'preview')]//div[contains(@class, 'user-picture')]//img");

    // Ensure that preview node is displayed after the submit buttons of
    // the form.
    $xpath = $this->assertSession()->buildXPathQuery('//div[@id=:id]/following-sibling::article', [':id' => 'edit-actions']);
    $this->assertSession()->elementExists('xpath', $xpath);
  }

  /**
   * Tests knowledge preview.
   */
  public function testKnowledgePreviewDuplicateSubmission() {
    // As admin user, configure knowledge settings.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgePreview(DRUPAL_OPTIONAL);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');
    $this->drupalLogout();

    // Log in as web user.
    $this->drupalLogin($this->webUser);

    // As the web user, fill in the knowledge form and preview the knowledge.
    $edit = [];
    $edit['subject[0][value]'] = $this->randomMachineName(8);
    $edit['knowledge_body[0][value]'] = $this->randomMachineName(16);
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm($edit, 'Preview');

    // Check that the preview is displaying the title and body.
    $this->assertSession()->titleEquals('Preview knowledge | Drupal');
    $this->assertSession()->pageTextContains($edit['subject[0][value]']);
    $this->assertSession()->pageTextContains($edit['knowledge_body[0][value]']);

    // Check that the title and body fields are displayed with the
    // correct values.
    $this->assertSession()->fieldValueEquals('subject[0][value]', $edit['subject[0][value]']);
    $this->assertSession()->fieldValueEquals('knowledge_body[0][value]', $edit['knowledge_body[0][value]']);

    // Store the content of this page.
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('Your knowledge has been posted.');
    $elements = $this->xpath('//section[contains(@class, "knowledge")]/article');
    $this->assertCount(1, $elements);

    // Go back and re-submit the form.
    $this->getSession()->getDriver()->back();
    $submit_button = $this->assertSession()->buttonExists('Save');
    $submit_button->click();
    $this->assertSession()->pageTextContains('Your knowledge has been posted.');
    $this->assertSession()->elementsCount('xpath', '//section[contains(@class, "knowledge")]/article', 2);
  }

  /**
   * Tests knowledge edit, preview, and save.
   */
  public function testKnowledgeEditPreviewSave() {
    $web_user = $this->drupalCreateUser([
      'access knowledge',
      'post knowledge',
      'skip knowledge approval',
      'edit own knowledge',
    ]);
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgePreview(DRUPAL_OPTIONAL);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');

    $edit = [];
    $date = new DrupalDateTime('2008-03-02 17:23');
    $edit['subject[0][value]'] = $this->randomMachineName(8);
    $edit['knowledge_body[0][value]'] = $this->randomMachineName(16);
    $edit['uid'] = $web_user->getAccountName() . ' (' . $web_user->id() . ')';
    $edit['date[date]'] = $date->format('Y-m-d');
    $edit['date[time]'] = $date->format('H:i:s');
    $raw_date = $date->getTimestamp();
    $expected_text_date = $this->container->get('date.formatter')->formatInterval(\Drupal::time()->getRequestTime() - $raw_date);
    $expected_form_date = $date->format('Y-m-d');
    $expected_form_time = $date->format('H:i:s');
    $knowledge = $this->postKnowledge($this->node, $edit['subject[0][value]'], $edit['knowledge_body[0][value]'], TRUE);
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->submitForm($edit, 'Preview');

    // Check that the preview is displaying the subject, knowledge, author
    // and date correctly.
    $this->assertSession()->titleEquals('Preview knowledge | Drupal');
    $this->assertSession()->pageTextContains($edit['subject[0][value]']);
    $this->assertSession()->pageTextContains($edit['knowledge_body[0][value]']);
    $this->assertSession()->pageTextContains($web_user->getAccountName());
    $this->assertSession()->pageTextContains($expected_text_date);

    // Check that the subject, knowledge, author and date fields are displayed
    // with the correct values.
    $this->assertSession()->fieldValueEquals('subject[0][value]', $edit['subject[0][value]']);
    $this->assertSession()->fieldValueEquals('knowledge_body[0][value]', $edit['knowledge_body[0][value]']);
    $this->assertSession()->fieldValueEquals('uid', $edit['uid']);
    $this->assertSession()->fieldValueEquals('date[date]', $edit['date[date]']);
    $this->assertSession()->fieldValueEquals('date[time]', $edit['date[time]']);

    // Check that saving a knowledge produces a success message.
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Your knowledge has been posted.');

    // Check that the knowledge fields are correct after loading
    // the saved knowledge.
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->assertSession()->fieldValueEquals('subject[0][value]', $edit['subject[0][value]']);
    $this->assertSession()->fieldValueEquals('knowledge_body[0][value]', $edit['knowledge_body[0][value]']);
    $this->assertSession()->fieldValueEquals('uid', $edit['uid']);
    $this->assertSession()->fieldValueEquals('date[date]', $expected_form_date);
    $this->assertSession()->fieldValueEquals('date[time]', $expected_form_time);

    // Submit the form using the displayed values.
    $displayed = [];
    $displayed['subject[0][value]'] = $this->assertSession()->fieldExists('edit-subject-0-value')->getValue();
    $displayed['knowledge_body[0][value]'] = $this->assertSession()->fieldExists('edit-knowledge-body-0-value')->getValue();
    $displayed['uid'] = $this->assertSession()->fieldExists('edit-uid')->getValue();
    $displayed['date[date]'] = $this->assertSession()->fieldExists('edit-date-date')->getValue();
    $displayed['date[time]'] = $this->assertSession()->fieldExists('edit-date-time')->getValue();
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->submitForm($displayed, 'Save');

    // Check that the saved knowledge is still correct.
    $knowledge_storage = \Drupal::entityTypeManager()->getStorage('knowledge');
    $knowledge_storage->resetCache([$knowledge->id()]);
    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge_loaded */
    $knowledge_loaded = Knowledge::load($knowledge->id());
    $this->assertEquals($edit['subject[0][value]'], $knowledge_loaded->label(), 'Subject loaded.');
    $this->assertEquals($edit['knowledge_body[0][value]'], $knowledge_loaded->knowledge_body->value, 'Knowledge body loaded.');
    $this->assertEquals($web_user->id(), $knowledge_loaded->getOwner()->id(), 'Name loaded.');
    $this->assertEquals($raw_date, $knowledge_loaded->getCreatedTime(), 'Date loaded.');
    $this->drupalLogout();

    // Check that the date and time of the knowledge are correct when edited by
    // non-admin users.
    $user_edit = [];
    $expected_created_time = $knowledge_loaded->getCreatedTime();
    $this->drupalLogin($web_user);
    // Web user cannot change the knowledge author.
    unset($edit['uid']);
    $this->drupalGet('knowledge/' . $knowledge->id() . '/edit');
    $this->submitForm($user_edit, 'Save');
    $knowledge_storage->resetCache([$knowledge->id()]);
    $knowledge_loaded = Knowledge::load($knowledge->id());
    $this->assertEquals($expected_created_time, $knowledge_loaded->getCreatedTime(), 'Expected date and time for knowledge edited.');
    $this->drupalLogout();
  }

}
