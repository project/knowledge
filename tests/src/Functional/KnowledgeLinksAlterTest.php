<?php

namespace Drupal\Tests\knowledge\Functional;

/**
 * Tests knowledge links altering.
 *
 * @group knowledge
 */
class KnowledgeLinksAlterTest extends KnowledgeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['knowledge_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable knowledge_test.module's
    // hook_knowledge_links_alter() implementation.
    $this->container->get('state')->set('knowledge_test_links_alter_enabled', TRUE);
  }

  /**
   * Tests knowledge links altering.
   */
  public function testKnowledgeLinksAlter() {
    $this->drupalLogin($this->webUser);
    $knowledge_text = $this->randomMachineName();
    $subject = $this->randomMachineName();
    $this->postKnowledge($this->node, $knowledge_text, $subject);

    $this->drupalGet('node/' . $this->node->id());

    $this->assertSession()->linkExists('Report');
  }

}
