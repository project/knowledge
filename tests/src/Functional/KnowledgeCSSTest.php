<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\Traits\Core\GeneratePermutationsTrait;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\user\RoleInterface;

/**
 * Tests CSS classes on knowledge.
 *
 * @group knowledge
 */
class KnowledgeCSSTest extends KnowledgeTestBase {

  use GeneratePermutationsTrait;

  /**
   * The theme to install as the default for testing.
   *
   * @var string
   *
   * @todo This test's reliance on classes makes Stark a bad fit as a base
   *   theme. Change the default theme to Starterkit once it is stable.
   *
   * @see https://www.drupal.org/project/drupal/issues/3267890
   */
  protected $defaultTheme = 'stable9';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Allow anonymous users to see knowledge.
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, [
      'access knowledge',
      'access content',
    ]);
  }

  /**
   * Tests CSS classes on knowledge.
   */
  public function testKnowledgeClasses() {
    // Create all permutations for knowledge, users, and nodes.
    $parameters = [
      'node_uid' => [0, $this->webUser->id()],
      'knowledge_uid' => [0, $this->webUser->id(), $this->adminUser->id()],
      'knowledge_status' => [
        KnowledgeInterface::PUBLISHED,
        KnowledgeInterface::NOT_PUBLISHED,
      ],
      'user' => ['anonymous', 'authenticated', 'admin'],
    ];
    $permutations = $this->generatePermutations($parameters);

    foreach ($permutations as $case) {
      // Create a new node.
      $node = $this->drupalCreateNode([
        'type' => 'article',
        'uid' => $case['node_uid'],
      ]);

      // Add a knowledge.
      /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
      $knowledge = Knowledge::create([
        'entity_id' => $node->id(),
        'entity_type' => 'node',
        'field_name' => 'knowledge',
        'uid' => $case['knowledge_uid'],
        'status' => $case['knowledge_status'],
        'subject' => $this->randomMachineName(),
        'language' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      ]);
      $knowledge->save();

      // Adjust the current/viewing user.
      switch ($case['user']) {
        case 'anonymous':
          if ($this->loggedInUser) {
            $this->drupalLogout();
          }
          $case['user_uid'] = 0;
          break;

        case 'authenticated':
          $this->drupalLogin($this->webUser);
          $case['user_uid'] = $this->webUser->id();
          break;

        case 'admin':
          $this->drupalLogin($this->adminUser);
          $case['user_uid'] = $this->adminUser->id();
          break;
      }
      // Request the node with the knowledge.
      $this->drupalGet('node/' . $node->id());
      $settings = $this->getDrupalSettings();

      // Verify the data-history-node-id attribute, which is necessary for the
      // by-viewer class and the "new" indicator, see below.
      $this->assertCount(1, $this->xpath('//*[@data-history-node-id="' . $node->id() . '"]'), 'data-history-node-id attribute is set on node.');

      // Verify classes if the knowledge is visible for the current user.
      if ($case['knowledge_status'] == KnowledgeInterface::PUBLISHED || $case['user'] == 'admin') {
        // Verify the by-anonymous class.
        $knowledge_links = $this->xpath('//*[contains(@class, "knowledge") and contains(@class, "by-anonymous")]');
        if ($case['knowledge_uid'] == 0) {
          $this->assertCount(1, $knowledge_links, 'by-anonymous class found.');
        }
        else {
          $this->assertCount(0, $knowledge_links, 'by-anonymous class not found.');
        }

        // Verify the by-node-author class.
        $knowledge_links = $this->xpath('//*[contains(@class, "knowledge") and contains(@class, "by-node-author")]');
        if ($case['knowledge_uid'] > 0 && $case['knowledge_uid'] == $case['node_uid']) {
          $this->assertCount(1, $knowledge_links, 'by-node-author class found.');
        }
        else {
          $this->assertCount(0, $knowledge_links, 'by-node-author class not found.');
        }

        // Verify the data-knowledge-user-id attribute, which is used by the
        // drupal.knowledge-by-viewer library to add a by-viewer when the
        // current user (the viewer) was the author of the knowledge. We do this
        // in JavaScript to prevent breaking the render cache.
        $this->assertCount(1, $this->xpath('//*[contains(@class, "knowledge") and @data-knowledge-user-id="' . $case['knowledge_uid'] . '"]'), 'data-knowledge-user-id attribute is set on knowledge.');
        $this->assertSession()->responseContains($this->getModulePath('knowledge') . '/js/knowledge-by-viewer.js');
      }

      // Verify the unpublished class.
      $knowledge_links = $this->xpath('//*[contains(@class, "knowledge") and contains(@class, "unpublished")]');
      if ($case['knowledge_status'] == KnowledgeInterface::NOT_PUBLISHED && $case['user'] == 'admin') {
        $this->assertCount(1, $knowledge_links, 'unpublished class found.');
      }
      else {
        $this->assertCount(0, $knowledge_links, 'unpublished class not found.');
      }

      // Verify the data-knowledge-timestamp attribute, which is used by the
      // drupal.knowledge-new-indicator library to add a "new" indicator to each
      // knowledge that was created or changed after the last time the current
      // user read the corresponding node.
      if ($case['knowledge_status'] == KnowledgeInterface::PUBLISHED || $case['user'] == 'admin') {
        $this->assertCount(1, $this->xpath('//*[contains(@class, "knowledge")]/*[@data-knowledge-timestamp="' . $knowledge->getChangedTime() . '"]'), 'data-knowledge-timestamp attribute is set on knowledge');
        $expectedJS = ($case['user'] !== 'anonymous');
        $this->assertSame($expectedJS, isset($settings['ajaxPageState']['libraries']) && in_array('knowledge/drupal.knowledge-new-indicator', explode(',', $settings['ajaxPageState']['libraries'])), 'drupal.knowledge-new-indicator library is present.');
      }
    }
  }

}
