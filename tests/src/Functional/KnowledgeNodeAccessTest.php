<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\knowledge\KnowledgeManagerInterface;

/**
 * Tests knowledge with node access.
 *
 * Verifies there is no PostgreSQL error when viewing a node with threaded
 * knowledge (a knowledge and a reply), if a node access module is in use.
 *
 * @group knowledge
 */
class KnowledgeNodeAccessTest extends KnowledgeTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node_access_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    node_access_rebuild();

    // Re-create user.
    $this->webUser = $this->drupalCreateUser([
      'access knowledge',
      'post knowledge',
      'create article content',
      'edit own knowledge',
      'node test view',
      'skip knowledge approval',
    ]);

    // Set the author of the created node to the web_user uid.
    $this->node->setOwnerId($this->webUser->id())->save();
  }

  /**
   * Tests that threaded knowledge can be viewed.
   */
  public function testThreadedKnowledgeView() {
    // Set knowledge to have subject required and preview disabled.
    $this->drupalLogin($this->adminUser);
    $this->setKnowledgePreview(DRUPAL_DISABLED);
    $this->setKnowledgeForm(TRUE);
    $this->setKnowledgeSubject(TRUE);
    $this->setKnowledgeSettings('default_mode', KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED, 'Knowledge paging changed.');
    $this->drupalLogout();

    // Post knowledge.
    $this->drupalLogin($this->webUser);
    $knowledge_text = $this->randomMachineName();
    $knowledge_subject = $this->randomMachineName();
    $knowledge = $this->postKnowledge($this->node, $knowledge_text, $knowledge_subject);
    $this->assertTrue($this->knowledgeExists($knowledge), 'Knowledge found.');

    // Check knowledge display.
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains($knowledge_subject);
    $this->assertSession()->pageTextContains($knowledge_text);

    // Reply to knowledge, creating second knowledge.
    $this->drupalGet('knowledge/reply/node/' . $this->node->id() . '/knowledge/' . $knowledge->id());
    $reply_text = $this->randomMachineName();
    $reply_subject = $this->randomMachineName();
    $reply = $this->postKnowledge(NULL, $reply_text, $reply_subject, TRUE);
    $this->assertTrue($this->knowledgeExists($reply, TRUE), 'Reply found.');

    // Go to the node page and verify knowledge and reply are visible.
    $this->drupalGet('node/' . $this->node->id());
    $this->assertSession()->pageTextContains($knowledge_text);
    $this->assertSession()->pageTextContains($knowledge_subject);
    $this->assertSession()->pageTextContains($reply_text);
    $this->assertSession()->pageTextContains($reply_subject);
  }

}
