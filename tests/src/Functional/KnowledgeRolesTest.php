<?php

namespace Drupal\Tests\knowledge\Functional;

/**
 * Tests knowledge statistics on nodes.
 *
 * @group knowledge
 */
class KnowledgeRolesTest extends KnowledgeTestBase {

  /**
   * A secondary user for posting knowledge.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser2;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a second user to post knowledge.
    $this->webUser2 = $this->drupalCreateUser([
      'post knowledge',
      'create article content',
      'edit own knowledge',
      'post knowledge',
      'skip knowledge approval',
      'access knowledge',
      'access content',
    ]);
  }

  /**
   * Tests the node knowledge statistics.
   */
  public function testKnowledgeRoles() {
    $this->assertTrue(TRUE);

  }

}
