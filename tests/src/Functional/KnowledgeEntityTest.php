<?php

namespace Drupal\Tests\knowledge\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;

/**
 * Tests knowledge with other entities.
 *
 * @group knowledge
 */
class KnowledgeEntityTest extends KnowledgeTestBase {

  use TaxonomyTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'block',
    'knowledge',
    'node',
    'history',
    'field_ui',
    'datetime',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A taxonomy vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocab;

  /**
   * The knowledge type to tests.
   *
   * @var \Drupal\knowledge\Entity\KnowledgeType
   */
  protected $knowledgeType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->vocab = $this->createVocabulary();
    $this->knowledgeType = KnowledgeType::create([
      'id' => 'taxonomy_knowledge',
      'label' => 'Taxonomy knowledge',
      'description' => '',
      'target_entity_type_id' => 'taxonomy_term',
    ]);
    $this->knowledgeType->save();
    $this->addDefaultKnowledgeField(
      'taxonomy_term',
      $this->vocab->id(),
      'field_knowledge',
      KnowledgeItemInterface::OPEN,
      $this->knowledgeType->id()
    );
  }

  /**
   * Tests CSS classes on knowledge.
   */
  public function testEntityChanges() {
    $this->drupalLogin($this->webUser);
    // Create a new node.
    $term = $this->createTerm($this->vocab, ['uid' => $this->webUser->id()]);

    // Add a knowledge.
    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = Knowledge::create([
      'entity_id' => $term->id(),
      'entity_type' => 'taxonomy_term',
      'field_name' => 'field_knowledge',
      'uid' => $this->webUser->id(),
      'status' => KnowledgeInterface::PUBLISHED,
      'subject' => $this->randomMachineName(),
      'language' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $knowledge->save();

    // Request the node with the knowledge.
    $this->drupalGet('taxonomy/term/' . $term->id());
    $settings = $this->getDrupalSettings();
    $this->assertFalse(isset($settings['ajaxPageState']['libraries']) && in_array('knowledge/drupal.knowledge-new-indicator', explode(',', $settings['ajaxPageState']['libraries'])), 'drupal.knowledge-new-indicator library is present.');
    $this->assertFalse(isset($settings['history']['lastReadTimestamps']) && in_array($term->id(), array_keys($settings['history']['lastReadTimestamps'])), 'history.lastReadTimestamps is present.');
  }

}
