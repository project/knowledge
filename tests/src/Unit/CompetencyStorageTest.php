<?php

declare(strict_types=1);

namespace Drupal\Tests\knowledge\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\CompetencyStorage;
use Drupal\knowledge\KnowledgeCompetencyInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @coversDefaultClass \Drupal\knowledge\CompetencyStorage
 * @group knowledge
 */
class CompetencyStorageTest extends UnitTestCase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityFieldManager;

  /**
   * The competency storage.
   *
   * @var \Drupal\knowledge\CompetencyStorage
   */
  protected $competencyStorage;

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityType;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $database;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $cache;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $languageManager;

  /**
   * The memory cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $memoryCache;

  /**
   * The entity type bundle manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeBundleManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->container = new ContainerBuilder();

    // $this->stringTranslation = $this->getStringTranslationStub();
    // $this->container->set('string_translation', $this->stringTranslation);
    $this->entityTypeManager = $this->createMock('\Drupal\Core\Entity\EntityTypeManager');
    $this->container->set('entity_type.manager', $this->entityTypeManager);

    $this->entityFieldManager = $this->createMock('\Drupal\Core\Entity\EntityFieldManager');
    $this->container->set('entity_field.manager', $this->entityFieldManager);

    $this->database = $this->createMock('\Drupal\Core\Database\Connection');
    $this->container->set('database', $this->database);

    $this->cache = $this->createMock('\Drupal\Core\Cache\CacheBackendInterface');
    $this->languageManager = $this->createMock('\Drupal\Core\Language\LanguageManager');
    $this->memoryCache = $this->createMock('\Drupal\Core\Cache\MemoryCache\MemoryCache');
    $this->entityTypeBundleManager = $this->createMock('\Drupal\Core\Entity\EntityTypeBundleInfo');

    \Drupal::setContainer($this->container);

    $this->entityType = $this->createMock('\Drupal\Core\Entity\EntityTypeInterface');

  }

  /**
   * @covers ::revisionIds
   */
  public function testRevisionIds() {
    $entity = $this->createMock(KnowledgeCompetencyInterface::class);
    $entity->expects($this->once())
      ->method('id')
      ->willReturn(123);
    $this->entityType->expects($this->exactly(3))
      ->method('id')
      ->willReturn('knowledge_competency');

    $entity_type = $this->createMock('\Drupal\Core\Entity\ContentEntityTypeInterface');
    $this->entityTypeManager->expects($this->once())
      ->method('getActiveDefinition')
      ->with('knowledge_competency')
      ->willReturn($entity_type);

    $this->entityFieldManager->expects($this->once())
      ->method('getActiveFieldStorageDefinitions')
      ->with('knowledge_competency')
      ->willReturn([]);

    $select_statement = $this->createMock('\Drupal\Core\Database\StatementInterface');
    $this->database->expects($this->once())
      ->method('query')
      ->with('SELECT vid FROM {knowledge_competency_revision} WHERE id=:id ORDER BY vid', [':id' => 123])
      ->willReturn($select_statement);

    $competencyStorage = new CompetencyStorage(
      $this->entityType,
      $this->database,
      $this->entityFieldManager,
      $this->cache,
      $this->languageManager,
      $this->memoryCache,
      $this->entityTypeBundleManager,
      $this->entityTypeManager,
    );
    $competencyStorage->revisionIds($entity);
  }

  /**
   * @covers ::userRevisionIds
   */
  public function testUserRevisionIds() {
    $account = $this->createMock('\Drupal\Core\Session\AccountInterface');
    $account->expects($this->once())
      ->method('id')
      ->willReturn(123);
    $this->entityType->expects($this->exactly(3))
      ->method('id')
      ->willReturn('knowledge_competency');

    $entity_type = $this->createMock('\Drupal\Core\Entity\ContentEntityTypeInterface');
    $this->entityTypeManager->expects($this->once())
      ->method('getActiveDefinition')
      ->with('knowledge_competency')
      ->willReturn($entity_type);

    $this->entityFieldManager->expects($this->once())
      ->method('getActiveFieldStorageDefinitions')
      ->with('knowledge_competency')
      ->willReturn([]);

    $select_statement = $this->createMock('\Drupal\Core\Database\StatementInterface');
    $this->database->expects($this->once())
      ->method('query')
      ->with('SELECT vid FROM {knowledge_competency_field_revision} WHERE uid = :uid ORDER BY vid', [':uid' => 123])
      ->willReturn($select_statement);

    $competencyStorage = new CompetencyStorage(
      $this->entityType,
      $this->database,
      $this->entityFieldManager,
      $this->cache,
      $this->languageManager,
      $this->memoryCache,
      $this->entityTypeBundleManager,
      $this->entityTypeManager,
    );
    $competencyStorage->userRevisionIds($account);
  }

}
