<?php

declare(strict_types=1);

namespace Drupal\Tests\knowledge\Unit;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\CompetencyAccessControlHandler;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tests the Competency access control handler.
 *
 * @coversDefaultClass \Drupal\knowledge\CompetencyAccessControlHandler
 *   CompetencyAccessControlHandler
 * @group knowledge
 */
class CompetencyAccessControlHandlerTest extends UnitTestCase {

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityType;

  /**
   * The competency access control handler.
   *
   * @var \Drupal\knowledge\CompetencyAccessControlHandler
   */
  protected $accessHandler;

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * The cache contexts manager.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $cacheContextsManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityType = $this->createMock('Drupal\Core\Entity\EntityTypeInterface');

    $this->container = new ContainerBuilder();

    $this->moduleHandler = $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface');
    $this->container->set('module_handler', $this->moduleHandler);

    // cache_contexts_manager.
    $this->cacheContextsManager = $this->createMock('Drupal\Core\Cache\Context\CacheContextsManager');
    $this->container->set('cache_contexts_manager', $this->cacheContextsManager);

    \Drupal::setContainer($this->container);

    $this->accessHandler = new CompetencyAccessControlHandler($this->entityType);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessViewOwn(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $field_item->expects($this->exactly(2))
      ->method('__get')
      ->with('target_id')
      ->willReturn(1);

    $owner = $this->createMock(UserInterface::class);
    $owner->expects($this->exactly(2))
      ->method('get')
      ->willReturnMap([
        ['knowledge_coach', $field_item],
        ['knowledge_leader', $field_item],
      ]);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(13);
    $account->method('hasPermission')
      ->willReturnMap([
        ['view own knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $competency->expects($this->once())
      ->method('getOwnerId')
      ->willReturn(13);
    $competency->expects($this->exactly(2))
      ->method('getOwner')
      ->willReturn($owner);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'view', $account], []],
        ['knowledge_competency_access', [$competency, 'view', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'view', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessViewAny(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $field_item->expects($this->exactly(2))
      ->method('__get')
      ->with('target_id')
      ->willReturn(15);

    $owner = $this->createMock(UserInterface::class);
    $owner->expects($this->exactly(2))
      ->method('get')
      ->willReturnMap([
        ['knowledge_coach', $field_item],
        ['knowledge_leader', $field_item],
      ]);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(10);
    $account->method('hasPermission')
      ->willReturnMap([
        ['view any knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $competency->expects($this->once())
      ->method('getOwnerId')
      ->willReturn(1);
    $competency->expects($this->exactly(2))
      ->method('getOwner')
      ->willReturn($owner);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'view', $account], []],
        ['knowledge_competency_access', [$competency, 'view', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'view', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessViewLearner(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $field_item->expects($this->once())
      ->method('__get')
      ->with('target_id')
      ->willReturn(10);

    $coach_field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $coach_field_item->expects($this->once())
      ->method('__get')
      ->with('target_id')
      ->willReturn(3);

    $owner = $this->createMock(UserInterface::class);
    $owner->expects($this->exactly(2))
      ->method('get')
      ->willReturnMap([
        ['knowledge_coach', $coach_field_item],
        ['knowledge_leader', $field_item],
      ]);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(3);
    $account->method('hasPermission')
      ->willReturnMap([
        ['view learner knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $competency->expects($this->once())
      ->method('getOwnerId')
      ->willReturn(1);
    $competency->expects($this->exactly(2))
      ->method('getOwner')
      ->willReturn($owner);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'view', $account], []],
        ['knowledge_competency_access', [$competency, 'view', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'view', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessViewFollower(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $field_item->expects($this->once())
      ->method('__get')
      ->with('target_id')
      ->willReturn(1);

    $coach_field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $coach_field_item->expects($this->once())
      ->method('__get')
      ->with('target_id')
      ->willReturn(10);

    $owner = $this->createMock(UserInterface::class);
    $owner->expects($this->exactly(2))
      ->method('get')
      ->willReturnMap([
        ['knowledge_coach', $coach_field_item],
        ['knowledge_leader', $field_item],
      ]);

    $account = $this->createMock(AccountInterface::class);
    $account->expects($this->exactly(5))
      ->method('id')
      ->willReturn(1);
    $account->method('hasPermission')
      ->willReturnMap([
        ['view follower knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $competency->expects($this->once())
      ->method('getOwnerId')
      ->willReturn(9);
    $competency->expects($this->exactly(2))
      ->method('getOwner')
      ->willReturn($owner);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'view', $account], []],
        ['knowledge_competency_access', [$competency, 'view', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'view', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessDelete(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(1);
    $account->method('hasPermission')
      ->willReturnMap([
        ['administer knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'delete', $account], []],
        ['knowledge_competency_access', [$competency, 'delete', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'delete', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testUnknownOperation(): void {
    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(1);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'unknown', $account], []],
        ['knowledge_competency_access', [$competency, 'unknown', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'unknown', $account);

    $this->assertNotNull($result);
    $this->assertFalse($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessUpdate(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $field_item->expects($this->once())
      ->method('__get')
      ->with('target_id')
      ->willReturn(1);

    $owner = $this->createMock(UserInterface::class);
    $owner->expects($this->once())
      ->method('id')
      ->willReturn(3);
    $owner->expects($this->once())
      ->method('get')
      ->willReturnMap([
        ['knowledge_coach', $field_item],
      ]);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(1);
    $account->method('hasPermission')
      ->willReturnMap([
        ['edit other knowledge_competency', TRUE],
        ['edit learner knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $competency->expects($this->once())
      ->method('getOwner')
      ->willReturn($owner);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'update', $account], []],
        ['knowledge_competency_access', [$competency, 'update', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'update', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkAccess
   */
  public function testCheckAccessUpdateOwn(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $field_item = $this->createMock('Drupal\Core\Field\FieldItemListInterface');
    $field_item->expects($this->once())
      ->method('__get')
      ->with('target_id')
      ->willReturn(1);

    $owner = $this->createMock(UserInterface::class);
    $owner->expects($this->once())
      ->method('id')
      ->willReturn(10);
    $owner->expects($this->once())
      ->method('get')
      ->willReturnMap([
        ['knowledge_coach', $field_item],
      ]);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(10);
    $account->method('hasPermission')
      ->willReturnMap([
        ['edit own knowledge_competency', TRUE],
      ]);

    $language = $this->createMock('Drupal\Core\Language\LanguageInterface');
    $language->expects($this->once())
      ->method('getId')
      ->willReturn('en');

    $competency = $this->createMock('Drupal\knowledge\KnowledgeCompetencyInterface');
    $competency->expects($this->once())
      ->method('language')
      ->willReturn($language);
    $competency->expects($this->exactly(2))
      ->method('getEntityTypeId')
      ->willReturn('knowledge_competency');

    $competency->expects($this->once())
      ->method('getOwner')
      ->willReturn($owner);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturnMap([
        ['entity_access', [$competency, 'update', $account], []],
        ['knowledge_competency_access', [$competency, 'update', $account], []],
      ]);

    $result = $this->accessHandler->access($competency, 'update', $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

  /**
   * @covers ::checkCreateAccess
   */
  public function testCreateAccess(): void {
    $this->cacheContextsManager->expects($this->exactly(2))
      ->method('assertValidTokens')
      ->with(['user.permissions'])
      ->willReturn(TRUE);

    $account = $this->createMock(AccountInterface::class);
    $account->method('id')
      ->willReturn(1);
    $account->method('hasPermission')
      ->willReturnMap([
        ['add knowledge_competency', TRUE],
      ]);

    $this->moduleHandler->expects($this->exactly(2))
      ->method('invokeAll')
      ->willReturn([]);

    $result = $this->accessHandler->createAccess(NULL, $account);

    $this->assertNotNull($result);
    $this->assertTrue($result);
  }

}
