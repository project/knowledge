<?php

namespace Drupal\Tests\knowledge\Unit\Plugin\views\field;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\Plugin\views\field\KnowledgeBulkForm;

/**
 * @coversDefaultClass \Drupal\knowledge\Plugin\views\field\KnowledgeBulkForm
 * @group knowledge
 */
class KnowledgeBulkFormTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    parent::tearDown();
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
  }

  /**
   * Tests the constructor assignment of actions.
   */
  public function testConstructor() {
    $actions = [];

    for ($i = 1; $i <= 2; $i++) {
      $action = $this->createMock('\Drupal\system\ActionConfigEntityInterface');
      $action->expects($this->any())
        ->method('getType')
        ->will($this->returnValue('knowledge'));
      $actions[$i] = $action;
    }

    $action = $this->createMock('\Drupal\system\ActionConfigEntityInterface');
    $action->expects($this->any())
      ->method('getType')
      ->will($this->returnValue('user'));
    $actions[] = $action;

    $entity_storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $entity_storage->expects($this->any())
      ->method('loadMultiple')
      ->will($this->returnValue($actions));

    $entity_type_manager = $this->createMock(EntityTypeManagerInterface::class);
    $entity_type_manager->expects($this->once())
      ->method('getStorage')
      ->with('action')
      ->will($this->returnValue($entity_storage));

    $entity_repository = $this->createMock(EntityRepositoryInterface::class);

    $language_manager = $this->createMock('Drupal\Core\Language\LanguageManagerInterface');

    $messenger = $this->createMock('Drupal\Core\Messenger\MessengerInterface');

    $views_data = $this->createMock('Drupal\views\ViewsData');
    $views_data->expects($this->any())
      ->method('get')
      ->with('knowledge')
      ->will($this->returnValue(['table' => ['entity type' => 'knowledge']]));
    $container = new ContainerBuilder();
    $container->set('views.views_data', $views_data);

    $route_match = $this->createMock('Drupal\Core\Routing\ResettableStackedRouteMatchInterface');
    $container->set('current_route_match', $route_match);

    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $storage = $this->createMock('Drupal\views\ViewEntityInterface');
    $storage->expects($this->any())
      ->method('get')
      ->with('base_table')
      ->will($this->returnValue('knowledge'));

    $executable = $this->createMock('Drupal\views\ViewExecutable');
    $executable->storage = $storage;

    $display = $this->createMock('Drupal\views\Plugin\views\display\DisplayPluginBase');

    $definition['title'] = '';
    $options = [];

    $knowledge_bulk_form = new KnowledgeBulkForm([], 'knowledge_bulk_form', $definition, $entity_type_manager, $language_manager, $messenger, $entity_repository, $route_match);
    $knowledge_bulk_form->init($executable, $display, $options);

    $reflected_actions = (new \ReflectionObject($knowledge_bulk_form))->getProperty('actions');
    $reflected_actions->setAccessible(TRUE);
    $this->assertEquals(array_slice($actions, 0, -1, TRUE), $reflected_actions->getValue($knowledge_bulk_form));
  }

}
