<?php

declare(strict_types=1);

namespace Drupal\Tests\knowledge\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\CompetencyHtmlRouteProvider;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollection;

/**
 * @coversDefaultClass \Drupal\knowledge\CompetencyHtmlRouteProvider
 * @group knowledge
 */
class CompetencyHtmlRouteProviderTest extends UnitTestCase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityFieldManager;

  /**
   * The competency HTML route provider.
   *
   * @var \Drupal\knowledge\CompetencyHtmlRouteProvider
   */
  protected $competencyHtmlRouteProvider;

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->container = new ContainerBuilder();

    // $this->stringTranslation = $this->getStringTranslationStub();
    // $this->container->set('string_translation', $this->stringTranslation);
    $this->entityTypeManager = $this->createMock('\Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->container->set('entity_type.manager', $this->entityTypeManager);

    $this->entityFieldManager = $this->createMock('\Drupal\Core\Entity\EntityFieldManagerInterface');
    $this->container->set('entity_field.manager', $this->entityFieldManager);

    \Drupal::setContainer($this->container);

    $this->competencyHtmlRouteProvider = new CompetencyHtmlRouteProvider($this->entityTypeManager, $this->entityFieldManager);
  }

  /**
   * Tests the getRoutes method.
   *
   * @covers ::getRoutes
   * @covers ::getHistoryRoute
   * @covers ::getRevisionRoute
   * @covers ::getRevisionRevertRoute
   * @covers ::getRevisionDeleteRoute
   * @covers ::getSettingsFormRoute
   * @covers ::getRoleFormRoute
   */
  public function testGetRoutes(): void {

    // EntityTypeInterface $entity_type.
    $entity_type = $this->createMock('\Drupal\Core\Entity\EntityTypeInterface');
    $entity_type->expects($this->exactly(5))
      ->method('id')
      ->willReturn('knowledge_competency');
    $entity_type->expects($this->exactly(3))
      ->method('getAdminPermission')
      ->willReturn('administer knowledge_competency');
    $entity_type->expects($this->exactly(11))
      ->method('hasLinkTemplate')
      ->willReturnMap([
        ['add-form', TRUE],
        ['canonical', TRUE],
        ['edit-form', TRUE],
        ['version-history', TRUE],
        ['delete-form', TRUE],
        ['collection', TRUE],
        ['delete-multiple-form', FALSE],
        ['revision', TRUE],
        ['revision_revert', TRUE],
        ['revision_delete', TRUE],
      ]);
    $entity_type->expects($this->exactly(7))
      ->method('getLinkTemplate')
      ->willReturnMap([
        ['add-form', '/admin/content/knowledge/competency/add'],
        ['canonical', '/admin/content/knowledge/competency/{knowledge_competency}'],
        ['version-history', '/admin/content/knowledge/competency/{knowledge_competency}/revisions'],
        ['edit-form', '/admin/content/knowledge/competency/{knowledge_competency}/edit'],
        ['delete-form', '/admin/content/knowledge/competency/{knowledge_competency}/delete'],
        ['collection', '/admin/content/knowledge/competency'],
        [
          'revision',
          '/admin/content/knowledge/competency/{knowledge_competency}/revisions/{knowledge_competency_revision}/view',
        ],
        [
          'revision_revert',
          '/admin/content/knowledge/competency/{knowledge_competency}/revisions/{knowledge_competency_revision}/revert',
        ],
        [
          'revision_delete',
          '/admin/content/knowledge/competency/{knowledge_competency}/revisions/{knowledge_competency_revision}/delete',
        ],
      ]);

    $routes = $this->competencyHtmlRouteProvider->getRoutes($entity_type);

    $this->assertNotNull($routes);
    $this->assertInstanceOf(RouteCollection::class, $routes);

  }

  /**
   * Tests the getRoutes method when some routes are NULL.
   *
   * @covers ::getRoutes
   * @covers ::getHistoryRoute
   * @covers ::getRevisionRoute
   * @covers ::getRevisionRevertRoute
   * @covers ::getRevisionDeleteRoute
   * @covers ::getSettingsFormRoute
   * @covers ::getRoleFormRoute
   */
  public function testGetRoutesNotHas(): void {

    // EntityTypeInterface $entity_type.
    $entity_type = $this->createMock('\Drupal\Core\Entity\EntityTypeInterface');
    $entity_type->expects($this->exactly(5))
      ->method('id')
      ->willReturn('knowledge_competency');
    $entity_type->expects($this->exactly(3))
      ->method('getAdminPermission')
      ->willReturn('administer knowledge_competency');
    $entity_type->expects($this->exactly(11))
      ->method('hasLinkTemplate')
      ->willReturnMap([
        ['add-form', TRUE],
        ['canonical', TRUE],
        ['edit-form', TRUE],
        ['version-history', FALSE],
        ['delete-form', TRUE],
        ['collection', TRUE],
        ['delete-multiple-form', FALSE],
        ['revision', FALSE],
        ['revision_revert', FALSE],
        ['revision_delete', FALSE],
      ]);
    $entity_type->expects($this->exactly(3))
      ->method('getLinkTemplate')
      ->willReturnMap([
        ['add-form', '/admin/content/knowledge/competency/add'],
        ['canonical', '/admin/content/knowledge/competency/{knowledge_competency}'],
        ['edit-form', '/admin/content/knowledge/competency/{knowledge_competency}/edit'],
        ['delete-form', '/admin/content/knowledge/competency/{knowledge_competency}/delete'],
        ['collection', '/admin/content/knowledge/competency'],
      ]);

    $routes = $this->competencyHtmlRouteProvider->getRoutes($entity_type);

    $this->assertNotNull($routes);
    $this->assertInstanceOf(RouteCollection::class, $routes);

  }

}
