<?php

declare(strict_types=1);

namespace Drupal\Tests\knowledge\Service\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\Service\CompetencyService;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @coversDefaultClass \Drupal\knowledge\Service\CompetencyService
 * @group knowledge
 */
class CompetencyServiceTest extends UnitTestCase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $stringTranslation;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $messenger;

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  protected function setup(): void {
    parent::setUp();

    $this->container = new ContainerBuilder();

    $this->configFactory = $this->createMock('\Drupal\Core\Config\ConfigFactoryInterface');
    $this->container->set('config.factory', $this->configFactory);

    $this->entityTypeManager = $this->createMock('\Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->container->set('entity_type.manager', $this->entityTypeManager);

    $this->stringTranslation = $this->getStringTranslationStub();
    $this->container->set('string_translation', $this->stringTranslation);

    $this->messenger = $this->createMock('\Drupal\Core\Messenger\MessengerInterface');
    $this->container->set('messenger', $this->messenger, $this->currentUser);

    $this->currentUser = $this->createMock('\Drupal\user\UserInterface');
    $this->container->set('current_user', $this->currentUser);

    \Drupal::setContainer($this->container);
  }

  /**
   * Test get method.
   *
   * @covers ::get
   * @covers ::sort
   */
  public function testGet() {
    $config = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $config->expects($this->once())
      ->method('get')
      ->with('roles')
      ->willReturn([
        [
          'role' => 'knowledge_contributor',
          'weight' => 1,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_publisher',
          'weight' => 2,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_candidate',
          'weight' => 0,
          'action' => 'auto',
          'promote' => 'self',
        ],
      ]);

    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('knowledge.competency.settings')
      ->willReturn($config);

    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);

    $expected = [
      [
        'role' => 'knowledge_candidate',
        'weight' => 0,
        'action' => 'auto',
        'promote' => 'self',
      ],
      [
        'role' => 'knowledge_contributor',
        'weight' => 1,
        'action' => 'auto',
        'promote' => 'self',
      ],
      [
        'role' => 'knowledge_publisher',
        'weight' => 2,
        'action' => 'auto',
        'promote' => 'self',
      ],
    ];
    $this->assertEquals($expected, $service->get());
  }

  /**
   * Test get method.
   *
   * @covers ::get
   * @covers ::sort
   */
  public function testGetSameWeight() {
    $config = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $config->expects($this->once())
      ->method('get')
      ->with('roles')
      ->willReturn([
        [
          'role' => 'knowledge_contributor',
          'weight' => 1,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_publisher',
          'weight' => 1,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_candidate',
          'weight' => 0,
          'action' => 'auto',
          'promote' => 'self',
        ],
      ]);

    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('knowledge.competency.settings')
      ->willReturn($config);

    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);

    $expected = [
      [
        'role' => 'knowledge_candidate',
        'weight' => 0,
        'action' => 'auto',
        'promote' => 'self',
      ],
      [
        'role' => 'knowledge_contributor',
        'weight' => 1,
        'action' => 'auto',
        'promote' => 'self',
      ],
      [
        'role' => 'knowledge_publisher',
        'weight' => 1,
        'action' => 'auto',
        'promote' => 'self',
      ],
    ];
    $this->assertEquals($expected, $service->get());
  }

  /**
   * Test __construct method.
   *
   * @covers ::__construct
   */
  public function testConstruct() {
    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);
    $this->assertInstanceOf(CompetencyService::class, $service);
  }

  /**
   * Test getRoleIds method.
   *
   * @covers ::getRoleIds
   */
  public function testGetRoleIds(): void {
    $config = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $config->expects($this->once())
      ->method('get')
      ->with('roles')
      ->willReturn([
        [
          'role' => 'knowledge_contributor',
          'weight' => 1,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_publisher',
          'weight' => 2,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_candidate',
          'weight' => 0,
          'action' => 'auto',
          'promote' => 'self',
        ],
      ]);

    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('knowledge.competency.settings')
      ->willReturn($config);
    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);
    $this->assertEquals(['knowledge_candidate', 'knowledge_contributor', 'knowledge_publisher'], $service->getRoleIds());
  }

  /**
   * Test hasRoleOrBetter method.
   *
   * @covers ::hasRoleOrBetter
   */
  public function testHasRoleOrBetter(): void {
    $config = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $config->expects($this->exactly(3))
      ->method('get')
      ->with('roles')
      ->willReturn([
        [
          'role' => 'knowledge_contributor',
          'weight' => 1,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_publisher',
          'weight' => 2,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_candidate',
          'weight' => 0,
          'action' => 'auto',
          'promote' => 'self',
        ],
      ]);

    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('knowledge.competency.settings')
      ->willReturn($config);
    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);

    $user_roles = ['authenticated', 'knowledge_contributor'];
    $this->assertTrue($service->hasRoleOrBetter('knowledge_candidate', $user_roles));
    $this->assertTrue($service->hasRoleOrBetter('knowledge_contributor', $user_roles));
    $this->assertFalse($service->hasRoleOrBetter('knowledge_publisher', $user_roles));
    $this->assertFalse($service->hasRoleOrBetter('knowledge_better', $user_roles));
  }

  /**
   * Test getUserCompetency method.
   *
   * @covers ::getUserCompetency
   */
  public function testGetUserCompetencyNew(): void {
    $query = $this->createMock('\Drupal\Core\Entity\Query\QueryInterface');
    $query->expects($this->once())
      ->method('condition')
      ->with('user_id', 1)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('accessCheck')
      ->with(FALSE)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('execute')
      ->willReturn([]);

    $role_storage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');

    $storage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');
    $storage->expects($this->once())
      ->method('getQuery')
      ->willReturn($query);
    $storage->expects($this->once())
      ->method('create')
      ->with(['user_id' => 1])
      ->willReturn('competency');

    $this->entityTypeManager->expects($this->exactly(2))
      ->method('getStorage')
      ->willReturnMap([
        ['knowledge_competency', $storage],
        ['user_role', $role_storage],
      ]);

    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);
    $this->assertEquals('competency', $service->getUserCompetency(1));
  }

  /**
   * Test getUserCompetency method.
   *
   * @covers ::getUserCompetency
   */
  public function testGetUserCompetencyExisting(): void {
    $query = $this->createMock('\Drupal\Core\Entity\Query\QueryInterface');
    $query->expects($this->once())
      ->method('condition')
      ->with('user_id', 1)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('accessCheck')
      ->with(FALSE)
      ->willReturnSelf();
    $query->expects($this->once())
      ->method('execute')
      ->willReturn([1]);
    $role_storage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');

    $storage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');
    $storage->expects($this->once())
      ->method('getQuery')
      ->willReturn($query);
    $storage->expects($this->once())
      ->method('load')
      ->with(1)
      ->willReturn('competency');

    $this->entityTypeManager->expects($this->exactly(2))
      ->method('getStorage')
      ->willReturnMap([
          ['knowledge_competency', $storage],
          ['user_role', $role_storage],
      ]);

    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);
    $this->assertEquals('competency', $service->getUserCompetency(1));
  }

  /**
   * Tests doRoleRemoval method.
   *
   * @covers ::doRoleRemoval
   * @covers ::getRemovableRoles
   */
  public function testDoRoleRemoval(): void {
    $config = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $config->expects($this->once())
      ->method('get')
      ->with('roles')
      ->willReturn([
        [
          'role' => 'knowledge_contributor',
          'weight' => 1,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_publisher',
          'weight' => 2,
          'action' => 'auto',
          'promote' => 'self',
        ],
        [
          'role' => 'knowledge_candidate',
          'weight' => 0,
          'action' => 'auto',
          'promote' => 'self',
        ],
      ]);

    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('knowledge.competency.settings')
      ->willReturn($config);

    $service = new CompetencyService($this->configFactory, $this->entityTypeManager, $this->stringTranslation, $this->messenger, $this->currentUser);
    $user = $this->createMock('\Drupal\user\UserInterface');
    $user->expects($this->once())
      ->method('getRoles')
      ->willReturn(['knowledge_contributor', 'knowledge_publisher']);

    $service->doRoleRemoval($user);
  }

}
