<?php

declare(strict_types=1);

namespace Drupal\Tests\knowledge\Unit;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\CompetencyListBuilder;

/**
 * @coversDefaultClass \Drupal\knowledge\CompetencyListBuilder
 * @group knowledge
 */
class CompetencyListBuilderTest extends UnitTestCase {

  /**
   * The entity type used for testing.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityType;

  /**
   * The module handler used for testing.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * The translation manager used for testing.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $translationManager;

  /**
   * The competency storage used for testing.
   *
   * @var \Drupal\knowledge\KnowledgeCompetencyStorageInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $competencyStorage;

  /**
   * The service container used for testing.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * The entity used to construct the EntityListBuilder.
   *
   * @var \Drupal\user\UserInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $user;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $redirectDestination;

  /**
   * The EntityListBuilder object to test.
   *
   * @var \Drupal\knowledge\CompetencyListBuilder
   */
  protected $competencyListBuilder;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->createMock('Drupal\user\UserInterface');
    $this->stringTranslation = $this->getStringTranslationStub();
    $this->competencyStorage = $this->createMock('\Drupal\knowledge\KnowledgeCompetencyStorageInterface');
    $this->moduleHandler = $this->createMock('\Drupal\Core\Extension\ModuleHandlerInterface');
    $this->entityType = $this->createMock('\Drupal\Core\Entity\EntityTypeInterface');
    $this->translationManager = $this->createMock('\Drupal\Core\StringTranslation\TranslationInterface');
    $this->competencyListBuilder = new CompetencyListBuilder($this->entityType, $this->competencyStorage);
    $this->redirectDestination = $this->createMock(RedirectDestinationInterface::class);
    $this->container = new ContainerBuilder();
    $this->container->set('module_handler', $this->moduleHandler);
    $this->container->set('string_translation', $this->stringTranslation);
    \Drupal::setContainer($this->container);
  }

  /**
   * @covers ::buildHeader
   */
  public function testBuildHeader(): void {
    $header = $this->competencyListBuilder->buildHeader();
    $this->assertIsArray($header);
    $this->assertArrayHasKey('id', $header);
    $this->assertArrayHasKey('user', $header);
    $this->assertArrayHasKey('operations', $header);
  }

  /**
   * @covers ::buildRow
   */
  public function testBuildRow() {
    $entity = $this->createMock('\Drupal\knowledge\Entity\Competency');
    $entity->expects($this->exactly(2))
      ->method('id')
      ->willReturn(1);
    $entity->expects($this->once())
      ->method('getOwner')
      ->willReturn($this->user);
    $this->user->expects($this->once())
      ->method('label')
      ->willReturn('test');
    $url = Url::fromRoute('entity.knowledge_competency.edit_form', ['knowledge_competency' => 1]);

    $operation_name = $this->randomMachineName();
    $operations = [
      $operation_name => [
        'title' => $this->randomMachineName(),
      ],
    ];
    $this->moduleHandler->expects($this->once())
      ->method('invokeAll')
      ->with('entity_operation', [$entity])
      ->willReturn($operations);
    $this->moduleHandler->expects($this->once())
      ->method('alter')
      ->with('entity_operation');

    $row = $this->competencyListBuilder->buildRow($entity);
    $this->assertIsArray($row);
    $this->assertArrayHasKey('id', $row);
    $this->assertArrayHasKey('user', $row);
    $this->assertArrayHasKey('operations', $row);
  }

  /**
   * @covers ::getOperations
   */
  public function testGetOperations(): void {
    $operation_name = $this->randomMachineName();
    $operations = [
      $operation_name => [
        'title' => $this->randomMachineName(),
      ],
    ];
    $this->moduleHandler->expects($this->once())
      ->method('invokeAll')
      ->with('entity_operation', [$this->user])
      ->willReturn($operations);
    $this->moduleHandler->expects($this->once())
      ->method('alter')
      ->with('entity_operation');

    $this->user->expects($this->any())
      ->method('access')
      ->willReturn(AccessResult::allowed());
    $this->user->expects($this->any())
      ->method('hasLinkTemplate')
      ->willReturn(TRUE);
    $url = Url::fromRoute('entity.user_role.collection');
    $this->user->expects($this->any())
      ->method('toUrl')
      ->willReturn($url);

    $this->redirectDestination->expects($this->atLeastOnce())
      ->method('getAsArray')
      ->willReturn(['destination' => '/foo/bar']);

    $list = new CompetencyListBuilder($this->entityType, $this->competencyStorage);
    $list->setStringTranslation($this->translationManager);
    $list->setRedirectDestination($this->redirectDestination);

    $operations = $list->getOperations($this->user);
    $this->assertIsArray($operations);
    $this->assertArrayHasKey('edit', $operations);
    $this->assertIsArray($operations['edit']);
    $this->assertArrayHasKey('title', $operations['edit']);
    $this->assertArrayHasKey('delete', $operations);
    $this->assertIsArray($operations['delete']);
    $this->assertArrayHasKey('title', $operations['delete']);
    $this->assertArrayHasKey($operation_name, $operations);
    $this->assertIsArray($operations[$operation_name]);
    $this->assertArrayHasKey('title', $operations[$operation_name]);
  }

}
