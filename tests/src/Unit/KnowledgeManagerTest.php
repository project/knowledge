<?php

namespace Drupal\Tests\knowledge\Unit;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\KnowledgeManager;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophet;

/**
 * @coversDefaultClass \Drupal\knowledge\KnowledgeManager
 * @group knowledge
 */
class KnowledgeManagerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The Prophet class.
   *
   * @var \Prophecy\Prophet
   */
  private $prophet;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->prophet = new Prophet();
  }

  /**
   * Tests the getFields method.
   *
   * @covers ::getFields
   */
  public function testGetFields() {
    // Set up a content entity type.
    $entity_type = $this->createMock('Drupal\Core\Entity\ContentEntityTypeInterface');
    $entity_type->expects($this->any())
      ->method('getClass')
      ->will($this->returnValue('Node'));
    $entity_type->expects($this->any())
      ->method('entityClassImplements')
      ->with(FieldableEntityInterface::class)
      ->will($this->returnValue(TRUE));

    $entity_field_manager = $this->createMock(EntityFieldManagerInterface::class);
    $entity_type_manager = $this->createMock(EntityTypeManagerInterface::class);

    $entity_field_manager->expects($this->once())
      ->method('getFieldMapByFieldType')
      ->will($this->returnValue([
        'node' => [
          'field_foobar' => [
            'type' => 'knowledge',
          ],
        ],
      ]));

    $entity_type_manager->expects($this->any())
      ->method('getDefinition')
      ->will($this->returnValue($entity_type));

    $knowledge_manager = new KnowledgeManager(
      $entity_type_manager,
      $this->createMock('Drupal\Core\Config\ConfigFactoryInterface'),
      $this->createMock('Drupal\Core\StringTranslation\TranslationInterface'),
      $this->createMock('Drupal\Core\Extension\ModuleHandlerInterface'),
      $this->createMock(AccountInterface::class),
      $entity_field_manager,
      $this->prophet->prophesize(EntityDisplayRepositoryInterface::class)->reveal(),
      $this->createMock('Drupal\Component\Datetime\Time')
    );
    $knowledge_fields = $knowledge_manager->getFields('node');
    $this->assertArrayHasKey('field_foobar', $knowledge_fields);
  }

}
