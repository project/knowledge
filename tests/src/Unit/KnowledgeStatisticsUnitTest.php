<?php

namespace Drupal\Tests\knowledge\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\knowledge\KnowledgeStatistics;

/**
 * @coversDefaultClass \Drupal\knowledge\KnowledgeStatistics
 * @group knowledge
 */
class KnowledgeStatisticsUnitTest extends UnitTestCase {

  /**
   * Mock statement.
   *
   * @var \Drupal\Core\Database\Statement
   */
  protected $statement;

  /**
   * Mock select interface.
   *
   * @var \Drupal\Core\Database\Query\SelectInterface
   */
  protected $select;

  /**
   * Mock database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * KnowledgeStatistics service under test.
   *
   * @var \Drupal\knowledge\KnowledgeStatisticsInterface
   */
  protected $knowledgeStatistics;

  /**
   * Counts calls to fetchAssoc().
   *
   * @var int
   */
  protected $callsToFetch;

  /**
   * Sets up required mocks and the KnowledgeStatistics service under test.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->statement = $this->createMock('Drupal\sqlite\Driver\Database\sqlite\Statement');

    $this->statement->expects($this->any())
      ->method('fetchObject')
      ->willReturnCallback([$this, 'fetchObjectCallback']);

    $this->select = $this->createMock('Drupal\Core\Database\Query\Select');

    $this->select->expects($this->any())
      ->method('fields')
      ->will($this->returnSelf());

    $this->select->expects($this->any())
      ->method('condition')
      ->will($this->returnSelf());

    $this->select->expects($this->any())
      ->method('execute')
      ->will($this->returnValue($this->statement));

    $this->database = $this->createMock('Drupal\Core\Database\Connection');

    $this->database->expects($this->once())
      ->method('select')
      ->will($this->returnValue($this->select));

    // $entity_field_manager
    $this->knowledgeStatistics = new KnowledgeStatistics(
      $this->database,
      $this->createMock('\Drupal\Core\Session\AccountInterface'),
      $this->createMock('\Drupal\Core\Entity\EntityTypeManagerInterface'),
      $this->createMock('\Drupal\Core\State\StateInterface'),
      $this->createMock('\Drupal\Component\Datetime\Time'),
      $this->createMock('\Drupal\Core\Entity\EntityFieldManagerInterface'),
      $this->createMock('\Drupal\Core\Extension\ModuleHandlerInterface'),
      $this->createMock('\Drupal\Core\Config\ConfigFactoryInterface'),
      $this->createMock('\Drupal\Core\Messenger\MessengerInterface'),
      $this->database,
      NULL
    );
  }

  /**
   * Tests the read method.
   *
   * @see \Drupal\knowledge\KnowledgeStatistics::read()
   *
   * @group Drupal
   * @group Knowledge
   */
  public function testRead() {
    $this->callsToFetch = 0;
    $results = $this->knowledgeStatistics->read(['1' => 'boo', '2' => 'foo'], 'snafus');
    $this->assertEquals(['something', 'something-else'], $results);
  }

  /**
   * Return value callback for fetchObject() function on mocked object.
   *
   * @return bool|string
   *   'Something' on first, 'something-else' on second and FALSE for the
   *   other calls to function.
   */
  public function fetchObjectCallback() {
    $this->callsToFetch++;
    switch ($this->callsToFetch) {
      case 1:
        return 'something';

      case 2:
        return 'something-else';

      default:
        return FALSE;
    }
  }

}
