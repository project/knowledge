<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Cache\Cache;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Tests the bubbling up of knowledge cache tags.
 *
 * When using the Knowledge list formatter on an entity.
 *
 * @group knowledge
 */
class KnowledgeDefaultFormatterCacheTagsTest extends EntityKernelTestBase {

  use KnowledgeTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user 1 so that the user created later in the test has a different
    // user ID.
    // @todo Remove in https://www.drupal.org/node/540008.
    $this->createUser([], 'usel1', TRUE, ['uid' => 1])->save();

    $this->container->get('module_handler')->loadInclude('knowledge', 'install');
    knowledge_install();

    $session = new Session();

    $request = Request::create('/');
    $request->setSession($session);

    /** @var \Symfony\Component\HttpFoundation\RequestStack $stack */
    $stack = $this->container->get('request_stack');
    $stack->pop();
    $stack->push($request);

    // Set the current user to one that can access knowledge. Specifically, this
    // user does not have access to the 'administer knowledge' permission, to
    // ensure only published knowledge are visible to the end user.
    $current_user = $this->container->get('current_user');
    $current_user->setAccount($this->createUser([
      'access knowledge',
      'post knowledge',
    ]));

    // Install tables and config needed to render knowledge.
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
    $this->installConfig(['system', 'filter', 'knowledge']);

    // Set up a field, so that the entity that'll be referenced bubbles up a
    // cache tag when rendering it entirely.
    $this->addDefaultKnowledgeField('entity_test', 'entity_test');
  }

  /**
   * Tests the bubbling of cache tags.
   */
  public function testCacheTags() {
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = $this->container->get('renderer');

    // Create the entity that will be knowledge linked upon.
    $linked_entity = EntityTest::create(['name' => $this->randomMachineName()]);
    $linked_entity->save();

    // Verify cache tags on the rendered entity before it has knowledge.
    $build = \Drupal::entityTypeManager()
      ->getViewBuilder('entity_test')
      ->view($linked_entity);
    $renderer->renderRoot($build);
    $expected_cache_tags = [
      'entity_test_view',
      'entity_test:' . $linked_entity->id(),
      'config:core.entity_form_display.knowledge.knowledge.default',
      'config:field.field.knowledge.knowledge.knowledge_body',
      'config:field.field.entity_test.entity_test.knowledge',
      'config:field.storage.knowledge.knowledge_body',
      'config:user.settings',
    ];
    $this->assertEqualsCanonicalizing($expected_cache_tags, $build['#cache']['tags']);

    // Create a knowledge on that entity. Knowledge loading requires that the
    // uid also exists in the {users} table.
    $user = $this->createUser();
    $user->save();
    $knowledge = Knowledge::create([
      'subject' => 'Llama',
      'knowledge_body' => [
        'value' => 'Llamas are cool!',
        'format' => 'plain_text',
      ],
      'entity_id' => $linked_entity->id(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'knowledge_type' => 'knowledge',
      'status' => KnowledgeInterface::PUBLISHED,
      'uid' => $user->id(),
    ]);
    $knowledge->save();

    // Load linked entity so total_count gets computed.
    // @todo Remove the $reset = TRUE parameter after
    //   https://www.drupal.org/node/597236 lands. It's a temporary work-around.
    $storage = $this->container->get('entity_type.manager')->getStorage('entity_test');
    $storage->resetCache([$linked_entity->id()]);
    $linked_entity = $storage->load($linked_entity->id());

    // Verify cache tags on the rendered entity when it has knowledge.
    $build = \Drupal::entityTypeManager()
      ->getViewBuilder('entity_test')
      ->view($linked_entity);
    $renderer->renderRoot($build);
    $expected_cache_tags = [
      'entity_test_view',
      'entity_test:' . $linked_entity->id(),
      'knowledge_view',
      'knowledge:' . $knowledge->id(),
      'config:filter.format.plain_text',
      'user_view',
      'user:' . $user->id(),
      'config:core.entity_form_display.knowledge.knowledge.default',
      'config:field.field.knowledge.knowledge.knowledge_body',
      'config:field.field.entity_test.entity_test.knowledge',
      'config:field.storage.knowledge.knowledge_body',
      'config:user.settings',
    ];
    $this->assertEqualsCanonicalizing($expected_cache_tags, $build['#cache']['tags']);

    // Build a render array with the entity in a sub-element so that lazy
    // builder elements bubble up outside of the entity and we can check that
    // it got the correct cache max age.
    $build = ['#type' => 'container'];
    $build['entity'] = \Drupal::entityTypeManager()
      ->getViewBuilder('entity_test')
      ->view($linked_entity);
    $renderer->renderRoot($build);

    // The entity itself was cached but the top-level element is max-age 0 due
    // to the bubbled up max age due to the lazy-built knowledge form.
    $this->assertSame(Cache::PERMANENT, $build['entity']['#cache']['max-age']);
    $this->assertSame(0, $build['#cache']['max-age'], 'Top level render array has max-age 0');

    // The children (fields) of the entity render array are only built in case
    // of a cache miss.
    $this->assertFalse(isset($build['entity']['knowledge']), 'Cache hit');
  }

}
