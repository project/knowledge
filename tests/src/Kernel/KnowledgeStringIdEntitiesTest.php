<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\KnowledgeType;

/**
 * Tests that knowledge fields cannot be added to entities with non-integer IDs.
 *
 * @group knowledge
 */
class KnowledgeStringIdEntitiesTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field_ui',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('knowledge');
    $this->installEntitySchema('entity_test_string_id');
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
    // Create the knowledge body field storage.
    $this->installConfig(['field']);
  }

  /**
   * Tests that knowledge fields cannot be added entities with non-integer IDs.
   */
  public function testKnowledgeFieldNonStringId() {
    $this->expectException(\UnexpectedValueException::class);
    $bundle = KnowledgeType::create([
      'id' => 'foo',
      'label' => 'foo',
      'description' => '',
      'target_entity_type_id' => 'entity_test_string_id',
    ]);
    $bundle->save();
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'foo',
      'entity_type' => 'entity_test_string_id',
      'settings' => [
        'knowledge_type' => 'entity_test_string_id',
      ],
      'type' => 'knowledge',
    ]);
    $field_storage->save();
  }

}
