<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\EntityViewTrait;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests loading and rendering orphan knowledge.
 *
 * @group knowledge
 */
class KnowledgeOrphanTest extends EntityKernelTestBase {

  use EntityViewTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['user']);
    $this->installEntitySchema('date_format');
    $this->installEntitySchema('knowledge');
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
  }

  /**
   * Test loading/deleting/rendering orphaned knowledge.
   *
   * @dataProvider providerTestOrphan
   */
  public function testOrphan($property) {

    DateFormat::create([
      'id' => 'fallback',
      'label' => 'Fallback',
      'pattern' => 'Y-m-d',
    ])->save();

    $knowledge_storage = $this->entityTypeManager->getStorage('knowledge');
    $node_storage = $this->entityTypeManager->getStorage('node');

    // Create a page node type.
    $this->entityTypeManager->getStorage('node_type')->create([
      'type' => 'page',
      'name' => 'page',
    ])->save();

    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'test',
    ]);
    $node->save();

    // Create knowledge field.
    $this->entityTypeManager->getStorage('field_storage_config')->create([
      'type' => 'text_long',
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ])->save();

    // Add knowledge field to page content.
    $this->entityTypeManager->getStorage('field_config')->create([
      'field_storage' => FieldStorageConfig::loadByName('node', 'knowledge'),
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => 'Knowledge',
    ])->save();

    // Make two knowledge.
    $knowledge_storage->create([
      'field_name' => 'knowledge',
      'knowledge_body' => 'test',
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'knowledge_type' => 'default',
    ])->save();

    $knowledge_storage->create([
      'field_name' => 'knowledge',
      'knowledge_body' => 'test',
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'knowledge_type' => 'default',
    ])->save();

    // Render the knowledge.
    $renderer = \Drupal::service('renderer');
    $knowledge_links = $knowledge_storage->loadMultiple();
    foreach ($knowledge_links as $knowledge) {
      $built = $this->buildEntityView($knowledge, 'full', NULL);
      $renderer->renderInIsolation($built);
    }

    // Make knowledge 2 an orphan by setting the property to an invalid value.
    \Drupal::database()->update('knowledge')
      ->fields([$property => 10])
      ->condition('kid', 2)
      ->execute();
    $knowledge_storage->resetCache();
    $node_storage->resetCache();

    // Render the knowledge with an orphan knowledge.
    $knowledge_links = $knowledge_storage->loadMultiple();
    foreach ($knowledge_links as $knowledge) {
      $built = $this->buildEntityView($knowledge, 'full', NULL);
      $renderer->renderInIsolation($built);
    }

    $node = $node_storage->load($node->id());
    $built = $this->buildEntityView($node, 'full', NULL);
    $renderer->renderInIsolation($built);
  }

  /**
   * Provides test data for testOrphan.
   */
  public static function providerTestOrphan() {
    return [
      ['entity_id'],
      ['uid'],
    ];
  }

}
