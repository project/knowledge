<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Tests knowledge validation constraints.
 *
 * @group knowledge
 */
class KnowledgeValidationTest extends EntityKernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
  }

  /**
   * Tests the knowledge validation constraints.
   */
  public function testValidation() {
    // Add a user.
    $user = User::create(['name' => 'test', 'status' => TRUE]);
    $user->save();

    // Add knowledge type.
    $this->entityTypeManager->getStorage('knowledge_type')->create([
      'id' => 'knowledge',
      'label' => 'knowledge',
      'target_entity_type_id' => 'node',
    ])->save();

    // Add knowledge field to content.
    $this->entityTypeManager->getStorage('field_storage_config')->create([
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'type' => 'knowledge',
      'settings' => [
        'knowledge_type' => 'knowledge',
      ],
    ])->save();

    // Create a page node type.
    $this->entityTypeManager->getStorage('node_type')->create([
      'type' => 'page',
      'name' => 'page',
    ])->save();

    // Add knowledge field to page content.
    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = $this->entityTypeManager->getStorage('field_config')->create([
      'field_name' => 'knowledge',
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => 'Knowledge settings',
    ]);
    $field->save();

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'page',
      'title' => 'test',
    ]);
    $node->save();

    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = $this->entityTypeManager->getStorage('knowledge')->create([
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'knowledge_body' => $this->randomMachineName(),
    ]);

    $violations = $knowledge->validate();
    $this->assertCount(0, $violations, 'No violations when validating a default knowledge.');

    $knowledge->set('subject', $this->randomString(65));
    $this->assertLengthViolation($knowledge, 'subject', 64);

    // Make the subject valid.
    $knowledge->set('subject', $this->randomString());
    $knowledge->set('name', $this->randomString(61));
    $this->assertLengthViolation($knowledge, 'name', 60);

    // Validate a name collision between an anonymous knowledge author name and
    // an existing user account name.
    $knowledge->set('name', 'test');
    $knowledge->set('uid', 0);
    $violations = $knowledge->validate();
    $this->assertCount(1, $violations, "Violation found on author name collision");
    $this->assertEquals("name", $violations[0]->getPropertyPath());
    $this->assertEquals(new TranslatableMarkup('The name you used (%name) belongs to a registered user.', ['%name' => 'test']), $violations[0]->getMessage());

    // Make the name valid.
    $knowledge->set('name', 'valid unused name');
    $knowledge->set('mail', 'invalid');
    $violations = $knowledge->validate();
    $this->assertCount(1, $violations, 'Violation found when email is invalid');
    $this->assertEquals('mail.0.value', $violations[0]->getPropertyPath());
    $this->assertEquals('This value is not a valid email address.', $violations[0]->getMessage());

    $knowledge->set('mail', NULL);

    // @todo This message should be improved in
    //   https://www.drupal.org/node/2012690.
    $this->assertEquals('This value should be of the correct primitive type.', $violations[0]->getMessage());

    // Force anonymous users to enter contact details.
    $field->setSetting('anonymous', KnowledgeInterface::ANONYMOUS_MUST_CONTACT);
    $field->save();
    // Reset the node entity.
    \Drupal::entityTypeManager()->getStorage('node')->resetCache([$node->id()]);
    $node = Node::load($node->id());
    // Create a new knowledge with the new field.
    $knowledge = $this->entityTypeManager->getStorage('knowledge')->create([
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'knowledge_body' => $this->randomMachineName(),
      'uid' => 0,
      'name' => '',
    ]);
    $violations = $knowledge->validate();
    $this->assertCount(1, $violations, 'Violation found when name is required, but empty and UID is anonymous.');
    $this->assertEquals('name', $violations[0]->getPropertyPath());
    $this->assertEquals('You have to specify a valid author.', $violations[0]->getMessage());

    // Test creating a default knowledge with a given user id works.
    $knowledge = $this->entityTypeManager->getStorage('knowledge')->create([
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'knowledge_body' => $this->randomMachineName(),
      'uid' => $user->id(),
    ]);
    $violations = $knowledge->validate();
    $this->assertCount(0, $violations, 'No violations when validating a default knowledge with an author.');

    // Test specifying a wrong author name does not work.
    $knowledge = $this->entityTypeManager->getStorage('knowledge')->create([
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
      'knowledge_body' => $this->randomMachineName(),
      'uid' => $user->id(),
      'name' => 'not-test',
    ]);
    $violations = $knowledge->validate();
    $this->assertCount(1, $violations, 'Violation found when author name and knowledge author do not match.');
    $this->assertEquals('name', $violations[0]->getPropertyPath());
    $this->assertEquals('The specified author name does not match the knowledge author.', $violations[0]->getMessage());
  }

  /**
   * Verifies that a length violation exists for the given field.
   *
   * @param \Drupal\knowledge\KnowledgeInterface $knowledge
   *   The knowledge object to validate.
   * @param string $field_name
   *   The field that violates the maximum length.
   * @param int $length
   *   Number of characters that was exceeded.
   *
   * @internal
   */
  protected function assertLengthViolation(KnowledgeInterface $knowledge, string $field_name, int $length): void {
    $violations = $knowledge->validate();
    $this->assertCount(1, $violations, "Violation found when $field_name is too long.");
    $this->assertEquals("{$field_name}.0.value", $violations[0]->getPropertyPath());
    $field_label = $knowledge->get($field_name)->getFieldDefinition()->getLabel();
    $this->assertEquals(new TranslatableMarkup('%name: may not be longer than @max characters.', [
      '%name' => $field_label,
      '@max' => $length,
    ]), $violations[0]->getMessage());
  }

}
