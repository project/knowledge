<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\knowledge\Entity\KnowledgeType;

/**
 * Tests that knowledge bundles behave as expected.
 *
 * @group knowledge
 */
class KnowledgeBundlesTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'node',
    'options',
    'search_api',
    'taxonomy',
    'text',
    'user',
    'workflows',
  ];

  /**
   * Entity type ids to use for target_entity_type_id on knowledge bundles.
   *
   * @var array
   */
  protected $targetEntityTypes;

  /**
   * The field entity manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityFieldManager = $this->container->get('entity_field.manager');

    $this->installEntitySchema('knowledge');

    // Create multiple knowledge bundles,
    // each of which has a different target entity type.
    $this->targetEntityTypes = [
      'knowledge' => 'Knowledge',
      'node' => 'Node',
      'taxonomy_term' => 'Taxonomy Term',
    ];
    foreach ($this->targetEntityTypes as $id => $label) {
      KnowledgeType::create([
        'id' => 'knowledge_on_' . $id,
        'label' => 'Knowledge on ' . $label,
        'target_entity_type_id' => $id,
      ])->save();
    }
  }

  /**
   * Tests that the entity_id field is set correctly for each knowledge bundle.
   */
  public function testEntityIdField() {
    $field_definitions = [];

    foreach (array_keys($this->targetEntityTypes) as $id) {
      $bundle = 'knowledge_on_' . $id;
      $field_definitions[$bundle] = $this->entityFieldManager
        ->getFieldDefinitions('knowledge', $bundle);
    }
    // Test that the value of the entity_id field for each bundle is correct.
    foreach ($field_definitions as $bundle => $definition) {
      $entity_type_id = str_replace('knowledge_on_', '', $bundle);
      $target_type = $definition['entity_id']->getSetting('target_type');
      $this->assertEquals($entity_type_id, $target_type);

      // Verify that the target type remains correct
      // in the deeply-nested object properties.
      $nested_target_type = $definition['entity_id']->getItemDefinition()->getFieldDefinition()->getSetting('target_type');
      $this->assertEquals($entity_type_id, $nested_target_type);
    }

  }

}
