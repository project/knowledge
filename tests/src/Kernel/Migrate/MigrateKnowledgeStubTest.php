<?php

namespace Drupal\Tests\knowledge\Kernel\Migrate;

use Drupal\Tests\migrate_drupal\Kernel\MigrateDrupalTestBase;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\migrate_drupal\Tests\StubTestTrait;
use Drupal\node\Entity\NodeType;

/**
 * Test stub creation for knowledge entities.
 *
 * @group knowledge
 */
class MigrateKnowledgeStubTest extends MigrateDrupalTestBase {

  use StubTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['knowledge', 'node'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('knowledge');
    $this->installEntitySchema('node');

    // Make sure uid 0 is created (default uid for knowledge is 0).
    $storage = \Drupal::entityTypeManager()->getStorage('user');
    // Insert a row for the anonymous user.
    $storage
      ->create([
        'uid' => 0,
        'status' => 0,
        'name' => '',
      ])
      ->save();
    // Need at least one node type and knowledge type present.
    NodeType::create([
      'type' => 'testnodetype',
      'name' => 'Test node type',
    ])->save();
    KnowledgeType::create([
      'id' => 'testknowledgetype',
      'label' => 'Test knowledge type',
      'target_entity_type_id' => 'node',
    ])->save();
  }

  /**
   * Tests creation of knowledge stubs.
   */
  public function testStub() {
    $this->performStubTest('knowledge');
  }

}
