<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Extension\ModuleUninstallValidatorException;
use Drupal\KernelTests\KernelTestBase;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\node\Entity\NodeType;

/**
 * Tests knowledge module uninstall.
 *
 * @group knowledge
 */
class KnowledgeUninstallTest extends KernelTestBase {

  use KnowledgeTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'system',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('knowledge');
    $this->installConfig(['knowledge']);
    $this->installSchema('user', ['users_data']);

    NodeType::create(['type' => 'article'])->save();

    // Create knowledge field on article so that it adds 'knowledge_body' field.
    FieldStorageConfig::create([
      'type' => 'text_long',
      'entity_type' => 'knowledge',
      'field_name' => 'knowledge',
    ])->save();
    $this->addDefaultKnowledgeField('node', 'article');
  }

  /**
   * Tests if knowledge module uninstall fails if the field exists.
   */
  public function testKnowledgeUninstallWithField() {
    // Ensure that the field exists before uninstalling.
    $field_storage = FieldStorageConfig::loadByName('knowledge', 'knowledge_body');
    $this->assertNotNull($field_storage);

    // Uninstall the knowledge module which should trigger an exception.
    $this->expectException(ModuleUninstallValidatorException::class);
    $this->expectExceptionMessage('The following reasons prevent the modules from being uninstalled: The <em class="placeholder">Knowledge</em> field type is used in the following field: node.knowledge');
    $this->container->get('module_installer')->uninstall(['knowledge']);
  }

  /**
   * Tests if uninstallation succeeds if the field has been deleted beforehand.
   */
  public function testKnowledgeUninstallWithoutField() {
    // Tests if uninstall succeeds if the field has been deleted beforehand.
    // Manually delete the knowledge_body field before module uninstall.
    FieldStorageConfig::loadByName('knowledge', 'knowledge_body')->delete();

    // Check that the field is now deleted.
    $field_storage = FieldStorageConfig::loadByName('knowledge', 'knowledge_body');
    $this->assertNull($field_storage);

    // Manually delete the knowledge field on the node before module uninstall.
    $field_storage = FieldStorageConfig::loadByName('node', 'knowledge');
    $this->assertNotNull($field_storage);
    $field_storage->delete();

    // Check that the field is now deleted.
    $field_storage = FieldStorageConfig::loadByName('node', 'knowledge');
    $this->assertNull($field_storage);

    field_purge_batch(10);
    // Ensure that uninstall succeeds even if the field has already been deleted
    // manually beforehand.
    $this->container->get('module_installer')->uninstall(['knowledge']);
  }

}
