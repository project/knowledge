<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\knowledge\Tests\KnowledgeTestTrait;

/**
 * Tests the new entity API for the knowledge field type.
 *
 * @group knowledge
 */
class KnowledgeItemTest extends FieldKernelTestBase {

  use KnowledgeTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('knowledge');
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
    $this->installConfig(['knowledge']);
  }

  /**
   * Tests using entity fields of the knowledge field type.
   */
  public function testKnowledgeItem() {
    $this->addDefaultKnowledgeField('entity_test', 'entity_test', 'knowledge');

    // Verify entity creation.
    $entity = EntityTest::create();
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Verify entity has been created properly.
    $id = $entity->id();
    $storage = $this->container->get('entity_type.manager')->getStorage('entity_test');
    $storage->resetCache([$id]);
    $entity = $storage->load($id);
    $this->assertInstanceOf(FieldItemListInterface::class, $entity->knowledge);
    $this->assertInstanceOf(KnowledgeItemInterface::class, $entity->knowledge[0]);

    // Test sample item generation.
    /** @var \Drupal\entity_test\Entity\EntityTest $entity */
    $entity = EntityTest::create();
    $entity->knowledge->generateSampleItems();
    $this->entityValidateAndSave($entity);
    $this->assertContains($entity->get('knowledge')->status, [
      KnowledgeItemInterface::HIDDEN,
      KnowledgeItemInterface::CLOSED,
      KnowledgeItemInterface::OPEN,
    ], 'Knowledge status value in defined range');

    $mainProperty = $entity->knowledge[0]->mainPropertyName();
    $this->assertEquals('status', $mainProperty);
  }

  /**
   * Tests knowledge author name.
   */
  public function testKnowledgeAuthorName() {
    $this->installEntitySchema('knowledge');
    $this->addDefaultKnowledgeField('entity_test', 'entity_test', 'knowledge');

    $host = EntityTest::create(['name' => $this->randomString()]);
    $host->save();

    // Create some knowledge.
    $knowledge = Knowledge::create([
      'subject' => 'My knowledge title',
      'uid' => 1,
      'name' => 'entity-test',
      'mail' => 'entity@localhost',
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'status' => 1,
    ]);
    $knowledge->save();

    // The entity fields for name and mail have no meaning if the user is not
    // Anonymous.
    $this->assertNull($knowledge->name->value);
    $this->assertNull($knowledge->mail->value);

    $knowledge_anonymous = Knowledge::create([
      'subject' => 'Anonymous knowledge title',
      'uid' => 0,
      'name' => 'barry',
      'mail' => 'test@example.com',
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'status' => 1,
    ]);
    $knowledge_anonymous->save();

    // The entity fields for name and mail have retained their values when
    // knowledge belongs to an anonymous user.
    $this->assertNotNull($knowledge_anonymous->name->value);
    $this->assertNotNull($knowledge_anonymous->mail->value);

    $knowledge_anonymous->setOwnerId(1)
      ->save();
    // The entity fields for name and mail have no meaning if the user is not
    // Anonymous.
    $this->assertNull($knowledge_anonymous->name->value);
    $this->assertNull($knowledge_anonymous->mail->value);
  }

}
