<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\knowledge_base_field_test\Entity\KnowledgeTestBaseField;

/**
 * Tests that knowledge as a base field.
 *
 * @group knowledge
 */
class KnowledgeBaseFieldTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge_base_field_test',
    'knowledge',
    'node',
    'options',
    'search_api',
    'system',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installEntitySchema('knowledge_test_base_field');
    $this->installEntitySchema('knowledge');
    $this->installEntitySchema('user');
  }

  /**
   * Tests knowledge as a base field.
   */
  public function testKnowledgeBaseField() {
    // Verify entity creation.
    $entity = KnowledgeTestBaseField::create([
      'name' => $this->randomMachineName(),
      'test_knowledge' => KnowledgeItemInterface::OPEN,
    ]);
    $entity->save();

    $knowledge = Knowledge::create([
      'entity_id' => $entity->id(),
      'entity_type' => 'knowledge_test_base_field',
      'field_name' => 'test_knowledge',
      'uid' => 0,
      'status' => KnowledgeInterface::PUBLISHED,
      'subject' => $this->randomMachineName(),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'knowledge_body' => [['value' => $this->randomMachineName()]],
    ]);
    $knowledge->save();
    $this->assertEquals('test_knowledge_type', $knowledge->bundle());
  }

}
