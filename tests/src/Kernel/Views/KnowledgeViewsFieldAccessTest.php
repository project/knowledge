<?php

namespace Drupal\Tests\knowledge\Kernel\Views;

use Drupal\Tests\views\Kernel\Handler\FieldFieldAccessTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\user\Entity\User;

/**
 * Tests base field access in Views for the knowledge entity.
 *
 * @group knowledge
 */
class KnowledgeViewsFieldAccessTest extends FieldFieldAccessTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    $this->installEntitySchema('node');
    $this->installEntitySchema('knowledge');
    $this->installEntitySchema('entity_test');
  }

  /**
   * Check access for knowledge fields.
   */
  public function testKnowledgeFields() {
    $user = User::create([
      'name' => 'test user',
    ]);
    $user->save();

    $host = EntityTest::create(['name' => $this->randomString()]);
    $host->save();

    $knowledge = Knowledge::create([
      'subject' => 'My knowledge title',
      'uid' => $user->id(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
    ]);
    $knowledge->save();

    $knowledge_anonymous = Knowledge::create([
      'subject' => 'Anonymous knowledge title',
      'uid' => 0,
      'name' => 'anonymous',
      'mail' => 'test@example.com',
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'created' => 123456,
      'status' => 1,
    ]);
    $knowledge_anonymous->save();

    // @todo Expand the test coverage in https://www.drupal.org/node/2464635
    $this->assertFieldAccess('knowledge', 'kid', $knowledge->id());
    $this->assertFieldAccess('knowledge', 'kid', $knowledge_anonymous->id());
    $this->assertFieldAccess('knowledge', 'uuid', $knowledge->uuid());
    $this->assertFieldAccess('knowledge', 'subject', 'My knowledge title');
    $this->assertFieldAccess('knowledge', 'subject', 'Anonymous knowledge title');
    $this->assertFieldAccess('knowledge', 'name', 'anonymous');
    $this->assertFieldAccess('knowledge', 'mail', 'test@example.com');
    $this->assertFieldAccess('knowledge', 'uid', $user->getAccountName());
    $this->assertFieldAccess('knowledge', 'status', 'On');
  }

}
