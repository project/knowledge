<?php

namespace Drupal\Tests\knowledge\Kernel\Views;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\node\Entity\NodeType;
use Drupal\views\Tests\ViewResultAssertionTrait;
use Drupal\views\Tests\ViewTestData;
use Drupal\views\Views;

/**
 * Tests the user posted or knowledge linked filter and argument handlers.
 *
 * @group knowledge
 */
class FilterAndArgumentUserUidTest extends KernelTestBase {

  use KnowledgeTestTrait;
  use NodeCreationTrait;
  use UserCreationTrait;
  use ViewResultAssertionTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'filter',
    'knowledge_test_views',
    'knowledge',
    'node',
    'options',
    'search_api',
    'system',
    'text',
    'user',
    'views',
    'workflows',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_knowledge_user_uid'];

  /**
   * Tests the user posted or knowledge linked filter and argument handlers.
   */
  public function testHandlers() {
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('knowledge');
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
    $this->installConfig(['filter']);

    NodeType::create(['type' => 'page'])->save();

    FieldStorageConfig::create([
      'type' => 'text_long',
      'entity_type' => 'knowledge',
      'field_name' => 'knowledge_body',
    ])->save();
    $this->addDefaultKnowledgeField('node', 'page', 'knowledge');

    $account = $this->createUser();
    $other_account = $this->createUser();

    $node_authored_by_account = $this->createNode([
      'uid' => $account->id(),
      'title' => "authored by {$account->id()}",
    ]);
    $node_knowledgeed_by_account = $this->createNode([
      'title' => "knowledge linked by {$account->id()}",
    ]);
    $arbitrary_node = $this->createNode();

    // Knowledge added by $account.
    Knowledge::create([
      'uid' => $account->id(),
      'entity_id' => $node_knowledgeed_by_account->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ])->save();
    // Knowledge added by $other_account on $node_knowledgeed_by_account.
    Knowledge::create([
      'uid' => $other_account->id(),
      'entity_id' => $node_knowledgeed_by_account->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ])->save();
    // Knowledge added by $other_account on an arbitrary node.
    Knowledge::create([
      'uid' => $other_account->id(),
      'entity_id' => $arbitrary_node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ])->save();

    ViewTestData::createTestViews(static::class, ['knowledge_test_views']);

    $expected_result = [
      [
        'nid' => $node_authored_by_account->id(),
        'title' => "authored by {$account->id()}",
      ],
      [
        'nid' => $node_knowledgeed_by_account->id(),
        'title' => "knowledge linked by {$account->id()}",
      ],
    ];
    $column_map = ['nid' => 'nid', 'title' => 'title'];
    $view = Views::getView('test_knowledge_user_uid');

    // Test the argument handler.
    $view->preview(NULL, [$account->id()]);
    $this->assertIdenticalResultset($view, $expected_result, $column_map);

    // Test the filter handler. Reuse the same view but replace the argument
    // handler with a filter handler.
    $view->removeHandler('default', 'argument', 'uid_touch');
    $options = [
      'id' => 'uid_touch',
      'table' => 'node_field_data',
      'field' => 'uid_touch',
      'value' => [$account->id()],
    ];
    $view->addHandler('default', 'filter', 'node_field_data', 'uid_touch', $options);

    $view->preview();
    $this->assertIdenticalResultset($view, $expected_result, $column_map);
  }

}
