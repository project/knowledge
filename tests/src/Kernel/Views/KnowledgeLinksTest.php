<?php

namespace Drupal\Tests\knowledge\Kernel\Views;

use Drupal\Core\Link;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\Url;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\KnowledgeManagerInterface;
use Drupal\views\Views;

/**
 * Tests the knowledge link field handlers.
 *
 * @group knowledge
 */
class KnowledgeLinksTest extends KnowledgeViewsKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'workflows',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_knowledge'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    $this->installEntitySchema('entity_test');
  }

  /**
   * Tests the knowledge approve link.
   */
  public function testLinkApprove() {
    $host = EntityTest::create(['name' => $this->randomString()]);
    $host->save();

    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = $this->knowledgeStorage->create([
      'uid' => $this->adminUser->id(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'status' => 0,
    ]);
    $knowledge->save();

    $view = Views::getView('test_knowledge');
    $view->setDisplay();

    $view->displayHandlers->get('default')->overrideOption('fields', [
      'approve_knowledge' => [
        'table' => 'knowledge',
        'field' => 'approve_knowledge',
        'id' => 'approve_knowledge',
        'plugin_id' => 'knowledge_link_approve',
      ],
    ]);
    $view->save();

    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo($this->adminUser);

    $view->preview();

    // Check if I can see the knowledge approve link on an unapproved knowledge.
    $approve_knowledge = $view->style_plugin->getField(0, 'approve_knowledge');
    $options = ['query' => ['destination' => '/']];
    $url = Url::fromRoute('knowledge.approve', ['knowledge' => $knowledge->id()], $options);
    $this->assertEquals((string) $approve_knowledge, Link::fromTextAndUrl('Approve', $url)->toString(), 'Found a knowledge approve link for an unapproved knowledge.');

    // Approve the knowledge.
    $knowledge->setPublished();
    $knowledge->save();
    $view = Views::getView('test_knowledge');
    $view->preview();

    // Check if I can see the knowledge approve link on an approved knowledge.
    $approve_knowledge = $view->style_plugin->getField(1, 'approve_knowledge');
    $this->assertEmpty((string) $approve_knowledge, "Didn't find a knowledge approve link for an already approved knowledge.");

    // Check if I can see the knowledge approve link on an approved knowledge as
    // an anonymous user.
    $account_switcher->switchTo(new AnonymousUserSession());
    // Set the knowledge as unpublished again.
    $knowledge->setUnpublished();
    $knowledge->save();

    $view = Views::getView('test_knowledge');
    $view->preview();
    $replyto_knowledge = $view->style_plugin->getField(0, 'approve_knowledge');
    $this->assertEmpty((string) $replyto_knowledge, "I can't approve the knowledge as an anonymous user.");
  }

  /**
   * Tests the knowledge reply link.
   */
  public function testLinkReply() {
    $this->enableModules(['field']);
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
    $this->installConfig(['field']);

    $field_storage_knowledge = FieldStorageConfig::create([
      'field_name' => 'knowledge',
      'type' => 'knowledge',
      'entity_type' => 'entity_test',
    ]);
    $field_storage_knowledge->save();
    // Create a knowledge field which allows threading.
    $field_knowledge = FieldConfig::create([
      'field_name' => 'knowledge',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'settings' => [
        'default_mode' => KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED,
      ],
    ]);
    $field_knowledge->save();

    $host = EntityTest::create(['name' => $this->randomString()]);
    $host->save();

    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = $this->knowledgeStorage->create([
      'uid' => $this->adminUser->id(),
      'entity_type' => 'entity_test',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'field_name' => $field_storage_knowledge->getName(),
      'status' => 0,
    ]);
    $knowledge->save();

    $view = Views::getView('test_knowledge');
    $view->setDisplay();

    $view->displayHandlers->get('default')->overrideOption('fields', [
      'replyto_knowledge' => [
        'table' => 'knowledge',
        'field' => 'replyto_knowledge',
        'id' => 'replyto_knowledge',
        'plugin_id' => 'knowledge_link_reply',
        'entity_type' => 'knowledge',
      ],
    ]);
    $view->save();

    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo($this->adminUser);
    $view->preview();

    // Check if I can see the reply link on an unapproved knowledge.
    $replyto_knowledge = $view->style_plugin->getField(0, 'replyto_knowledge');
    $this->assertEmpty((string) $replyto_knowledge, "I can't reply to an unapproved knowledge.");

    // Approve the knowledge.
    $knowledge->setPublished();
    $knowledge->save();
    $view = Views::getView('test_knowledge');
    $view->preview();

    // Check if I can see the reply link on an approved knowledge.
    $replyto_knowledge = $view->style_plugin->getField(0, 'replyto_knowledge');
    $url = Url::fromRoute('knowledge.reply', [
      'entity_type' => 'entity_test',
      'entity' => $host->id(),
      'field_name' => 'knowledge',
    ]);
    $this->assertEquals((string) $replyto_knowledge, Link::fromTextAndUrl('Reply', $url)->toString(), 'Found the knowledge reply link as an admin user.');

    // Check if I can see the reply link as an anonymous user.
    $account_switcher->switchTo(new AnonymousUserSession());
    $view = Views::getView('test_knowledge');
    $view->preview();
    $replyto_knowledge = $view->style_plugin->getField(0, 'replyto_knowledge');
    $this->assertEmpty((string) $replyto_knowledge, "Didn't find the knowledge reply link as an anonymous user.");
  }

}
