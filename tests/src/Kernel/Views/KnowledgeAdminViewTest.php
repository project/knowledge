<?php

namespace Drupal\Tests\knowledge\Kernel\Views;

use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\views\Views;

/**
 * Tests knowledge admin view filters.
 *
 * @group knowledge
 */
class KnowledgeAdminViewTest extends ViewsKernelTestBase {

  /**
   * Knowledge.
   *
   * @var \Drupal\knowledge\Entity\Knowledge[]
   */
  protected $knowledge = [];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'language',
    'locale',
    'node',
    'options',
    'search_api',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    $this->installEntitySchema('user');
    $this->installEntitySchema('knowledge');
    $this->installEntitySchema('entity_test');
    // Create the anonymous role.
    $this->installConfig(['user']);

    // Create user 1 so that the user created later in the test has a different
    // user ID.
    // @todo Remove in https://www.drupal.org/node/540008.
    User::create(['uid' => 1, 'name' => 'user1'])->save();

    // Enable another language.
    ConfigurableLanguage::createFromLangcode('ur')->save();
    // Rebuild the container to update the default language container variable.
    $this->container->get('kernel')->rebuildContainer();

    // Create an anonymous user.
    $storage = \Drupal::entityTypeManager()->getStorage('user');
    // Insert a row for the anonymous user.
    $storage
      ->create([
        'uid' => 0,
        'name' => '',
        'status' => 0,
      ])
      ->save();
    // Created admin role.
    $admin_role = Role::create([
      'id' => 'admin',
      'permissions' => ['administer knowledge', 'skip knowledge approval'],
      'label' => 'Admin',
    ]);
    $admin_role->save();
    // Create the admin user.
    $this->adminUser = User::create([
      'name' => $this->randomMachineName(),
      'roles' => [$admin_role->id()],
    ]);
    $this->adminUser->save();
    // Create a knowledge type.
    KnowledgeType::create([
      'id' => 'knowledge',
      'label' => 'Default knowledge',
      'target_entity_type_id' => 'entity_test',
      'description' => 'Default knowledge field',
    ])->save();
    // Create a linked entity.
    $entity = EntityTest::create();
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Create some knowledge.
    $knowledge = Knowledge::create([
      'subject' => 'My knowledge title',
      'uid' => $this->adminUser->id(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'knowledge_type' => 'knowledge',
      'status' => 1,
      'entity_id' => $entity->id(),
    ]);
    $knowledge->save();

    $this->knowledge[] = $knowledge;

    $knowledge_anonymous = Knowledge::create([
      'subject' => 'Anonymous knowledge title',
      'uid' => 0,
      'name' => 'barry',
      'mail' => 'test@example.com',
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'knowledge_type' => 'knowledge',
      'created' => 123456,
      'status' => 1,
      'entity_id' => $entity->id(),
    ]);
    $knowledge_anonymous->save();
    $this->knowledge[] = $knowledge_anonymous;
  }

  /**
   * Tests knowledge admin view filters.
   */
  public function testFilters() {
    $this->doTestFilters('page_published');
    // Unpublish the knowledge to test the Unapproved knowledge tab.
    foreach ($this->knowledge as $knowledge) {
      $knowledge->setUnpublished();
      $knowledge->save();
    }
    $this->doTestFilters('page_unapproved');
  }

  /**
   * Tests knowledge admin view display.
   *
   * @param string $display_id
   *   The display ID.
   */
  protected function doTestFilters($display_id) {
    $knowledge = $this->knowledge[0];
    $knowledge_anonymous = $this->knowledge[1];
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');

    $account_switcher->switchTo($this->adminUser);
    $executable = Views::getView('knowledge');
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    // Assert the exposed filters on the admin page.
    $this->assertField('subject');
    $this->assertField('author_name');
    $this->assertField('langcode');

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(2, $elements, 'There are two knowledge on the page.');
    $this->assertText($knowledge->label());
    $this->assertText($knowledge_anonymous->label());
    $executable->destroy();

    // Test the Subject filter.
    $executable->setExposedInput(['subject' => 'Anonymous']);
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(1, $elements, 'Only anonymous knowledge is visible.');
    $this->assertNoText($knowledge->label());
    $this->assertText($knowledge_anonymous->label());
    $executable->destroy();

    $executable->setExposedInput(['subject' => 'My knowledge']);
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(1, $elements, 'Only admin knowledge is visible.');
    $this->assertText($knowledge->label());
    $this->assertNoText($knowledge_anonymous->label());
    $executable->destroy();

    // Test the combine filter using author name.
    $executable->setExposedInput(['author_name' => 'barry']);
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(1, $elements, 'Only anonymous knowledge is visible.');
    $this->assertNoText($knowledge->label());
    $this->assertText($knowledge_anonymous->label());
    $executable->destroy();

    // Test the combine filter using username.
    $executable->setExposedInput(['author_name' => $this->adminUser->label()]);
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(1, $elements, 'Only admin knowledge is visible.');
    $this->assertText($knowledge->label());
    $this->assertNoText($knowledge_anonymous->label());
    $executable->destroy();

    // Test the language filter.
    $executable->setExposedInput(['langcode' => '***LANGUAGE_site_default***']);
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(2, $elements, 'Both knowledge are visible.');
    $this->assertText($knowledge->label());
    $this->assertText($knowledge_anonymous->label());
    $executable->destroy();

    // Tests knowledge translation filter.
    if (!$knowledge->hasTranslation('ur')) {
      // If we don't have the translation then create one.
      $knowledge_translation = $knowledge->addTranslation('ur', ['subject' => 'ur title']);
      $knowledge_translation->save();
    }
    else {
      // If we have the translation then unpublish it.
      $knowledge_translation = $knowledge->getTranslation('ur');
      $knowledge_translation->setUnpublished();
      $knowledge_translation->save();
    }
    if (!$knowledge_anonymous->hasTranslation('ur')) {
      // If we don't have the translation then create one.
      $knowledge_anonymous_translation = $knowledge_anonymous->addTranslation('ur', ['subject' => 'ur Anonymous title']);
      $knowledge_anonymous_translation->save();
    }
    else {
      // If we have the translation then unpublish it.
      $knowledge_anonymous_translation = $knowledge_anonymous->getTranslation('ur');
      $knowledge_anonymous_translation->setUnpublished();
      $knowledge_anonymous_translation->save();
    }

    $executable->setExposedInput(['langcode' => 'ur']);
    $build = $executable->preview($display_id);
    $this->setRawContent($renderer->renderRoot($build));

    $elements = $this->cssSelect('input[type="checkbox"]');
    $this->assertCount(2, $elements, 'Both knowledge are visible.');
    $this->assertNoText($knowledge->label());
    $this->assertNoText($knowledge_anonymous->label());
    $this->assertText($knowledge_translation->label());
    $this->assertText($knowledge_anonymous_translation->label());
    $executable->destroy();
  }

}
