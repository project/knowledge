<?php

namespace Drupal\Tests\knowledge\Kernel\Views;

use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;
use Drupal\views\Views;

/**
 * Tests knowledge user name field.
 *
 * @group knowledge
 */
class KnowledgeUserNameTest extends ViewsKernelTestBase {

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    $this->installEntitySchema('user');
    $this->installEntitySchema('knowledge');
    $this->installEntitySchema('entity_test');
    // Create the anonymous role.
    $this->installConfig(['user']);

    // Create an anonymous user.
    $storage = \Drupal::entityTypeManager()->getStorage('user');
    // Insert a row for the anonymous user.
    $storage
      ->create([
        'uid' => 0,
        'name' => '',
        'status' => 0,
      ])
      ->save();

    $admin_role = Role::create([
      'id' => 'admin',
      'permissions' => ['administer knowledge', 'access user profiles'],
      'label' => 'Admin',
    ]);
    $admin_role->save();

    /** @var \Drupal\user\RoleInterface $anonymous_role */
    $anonymous_role = Role::load(Role::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access knowledge');
    $anonymous_role->save();

    $this->adminUser = User::create([
      'name' => $this->randomMachineName(),
      'roles' => [$admin_role->id()],
    ]);
    $this->adminUser->save();

    $host = EntityTest::create(['name' => $this->randomString()]);
    $host->save();

    // Create some knowledge.
    $knowledge = Knowledge::create([
      'subject' => 'My knowledge title',
      'uid' => $this->adminUser->id(),
      'name' => $this->adminUser->label(),
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'status' => 1,
    ]);
    $knowledge->save();

    $knowledge_anonymous = Knowledge::create([
      'subject' => 'Anonymous knowledge title',
      'uid' => 0,
      'name' => 'barry',
      'mail' => 'test@example.com',
      'entity_type' => 'entity_test',
      'field_name' => 'knowledge',
      'entity_id' => $host->id(),
      'knowledge_type' => 'entity_test',
      'created' => 123456,
      'status' => 1,
    ]);
    $knowledge_anonymous->save();
  }

  /**
   * Tests the username formatter.
   */
  public function testUsername() {
    $view_id = $this->randomMachineName();
    $view = View::create([
      'id' => $view_id,
      'base_table' => 'knowledge',
      'display' => [
        'default' => [
          'display_plugin' => 'default',
          'id' => 'default',
          'display_options' => [
            'fields' => [
              'name' => [
                'table' => 'knowledge',
                'field' => 'name',
                'id' => 'name',
                'plugin_id' => 'field',
                'type' => 'knowledge_username',
              ],
              'subject' => [
                'table' => 'knowledge',
                'field' => 'subject',
                'id' => 'subject',
                'plugin_id' => 'field',
                'type' => 'string',
                'settings' => [
                  'link_to_entity' => TRUE,
                ],
              ],
            ],
          ],
        ],
      ],
    ]);
    $view->save();

    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');

    $account_switcher->switchTo($this->adminUser);
    $executable = Views::getView($view_id);
    $build = $executable->preview();
    $this->setRawContent($renderer->renderRoot($build));

    $this->assertLink('My knowledge title');
    $this->assertLink('Anonymous knowledge title');
    // Display plugin of the view is showing the name field. When knowledge
    // belongs to an authenticated user the name field has no value.
    $knowledge_author = $this->xpath('//div[contains(@class, :class)]/span[normalize-space(text())=""]', [
      ':class' => 'views-field-subject',
    ]);
    $this->assertNotEmpty($knowledge_author);
    // When knowledge belongs to an anonymous user the name field has a value
    // and it is rendered correctly.
    $this->assertLink('barry (not verified)');

    $account_switcher->switchTo(new AnonymousUserSession());
    $executable = Views::getView($view_id);
    $executable->storage->invalidateCaches();

    $build = $executable->preview();
    $this->setRawContent($renderer->renderRoot($build));

    // No access to user-profiles, so shouldn't be able to see links.
    $this->assertNoLink($this->adminUser->label());
    // Note: External users aren't pointing to drupal user profiles.
    $this->assertLink('barry (not verified)');
    $this->assertLink('My knowledge title');
    $this->assertLink('Anonymous knowledge title');
  }

}
