<?php

namespace Drupal\Tests\knowledge\Kernel\Views;

use Drupal\Core\Render\RenderContext;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\node\Entity\NodeType;
use Drupal\views\Tests\ViewResultAssertionTrait;
use Drupal\views\Tests\ViewTestData;
use Drupal\views\Views;

/**
 * Tests the knowledge field name field.
 *
 * @group knowledge
 */
class KnowledgeFieldNameTest extends KernelTestBase {

  use KnowledgeTestTrait;
  use NodeCreationTrait;
  use UserCreationTrait;
  use ViewResultAssertionTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'filter',
    'knowledge_test_views',
    'knowledge',
    'node',
    'node',
    'options',
    'search_api',
    'system',
    'text',
    'user',
    'views',
    'workflows',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_knowledge_field_name'];

  /**
   * Tests knowledge field name.
   */
  public function testKnowledgeFieldName() {
    $renderer = $this->container->get('renderer');

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('knowledge');
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
    $this->installConfig(['filter']);

    NodeType::create(['type' => 'page'])->save();
    FieldStorageConfig::create([
      'type' => 'text_long',
      'entity_type' => 'knowledge',
      'field_name' => 'knowledge_body',
    ])->save();
    $this->addDefaultKnowledgeField('node', 'page', 'knowledge');
    $this->addDefaultKnowledgeField('node', 'page', 'knowledge_custom');

    ViewTestData::createTestViews(static::class, ['knowledge_test_views']);

    $node = $this->createNode();
    $knowledge = Knowledge::create([
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge',
    ]);
    $knowledge->save();
    $knowledge2 = Knowledge::create([
      'entity_id' => $node->id(),
      'entity_type' => 'node',
      'field_name' => 'knowledge_custom',
    ]);
    $knowledge2->save();

    $view = Views::getView('test_knowledge_field_name');
    $view->preview();

    $expected_result = [
      [
        'kid' => $knowledge->id(),
        'field_name' => $knowledge->getFieldName(),
      ],
      [
        'kid' => $knowledge2->id(),
        'field_name' => $knowledge2->getFieldName(),
      ],
    ];
    $column_map = [
      'kid' => 'kid',
      'knowledge_field_data_field_name' => 'field_name',
    ];
    $this->assertIdenticalResultset($view, $expected_result, $column_map);

    // Test that data rendered correctly.
    $expected_output = $renderer->executeInRenderContext(new RenderContext(), function () use ($view) {
      return $view->field['field_name']->advancedRender($view->result[0]);
    });
    $this->assertEquals($expected_output, $knowledge->getFieldName());
    $expected_output = $renderer->executeInRenderContext(new RenderContext(), function () use ($view) {
      return $view->field['field_name']->advancedRender($view->result[1]);
    });
    $this->assertEquals($expected_output, $knowledge2->getFieldName());
  }

}
