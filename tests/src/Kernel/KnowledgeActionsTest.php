<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\filter\Entity\FilterFormat;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\system\Entity\Action;

/**
 * Tests actions provided by the Knowledge module.
 *
 * @group knowledge
 */
class KnowledgeActionsTest extends EntityKernelTestBase {
  use KnowledgeTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'workflows',
  ];

  /**
   * Keywords used for testing.
   *
   * @var string[]
   */
  protected $keywords;

  /**
   * The knowledge entity.
   *
   * @var \Drupal\knowledge\KnowledgeInterface
   */
  protected $knowledge;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['user', 'knowledge']);
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);

    // Create a knowledge type.
    KnowledgeType::create([
      'id' => 'knowledge',
      'label' => 'Default knowledge',
      'description' => 'Default knowledge field',
      'target_entity_type_id' => 'entity_test',
    ])->save();
    $this->addDefaultKnowledgeField('entity_test', 'entity_test', 'knowledge');

    // Setup date format to render knowledge date.
    DateFormat::create([
      'id' => 'fallback',
      'pattern' => 'D, m/d/Y - H:i',
    ])->save();

    // Create format without filters to prevent filtering.
    FilterFormat::create([
      'format' => 'no_filters',
      'name' => 'No filters',
      'filters' => [],
    ])->save();

    // Set current user to allow filters display knowledge body.
    $this->drupalSetCurrentUser($this->drupalCreateUser());

    $this->keywords = [$this->randomMachineName(), $this->randomMachineName()];

    // Create a knowledge against a test entity.
    $host = EntityTest::create();
    $host->save();

    $this->knowledge = Knowledge::create([
      'entity_type' => 'entity_test',
      'entity_id' => $host->id(),
      'knowledge_type' => 'knowledge',
      'field_name' => 'knowledge',
    ]);
    $this->knowledge->setPublished();
  }

  /**
   * Tests knowledge module's default config actions.
   *
   * @see \Drupal\Core\Entity\Form\DeleteMultipleForm::submitForm()
   * @see \Drupal\Core\Action\Plugin\Action\DeleteAction
   * @see \Drupal\Core\Action\Plugin\Action\Derivative\EntityDeleteActionDeriver
   * @see \Drupal\Core\Action\Plugin\Action\PublishAction
   * @see \Drupal\Core\Action\Plugin\Action\SaveAction
   */
  public function testKnowledgeDefaultConfigActions() {
    $this->assertTrue($this->knowledge->isNew());
    $action = Action::load('knowledge_save_action');
    $action->execute([$this->knowledge]);
    $this->assertFalse($this->knowledge->isNew());

    $this->assertTrue($this->knowledge->isPublished());
    // Tests knowledge unpublish.
    $action = Action::load('knowledge_unpublish_action');
    $action->execute([$this->knowledge]);
    $this->assertFalse($this->knowledge->isPublished(), 'Knowledge was unpublished');
    $this->assertSame(['module' => ['knowledge']], $action->getDependencies());
    // Tests knowledge publish.
    $action = Action::load('knowledge_publish_action');
    $action->execute([$this->knowledge]);
    $this->assertTrue($this->knowledge->isPublished(), 'Knowledge was published');

    $action = Action::load('knowledge_delete_action');
    $action->execute([$this->knowledge]);
    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store */
    $temp_store = $this->container->get('tempstore.private');
    $account_id = $this->container->get('current_user')->id();
    $store_entries = $temp_store->get('entity_delete_multiple_confirm')->get($account_id . ':knowledge');
    $this->assertSame([$account_id => ['en' => 'en']], $store_entries);
  }

  /**
   * Tests the unpublish knowledge by keyword action.
   *
   * @see \Drupal\knowledge\Plugin\Action\UnpublishByKeywordKnowledge
   */
  public function testKnowledgeUnpublishByKeyword() {
    $this->knowledge->save();
    $action = Action::create([
      'id' => 'knowledge_unpublish_by_keyword_action',
      'label' => $this->randomMachineName(),
      'type' => 'knowledge',
      'plugin' => 'knowledge_unpublish_by_keyword_action',
    ]);

    // Tests no keywords.
    $action->execute([$this->knowledge]);
    $this->assertTrue($this->knowledge->isPublished(), 'The knowledge status was set to published.');

    // Tests keyword in subject.
    $action->set('configuration', ['keywords' => [$this->keywords[0]]]);
    $action->execute([$this->knowledge]);
    $this->assertFalse($this->knowledge->isPublished(), 'The knowledge status was set to not published.');

    // Tests keyword in knowledge body.
    $this->knowledge->setPublished();
    $action->set('configuration', ['keywords' => [$this->keywords[1]]]);
    $action->execute([$this->knowledge]);
    $this->assertFalse($this->knowledge->isPublished(), 'The knowledge status was set to not published.');
  }

}
