<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;

/**
 * Tests integration of knowledge with other components.
 *
 * @group knowledge
 */
class KnowledgeIntegrationTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'dblog',
    'entity_test',
    'field',
    'field',
    'knowledge',
    'node',
    'options',
    'search_api',
    'system',
    'text',
    'user',
    'workflows',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
    $this->installEntitySchema('knowledge');
    $this->installSchema('dblog', ['watchdog']);

    // Create a new 'knowledge' knowledge-type.
    KnowledgeType::create([
      'id' => 'knowledge',
      'label' => $this->randomString(),
      'target_entity_type_id' => 'entity_test',
    ])->save();
  }

  /**
   * Tests view mode setting integration.
   *
   * @see knowledge_entity_view_display_presave()
   * @see KnowledgeDefaultFormatter::calculateDependencies()
   */
  public function testViewMode() {
    $mode = mb_strtolower($this->randomMachineName());
    // Create a new knowledge view mode and a view display entity.
    EntityViewMode::create([
      'id' => "knowledge.$mode",
      'targetEntityType' => 'knowledge',
      'settings' => ['knowledge_type' => 'knowledge'],
    ])->save();
    EntityViewDisplay::create([
      'targetEntityType' => 'knowledge',
      'bundle' => 'knowledge',
      'mode' => $mode,
    ])->setStatus(TRUE)->save();

    // Create a knowledge field attached to a host 'entity_test' entity.
    FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'type' => 'knowledge',
      'field_name' => $field_name = mb_strtolower($this->randomMachineName()),
      'settings' => [
        'knowledge_type' => 'knowledge',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => $field_name,
    ])->save();

    $component = [
      'type' => 'knowledge_default',
      'settings' => ['view_mode' => $mode, 'pager_id' => 0],
    ];
    // Create a new 'entity_test' view display on host entity that uses the
    // custom knowledge display in field formatter to show the field.
    EntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
    ])->setComponent($field_name, $component)->setStatus(TRUE)->save();

    $host_display_id = 'entity_test.entity_test.default';
    $knowledge_display_id = "knowledge.knowledge.$mode";

    // Disable the "knowledge.knowledge.$mode" display.
    EntityViewDisplay::load($knowledge_display_id)->setStatus(FALSE)->save();

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $host_display */
    $host_display = EntityViewDisplay::load($host_display_id);

    // Check that the field formatter has been disabled on host view display.
    $this->assertNull($host_display->getComponent($field_name));
    $this->assertTrue($host_display->get('hidden')[$field_name]);

    // Check that the proper warning has been logged.
    $arguments = [
      '@id' => $host_display_id,
      '@name' => $field_name,
      '@display' => EntityViewMode::load("knowledge.$mode")->label(),
      '@mode' => $mode,
    ];
    $logged = Database::getConnection()->select('watchdog')
      ->fields('watchdog', ['variables'])
      ->condition('type', 'system')
      ->condition('message', "View display '@id': Knowledge field formatter '@name' was disabled because it is using the knowledge view display '@display' (@mode) that was just disabled.")
      ->execute()
      ->fetchField();
    $this->assertEquals(serialize($arguments), $logged);

    // Re-enable the knowledge view display.
    EntityViewDisplay::load($knowledge_display_id)->setStatus(TRUE)->save();
    // Re-enable the knowledge field formatter on host entity view display.
    EntityViewDisplay::load($host_display_id)->setComponent($field_name, $component)->save();

    // Delete the "knowledge.$mode" view mode.
    EntityViewMode::load("knowledge.$mode")->delete();

    // Check that the knowledge view display entity has been deleted too.
    $this->assertNull(EntityViewDisplay::load($knowledge_display_id));

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $host_display */
    $host_display = EntityViewDisplay::load($host_display_id);

    // Check that the field formatter has been disabled on host view display.
    $this->assertNull($host_display->getComponent($field_name));
    $this->assertTrue($host_display->get('hidden')[$field_name]);
  }

  /**
   * Tests the default owner of knowledge entities.
   */
  public function testKnowledgeDefaultOwner() {
    $knowledge = Knowledge::create([
      'knowledge_type' => 'knowledge',
    ]);
    $this->assertEquals(0, $knowledge->getOwnerId());

    $user = $this->createUser();
    $this->container->get('current_user')->setAccount($user);
    $knowledge = Knowledge::create([
      'knowledge_type' => 'knowledge',
    ]);
    $this->assertEquals($user->id(), $knowledge->getOwnerId());
  }

}
