<?php

namespace Drupal\Tests\knowledge\Kernel;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\Traits\Core\GeneratePermutationsTrait;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\knowledge\Entity\Knowledge;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\knowledge\KnowledgeInterface;
use Drupal\knowledge\Tests\KnowledgeTestTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests knowledge field level access.
 *
 * @group knowledge
 * @group Access
 */
class KnowledgeFieldAccessTest extends EntityKernelTestBase {

  use KnowledgeTestTrait;
  use GeneratePermutationsTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'content_moderation',
    'entity_test',
    'field',
    'knowledge',
    'knowledge_field',
    'node',
    'options',
    'search_api',
    'text',
    'user',
    'workflows',
  ];

  /**
   * Fields that only users with administer knowledge permissions can change.
   *
   * @var array
   */
  protected $administrativeFields = [
    'uid',
    'status',
    'created',
  ];

  /**
   * These fields are automatically managed and can not be changed by any user.
   *
   * @var array
   */
  protected $readOnlyFields = [
    'changed',
    'kid',
  ];

  /**
   * These fields can be edited on create only.
   *
   * @var array
   */
  protected $createOnlyFields = [
    'uuid',
    'knowledge_type',
    'entity_id',
    'entity_type',
    'field_name',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['user', 'knowledge']);
    $this->installSchema('knowledge', ['knowledge_entity_statistics']);
  }

  /**
   * Tests permissions on knowledge fields.
   */
  public function testAccessToAdministrativeFields() {
    // Create a knowledge type.
    $knowledge_type = KnowledgeType::create([
      'id' => 'knowledge',
      'label' => 'Default knowledge',
      'description' => 'Default knowledge field',
      'target_entity_type_id' => 'entity_test',
    ]);
    $knowledge_type->save();

    // Create a knowledge against a test entity.
    $host = EntityTest::create();
    $host->save();

    // An administrator user. No user exists yet, ensure that the first user
    // does not have UID 1.
    $knowledge_default_user = $this->createUser([
      'administer knowledge',
      'access knowledge',
    ], 'admin', FALSE, ['uid' => 2]);

    // Two knowledge enabled users, one with edit access.
    $knowledge_enabled_user = $this->createUser([
      'post knowledge',
      'skip knowledge approval',
      'edit own knowledge',
      'access knowledge',
    ], 'enabled');
    $knowledge_no_edit_user = $this->createUser([
      'post knowledge',
      'skip knowledge approval',
      'access knowledge',
    ], 'no edit');

    // An unprivileged user.
    $knowledge_disabled_user = $this->createUser(['access content'], 'disabled');

    $role = Role::load(RoleInterface::ANONYMOUS_ID);
    $role->grantPermission('post knowledge')
      ->save();

    $anonymous_user = new AnonymousUserSession();

    // Add two fields.
    $this->addDefaultKnowledgeField('entity_test', 'entity_test', 'knowledge');
    $this->addDefaultKnowledgeField('entity_test', 'entity_test', 'knowledge_other');

    // Change the second field's anonymous contact setting.
    /** @var \Drupal\field\FieldConfigInterface $instance */
    $instance = FieldConfig::loadByName('entity_test', 'entity_test', 'knowledge_other');
    // Default is 'May not contact', for this field - they may contact.
    $instance->setSetting('anonymous', KnowledgeInterface::ANONYMOUS_MAY_CONTACT);
    $instance->save();

    // Create three "Knowledges". One is owned by our edit-enabled user.
    $knowledge1 = Knowledge::create([
      'entity_type' => 'entity_test',
      'name' => 'Tony',
      'mail' => 'tonythemagicalpony@example.com',
      'subject' => 'Bruce the Mesopotamian moose',
      'entity_id' => $host->id(),
      'knowledge_type' => 'knowledge',
      'field_name' => 'knowledge',
      'uid' => 0,
      'status' => 1,
    ]);
    $knowledge1->save();
    $knowledge2 = Knowledge::create([
      'entity_type' => 'entity_test',
      'subject' => 'Brian the messed up lion',
      'entity_id' => $host->id(),
      'knowledge_type' => 'knowledge',
      'field_name' => 'knowledge',
      'status' => 1,
      'uid' => $knowledge_enabled_user->id(),
    ]);
    $knowledge2->save();
    $knowledge3 = Knowledge::create([
      'entity_type' => 'entity_test',
      // Unpublished.
      'status' => 0,
      'subject' => 'Gail the minky whale',
      'entity_id' => $host->id(),
      'knowledge_type' => 'knowledge',
      'field_name' => 'knowledge_other',
      'uid' => $knowledge_no_edit_user->id(),
    ]);
    $knowledge3->save();
    // Note we intentionally don't save this knowledge so it remains 'new'.
    $knowledge4 = Knowledge::create([
      'entity_type' => 'entity_test',
      // Unpublished.
      'status' => 0,
      'subject' => 'Daniel the Cocker-Spaniel',
      'entity_id' => $host->id(),
      'knowledge_type' => 'knowledge',
      'field_name' => 'knowledge_other',
      'uid' => $anonymous_user->id(),
    ]);

    // Generate permutations.
    $combinations = [
      'knowledge' => [$knowledge1, $knowledge2, $knowledge3, $knowledge4],
      'user' => [
        $knowledge_default_user,
        $knowledge_enabled_user,
        $knowledge_no_edit_user,
        $knowledge_disabled_user,
        $anonymous_user,
      ],
    ];
    $permutations = $this->generatePermutations($combinations);

    // Check access to administrative fields.
    foreach ($this->administrativeFields as $field) {
      foreach ($permutations as $set) {
        $may_view = $set['knowledge']->{$field}->access('view', $set['user']);
        $may_update = $set['knowledge']->{$field}->access('edit', $set['user']);
        $this->assertTrue($may_view, new FormattableMarkup('User @user can view field @field on knowledge @knowledge', [
          '@user' => $set['user']->getAccountName(),
          '@knowledge' => $set['knowledge']->label(),
          '@field' => $field,
        ]));
        $this->assertEquals($may_update, $set['user']->hasPermission('administer knowledge'), new FormattableMarkup('User @user @state update field @field on knowledge @knowledge', [
          '@user' => $set['user']->getAccountName(),
          '@state' => $may_update ? 'can' : 'cannot',
          '@knowledge' => $set['knowledge']->label(),
          '@field' => $field,
        ]));
      }
    }

    // Check access to normal field.
    foreach ($permutations as $set) {
      $may_update = $set['knowledge']->access('update', $set['user']) && $set['knowledge']->subject->access('edit', $set['user']);
      $this->assertEquals($may_update, $set['user']->hasPermission('administer knowledge') || ($set['user']->hasPermission('edit own knowledge') && $set['user']->id() == $set['knowledge']->getOwnerId()), new FormattableMarkup('User @user @state update field subject on knowledge @knowledge', [
        '@user' => $set['user']->getAccountName(),
        '@state' => $may_update ? 'can' : 'cannot',
        '@knowledge' => $set['knowledge']->label(),
      ]));
    }

    // Check read-only fields.
    foreach ($this->readOnlyFields as $field) {
      // Check view operation.
      foreach ($permutations as $set) {
        $may_view = $set['knowledge']->{$field}->access('view', $set['user']);
        $may_update = $set['knowledge']->{$field}->access('edit', $set['user']);
        $view_access = TRUE;
        $state = 'can';

        $this->assertEquals($may_view, $view_access, new FormattableMarkup('User @user @state view field @field on knowledge @knowledge', [
          '@user' => $set['user']->getAccountName(),
          '@knowledge' => $set['knowledge']->label(),
          '@field' => $field,
          '@state' => $state,
        ]));
        $this->assertFalse($may_update, new FormattableMarkup('User @user @state update field @field on knowledge @knowledge', [
          '@user' => $set['user']->getAccountName(),
          '@state' => $may_update ? 'can' : 'cannot',
          '@knowledge' => $set['knowledge']->label(),
          '@field' => $field,
        ]));
      }
    }

    // Check create-only fields.
    foreach ($this->createOnlyFields as $field) {
      // Check view operation.
      foreach ($permutations as $set) {
        $may_view = $set['knowledge']->{$field}->access('view', $set['user']);
        $may_update = $set['knowledge']->{$field}->access('edit', $set['user']);
        $this->assertTrue($may_view, new FormattableMarkup('User @user can view field @field on knowledge @knowledge', [
          '@user' => $set['user']->getAccountName(),
          '@knowledge' => $set['knowledge']->label(),
          '@field' => $field,
        ]));
        $this->assertEquals($may_update, $set['user']->hasPermission('post knowledge') && $set['knowledge']->isNew(), new FormattableMarkup('User @user @state update field @field on knowledge @knowledge', [
          '@user' => $set['user']->getAccountName(),
          '@state' => $may_update ? 'can' : 'cannot',
          '@knowledge' => $set['knowledge']->label(),
          '@field' => $field,
        ]));
      }
    }

  }

}
