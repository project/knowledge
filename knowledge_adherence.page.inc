<?php

/**
 * @file
 * Contains knowledge_adherence.page.inc.
 *
 * Page callback for Adherence entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Adherence templates.
 *
 * Default template: knowledge_adherence.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_knowledge_adherence(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
