/**
 * @file
 * Attaches knowledge behaviors to the entity form.
 */

(function ($, Drupal) {
  /**
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.knowledgeFieldsetSummaries = {
    attach(context) {
      const $context = $(context);
      $context
        .find('fieldset.knowledge-entity-settings-form')
        .drupalSetSummary((context) =>
          Drupal.checkPlain(
            $(context)
              .find('.js-form-item-knowledge input:checked')
              .next('label')[0].textContent,
          ),
        );
    },
  };
})(jQuery, Drupal);
