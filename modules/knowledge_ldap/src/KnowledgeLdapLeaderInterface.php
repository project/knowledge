<?php

namespace Drupal\knowledge_ldap;

use Drupal\ldap_servers\Entity\Server;
use Drupal\user\UserInterface;
use Symfony\Component\Ldap\Entry;

/**
 * Knowledge Ldap Leader Interface.
 */
interface KnowledgeLdapLeaderInterface {

  /**
   * Update the leader field.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   * @param \Symfony\Component\Ldap\Entry $ldap_entry
   *   The ldap entry.
   * @param \Drupal\ldap_servers\Entity\Server $server
   *   The ldap server.
   */
  public function updateLeaderField(UserInterface $account, Entry $ldap_entry, Server $server);

}
