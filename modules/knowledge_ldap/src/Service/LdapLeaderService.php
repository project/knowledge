<?php

namespace Drupal\knowledge_ldap\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\knowledge_ldap\KnowledgeLdapLeaderInterface;
use Drupal\ldap_servers\Entity\Server;
use Drupal\ldap_servers\LdapBridgeInterface;
use Drupal\ldap_servers\LdapGroupManager;
use Drupal\ldap_servers\Processor\TokenProcessor;
use Drupal\ldap_user\Processor\DrupalUserProcessor;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Ldap\Entry;

/**
 * Leadership Service.
 */
class LdapLeaderService implements KnowledgeLdapLeaderInterface {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The knowledge_ldap settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The token processor service.
   *
   * @var \Drupal\ldap_servers\Processor\TokenProcessor
   */
  protected $tokenProcessor;

  /**
   * The ldap bridge service.
   *
   * @var \Drupal\ldap_servers\LdapBridgeInterface
   */
  protected $ldapBridge;

  /**
   * The ldap group manager service.
   *
   * @var \Drupal\ldap_servers\LdapGroupManager
   */
  protected $ldapGroupManager;

  /**
   * The user storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The ldap server entity.
   *
   * @var \Drupal\ldap_servers\Entity\Server
   */
  protected $server;

  /**
   * The ldap user processor service.
   *
   * @var \Drupal\ldap_user\Processor\DrupalUserProcessor
   */
  protected $userProcessor;

  /**
   * Constructs a new LeaderService object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\ldap_servers\Processor\TokenProcessor $token_processor
   *   The token processor service.
   * @param \Drupal\ldap_servers\LdapBridgeInterface $ldap_bridge
   *   The ldap bridge service.
   * @param \Drupal\ldap_servers\LdapGroupManager $ldap_group_manager
   *   The ldap group manager service.
   * @param \Drupal\ldap_user\Processor\DrupalUserProcessor $ldap_user_processor
   *   The ldap user processor service.
   */
  public function __construct(
    LoggerInterface $logger,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    TokenProcessor $token_processor,
    LdapBridgeInterface $ldap_bridge,
    LdapGroupManager $ldap_group_manager,
    DrupalUserProcessor $ldap_user_processor,
  ) {

    $this->logger = $logger;
    $this->settings = $config_factory->get('knowledge_ldap.settings');
    $this->tokenProcessor = $token_processor;
    $this->ldapBridge = $ldap_bridge;
    $this->ldapGroupManager = $ldap_group_manager;
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->userProcessor = $ldap_user_processor;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLeaderField(UserInterface $user, Entry $ldap_entry, Server $ldap_server) {

    $this->server = $ldap_server;
    $leader_token = $this->settings->get('leader_token');
    $leader_attribute = sprintf('[%s]', $leader_token);
    $this->tokenProcessor->ldapEntryReplacementsForDrupalAccount(
      $ldap_entry,
      $leader_attribute
    );
    $leader_dn = '';
    foreach ($this->tokenProcessor->getTokens() as $key => $value) {
      if ($key == $leader_attribute) {
        $leader_dn = $value;
      }
    }

    if ($user->get('ldap_user_current_dn')->value == $leader_dn) {
      return;
    }

    $leader_id = $this->lookupExistingUser($leader_dn);

    if (FALSE && !$leader_id) {
      $leader_id = $this->createUser($leader_dn);
    }
    if ($leader_id != $user->get('knowledge_leader')->target_id) {
      $user->set('knowledge_leader', $leader_id);
    }
  }

  /**
   * Returns the user if it exists.
   */
  protected function lookupExistingUser(string $ldap_dn) {
    $this->userStorage->resetCache();
    $leader_id = $this->userStorage->getQuery()
      ->condition('ldap_user_current_dn', $ldap_dn)
      ->range(0, 1)
      ->accessCheck(FALSE)
      ->execute();

    return reset($leader_id);
  }

  /**
   * Creates a user.
   */
  protected function createUser(string $leader_dn) {
    $dn_array = explode(',', $leader_dn);
    $cn = '';
    foreach ($dn_array as $dn) {
      if (strpos($dn, 'CN=') === 0) {
        $cn = substr($dn, 3);
      }
    }

    $base_dn_array = $this->server->getBaseDn();
    $username_attribute = $this->server->getAuthenticationNameAttribute();

    $filter = "(&(objectclass=user)(cn=$cn))";
    $options = [
      'filter' => [
        'objectclass',
        'name',
        'cn',
        $username_attribute,
      ],
    ];

    $username = NULL;
    foreach ($base_dn_array as $base_dn) {
      $response = $this->ldapBridge->get()
        ->query($base_dn, $filter, $options)
        ->execute()
        ->toArray();
      if (count($response) > 0) {
        foreach ($response as $entry) {
          $username = $entry->getAttribute($username_attribute, FALSE);
        }
        break;
      }
    }

    if (empty($username)) {
      return NULL;
    }

    $user_values = [
      'name' => $username[0],
      'ldap_user_current_dn' => $leader_dn,
      'status' => 1,
    ];

    $this->userProcessor->createDrupalUserFromLdapEntry($user_values);

    return $this->lookupExistingUser($leader_dn);
  }

}
