<?php

namespace Drupal\knowledge_field\Helper;

/**
 * Helper class for the knowledge competency entity.
 */
class CompetencyField {

  /**
   * Get the field definitions for the knowledge competency entity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions
   *   The field definitions.
   *
   * @return array
   *   The field definitions for the knowledge competency entity.
   */
  public static function roleFields($field_definitions) {
    $fields = [];
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_definition->getType() != 'boolean') {
        continue;
      }
      if (get_class($field_definition) != 'Drupal\field\Entity\FieldConfig') {
        continue;
      }
      $role = $field_definition->getThirdPartySetting('knowledge', 'competency_role', '_none');
      if ($role == '_none') {
        continue;
      }
      $fields[$role][] = $field_name;
    }

    return $fields;
  }

}
