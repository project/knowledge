<?php

declare(strict_types=1);

namespace Drupal\knowledge_field\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\knowledge_field\Helper\CompetencyField;

/**
 * Defines the 'knowledge_competency_role' field type.
 *
 * @FieldType(
 *   id = "knowledge_competency_role",
 *   label = @Translation("Competency Roles"),
 *   description = @Translation("Tracks role progress."),
 *   default_widget = "knowledge_competency_role",
 *   default_formatter = "knowledge_competency_role",
 *   no_ui = TRUE,
 * )
 */
final class KnowledgeCompetencyRoleItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'role';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return match ($this->get('role')->getValue()) {
      NULL, '' => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'user',
      'title' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'handler' => 'default',
      'target_type' => 'user',
      'handler_settings' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetDefinition() {
    return $this->targetDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['role'] = DataDefinition::create('string')
      ->setLabel(t('Role'))
      ->setRequired(TRUE);

    $properties['correct'] = DataDefinition::create('integer')
      ->setLabel(t('Correct'))
      ->setRequired(TRUE);

    $properties['total'] = DataDefinition::create('integer')
      ->setLabel(t('Total'))
      ->setRequired(TRUE);

    $properties['proposer'] = DataDefinition::create('entity_reference')
      ->setLabel(t('Proposer'))
      ->setSetting('target_type', 'user')
      ->setRequired(FALSE);

    $properties['approver'] = DataDefinition::create('entity_reference')
      ->setLabel(t('Approver'))
      ->setSetting('target_type', 'user')
      ->setRequired(FALSE);

    $properties['proposed'] = DataDefinition::create('integer')
      ->setLabel(t('Proposed'))
      ->setRequired(FALSE);

    $properties['approved'] = DataDefinition::create('integer')
      ->setLabel(t('Approved'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    $constraints = parent::getConstraints();

    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();

    // @DCG Suppose our value must not be longer than 10 characters.
    // $options['value']['Length']['max'] = 10;
    // @DCG
    // See /core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint
    // directory for available constraints.
    // $constraints[] = $constraint_manager->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    $columns = [
      'role' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Column description.',
        'length' => 255,
      ],
      'correct' => [
        'type' => 'int',
        'not null' => FALSE,
        'description' => 'Column description.',
      ],
      'total' => [
        'type' => 'int',
        'not null' => FALSE,
        'description' => 'Column description.',
      ],
      'proposer' => [
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'description' => 'The user who proposed the advancement.',
      ],
      'approver' => [
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'description' => 'The users that approved the advancement.',
      ],
      'proposed' => [
        'type' => 'int',
        'not null' => FALSE,
        'description' => 'When advancement is proposed.',
      ],
      'approved' => [
        'type' => 'int',
        'not null' => FALSE,
        'description' => 'When advancement is approved.',
      ],

    ];

    $schema = [
      'columns' => $columns,
      // @todo Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $entity = $this->getEntity();
    $definitions = $entity->getFieldDefinitions();
    $roles = CompetencyField::roleFields($definitions);
    $role = $this->values['role'];
    $fields = $roles[$role] ?? [];
    $this->total = 0;
    $this->correct = 0;
    foreach ($fields as $field) {
      $value = $entity->get($field)->value;
      if ($value) {
        $this->correct += 1;
      }
      $this->total += 1;
    }

    parent::preSave();
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $random = new Random();
    $values['role'] = $random->word(mt_rand(1, 50));
    return $values;
  }

  /**
   * Is the field item pending approval.
   *
   * @return bool
   *   TRUE if the field item is pending approval.
   */
  public function isPending(): bool {
    return $this->proposed !== NULL && $this->approved === NULL;
  }

}
