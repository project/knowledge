<?php

/**
 * @file
 * Hooks provided by the Knowledge module.
 */

use Drupal\Core\Url;
use Drupal\knowledge\KnowledgeInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the links of a knowledge.
 *
 * @param array &$links
 *   A renderable array representing the knowledge links.
 * @param \Drupal\knowledge\KnowledgeInterface $entity
 *   The knowledge being rendered.
 * @param array &$context
 *   Various aspects of the context in which the knowledge links are going to be
 *   displayed, with the following keys:
 *   - 'view_mode': the view mode in which the knowledge is being viewed
 *   - 'langcode': the language in which the knowledge is being viewed
 *   - 'knowledgeed_entity': the entity to which the knowledge is attached.
 *
 * @see \Drupal\knowledge\KnowledgeViewBuilder::renderLinks()
 * @see \Drupal\knowledge\KnowledgeViewBuilder::buildLinks()
 */
function hook_knowledge_links_alter(array &$links, KnowledgeInterface $entity, array &$context) {
  $links['mymodule'] = [
    '#theme' => 'links__knowledge__mymodule',
    '#attributes' => ['class' => ['links', 'inline']],
    '#links' => [
      'knowledge-report' => [
        'title' => t('Report'),
        'url' => Url::fromRoute('knowledge_test.report', ['knowledge' => $entity->id()], ['query' => ['token' => \Drupal::getContainer()->get('csrf_token')->get("knowledge/{$entity->id()}/report")]]),
      ],
    ],
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
