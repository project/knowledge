<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Quality entities.
 *
 * @ingroup knowledge
 */
class KnowledgeQualityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Quality ID');
    $header['name'] = $this->t('Name');
    $header['score'] = $this->t('Score');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\knowledge\Entity\KnowledgeQuality $entity
     */
    $node = $entity->get('entity_id')->entity;
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $node->label(),
      'entity.knowledge_quality.edit_form',
      ['knowledge_quality' => $entity->id()]
    );
    $row['name'] = $node->toLink();
    $row['score'] = $entity->getScore() . ' %';

    return $row + parent::buildRow($entity);
  }

}
