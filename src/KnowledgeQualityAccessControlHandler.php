<?php

namespace Drupal\knowledge;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Quality entity.
 *
 * @see \Drupal\knowledge\Entity\KnowledgeQuality.
 */
class KnowledgeQualityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\knowledge\Entity\KnowledgeQualityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished quality entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published quality entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit quality entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete quality entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add quality entities');
  }

}
