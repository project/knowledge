<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\knowledge\Entity\KnowledgeQualityInterface;

/**
 * Defines the storage handler class for Quality entities.
 *
 * This extends the base storage class, adding required special handling for
 * Quality entities.
 *
 * @ingroup knowledge
 */
interface KnowledgeQualityStorageInterface extends RevisionableStorageInterface {

  /**
   * Gets a list of Quality revision IDs for a specific Quality.
   *
   * @param \Drupal\knowledge\Entity\KnowledgeQualityInterface $entity
   *   The Quality entity.
   *
   * @return int[]
   *   Quality revision IDs (in ascending order).
   */
  public function revisionIds(KnowledgeQualityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Quality author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Quality revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
