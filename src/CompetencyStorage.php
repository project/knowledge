<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Competency entities.
 *
 * This extends the base storage class, adding required special handling for
 * Competency entities.
 *
 * @ingroup knowledge
 */
class CompetencyStorage extends SqlContentEntityStorage implements KnowledgeCompetencyStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(KnowledgeCompetencyInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {knowledge_competency_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {knowledge_competency_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
