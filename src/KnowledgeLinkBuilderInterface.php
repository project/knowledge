<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Defines an interface for building knowledge links on a knowledge entity.
 *
 * Links include 'log in to post new knowledge', 'add new knowledge' etc.
 */
interface KnowledgeLinkBuilderInterface {

  /**
   * Builds links for the given entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Entity for which the links are being built.
   * @param array $context
   *   Array of context passed from the entity view builder.
   *
   * @return array
   *   Array of entity links.
   */
  public function buildKnowledgeedEntityLinks(FieldableEntityInterface $entity, array &$context);

}
