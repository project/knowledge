<?php

declare(strict_types=1);

namespace Drupal\knowledge;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a wave entity type.
 */
interface KnowledgeWaveInterface extends ContentEntityInterface, EntityChangedInterface {

}
