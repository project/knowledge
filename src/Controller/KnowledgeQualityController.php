<?php

namespace Drupal\knowledge\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\knowledge\Entity\KnowledgeQualityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class KnowledgeQualityController.
 *
 *  Returns responses for Quality routes.
 */
class KnowledgeQualityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Quality revision.
   *
   * @param int $knowledge_quality_revision
   *   The Quality revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($knowledge_quality_revision) {
    /** @var \Drupal\knowledge\KnowledgeQualityStorageInterface $quality_storage */
    $quality_storage = $this->entityTypeManager()->getStorage('knowledge_quality');
    /** @var \Drupal\knowledge\Entity\KnowledgeQualityInterface $knowledge_quality */
    $knowledge_quality = $quality_storage->loadRevision($knowledge_quality_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('knowledge_quality');

    return $view_builder->view($knowledge_quality);
  }

  /**
   * Page title callback for a Quality revision.
   *
   * @param int $knowledge_quality_revision
   *   The Quality revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($knowledge_quality_revision) {
    /** @var \Drupal\knowledge\KnowledgeQualityStorageInterface $quality_storage */
    $quality_storage = $this->entityTypeManager()->getStorage('knowledge_quality');
    /** @var \Drupal\knowledge\Entity\KnowledgeQualityInterface $knowledge_quality */
    $knowledge_quality = $quality_storage->loadRevision($knowledge_quality_revision);

    return $this->t('Revision of %title from %date', [
      '%title' => $knowledge_quality->label(),
      '%date' => $this->dateFormatter->format($knowledge_quality->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Quality.
   *
   * @param \Drupal\knowledge\Entity\KnowledgeQualityInterface $knowledge_quality
   *   A Quality object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(KnowledgeQualityInterface $knowledge_quality) {
    $account = $this->currentUser();
    /** @var \Drupal\knowledge\KnowledgeQualityStorageInterface $knowledge_quality_storage */
    $knowledge_quality_storage = $this->entityTypeManager()->getStorage('knowledge_quality');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $knowledge_quality->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all quality revisions") || $account->hasPermission('administer quality entities')));
    $delete_permission = (($account->hasPermission("delete all quality revisions") || $account->hasPermission('administer quality entities')));

    $rows = [];

    $vids = $knowledge_quality_storage->revisionIds($knowledge_quality);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\knowledge\Entity\KnowledgeQualityInterface $revision */
      $revision = $knowledge_quality_storage->loadRevision($vid);
      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
      if ($vid != $knowledge_quality->getRevisionId()) {
        $link = Link::fromTextAndUrl($date, new Url('entity.knowledge_quality.revision', [
          'knowledge_quality' => $knowledge_quality->id(),
          'knowledge_quality_revision' => $vid,
        ]))->toString();
      }
      else {
        $link = $knowledge_quality->toLink($date)->toString();
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderInIsolation($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      $row[] = $column;

      if ($latest_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];
        foreach ($row as &$current) {
          $current['class'] = ['revision-current'];
        }
        $latest_revision = FALSE;
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $this->t('Revert'),
            'url' => Url::fromRoute('entity.knowledge_quality.revision_revert', [
              'knowledge_quality' => $knowledge_quality->id(),
              'knowledge_quality_revision' => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.knowledge_quality.revision_delete', [
              'knowledge_quality' => $knowledge_quality->id(),
              'knowledge_quality_revision' => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }

      $rows[] = $row;
    }

    $build['knowledge_quality_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
