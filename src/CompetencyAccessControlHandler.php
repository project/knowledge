<?php

namespace Drupal\knowledge;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Competency entity.
 *
 * @see \Drupal\knowledge\Entity\Competency.
 */
class CompetencyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\knowledge\KnowledgeCompetencyInterface $entity */

    switch ($operation) {

      case 'view':
        $permissions = [
          'view any knowledge_competency',
        ];
        $own = $entity->getOwnerId() == $account->id();
        if ($own) {
          $permissions[] = 'view own knowledge_competency';
        }

        $is_learner = $entity->getOwner()->get('knowledge_coach')->target_id == $account->id();
        if ($is_learner) {
          $permissions[] = 'view learner knowledge_competency';
        }

        $is_leader = $entity->getOwner()->get('knowledge_leader')->target_id == $account->id();
        if ($is_leader) {
          $permissions[] = 'view follower knowledge_competency';
        }

        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'update':
        $permissions = [];
        $owner = $entity->getOwner();
        $account_id = $account->id();

        if ($account_id == $owner->id()) {
          $permissions[] = 'edit own knowledge_competency';
        }
        else {
          $permissions[] = 'edit other knowledge_competency';
        }

        $coach_id = $owner->get('knowledge_coach')?->target_id;
        if ($account_id == $coach_id) {
          $permissions[] = 'edit learner knowledge_competency';
        }

        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'administer knowledge_competency');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add knowledge_competency');
  }

}
