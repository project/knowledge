<?php

namespace Drupal\knowledge\Tests;

use Drupal\Component\Utility\Unicode;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;

/**
 * Provides common functionality for the Knowledge test classes.
 */
trait KnowledgeTestTrait {

  /**
   * Adds the default knowledge field to an entity.
   *
   * Attaches a knowledge field named 'knowledge' to the given entity type and
   * bundle. Largely replicates the default behavior in Drupal 7 and earlier.
   *
   * @param string $entity_type
   *   The entity type to attach the default knowledge field to.
   * @param string $bundle
   *   The bundle to attach the default knowledge field to.
   * @param string $field_name
   *   (optional) Field name to use for the knowledge field. Defaults to
   *     'knowledge'.
   * @param int $default_value
   *   (optional) Default value, one of KnowledgeItemInterface::HIDDEN,
   *   KnowledgeItemInterface::OPEN, KnowledgeItemInterface::CLOSED. Defaults to
   *   KnowledgeItemInterface::OPEN.
   * @param string $knowledge_type_id
   *   (optional) ID of knowledge type to use. Defaults to 'knowledge'.
   * @param string $knowledge_view_mode
   *   (optional) The knowledge view mode to be used in knowledge field
   *   formatter.
   *   Defaults to 'full'.
   */
  public function addDefaultKnowledgeField($entity_type, $bundle, $field_name = 'knowledge', $default_value = KnowledgeItemInterface::OPEN, $knowledge_type_id = 'knowledge', $knowledge_view_mode = 'full') {
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_display_repository = \Drupal::service('entity_display.repository');
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    // Create the knowledge type if needed.
    $knowledge_type_storage = $entity_type_manager->getStorage('knowledge_type');
    /** @var \Drupal\knowledge\KnowledgeTypeInterface $knowledge_type */
    $knowledge_type = $knowledge_type_storage->load($knowledge_type_id);
    if ($knowledge_type) {
      if ($knowledge_type->getTargetEntityTypeId() !== $entity_type) {
        throw new \InvalidArgumentException("The given knowledge type id $knowledge_type_id can only be used with the $entity_type entity type");
      }
    }
    else {
      $knowledge_type_storage->create([
        'id' => $knowledge_type_id,
        'label' => Unicode::ucfirst($knowledge_type_id),
        'target_entity_type_id' => $entity_type,
        'description' => 'Default knowledge field',
      ])->save();
    }
    // Add a knowledge field to the host entity type. Create the field storage
    // if needed.
    if (!array_key_exists($field_name, $entity_field_manager->getFieldStorageDefinitions($entity_type))) {
      $entity_type_manager->getStorage('field_storage_config')->create([
        'entity_type' => $entity_type,
        'field_name' => $field_name,
        'type' => 'knowledge',
        'translatable' => TRUE,
        'settings' => [
          'knowledge_type' => $knowledge_type_id,
        ],
      ])->save();
    }
    // Create the field if needed, and configure its form and view displays.
    if (!array_key_exists($field_name, $entity_field_manager->getFieldDefinitions($entity_type, $bundle))) {
      $entity_type_manager->getStorage('field_config')->create([
        'label' => 'Knowledge',
        'description' => '',
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'required' => 1,
        'default_value' => [
          [
            'status' => $default_value,
            'kid' => 0,
            'last_knowledge_timestamp' => 0,
            'last_knowledge_uid' => 0,
          ],
        ],
      ])->save();

      // Entity form displays: assign widget settings for the default form
      // mode, and hide the field in all other form modes.
      $entity_display_repository->getFormDisplay($entity_type, $bundle)
        ->setComponent($field_name, [
          'type' => 'knowledge_default',
          'weight' => 20,
        ])
        ->save();
      foreach ($entity_display_repository->getFormModes($entity_type) as $id => $form_mode) {
        $display = $entity_display_repository->getFormDisplay($entity_type, $bundle, $id);
        // Only update existing displays.
        if ($display && !$display->isNew()) {
          $display->removeComponent($field_name)->save();
        }
      }

      // Entity view displays: assign widget settings for the default view
      // mode, and hide the field in all other view modes.
      $entity_display_repository->getViewDisplay($entity_type, $bundle)
        ->setComponent($field_name, [
          'label' => 'above',
          'type' => 'knowledge_default',
          'weight' => 20,
          'settings' => ['view_mode' => $knowledge_view_mode],
        ])
        ->save();
      foreach ($entity_display_repository->getViewModes($entity_type) as $id => $view_mode) {
        $display = $entity_display_repository->getViewDisplay($entity_type, $bundle, $id);
        // Only update existing displays.
        if ($display && !$display->isNew()) {
          $display->removeComponent($field_name)->save();
        }
      }
    }
  }

}
