<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\views\EntityViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides views data for the knowledge entity type.
 */
class KnowledgeViewsData extends EntityViewsData {

  /**
   * The knowledge manager service.
   *
   * @var \Drupal\knowledge\KnowledgeManagerInterface
   */
  protected $knowledgeManager;

  /**
   * The entity types.
   *
   * @var mixed[]
   */
  protected $entitiesTypes;

  /**
   * Constructs a KnowledgeViewsData object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to provide views integration for.
   * @param \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $storage_controller
   *   The storage handler used for this entity type.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The translation manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\knowledge\KnowledgeManagerInterface $knowledge_manager
   *   The knowledge manager service.
   */
  public function __construct(EntityTypeInterface $entity_type, SqlEntityStorageInterface $storage_controller, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, TranslationInterface $translation_manager, EntityFieldManagerInterface $entity_field_manager, KnowledgeManagerInterface $knowledge_manager) {
    parent::__construct($entity_type, $storage_controller, $entity_type_manager, $module_handler, $translation_manager, $entity_field_manager);
    $this->knowledgeManager = $knowledge_manager;
    $this->entitiesTypes = $entity_type_manager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($entity_type, $container
      ->get('entity_type.manager')
      ->getStorage($entity_type->id()), $container
      ->get('entity_type.manager'), $container
      ->get('module_handler'), $container
      ->get('string_translation'), $container
      ->get('entity_field.manager'), $container
      ->get('knowledge.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['knowledge']['table']['base']['help'] = $this->t('Knowledge is responses to content.');
    $data['knowledge']['table']['base']['access query tag'] = 'knowledge_access';

    $data['knowledge']['table']['wizard_id'] = 'knowledge';

    $data['knowledge']['created']['title'] = $this->t('Post date');
    $data['knowledge']['created']['help'] = $this->t('Date and time of when the knowledge was created.');

    $data['knowledge']['created_fulldata'] = [
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_fulldate',
      ],
    ];

    $data['knowledge']['created_year_month'] = [
      'title' => $this->t('Created year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year_month',
      ],
    ];

    $data['knowledge']['created_year'] = [
      'title' => $this->t('Created year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year',
      ],
    ];

    $data['knowledge']['created_month'] = [
      'title' => $this->t('Created month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_month',
      ],
    ];

    $data['knowledge']['created_day'] = [
      'title' => $this->t('Created day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_day',
      ],
    ];

    $data['knowledge']['created_week'] = [
      'title' => $this->t('Created week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_week',
      ],
    ];

    $data['knowledge']['created_weekly'] = [
      'title' => $this->t('Created weekly past two years'),
      'help' => $this->t('Date in the form of Y-WW (01 - 53).'),
      'field' => [
        'field' => 'created',
        'id' => 'knowledge_weekly',
      ],
      'sort' => [
        'field' => 'created',
        'id' => 'standard',
      ],
    ];

    $data['knowledge']['changed']['title'] = $this->t('Updated date');
    $data['knowledge']['changed']['help'] = $this->t('Date and time of when the knowledge was last updated.');

    $data['knowledge']['changed_fulldata'] = [
      'title' => $this->t('Changed date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_fulldate',
      ],
    ];

    $data['knowledge']['changed_year_month'] = [
      'title' => $this->t('Changed year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_year_month',
      ],
    ];

    $data['knowledge']['changed_year'] = [
      'title' => $this->t('Changed year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_year',
      ],
    ];

    $data['knowledge']['changed_month'] = [
      'title' => $this->t('Changed month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_month',
      ],
    ];

    $data['knowledge']['changed_day'] = [
      'title' => $this->t('Changed day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_day',
      ],
    ];

    $data['knowledge']['changed_week'] = [
      'title' => $this->t('Changed week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_week',
      ],
    ];

    $data['knowledge']['status']['title'] = $this->t('Approved status');
    $data['knowledge']['status']['help'] = $this->t('Whether the knowledge is approved (or still in the moderation queue).');
    $data['knowledge']['status']['filter']['label'] = $this->t('Approved knowledge status');
    $data['knowledge']['status']['filter']['type'] = 'yes-no';

    $data['knowledge']['approve_knowledge'] = [
      'field' => [
        'title' => $this->t('Link to approve knowledge'),
        'help' => $this->t('Provide a simple link to approve the knowledge.'),
        'id' => 'knowledge_link_approve',
      ],
    ];

    $data['knowledge']['entity_id']['field']['id'] = 'linked_entity';
    unset($data['knowledge']['entity_id']['relationship']);
    $data['knowledge']['incident_id']['field']['id'] = 'linked_incident';
    unset($data['knowledge']['incident_id']['relationship']);

    $data['knowledge']['knowledge_bulk_form'] = [
      'title' => $this->t('Knowledge operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple knowledge.'),
      'field' => [
        'id' => 'knowledge_bulk_form',
      ],
    ];

    // Provide a relationship for each entity type except knowledge.
    foreach ($this->entitiesTypes as $type => $entity_type) {
      if (in_array($type, ['knowledge', 'knowledge_adherence', 'knowledge_competency']) || !$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
        continue;
      }
      if ($this->knowledgeManager->getFields($type)) {
        $data['knowledge']['entity_' . $type] = [
          'relationship' => [
            'title' => $entity_type->getLabel(),
            'help' => $this->t('The @entity_type to which the entity is linked.', ['@entity_type' => $entity_type->getLabel()]),
            'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
            'base field' => $entity_type->getKey('id'),
            'relationship field' => 'entity_id',
            'id' => 'standard',
            'label' => $entity_type->getLabel(),
            'extra' => [
              [
                'field' => 'entity_type',
                'value' => $type,
                'table' => 'knowledge',
              ],
            ],
          ],
        ];

      }
      $data['knowledge']['incident_' . $type] = [
        'relationship' => [
          'title' => $entity_type->getLabel(),
          'help' => $this->t('The @entity_type to which the incident is linked.', ['@entity_type' => $entity_type->getLabel()]),
          'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
          'base field' => $entity_type->getKey('id'),
          'relationship field' => 'incident_id',
          'id' => 'standard',
          'label' => $entity_type->getLabel(),
          'extra' => [
            [
              'field' => 'incident_type',
              'value' => $type,
              'table' => 'knowledge',
            ],
          ],
        ],
      ];
    }

    $data['knowledge']['uid']['title'] = $this->t('Author uid');
    $data['knowledge']['uid']['help'] = $this->t('If you need more fields than the uid add the knowledge: author relationship');
    $data['knowledge']['uid']['relationship']['title'] = $this->t('Author');
    $data['knowledge']['uid']['relationship']['help'] = $this->t("The User ID of the knowledge's author.");
    $data['knowledge']['uid']['relationship']['label'] = $this->t('author');

    // Define the base group of this table. Fields that don't have a group
    // defined will go into this field by default.
    $data['knowledge_entity_statistics']['table']['group'] = $this->t('Knowledge Statistics');

    // Provide a relationship for each entity type except knowledge.
    foreach ($this->entitiesTypes as $type => $entity_type) {
      if ($type == 'knowledge' || !$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
        continue;
      }
      // This relationship does not use the 'field id' column, if the entity has
      // multiple knowledge-fields, then this might introduce duplicates, in
      // which case the site-builder should enable aggregation and SUM the
      // total_count field. We cannot create a relationship from the base table
      // to {knowledge_entity_statistics} for each field as multiple joins
      // between the same two tables is not supported.
      if ($this->knowledgeManager->getFields($type)) {
        $data['knowledge_entity_statistics']['table']['join'][$entity_type->getDataTable() ?: $entity_type->getBaseTable()] = [
          'type' => 'LEFT',
          'left_field' => $entity_type->getKey('id'),
          'field' => 'entity_id',
          'extra' => [
            [
              'field' => 'entity_type',
              'value' => $type,
            ],
          ],
        ];

      }
    }

    $data['knowledge_entity_statistics']['last_knowledge_timestamp'] = [
      'title' => $this->t('Last knowledge time'),
      'help' => $this->t('Date and time of when the last knowledge was posted.'),
      'field' => [
        'id' => 'knowledge_last_timestamp',
      ],
      'sort' => [
        'id' => 'date',
      ],
      'filter' => [
        'id' => 'date',
      ],
    ];

    $data['knowledge_entity_statistics']['total_count'] = [
      'title' => $this->t('Knowledge count'),
      'help' => $this->t('The amount of knowledge an entity has.'),
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    $data['knowledge_entity_statistics']['long_count'] = [
      'title' => $this->t('Knowledge Long count'),
      'help' => $this->t('The amount of knowledge an entity has.'),
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    $data['knowledge_entity_statistics']['medium_count'] = [
      'title' => $this->t('Knowledge medium count'),
      'help' => $this->t('The medium amount of knowledge an entity has.'),
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    $data['knowledge_entity_statistics']['short_count'] = [
      'title' => $this->t('Knowledge short count'),
      'help' => $this->t('The short amount of knowledge an entity has.'),
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    $data['knowledge_entity_statistics']['first_updated'] = [
      'title' => $this->t('Updated/linked date'),
      'help' => $this->t('The first knowledge posted or entity updated time.'),
      'field' => [
        'id' => 'knowledge_ces_first_updated',
        'no group by' => TRUE,
      ],
      'sort' => [
        'id' => 'knowledge_ces_first_updated',
        'no group by' => TRUE,
      ],
      'filter' => [
        'id' => 'knowledge_ces_first_updated',
      ],
    ];

    $data['knowledge_entity_statistics']['first_knowledge'] = [
      'title' => $this->t('First knowledge'),
      'help' => $this->t('Display the first knowledge of an entity'),
      'relationship' => [
        'title' => $this->t('First knowledge'),
        'help' => $this->t('The first knowledge of an entity.'),
        'group' => $this->t('Knowledge'),
        'base' => 'knowledge',
        'base field' => 'first_knowledge',
        'id' => 'standard',
        'label' => $this->t('First Knowledge'),
      ],
    ];

    $data['knowledge_entity_statistics']['first_knowledge_uid'] = [
      'title' => $this->t('First knowledge uid'),
      'help' => $this->t('The User ID of the author of the first knowledge of an entity.'),
      'relationship' => [
        'title' => $this->t('First knowledge author'),
        'base' => 'users',
        'base field' => 'uid',
        'id' => 'standard',
        'label' => $this->t('First knowledge author'),
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'argument' => [
        'id' => 'numeric',
      ],
      'field' => [
        'id' => 'numeric',
      ],
    ];

    $data['knowledge_entity_statistics']['last_updated'] = [
      'title' => $this->t('Updated/linked date'),
      'help' => $this->t('The most recent of last knowledge posted or entity updated time.'),
      'field' => [
        'id' => 'knowledge_ces_last_updated',
        'no group by' => TRUE,
      ],
      'sort' => [
        'id' => 'knowledge_ces_last_updated',
        'no group by' => TRUE,
      ],
      'filter' => [
        'id' => 'knowledge_ces_last_updated',
      ],
    ];

    $data['knowledge_entity_statistics']['last_knowledge'] = [
      'title' => $this->t('Last knowledge KID'),
      'help' => $this->t('Display the last knowledge of an entity'),
      'relationship' => [
        'title' => $this->t('Last knowledge'),
        'help' => $this->t('The last knowledge of an entity.'),
        'group' => $this->t('Knowledge'),
        'base' => 'knowledge',
        'base field' => 'last_knowledge',
        'id' => 'standard',
        'label' => $this->t('Last Knowledge'),
      ],
    ];

    $data['knowledge_entity_statistics']['last_knowledge_uid'] = [
      'title' => $this->t('Last knowledge uid'),
      'help' => $this->t('The User ID of the author of the last knowledge of an entity.'),
      'relationship' => [
        'title' => $this->t('Last knowledge author'),
        'base' => 'users',
        'base field' => 'uid',
        'id' => 'standard',
        'label' => $this->t('Last knowledge author'),
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'argument' => [
        'id' => 'numeric',
      ],
      'field' => [
        'id' => 'numeric',
      ],
    ];

    $data['knowledge_entity_statistics']['entity_type'] = [
      'title' => $this->t('Entity type'),
      'help' => $this->t('The entity type to which the knowledge is a reply to.'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'string',
      ],
      'argument' => [
        'id' => 'string',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];
    $data['knowledge_entity_statistics']['field_name'] = [
      'title' => $this->t('Knowledge field name'),
      'help' => $this->t('The field name from which the knowledge originated.'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'string',
      ],
      'argument' => [
        'id' => 'string',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];

    return $data;
  }

}
