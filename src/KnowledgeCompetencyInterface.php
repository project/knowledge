<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Competency entities.
 *
 * @ingroup knowledge
 */
interface KnowledgeCompetencyInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Competency creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Competency.
   */
  public function getCreatedTime();

  /**
   * Sets the Competency creation timestamp.
   *
   * @param int $timestamp
   *   The Competency creation timestamp.
   *
   * @return \Drupal\knowledge\KnowledgeCompetencyInterface
   *   The called Competency entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Competency revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Competency revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\knowledge\KnowledgeCompetencyInterface
   *   The called Competency entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Competency revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Competency revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\knowledge\KnowledgeCompetencyInterface
   *   The called Competency entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Is the competency pending approval.
   *
   * @return string|bool
   *   The role id if the competency is pending approval.
   */
  public function isPending();

}
