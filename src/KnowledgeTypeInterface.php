<?php

namespace Drupal\knowledge;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a knowledge type entity.
 */
interface KnowledgeTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the knowledge type description.
   *
   * @return string
   *   The knowledge-type description.
   */
  public function getDescription();

  /**
   * Sets the description of the knowledge type.
   *
   * @param string $description
   *   The new description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the target entity type id for this knowledge type.
   *
   * @return string
   *   The target entity type id.
   */
  public function getTargetEntityTypeId();

  /**
   * Gets the incident entity type id for this knowledge type.
   *
   * @return string
   *   The target entity type id.
   */
  public function getIncidentEntityTypeId();

}
