<?php

namespace Drupal\knowledge;

/**
 * Returns which entity types are used in knowledge links.
 */
interface KnowledgeLinkRelationshipInterface {

  /**
   * Gets the entity types that are used in knowledge links an article entity.
   *
   * @return EntityInterface[]
   *   An array of article entity types.
   */
  public function getArticleEntityTypes(): array;

  /**
   * Gets the entity types that are used in knowledge links an incident entity.
   *
   * @return EntityInterface[]
   *   An array of incident entity types.
   */
  public function getIncidentEntityTypes(): array;

}
