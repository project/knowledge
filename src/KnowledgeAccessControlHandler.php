<?php

namespace Drupal\knowledge;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the knowledge entity type.
 *
 * @see \Drupal\knowledge\Entity\Knowledge
 */
class KnowledgeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\knowledge\KnowledgeInterface $entity */

    $knowledge_default = $account->hasPermission('administer knowledge');
    if ($operation == 'approve') {
      return AccessResult::allowedIf($knowledge_default && !$entity->isPublished())
        ->cachePerPermissions()
        ->addCacheableDependency($entity);
    }

    if ($knowledge_default) {
      $access = AccessResult::allowed()->cachePerPermissions();
      return ($operation != 'view') ? $access : $access->andIf($entity->getKnowledgeedEntity()->access($operation, $account, TRUE));
    }

    switch ($operation) {
      case 'view':
        $access_result = AccessResult::allowedIf($account->hasPermission('access knowledge') && $entity->isPublished())->cachePerPermissions()->addCacheableDependency($entity)
          ->andIf($entity->getKnowledgeedEntity()->access($operation, $account, TRUE));
        if (!$access_result->isAllowed()) {
          $access_result->setReason("The 'access knowledge' permission is required and the knowledge must be published.");
        }

        return $access_result;

      case 'update':
        $access_result = AccessResult::allowedIf($account->id() && $account->id() == $entity->getOwnerId() && $entity->isPublished() && $account->hasPermission('edit own knowledge'))
          ->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
        if (!$access_result->isAllowed()) {
          $access_result->setReason("The 'edit own knowledge' permission is required, the user must be the knowledge author, and the knowledge must be published.");
        }
        return $access_result;

      default:
        // No opinion.
        return AccessResult::neutral()->cachePerPermissions();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'post knowledge');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL) {
    if ($operation == 'edit') {
      // Only users with the "administer knowledge" permission can edit
      // administrative fields.
      $administrative_fields = [
        'uid',
        'status',
        'created',
        'date',
      ];
      if (in_array($field_definition->getName(), $administrative_fields, TRUE)) {
        return AccessResult::allowedIfHasPermission($account, 'administer knowledge');
      }

      // No user can change read-only fields.
      $read_only_fields = [
        'changed',
        'kid',
      ];
      // These fields can be edited during knowledge creation.
      $create_only_fields = [
        'knowledge_type',
        'uuid',
        'entity_id',
        'entity_type',
        'field_name',
      ];
      if ($items && ($entity = $items->getEntity()) && $entity->isNew() && in_array($field_definition->getName(), $create_only_fields, TRUE)) {
        // We are creating a new knowledge, user can edit create only fields.
        return AccessResult::allowedIfHasPermission($account, 'post knowledge')->addCacheableDependency($entity);
      }
      // We are editing an existing knowledge - create only fields are now read
      // only.
      $read_only_fields = array_merge($read_only_fields, $create_only_fields);
      if (in_array($field_definition->getName(), $read_only_fields, TRUE)) {
        return AccessResult::forbidden();
      }

      // If the field is configured to accept anonymous contact details - admins
      // can edit name. Anonymous users can also fill in the
      // fields on knowledge creation.
      if (in_array($field_definition->getName(), ['name'], TRUE)) {
        if (!$items) {
          // We cannot make a decision about access to edit these fields if we
          // don't have any items and therefore cannot determine the Knowledge
          // entity. In this case we err on the side of caution and prevent edit
          // access.
          return AccessResult::forbidden();
        }
        $is_name = $field_definition->getName() === 'name';
        /** @var \Drupal\knowledge\KnowledgeInterface $entity */
        $entity = $items->getEntity();
        $linked_entity = $entity->getKnowledgeedEntity();
        $anonymous_contact = $linked_entity->get($entity->getFieldName())->getFieldDefinition()->getSetting('anonymous');
        $admin_access = AccessResult::allowedIfHasPermission($account, 'administer knowledge');
        $anonymous_access = AccessResult::allowedIf($entity->isNew() && $account->isAnonymous() && ($anonymous_contact != KnowledgeInterface::ANONYMOUS_MAYNOT_CONTACT || $is_name) && $account->hasPermission('post knowledge'))
          ->cachePerPermissions()
          ->addCacheableDependency($entity)
          ->addCacheableDependency($field_definition->getConfig($linked_entity->bundle()))
          ->addCacheableDependency($linked_entity);
        return $admin_access->orIf($anonymous_access);
      }
    }

    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
