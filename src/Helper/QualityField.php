<?php

namespace Drupal\knowledge\Helper;

/**
 * Helper class for the knowledge competency entity.
 */
class QualityField {

  /**
   * Get the field definitions for the knowledge competency entity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions
   *   The field definitions.
   *
   * @return array
   *   The field definitions for the knowledge competency entity.
   */
  public static function categoryFields($field_definitions) {
    $fields = [];
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_definition->getType() != 'boolean') {
        continue;
      }
      if (get_class($field_definition) != 'Drupal\field\Entity\FieldConfig') {
        continue;
      }
      $category = $field_definition->getThirdPartySetting('knowledge', 'quality_category', '_none');
      if ($category == '_none') {
        continue;
      }
      $fields[$category][] = $field_name;
    }

    return $fields;
  }

}
