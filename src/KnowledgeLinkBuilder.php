<?php

namespace Drupal\knowledge;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;

/**
 * Defines a class for building markup for knowledge links on a linked entity.
 *
 * Knowledge links include 'log in to post new knowledge', 'link knowledge' etc.
 */
class KnowledgeLinkBuilder implements KnowledgeLinkBuilderInterface {

  use StringTranslationTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Knowledge manager service.
   *
   * @var \Drupal\knowledge\KnowledgeManagerInterface
   */
  protected $knowledgeManager;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new KnowledgeLinkBuilder object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\knowledge\KnowledgeManagerInterface $knowledge_manager
   *   Knowledge manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(AccountInterface $current_user, KnowledgeManagerInterface $knowledge_manager, ModuleHandlerInterface $module_handler, TranslationInterface $string_translation, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $current_user;
    $this->knowledgeManager = $knowledge_manager;
    $this->moduleHandler = $module_handler;
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildKnowledgeedEntityLinks(FieldableEntityInterface $entity, array &$context) {
    $entity_links = [];
    $view_mode = $context['view_mode'];
    if ($view_mode == 'search_index' || $view_mode == 'search_result' || $view_mode == 'print' || $view_mode == 'rss') {
      // Do not add any links if the entity is displayed for:
      // - search indexing.
      // - constructing a search result excerpt.
      // - print.
      // - rss.
      return [];
    }

    $fields = $this->knowledgeManager->getFields($entity->getEntityTypeId());
    foreach ($fields as $field_name => $detail) {
      // Skip fields that the entity does not have.
      if (!$entity->hasField($field_name)) {
        continue;
      }
      $links = [];
      $linking_status = $entity->get($field_name)->status;
      if ($linking_status != KnowledgeItemInterface::HIDDEN) {
        // Entity has knowledge linking status open or closed.
        if ($view_mode == 'teaser') {
          // Teaser view: display the amount of knowledge that have been posted,
          // or a link to add new knowledge if the user has permission, the
          // entity is open to new knowledge links, and there currently are
          // none.
          if ($this->currentUser->hasPermission('access knowledge')) {
            if (!empty($entity->get($field_name)->total_count)) {
              $links['knowledge-links'] = [
                'title' => $this->formatPlural($entity->get($field_name)->total_count, '1 knowledge', '@count knowledge'),
                'attributes' => ['title' => $this->t('Jump to the first knowledge.')],
                'fragment' => 'knowledge',
                'url' => $entity->toUrl(),
              ];
              if ($this->moduleHandler->moduleExists('history')) {
                $links['knowledge-new-knowledge'] = [
                  'title' => '',
                  'url' => Url::fromRoute('<current>'),
                  'attributes' => [
                    'class' => 'hidden',
                    'title' => $this->t('Jump to the first new knowledge.'),
                    'data-history-node-last-knowledge-timestamp' => $entity->get($field_name)->last_knowledge_timestamp,
                    'data-history-node-field-name' => $field_name,
                  ],
                ];
              }
            }
          }
          // Provide a link to new knowledge form.
          if ($linking_status == KnowledgeItemInterface::OPEN) {

            if ($this->currentUser->isAnonymous()) {
              $links['knowledge-forbidden'] = [
                'title' => $this->knowledgeManager->forbiddenMessage($entity, $field_name),
              ];
            }
          }
        }
        else {
          // Entity in other view modes: add a "post knowledge" link if the user
          // is allowed to post knowledge and if this entity is allowing new
          // knowledge links.
          if ($linking_status == KnowledgeItemInterface::OPEN) {

            if ($this->currentUser->isAnonymous()) {
              $links['knowledge-forbidden'] = [
                'title' => $this->knowledgeManager->forbiddenMessage($entity, $field_name),
              ];
            }
          }
        }
      }

      if (!empty($links)) {
        $entity_links['knowledge__' . $field_name] = [
          '#theme' => 'links__entity__knowledge__' . $field_name,
          '#links' => $links,
          '#attributes' => ['class' => ['links', 'inline']],
        ];
        if ($view_mode == 'teaser' && $this->moduleHandler->moduleExists('history') && $this->currentUser->isAuthenticated()) {
          $entity_links['knowledge__' . $field_name]['#cache']['contexts'][] = 'user';
          $entity_links['knowledge__' . $field_name]['#attached']['library'][] = 'knowledge/drupal.node-new-knowledge-link';
          // Embed the metadata for the "X new knowledge" link (if any) on this
          // entity.
          $entity_links['knowledge__' . $field_name]['#attached']['drupalSettings']['history']['lastReadTimestamps'][$entity->id()] = (int) history_read($entity->id());
          $new_knowledge = $this->knowledgeManager->getCountNewKnowledges($entity);
          if ($new_knowledge > 0) {
            /** @var \Drupal\knowledge\KnowledgeStorageInterface $knowledge_storage */
            $knowledge_storage = $this->entityTypeManager->getStorage('knowledge');
            $page_number = $knowledge_storage
              ->getNewKnowledgePageNumber($entity->{$field_name}->total_count, $new_knowledge, $entity, $field_name);
            $query = $page_number ? ['page' => $page_number] : NULL;
            $value = [
              'new_total_count' => (int) $new_knowledge,
              'first_new_knowledge_link' => $entity->toUrl('canonical', [
                'query' => $query,
                'fragment' => 'new',
              ])->toString(),
            ];
            $parents = [
              'knowledge',
              'newKnowledgeLinks',
              $entity->getEntityTypeId(),
              $field_name, $entity->id(),
            ];
            NestedArray::setValue($entity_links['knowledge__' . $field_name]['#attached']['drupalSettings'], $parents, $value);
          }
        }
      }
    }
    return $entity_links;
  }

}
