<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Competency entities.
 *
 * This extends the base storage class, adding required special handling for
 * Competency entities.
 *
 * @ingroup knowledge
 */
interface KnowledgeCompetencyStorageInterface extends RevisionableStorageInterface {

  /**
   * Gets a list of Competency revision IDs for a specific Competency.
   *
   * @param \Drupal\knowledge\KnowledgeCompetencyInterface $entity
   *   The Competency entity.
   *
   * @return int[]
   *   Competency revision IDs (in ascending order).
   */
  public function revisionIds(KnowledgeCompetencyInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Competency author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Competency revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
