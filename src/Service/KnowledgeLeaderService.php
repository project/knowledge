<?php

namespace Drupal\knowledge\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Error;
use Drupal\knowledge\KnowledgeLeaderInterface;
use Psr\Log\LoggerInterface;

/**
 * Knowledge Leader manages giving users the knowledge_leader role.
 */
class KnowledgeLeaderService implements KnowledgeLeaderInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Construct the KnowledgeLeader object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function addKnowledgeLeader(&$leader) {
    try {
      $leader->addRole('knowledge_leader');
      $leader->save();
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeKnowledgeLeader(&$leader) {
    try {
      $lid = $leader->id();
      $user_query = $this->entityTypeManager
        ->getStorage('user')
        ->getQuery();
      $user_ids = $user_query
        ->condition('knowledge_leader', $lid)
        ->accessCheck(FALSE)
        ->execute();

      if (count($user_ids) == 0) {
        $leader->removeRole('knowledge_leader');
        $leader->save();
      }
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateKnowledgeLeader(&$user, &$leader) {
    $uid = $user->id();
    $lid = $leader->id();
    $user_query = $this->entityTypeManager
      ->getStorage('user')
      ->getQuery();
    $user_ids = $user_query
      ->condition('knowledge_leader', $lid)
      ->condition('uid', $uid, '<>')
      ->accessCheck(FALSE)
      ->execute();

    if (count($user_ids) == 0) {
      $leader->removeRole('knowledge_leader');
      $leader->save();
    }
    elseif (!in_array('knowledge_leader', $leader->getRoles(TRUE))) {
      $leader->addRole('knowledge_leader');
      $leader->save();
    }
  }

}
