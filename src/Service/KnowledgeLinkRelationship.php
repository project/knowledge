<?php

namespace Drupal\knowledge\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\knowledge\KnowledgeLinkRelationshipInterface;

/**
 * Returns which entity types are used in knowledge links.
 */
class KnowledgeLinkRelationship implements KnowledgeLinkRelationshipInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct the KnowledgeLinkRelationship object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getArticleEntityTypes(): array {
    $article_type_ids = $this->getEntityTypes()['articles'];
    $definitions = $this->entityTypeManager->getDefinitions();
    $article_types = [];
    foreach ($article_type_ids as $type_id) {
      $article_types[$type_id] = $definitions[$type_id];
    }

    return $article_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getIncidentEntityTypes(): array {
    $incident_type_ids = $this->getEntityTypes()['incidents'];
    $definitions = $this->entityTypeManager->getDefinitions();
    $incident_types = [];
    foreach ($incident_type_ids as $type_id) {
      $incident_types[$type_id] = $definitions[$type_id];
    }

    return $incident_types;
  }

  /**
   * Gets the entity types that are used in knowledge links.
   *
   * @return array
   *   An keyed array of entity types.
   */
  protected function getEntityTypes(): array {
    $storage = $this->entityTypeManager->getStorage('knowledge_type');
    $types = $storage->loadMultiple();
    $articles = [];
    $incidents = [];
    foreach ($types as $type) {
      $articles[] = $type->getTargetEntityTypeId();
      $incidents[] = $type->getIncidentEntityTypeId();
    }
    $articles = array_unique($articles);
    $incidents = array_unique($incidents);
    return [
      'articles' => $articles,
      'incidents' => $incidents,
    ];
  }

}
