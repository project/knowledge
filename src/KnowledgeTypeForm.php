<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form handler for knowledge type edit forms.
 *
 * @internal
 */
class KnowledgeTypeForm extends EntityForm {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The knowledge manager.
   *
   * @var \Drupal\knowledge\KnowledgeManagerInterface
   */
  protected $knowledgeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('knowledge'),
      $container->get('knowledge.manager')
    );
  }

  /**
   * Constructs a KnowledgeTypeFormController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\knowledge\KnowledgeManagerInterface $knowledge_manager
   *   The knowledge manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, KnowledgeManagerInterface $knowledge_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->knowledgeManager = $knowledge_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\knowledge\KnowledgeTypeInterface $knowledge_type */
    $knowledge_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $knowledge_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $knowledge_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\knowledge\Entity\KnowledgeType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$knowledge_type->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $knowledge_type->getDescription(),
      '#description' => $this->t('Describe this knowledge type. The text will be displayed on the <em>Knowledge types</em> administration overview page.'),
      '#title' => $this->t('Description'),
    ];

    if ($knowledge_type->isNew()) {
      $options = [];
      // Only expose entities that have field UI enabled, only those can
      // get knowledge fields added in the UI. Also, ensure to include only
      // entities that have integer id.
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
        if ($this->entityTypeSupportsKnowledges($entity_type)) {
          if ($entity_type->get('field_ui_base_route')) {
            $options[$entity_type->id()] = $entity_type->getLabel();
          }
        }
      }
      $form['target_entity_type_id'] = [
        '#type' => 'select',
        '#default_value' => $knowledge_type->getTargetEntityTypeId(),
        '#title' => $this->t('Target entity type'),
        '#options' => $options,
        '#description' => $this->t('The target entity type can not be changed after the knowledge type has been created.'),
      ];
      $form['incident_entity_type_id'] = [
        '#type' => 'select',
        '#default_value' => $knowledge_type->getIncidentEntityTypeId(),
        '#title' => $this->t('Incident entity type'),
        '#options' => $options,
        '#description' => $this->t('The incident entity type can not be changed after the knowledge type has been created.'),
      ];
    }
    else {
      $form['target_entity_type_id_display'] = [
        '#type' => 'item',
        '#markup' => $this->entityTypeManager->getDefinition($knowledge_type->getTargetEntityTypeId())->getLabel(),
        '#title' => $this->t('Target entity type'),
      ];

      $form['incident_entity_type_id_display'] = [
        '#type' => 'item',
        '#markup' => $this->entityTypeManager->getDefinition($knowledge_type->getIncidentEntityTypeId())->getLabel(),
        '#title' => $this->t('Incident entity type'),
      ];

    }

    if ($this->moduleHandler->moduleExists('content_translation')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];

      $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle('knowledge', $knowledge_type->id());
      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'knowledge',
          'bundle' => $knowledge_type->id(),
        ],
        '#default_value' => $language_configuration,
      ];

      $form['#submit'][] = 'language_configuration_element_submit';
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Wraps _knowledge_entity_uses_integer_id().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type being tested.
   *
   * @return bool
   *   TRUE if entity-type uses integer IDs.
   */
  protected function entityTypeSupportsKnowledges(EntityTypeInterface $entity_type) {
    return $entity_type->entityClassImplements(FieldableEntityInterface::class) && _knowledge_entity_uses_integer_id($entity_type->id());
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $knowledge_type = $this->entity;
    $status = $knowledge_type->save();

    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')->toString();
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Knowledge type %label has been updated.', ['%label' => $knowledge_type->label()]));
      $this->logger->notice('Knowledge type %label has been updated.', [
        '%label' => $knowledge_type->label(),
        'link' => $edit_link,
      ]);
    }
    else {
      $this->messenger()->addStatus($this->t('Knowledge type %label has been added.', ['%label' => $knowledge_type->label()]));
      $this->logger->notice('Knowledge type %label has been added.', [
        '%label' => $knowledge_type->label(),
        'link' => $edit_link,
      ]);
    }

    $form_state->setRedirectUrl($knowledge_type->toUrl('collection'));

    return $status;
  }

}
