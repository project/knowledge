<?php

namespace Drupal\knowledge;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an item list class for knowledge fields.
 */
class KnowledgeFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function get($index) {
    // The Field API only applies the "field default value" to newly created
    // entities. In the specific case of the "knowledge status", though, we need
    // this default value to be also applied for existing entities created
    // before the knowledge field was added, which have no value stored for the
    // field.
    if ($index == 0 && empty($this->list)) {
      $field_default_value = $this->getFieldDefinition()->getDefaultValue($this->getEntity());
      return $this->appendItem($field_default_value[0]);
    }
    return parent::get($index);
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function offsetExists($offset) {
    // For consistency with what happens in get(), we force offsetExists() to
    // be TRUE for delta 0.
    if ($offset === 0) {
      return TRUE;
    }
    return parent::offsetExists($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($operation === 'edit') {
      // Only users with administer knowledge permission can edit the knowledge
      // status field.
      $result = AccessResult::allowedIfHasPermission($account ?: \Drupal::currentUser(), 'administer knowledge');
      return $return_as_object ? $result : $result->isAllowed();
    }
    if ($operation === 'view') {
      // Only users with "post knowledge" or "access knowledge" permission can
      // view the field value. The formatter,
      // Drupal\knowledge\Plugin\Field\FieldFormatter\KnowledgeDefaultFormatter,
      // takes care of showing the thread and form based on individual
      // permissions, so if a user only has ‘post knowledge’ access, only the
      // form will be shown and not the knowledge.
      $result = AccessResult::allowedIfHasPermission($account ?: \Drupal::currentUser(), 'access knowledge')
        ->orIf(AccessResult::allowedIfHasPermission($account ?: \Drupal::currentUser(), 'post knowledge'));
      return $return_as_object ? $result : $result->isAllowed();
    }
    return parent::access($operation, $account, $return_as_object);
  }

}
