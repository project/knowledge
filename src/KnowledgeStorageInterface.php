<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Defines an interface for knowledge entity storage classes.
 */
interface KnowledgeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Calculates the page number for the first new knowledge.
   *
   * @param int $total_knowledge
   *   The total amount of knowledge that the entity has.
   * @param int $new_knowledge
   *   The number of new knowledge that the entity has.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to which the knowledge belong.
   * @param string $field_name
   *   The field name on the entity to which knowledge is attached.
   *
   * @return array|null
   *   Page number where first new knowledge appears. (First page returns 0.)
   */
  public function getNewKnowledgePageNumber($total_knowledge, $new_knowledge, FieldableEntityInterface $entity, $field_name);

  /**
   * Gets the display ordinal or page number for a knowledge.
   *
   * @param \Drupal\knowledge\KnowledgeInterface $knowledge
   *   The knowledge to use as a reference point.
   * @param int $knowledge_mode
   *   Knowledge display mode: KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT or
   *   KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED.
   * @param int $divisor
   *   Defaults to 1, which returns the display ordinal for a knowledge. If the
   *   amount of knowledge per page is provided, the returned value will be the
   *   page number. (The return value will be divided by $divisor.)
   *
   * @return int
   *   The display ordinal or page number for the knowledge. It is 0-based, so
   *   will represent the number of items before the given knowledge/page.
   */
  public function getDisplayOrdinal(KnowledgeInterface $knowledge, $knowledge_mode, $divisor = 1);

  /**
   * Gets the knowledge ids of the passed knowledge entities' children.
   *
   * @param \Drupal\knowledge\KnowledgeInterface[] $knowledge_links
   *   An array of knowledge entities keyed by their ids.
   *
   * @return array
   *   The entity ids of the passed knowledge entities' children as an array.
   */
  public function getChildKids(array $knowledge_links);

  /**
   * Retrieves knowledge for a thread, sorted in an order suitable for display.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose knowledge(s) needs rendering.
   * @param string $field_name
   *   The field_name whose knowledge(s) needs rendering.
   * @param int $mode
   *   Knowledge display mode: KnowledgeManagerInterface::KNOWLEDGE_MODE_FLAT or
   *   KnowledgeManagerInterface::KNOWLEDGE_MODE_THREADED.
   * @param int $knowledge_per_page
   *   (optional) The amount of knowledge to display per page.
   *   Defaults to 0, which means show all knowledge.
   * @param int $pager_id
   *   (optional) Pager id to use in case of multiple pagers on the one page.
   *   Defaults to 0; is only used when $knowledge_per_page is greater than
   *   zero.
   *
   * @return array
   *   Ordered array of knowledge objects, keyed by knowledge id.
   */
  public function loadThread(EntityInterface $entity, $field_name, $mode, $knowledge_per_page = 0, $pager_id = 0);

  /**
   * Returns the number of unapproved knowledge links.
   *
   * @return int
   *   The number of unapproved knowledge links.
   */
  public function getUnapprovedCount();

}
