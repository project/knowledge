<?php

namespace Drupal\knowledge;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Adherence entity.
 *
 * @see \Drupal\knowledge\Entity\KnowledgeAdherence.
 */
class KnowledgeAdherenceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\knowledge\Entity\KnowledgeAdherenceInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished adherence entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published adherence entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit adherence entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete adherence entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add adherence entities');
  }

}
