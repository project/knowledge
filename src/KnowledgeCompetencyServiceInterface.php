<?php

namespace Drupal\knowledge;

use Drupal\user\UserInterface;

/**
 * Interface for the Knowledge Competency service.
 */
interface KnowledgeCompetencyServiceInterface {

  /**
   * Get the roles.
   *
   * @return array
   *   The roles.
   */
  public function get();

  /**
   * Get the role ids.
   *
   * @return array
   *   The role ids.
   */
  public function getRoleIds();

  /**
   * Check if the user has the role or better.
   *
   * @param string $role
   *   The role.
   * @param array $user_roles
   *   The user roles.
   *
   * @return bool
   *   TRUE if the user has the role or better, FALSE otherwise.
   */
  public function hasRoleOrBetter($role, $user_roles);

  /**
   * Remove lesser competency roles.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   */
  public function doRoleRemoval(UserInterface &$user);

  /**
   * Calculate values to be saved.
   *
   * @param array $roles
   *   The roles.
   * @param array $orginal_roles
   *   The original roles.
   */
  public function doPreSave(&$roles, $orginal_roles);

  /**
   * Perform actions if any after saving.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param array $roles
   *   The roles.
   * @param array $orginal_roles
   *   The original roles.
   */
  public function doPostSave($user, $roles, $orginal_roles);

  /**
   * Get the user competency.
   *
   * @param int $user_id
   *   The user.
   *
   * @return array
   *   The user competency.
   */
  public function getUserCompetency($user_id);

}
