<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides an interface for storing and retrieving knowledge statistics.
 */
interface KnowledgeStatisticsInterface {

  /**
   * Minimum amount of time for statistics.
   */
  const HOURS_24 = 86400;

  /**
   * Returns an array of ranking information for hook_ranking().
   *
   * @return array
   *   Array of ranking information as expected by hook_ranking().
   *
   * @see hook_ranking()
   * @see knowledge_ranking()
   */
  public function getRankingInfo();

  /**
   * Read knowledge statistics records for an array of entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   Array of entities on which knowledge is enabled, keyed by id.
   * @param string $entity_type
   *   The entity type of the passed entities.
   * @param bool $accurate
   *   (optional) Indicates if results must be completely up to date. If set to
   *   FALSE, a replica database will used if available. Defaults to TRUE.
   *
   * @return object[]
   *   Array of statistics records.
   */
  public function read(array $entities, $entity_type, $accurate = TRUE);

  /**
   * Delete knowledge statistics records for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which knowledge statistics should be deleted.
   */
  public function delete(EntityInterface $entity);

  /**
   * Update or insert knowledge statistics records after a knowledge is added.
   *
   * @param \Drupal\knowledge\KnowledgeInterface $knowledge
   *   The knowledge added or updated.
   */
  public function update(KnowledgeInterface $knowledge);

  /**
   * Find the maximum number of knowledge for the given entity type.
   *
   * Used to influence search rankings.
   *
   * @param string $entity_type
   *   The entity type to consider when fetching the maximum knowledge count.
   *
   * @return int
   *   The maximum number of knowledge for and entity of the given type.
   *
   * @see knowledge_update_index()
   */
  public function getMaximumCount($entity_type);

  /**
   * Insert an empty record for the given entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The created entity for which a statistics record is to be initialized.
   * @param array $fields
   *   Array of knowledge field definitions for the given entity.
   */
  public function create(FieldableEntityInterface $entity, array $fields);

}
