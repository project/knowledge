<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\EntityInterface;

/**
 * Knowledge manager contains common functions to manage knowledge fields.
 */
interface KnowledgeManagerInterface {

  /**
   * Knowledge is displayed in a flat list - expanded.
   */
  const KNOWLEDGE_MODE_FLAT = 0;

  /**
   * Knowledge is displayed as a threaded list - expanded.
   */
  const KNOWLEDGE_MODE_THREADED = 1;

  /**
   * Utility function to return an array of knowledge fields.
   *
   * @param string $entity_type_id
   *   The content entity type to which the knowledge fields are attached.
   *
   * @return array
   *   An array of knowledge field map definitions, keyed by field name. Each
   *   value is an array with two entries:
   *   - type: The field type.
   *   - bundles: Bundles in which the field appears, as an array with entity
   *     types as keys and the array of bundle names as values.
   */
  public function getFields($entity_type_id);

  /**
   * Provides a message if posting knowledge is forbidden.
   *
   * If authenticated users can post knowledge, a message is returned that
   * prompts the anonymous user to log in (or register, if applicable) that
   * redirects to entity knowledge form. Otherwise, no message is returned.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to which knowledge is attached to.
   * @param string $field_name
   *   The field name on the entity to which knowledge is attached to.
   *
   * @return string
   *   HTML for a "you can't post knowledge" notice.
   */
  public function forbiddenMessage(EntityInterface $entity, $field_name);

  /**
   * Returns the number of new links available on a given entity for a user.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to which the knowledge is attached to.
   * @param string $field_name
   *   (optional) The field_name to count knowledge for. Defaults to any field.
   * @param int $timestamp
   *   (optional) Time to count from. Defaults to time of last user access the
   *   entity.
   *
   * @return int|false
   *   The number of new links or FALSE if the user is not authenticated.
   */
  public function getCountNewKnowledges(EntityInterface $entity, $field_name = NULL, $timestamp = 0);

}
