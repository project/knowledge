<?php

namespace Drupal\knowledge;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the storage handler class for knowledge.
 *
 * This extends the Drupal\Core\Entity\Sql\SqlContentEntityStorage class,
 * adding required special handling for knowledge entities.
 */
class KnowledgeStorage extends SqlContentEntityStorage implements KnowledgeStorageInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a KnowledgeStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_info
   *   An array of entity info for the entity type.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_info, Connection $database, EntityFieldManagerInterface $entity_field_manager, AccountInterface $current_user, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_info, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_info) {
    return new static(
      $entity_info,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDataTable() {
    return 'knowledge';
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayOrdinal(KnowledgeInterface $knowledge, $knowledge_mode, $divisor = 1) {
    // Count how many knowledge (k1) are before $knowledge (k2) in display
    // order. This is the 0-based display ordinal.
    $data_table = $this->getDataTable();
    $query = $this->database->select($data_table, 'c1');
    $query->innerJoin($data_table, 'c2', '[c2].[entity_id] = [c1].[entity_id] AND [c2].[entity_type] = [c1].[entity_type] AND [c2].[field_name] = [c1].[field_name]');
    $query->addExpression('COUNT(*)', 'count');
    $query->condition('c2.kid', $knowledge->id());
    if (!$this->currentUser->hasPermission('administer knowledge')) {
      $query->condition('c1.status', KnowledgeInterface::PUBLISHED);
    }

    // For rendering flat knowledge, kid is used for ordering knowledge due to
    // unpredictable behavior with timestamp, so we make the same assumption
    // here.
    $query->condition('c1.kid', $knowledge->id(), '<');

    // $query->condition('c1.default_langcode', 1);
    // $query->condition('c2.default_langcode', 1);
    $ordinal = $query->execute()->fetchField();

    return ($divisor > 1) ? floor($ordinal / $divisor) : $ordinal;
  }

  /**
   * {@inheritdoc}
   */
  public function getNewKnowledgePageNumber($total_knowledge, $new_knowledge, FieldableEntityInterface $entity, $field_name) {
    $field = $entity->getFieldDefinition($field_name);
    $knowledge_per_page = $field->getSetting('per_page');

    if ($total_knowledge <= $knowledge_per_page) {
      // Only one page of knowledge.
      $count = 0;
    }
    else {
      // Flat knowledge.
      $count = $total_knowledge - $new_knowledge;
    }

    return $knowledge_per_page > 0 ? (int) ($count / $knowledge_per_page) : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildKids(array $knowledge_links) {
    return $this->database->select($this->getDataTable(), 'c')
      ->fields('c', ['kid'])
      // ->condition('default_langcode', 1)
      ->execute()
      ->fetchCol();
  }

  /**
   * {@inheritdoc}
   *
   * To display threaded knowledge in the correct order we keep a 'thread' field
   * and order by that value. This field keeps this data in
   * a way which is easy to update and convenient to use.
   *
   * A "thread" value starts at "1". If we add a child (A) to this knowledge,
   * we assign it a "thread" = "1.1". A child of (A) will have "1.1.1". Next
   * brother of (A) will get "1.2". Next brother of the parent of (A) will get
   * "2" and so on.
   *
   * First of all note that the thread field stores the depth of the knowledge:
   * depth 0 will be "X", depth 1 "X.X", depth 2 "X.X.X", etc.
   *
   * Now to get the ordering right, consider this example:
   *
   * 1
   * 1.1
   * 1.1.1
   * 1.2
   * 2
   *
   * If we "ORDER BY thread ASC" we get the above result, and this is the
   * natural order sorted by time. However, if we "ORDER BY thread DESC"
   * we get:
   *
   * 2
   * 1.2
   * 1.1.1
   * 1.1
   * 1
   *
   * Clearly, this is not a natural way to see a thread, and users will get
   * confused. The natural order to show a thread by time desc would be:
   *
   * 2
   * 1
   * 1.2
   * 1.1
   * 1.1.1
   *
   * which is what we already did before the standard pager patch. To achieve
   * this we simply add a "/" at the end of each "thread" value. This way, the
   * thread fields will look like this:
   *
   * 1/
   * 1.1/
   * 1.1.1/
   * 1.2/
   * 2/
   *
   * we add "/" since this char is, in ASCII, higher than every number, so if
   * now we "ORDER BY thread DESC" we get the correct order. However this would
   * spoil the reverse ordering, "ORDER BY thread ASC" -- here, we do not need
   * to consider the trailing "/" so we use a substring only.
   */
  public function loadThread(EntityInterface $entity, $field_name, $mode, $knowledge_per_page = 0, $pager_id = 0) {
    $data_table = $this->getDataTable();
    $query = $this->database->select($data_table, 'c');
    $query->addField('c', 'kid');
    $query
      ->condition('c.entity_id', $entity->id())
      ->condition('c.entity_type', $entity->getEntityTypeId())
      ->condition('c.field_name', $field_name)
      ->addTag('entity_access')
      ->addTag('knowledge_filter')
      ->addMetaData('base_table', 'knowledge')
      ->addMetaData('entity', $entity)
      ->addMetaData('field_name', $field_name);

    if ($knowledge_per_page) {
      $query = $query->extend(PagerSelectExtender::class)
        ->limit($knowledge_per_page);
      if ($pager_id) {
        $query->element($pager_id);
      }

      $count_query = $this->database->select($data_table, 'c');
      $count_query->addExpression('COUNT(*)');
      $count_query
        ->condition('c.entity_id', $entity->id())
        ->condition('c.entity_type', $entity->getEntityTypeId())
        ->condition('c.field_name', $field_name)
        // ->condition('c.default_langcode', 1)
        ->addTag('entity_access')
        ->addTag('knowledge_filter')
        ->addMetaData('base_table', 'knowledge')
        ->addMetaData('entity', $entity)
        ->addMetaData('field_name', $field_name);
      $query->setCountQuery($count_query);
    }

    if (!$this->currentUser->hasPermission('administer knowledge')) {
      $query->condition('c.status', KnowledgeInterface::PUBLISHED);
      if ($knowledge_per_page) {
        $count_query->condition('c.status', KnowledgeInterface::PUBLISHED);
      }
    }

    $query->orderBy('c.kid', 'DESC');
    $kids = $query->execute()->fetchCol();

    $knowledge_links = [];
    if ($kids) {
      $knowledge_links = $this->loadMultiple($kids);
    }

    return $knowledge_links;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnapprovedCount() {
    return $this->database->select($this->getDataTable(), 'c')
      ->condition('status', KnowledgeInterface::NOT_PUBLISHED, '=')
      // ->condition('default_langcode', 1)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}
