<?php

namespace Drupal\knowledge\Plugin\Menu\LocalTask;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\knowledge\KnowledgeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a local task that shows the amount of unapproved knowledge.
 */
class UnapprovedKnowledge extends LocalTaskDefault implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The knowledge storage service.
   *
   * @var \Drupal\knowledge\KnowledgeStorageInterface
   */
  protected $knowledgeStorage;

  /**
   * Construct the UnapprovedKnowledge object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\knowledge\KnowledgeStorageInterface $knowledge_storage
   *   The knowledge storage service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, KnowledgeStorageInterface $knowledge_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->knowledgeStorage = $knowledge_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('knowledge')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(?Request $request = NULL) {
    return $this->t('Unlinked knowledge (@count)', ['@count' => $this->knowledgeStorage->getUnapprovedCount()]);
  }

}
