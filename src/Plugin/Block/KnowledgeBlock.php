<?php

namespace Drupal\knowledge\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allow linking outside the knowledge field.
 *
 * @Block(
 *   id = "knowledge_block",
 *   admin_label = @Translation("Knowledge block"),
 *   category = @Translation("Knowledge")
 * )
 */
class KnowledgeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Knowledge Block constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteMatchInterface $current_route_match,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Type'),
      '#description' => $this->t('The knowledge link type.'),
      '#default_value' => $config['type'] ?? NULL,
    ];

    $form['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field name'),
      '#description' => $this->t('The knowledge link field name.'),
      '#default_value' => $config['field_name'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $type = $form_state->getValue('type');
    $field_name = $form_state->getValue('field_name');

    $this->setConfigurationValue('type', $type);
    $this->setConfigurationValue('field_name', $field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $routeMatch = $this->currentRouteMatch;

    $entity = $routeMatch->getParameter('node');
    if (empty($entity)) {
      return [];
    }
    $configuration = $this->getConfiguration();
    $field_name = $configuration['field_name'];
    $knowledge_type = $configuration['type'];
    $output['knowledge_form'] = [
      '#lazy_builder' => [
        'knowledge.lazy_builders:renderForm',
        [
          $entity->getEntityTypeId(),
          $entity->id(),
          $field_name,
          $knowledge_type,
        ],
      ],
      '#create_placeholder' => TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
