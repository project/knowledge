<?php

namespace Drupal\knowledge\Plugin\Field\FieldType;

/**
 * Interface definition for Knowledge items.
 */
interface KnowledgeItemInterface {

  /**
   * Knowledges for this entity are hidden.
   */
  const HIDDEN = 0;

  /**
   * Knowledges for this entity are closed.
   */
  const CLOSED = 1;

  /**
   * Knowledges for this entity are open.
   */
  const OPEN = 2;

  /**
   * Knowledge form should be displayed on a separate page.
   */
  const FORM_SEPARATE_PAGE = 0;

  /**
   * Knowledge form should be shown below post or list of knowledge.
   */
  const FORM_BELOW = 1;

}
