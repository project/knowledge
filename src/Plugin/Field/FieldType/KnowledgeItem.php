<?php

namespace Drupal\knowledge\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;
use Drupal\knowledge\Entity\KnowledgeType;
use Drupal\knowledge\KnowledgeInterface;

/**
 * Plugin implementation of the 'knowledge' field type.
 *
 * @FieldType(
 *   id = "knowledge",
 *   label = @Translation("Knowledges"),
 *   description = @Translation("This field manages configuration and presentation of knowledge on an entity."),
 *   list_class = "\Drupal\knowledge\KnowledgeFieldItemList",
 *   default_widget = "knowledge_default",
 *   default_formatter = "knowledge_default",
 *   cardinality = 1,
 * )
 */
class KnowledgeItem extends FieldItemBase implements KnowledgeItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'knowledge_type' => '',
      'short_period' => '15',
      'medium_period' => '90',
      'long_period' => '365',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'per_page' => 50,
      'form_location' => KnowledgeItemInterface::FORM_BELOW,
      'anonymous' => KnowledgeInterface::ANONYMOUS_MAYNOT_CONTACT,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['status'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Knowledge status'))
      ->setRequired(TRUE);

    $properties['kid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Last knowledge ID'));

    $properties['last_knowledge_timestamp'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Last knowledge timestamp'))
      ->setDescription(new TranslatableMarkup('The time that the last knowledge was created.'));

    $properties['last_knowledge_uid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Last knowledge user ID'));

    $properties['total_count'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Amount of knowledge'))
      ->setDescription(new TranslatableMarkup('The amount of knowledge.'));

    $properties['long_count'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Long knowledge count'))
      ->setDescription(new TranslatableMarkup('The amount of knowledge.'));

    $properties['medium_count'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Medium knowledge count'))
      ->setDescription(new TranslatableMarkup('The amount of knowledge.'));

    $properties['short_count'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Short knowledge count'))
      ->setDescription(new TranslatableMarkup('The amount of knowledge.'));

    $properties['total_citation'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Amount of citations'))
      ->setDescription(new TranslatableMarkup('The amount of citations.'));

    $properties['long_citation'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Long citation count'))
      ->setDescription(new TranslatableMarkup('The amount of citations.'));

    $properties['medium_citation'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Medium citation count'))
      ->setDescription(new TranslatableMarkup('The amount of citations.'));

    $properties['short_citation'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Short citation count'))
      ->setDescription(new TranslatableMarkup('The amount of citations.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'status' => [
          'description' => 'Whether knowledge is allowed on this entity: 0 = no, 1 = closed (read only), 2 = open (read/write).',
          'type' => 'int',
          'default' => 0,
        ],
      ],
      'indexes' => [],
      'foreign keys' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $settings = $this->getSettings();

    $anonymous_user = new AnonymousUserSession();

    $element['per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Links per page'),
      '#default_value' => $settings['per_page'],
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 1000,
    ];
    $element['anonymous'] = [
      '#type' => 'select',
      '#title' => $this->t('Anonymous linking'),
      '#default_value' => $settings['anonymous'],
      '#options' => [
        KnowledgeInterface::ANONYMOUS_MAYNOT_CONTACT => $this->t('Anonymous posters may not enter their contact information'),
        KnowledgeInterface::ANONYMOUS_MAY_CONTACT => $this->t('Anonymous posters may leave their contact information'),
        KnowledgeInterface::ANONYMOUS_MUST_CONTACT => $this->t('Anonymous posters must leave their contact information'),
      ],
      '#access' => $anonymous_user->hasPermission('post knowledge'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'status';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // There is always a value for this field, it is one of
    // KnowledgeItemInterface::OPEN, KnowledgeItemInterface::CLOSED or
    // KnowledgeItemInterface::HIDDEN.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    // @todo Inject entity storage once typed-data supports container injection.
    //   See https://www.drupal.org/node/2053415 for more details.
    $knowledge_types = KnowledgeType::loadMultiple();
    $options = [];
    $entity_type = $this->getEntity()->getEntityTypeId();
    foreach ($knowledge_types as $knowledge_type) {
      if ($knowledge_type->getTargetEntityTypeId() == $entity_type) {
        $options[$knowledge_type->id()] = $knowledge_type->label();
      }
    }
    $element['knowledge_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Knowledge type'),
      '#options' => $options,
      '#required' => TRUE,
      '#description' => $this->t('Select the Knowledge type to use for this knowledge field. Manage the knowledge types from the <a href=":url">administration overview page</a>.', [':url' => Url::fromRoute('entity.knowledge_type.collection')->toString()]),
      '#default_value' => $this->getSetting('knowledge_type'),
      '#disabled' => $has_data,
    ];

    $element['short_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Short Frequency'),
      '#default_value' => $this->getSetting('short_period'),
    ];
    $element['medium_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Medium Frequency'),
      '#default_value' => $this->getSetting('medium_period'),
    ];
    $element['long_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Long Frequency'),
      '#default_value' => $this->getSetting('long_period'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $statuses = [
      KnowledgeItemInterface::HIDDEN,
      KnowledgeItemInterface::CLOSED,
      KnowledgeItemInterface::OPEN,
    ];
    return [
      'status' => $statuses[mt_rand(0, count($statuses) - 1)],
    ];
  }

}
