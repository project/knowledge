<?php

namespace Drupal\knowledge\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;

/**
 * Provides a default knowledge widget.
 *
 * @FieldWidget(
 *   id = "knowledge_default",
 *   label = @Translation("Knowledge"),
 *   field_types = {
 *     "knowledge"
 *   }
 * )
 */
class KnowledgeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $items->getEntity();

    $element['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Knowledge'),
      '#title_display' => 'invisible',
      '#default_value' => $items->status,
      '#options' => [
        KnowledgeItemInterface::OPEN => $this->t('Open'),
        KnowledgeItemInterface::CLOSED => $this->t('Closed'),
        KnowledgeItemInterface::HIDDEN => $this->t('Hidden'),
      ],
      KnowledgeItemInterface::OPEN => [
        '#description' => $this->t('Users with the "Post knowledge" permission can post knowledge.'),
      ],
      KnowledgeItemInterface::CLOSED => [
        '#description' => $this->t('Users cannot post knowledge, but existing knowledge will be displayed.'),
      ],
      KnowledgeItemInterface::HIDDEN => [
        '#description' => $this->t('Knowledge is hidden from view.'),
      ],
    ];
    // If the entity doesn't have any knowledge, the "hidden" option makes no
    // sense, so don't even bother presenting it to the user unless this is the
    // default value widget on the field settings form.
    if (!$this->isDefaultValueWidget($form_state) && !$items->total_count) {
      $element['status'][KnowledgeItemInterface::HIDDEN]['#access'] = FALSE;
      // Also adjust the description of the "closed" option.
      $element['status'][KnowledgeItemInterface::CLOSED]['#description'] = $this->t('Users cannot post knowledge.');
    }
    // If the advanced settings tabs-set is available (normally rendered in the
    // second column on wide-resolutions), place the field as a details element
    // in this tab-set.
    if (isset($form['advanced'])) {
      // Get default value from the field.
      $field_default_values = $this->fieldDefinition->getDefaultValue($entity);

      // Override widget title to be helpful for end users.
      $element['#title'] = $this->t('Link settings');

      $element += [
        '#type' => 'details',
        // Open the details when the selected value is different to the stored
        // default values for the field.
        '#open' => ($items->status != $field_default_values[0]['status']),
        '#group' => 'advanced',
        '#attributes' => [
          'class' => ['knowledge-' . Html::getClass($entity->getEntityTypeId()) . '-settings-form'],
        ],
        '#attached' => [
          'library' => ['knowledge/drupal.knowledge'],
        ],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Add default values for statistics properties because we don't want to
    // have them in form.
    foreach ($values as &$value) {
      $value += [
        'kid' => 0,
        'last_knowledge_timestamp' => 0,
        'last_knowledge_uid' => 0,
        'total_count' => 0,
      ];
    }
    return $values;
  }

}
