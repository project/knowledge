<?php

namespace Drupal\knowledge\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;

/**
 * Plugin implementation of the 'knowledge_permalink' formatter.
 *
 * All the other entities use 'canonical' or 'revision' links to link the entity
 * to itself but knowledge use permalink URL.
 *
 * @FieldFormatter(
 *   id = "knowledge_permalink",
 *   label = @Translation("Knowledge Permalink"),
 *   field_types = {
 *     "string",
 *     "uri",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class KnowledgePermalinkFormatter extends StringFormatter {

  /**
   * {@inheritdoc}
   */
  protected function getEntityUrl(EntityInterface $knowledge) {
    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge_permalink = $knowledge->permalink();

    return $knowledge_permalink;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return parent::isApplicable($field_definition) && $field_definition->getTargetEntityTypeId() === 'knowledge' && $field_definition->getName() === 'subject';
  }

}
