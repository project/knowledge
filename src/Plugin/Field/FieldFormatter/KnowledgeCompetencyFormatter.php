<?php

namespace Drupal\knowledge\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BooleanFormatter;

/**
 * Plugin implementation of the 'Competency' formatter.
 *
 * @FieldFormatter(
 *   id = "knowledge_competency",
 *   label = @Translation("Competency"),
 *   field_types = {
 *     "boolean"
 *   }
 * )
 */
class KnowledgeCompetencyFormatter extends BooleanFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $definition = $items->getFieldDefinition();
    $description = $definition->getDescription();
    foreach ($items as $delta => $item) {
      $elements[$delta]['#suffix'] = '<div>' . $description . '</div><br/>';
    }

    return $elements;
  }

}
