<?php

namespace Drupal\knowledge\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\EntityConfigBase;
use Drupal\migrate\Row;

/**
 * Migrates the knowledge type.
 *
 * @MigrateDestination(
 *   id = "entity:knowledge_type"
 * )
 */
class EntityKnowledgeType extends EntityConfigBase {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $entity_ids = parent::import($row, $old_destination_id_values);
    return $entity_ids;
  }

}
