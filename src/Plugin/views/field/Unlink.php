<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to reply to a knowledge.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("knowledge_link_reply")
 */
class Unlink extends LinkBase {

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Unlink');
  }

}
