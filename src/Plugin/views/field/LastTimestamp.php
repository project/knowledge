<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\Date;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Field handler to display the timestamp of a link with the count of knowledge.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("knowledge_last_timestamp")
 */
class LastTimestamp extends Date {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->additional_fields['total_count'] = 'total_count';
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $total_count = $this->getValue($values, 'total_count');
    if (empty($this->options['empty_zero']) || $total_count) {
      return parent::render($values);
    }
    else {
      return NULL;
    }
  }

}
