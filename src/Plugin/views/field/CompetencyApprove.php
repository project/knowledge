<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Url;
use Drupal\knowledge\KnowledgeCompetencyServiceInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contact User Relation field plugin.
 *
 * @ViewsField("knowledge_competency_approve")
 */
class CompetencyApprove extends FieldPluginBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path current service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * The competency service.
   *
   * @var \Drupal\knowledge\KnowledgeCompetencyServiceInterface
   */
  protected $competencyService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('knowledge.competency'),
    );
  }

  /**
   * Creates knowledge competency filter plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The renderer service.
   * @param \Drupal\knowledge\KnowledgeCompetencyServiceInterface $competency_service
   *   The competency service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    CurrentPathStack $path_current,
    KnowledgeCompetencyServiceInterface $competency_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->pathCurrent = $path_current;
    $this->competencyService = $competency_service;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $entity = $values->_entity;
    $entity_id = $entity->id();

    $current_path = $this->pathCurrent->getPath();
    $link = NULL;

    $competency = $this->competencyService->getUserCompetency($entity_id);
    $role_id = $competency->isPending();
    if ($role_id) {
      $role = $this->competencyService->getPromotionRole($role_id);
      $role = $this->entityTypeManager->getStorage('user_role')->load($role);
      $text = $this->t('Promote to @role', ['@role' => $role->label()]);
      $current_url = Url::fromUri("internal:$current_path")->toString();
      $url = Url::fromRoute(
        'entity.user.knowledge_competency.approval_form',
        ['user' => $entity_id],
        [
          'query' => ['destination' => $current_url],
        ]
      );

      $link = Link::fromTextAndUrl($text, $url)->toString();
    }

    return $link;
  }

}
