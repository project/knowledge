<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\knowledge\KnowledgeLinkRelationshipInterface;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a field that links to the user contact page, if access is permitted.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("knowledge_adherence_link")
 */
class AdherenceLink extends LinkBase {

  /**
   * The id of the knowledge adherence.
   *
   * @var int|null
   */
  protected $adherenceId;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $account;

  /**
   * The path current service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * The link relationship service.
   *
   * @var \Drupal\knowledge\KnowledgeLinkRelationshipInterface
   */
  protected $link;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('access_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('current_user'),
      $container->get('path.current'),
      $container->get('knowledge.link_relationship'),
    );
  }

  /**
   * Creates node type filter plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Session\AccountProxy $account
   *   The entity type manager service.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The renderer service.
   * @param \Drupal\knowledge\KnowledgeLinkRelationshipInterface $link_relationship
   *   The link relationship service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccessManagerInterface $access_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    AccountProxy $account,
    CurrentPathStack $path_current,
    KnowledgeLinkRelationshipInterface $link_relationship,
  ) {
    parent::__construct(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $access_manager,
        $entity_type_manager,
        $entity_repository,
        $language_manager);
    $this->account = $account;
    $this->pathCurrent = $path_current;
    $this->link = $link_relationship;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {

    $build = [
      '#markup' => $this->renderLink($row),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['text']);

    $form['incident_type'] = [
      '#title' => $this->t('Incident Type'),
      '#type' => 'select',
      '#options' => $this->getIncidentOptions(),
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['incident_type'] = [
      'default' => '',
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    if (empty($this->adherenceId)) {
      $this->getAdherenceId($row);
    }
    $incident_type = $this->options['incident_type'];
    $incident_id = $row->id;
    $query['destination'] = $this->pathCurrent->getPath();
    if ($this->adherenceId) {
      return Url::fromRoute('entity.knowledge_adherence.edit_form', [
        'knowledge_adherence' => $this->adherenceId,
      ],
      [
        'query' => $query,
      ]);
    }
    else {
      $query['incident_id'] = $incident_id;
      $query['incident_type'] = $incident_type;

      return Url::fromRoute('entity.knowledge_adherence.add_form', [], [
        'query' => $query,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $adherence_id = $this->getAdherenceId($row);
    $title = "";
    if (empty($adherence_id)) {
      if ($this->account->hasPermission('add adherence entities')) {
        $title = $this->t('Add');
        $this->options['alter']['make_link'] = TRUE;
      }
    }
    else {
      $adherence_storage = $this->entityTypeManager
        ->getStorage('knowledge_adherence');
      /** @var \Drupal\knowledge\Entity\KnowledgeAdherenceInterface $adherence */
      $adherence = $adherence_storage->load($this->adherenceId);
      $title = $adherence->disposition->value;
      if ($this->account->hasPermission('edit adherence entities')) {
        $this->options['alter']['make_link'] = TRUE;
      }
    }

    $this->options['alter']['url'] = $this->getUrlInfo($row);

    return $title;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    if ($this->adherenceId) {
      return $this->t('Edit');
    }
    return $this->t('Add');
  }

  /**
   * Returns the adherence Id, if one exists.
   */
  protected function getAdherenceId(ResultRow $row) {
    $incident_type = $this->options['incident_type'];
    $incident_id = $row->id;
    $link_name = 'knowledge_' . $incident_type . '_kid';

    $query = $this->entityTypeManager
      ->getStorage('knowledge_adherence')
      ->getQuery();

    $query
      ->condition('incident_type', $incident_type)
      ->condition('incident_id', $incident_id);

    $link_value = $row->{$link_name};
    if (is_null($link_value)) {
      $query->condition('knowledge_id', NULL, 'IS NULL');
    }
    else {
      $query->condition('knowledge_id', $link_value);
    }

    $adherence_id = $query->accessCheck(FALSE)->execute();
    $this->adherenceId = current($adherence_id);

    return $this->adherenceId;
  }

  /**
   * Returns the incident types.
   */
  protected function getIncidentOptions() {
    $incident_types = $this->link->getIncidentEntityTypes();

    $options = [];
    foreach ($incident_types as $name => $type) {
      $options[$name] = $type->get('label');
    }

    return $options;
  }

}
