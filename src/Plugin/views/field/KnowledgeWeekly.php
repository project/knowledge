<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\Component\Utility\Xss as UtilityXss;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Render\ViewsRenderPipelineMarkup;

/**
 * Field handler to display the hour (server) of the visit.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("knowledge_weekly")
 */
final class KnowledgeWeekly extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    // Add the field.
    $params = $this->options['group_type'] != 'group' ? ['function' => $this->options['group_type']] : [];
    $this->field_alias = $this->query->addField(NULL, 'date_format(from_unixtime(' . $this->tableAlias . ".created), '%X%V')", 'knowledge_weekly', $params);

    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {

    $value = $this->getValue($values);
    $year = (int) substr($value, 0, 4);
    $week = (int) substr($value, 4, 2);

    // Converts week of year to date.
    $date = new \DateTime();
    $date->setISODate($year, $week);

    return ViewsRenderPipelineMarkup::create(UtilityXss::filterAdmin($date->format('Y-m-d')));
  }

}
