<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\ResultRow;

/**
 * Views field display for linked incident.
 *
 * @ViewsField("linked_incident")
 */
class KnowledgeedIncident extends EntityField {

  /**
   * Array of entities that has knowled.
   *
   * We use this to load all the linked entities of same entity type at once
   * to the EntityStorageController static cache.
   *
   * @var array
   */
  protected $loadedKnowledgeedIncidents = [];

  /**
   * {@inheritdoc}
   */
  public function getItems(ResultRow $values) {
    if (empty($this->loadedKnowledgeedIncidents)) {
      $result = $this->view->result;

      $entity_ids_per_type = [];
      foreach ($result as $value) {
        /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
        $knowledge = $this->getEntity($value);
        if ($knowledge) {
          $entity_ids_per_type[$knowledge->getIncidentEntityTypeId()][] = $knowledge->getIncidentEntityId();
        }
      }

      foreach ($entity_ids_per_type as $type => $ids) {
        $this->loadedKnowledgeedIncidents[$type] = $this->entityTypeManager->getStorage($type)->loadMultiple($ids);
      }
    }

    return parent::getItems($values);
  }

}
