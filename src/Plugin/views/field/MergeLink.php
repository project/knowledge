<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a field that links to the user contact page, if access is permitted.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("knowledge_merge_link")
 */
class MergeLink extends LinkBase {

  /**
   * The id of the knowledge adherence.
   *
   * @var int|null
   */
  protected $adherenceId;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $account;

  /**
   * The path current service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('access_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('current_user'),
      $container->get('path.current'),
    );
  }

  /**
   * Creates node type filter plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Session\AccountProxy $account
   *   The entity type manager service.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The renderer service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccessManagerInterface $access_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    AccountProxy $account,
    CurrentPathStack $path_current,
  ) {
    parent::__construct(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $access_manager,
        $entity_type_manager,
        $entity_repository,
        $language_manager);
    $this->account = $account;
    $this->pathCurrent = $path_current;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {

    $build = [
      '#markup' => $this->renderLink($row),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['text']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {

    $original_id = $row->_relationship_entities['flag_relationship']
      ->field_node_ref
      ->target_id;
    $duplicate_id = $row->_entity->id();

    $query['destination'] = $this->pathCurrent->getPath();

    return Url::fromRoute('knowledge.node.merge', [
      'original' => $original_id,
      'duplicate' => $duplicate_id,
    ],
    [
      'query' => $query,
    ]);

  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $title = '';
    if ($this->account->hasPermission('merge duplicate knowledge')) {
      $title = $this->t('Merge');
      $this->options['alter']['make_link'] = TRUE;
    }

    $this->options['alter']['url'] = $this->getUrlInfo($row);

    return $title;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Merge');
  }

}
