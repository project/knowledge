<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\views\Plugin\views\field\Date;

/**
 * Field handler to display the newer of last knowledge / node updated.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("knowledge_ces_last_updated")
 */
class StatisticsLastUpdated extends Date {

  /**
   * The node table.
   *
   * @var string
   */
  // phpcs:ignore
  protected $node_table;

  /**
   * {@inheritdoc}
   */
  // phpcs:ignore
  public $field_alias;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $this->node_table = $query->ensureTable('node_field_data', $this->relationship);
    $this->field_alias = $query->addField(NULL, "GREATEST(" . $this->node_table . ".changed, " . $this->tableAlias . ".last_knowledge_timestamp)", $this->tableAlias . '_' . $this->field);
  }

}
