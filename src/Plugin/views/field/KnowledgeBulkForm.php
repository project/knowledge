<?php

namespace Drupal\knowledge\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a knowledge operations bulk form element.
 *
 * @ViewsField("knowledge_bulk_form")
 */
class KnowledgeBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('Select one or more knowledge to perform the update on.');
  }

}
