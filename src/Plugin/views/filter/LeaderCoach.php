<?php

namespace Drupal\knowledge\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Views;

/**
 * Filter handler to check for nodes that user posted or knowledge on.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("knowledge_leader_coach")
 */
class LeaderCoach extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    // Alias for the main table.
    $alias = $this->tableAlias;

    $configuration = [
      'left_table' => $alias,
      'left_field' => 'uid',
      'table' => 'user__knowledge_leader',
      'field' => 'entity_id',
      'operator' => '=',
      'type' => 'INNER',
    ];

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $this->query->addRelationship('k_llc', $join, 'user__knowledge_leader');

    $configuration = [
      'left_table' => $alias,
      'left_field' => 'uid',
      'table' => 'user__knowledge_coach',
      'field' => 'entity_id',
      'operator' => '=',
      'type' => 'INNER',
    ];

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $this->query->addRelationship('k_clc', $join, 'user__knowledge_coach');

    $this->query->addWhereExpression($this->options['group'], 'k_llc.knowledge_leader_target_id = k_clc.knowledge_coach_target_id');

  }

}
