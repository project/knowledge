<?php

namespace Drupal\knowledge\Plugin\views\filter;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters by given list of node title options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("knowledge_node_type")
 */
class KnowledgeNodeType extends InOperator {

  /**
   * Knowledge module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $knowledgeSettings;

  /**
   * The node type storage service.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('node_type'),
      $container->get('config.factory')->get('knowledge.settings')
    );
  }

  /**
   * Creates node type filter plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $node_storage
   *   The node type storage service.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The knowledge settings.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ConfigEntityStorageInterface $node_storage, ImmutableConfig $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nodeTypeStorage = $node_storage;
    $this->knowledgeSettings = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = $this->t('Allowed node types');
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * {@inheritdoc}
   */
  public function generateOptions() {
    $node_types = $this->nodeTypeStorage
      ->loadMultiple($this->knowledgeSettings->get('node_types'));
    $types = [];
    foreach ($node_types as $machine => $type) {
      $types[$machine] = $type->label();
    }

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['config:knowledge.settings.node_types'];
  }

}
