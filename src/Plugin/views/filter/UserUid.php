<?php

namespace Drupal\knowledge\Plugin\views\filter;

use Drupal\Core\Database\Database;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter handler to check for nodes that user posted or knowledge on.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("knowledge_user_uid")
 */
class UserUid extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    $subselect = Database::getConnection()->select('knowledge', 'c');
    $subselect->addField('c', 'kid');
    $subselect->condition('c.uid', $this->value, $this->operator);

    $entity_id = $this->definition['entity_id'];
    $entity_type = $this->definition['entity_type'];
    $subselect->where("[c].[entity_id] = [$this->tableAlias].[$entity_id]");
    $subselect->condition('c.entity_type', $entity_type);

    /** @var \Drupal\views\Plugin\views\query\Sql $view_query */
    $view_query = $this->view->query;
    $condition = ($view_query->getConnection()->condition('OR'))
      ->condition("$this->tableAlias.uid", $this->value, $this->operator)
      ->exists($subselect);

    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $query->addWhere($this->options['group'], $condition);
  }

}
