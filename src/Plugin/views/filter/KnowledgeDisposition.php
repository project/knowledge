<?php

namespace Drupal\knowledge\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;

/**
 * Filters by given list of node title options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("knowledge_disposition")
 */
class KnowledgeDisposition extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = $this->t('Knowledge Disposition');
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * Override the query.
   *
   * If no filtering takes place if the user doesn't select any options.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation.
   *
   * If no options have been chosen so we can use it as a non-filter.
   */
  public function validate() {
    if (!empty($this->value)) {
      return parent::validate();
    }

    return NULL;
  }

  /**
   * Helper function that generates the options.
   *
   * @return string[]
   *   The valid disposition values.
   */
  public function generateOptions() {
    return [
      'Create Loss' => 'Create Loss',
      'Created' => 'Created',
      'Improve loss' => 'Improve loss',
      'Inaccurate Reuse' => 'Inaccurate Reuse',
      'No Link Required' => 'No Link Required',
      'Reuse' => 'Reuse',
      'Reuse Loss' => 'Reuse Loss',
    ];
  }

}
