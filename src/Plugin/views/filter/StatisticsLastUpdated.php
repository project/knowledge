<?php

namespace Drupal\knowledge\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\Date;

/**
 * Filter handler for the newer of last knowledge / node updated.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("knowledge_ces_last_updated")
 */
class StatisticsLastUpdated extends Date {

  /**
   * The node table.
   *
   * @var string
   */
  // phpcs:ignore
  protected $node_table;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $this->node_table = $query->ensureTable('node', $this->relationship);

    $field = "GREATEST(" . $this->node_table . ".changed, " . $this->tableAlias . ".last_knowledge_timestamp)";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

}
