<?php

namespace Drupal\knowledge\Plugin\views\filter;

use Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface;
use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Filter based on knowledge node status.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("node_knowledge")
 */
class NodeKnowledge extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    $this->valueOptions = [
      KnowledgeItemInterface::HIDDEN => $this->t('Hidden'),
      KnowledgeItemInterface::CLOSED => $this->t('Closed'),
      KnowledgeItemInterface::OPEN => $this->t('Open'),
    ];
    return $this->valueOptions;
  }

}
