<?php

namespace Drupal\knowledge\Plugin\views\argument;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler to accept a user id.
 *
 * Check for nodes that user posted or linked knowledge on.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("argument_knowledge_user_uid")
 */
class UserUid extends ArgumentPluginBase {

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Knowledge module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $userSettings;

  /**
   * Constructs a \Drupal\knowledge\Plugin\views\argument\UserUid object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database Service Object.
   * @param \Drupal\Core\Config\ImmutableConfig $user_settings
   *   The user settings.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, ImmutableConfig $user_settings) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->database = $database;
    $this->userSettings = $user_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('config.factory')->get('user.settings'));
  }

  /**
   * {@inheritdoc}
   */
  public function title() {
    if (!$this->argument) {
      $title = $this->userSettings->get('anonymous');
    }
    else {
      $title = $this->database->query('SELECT [name] FROM {users_field_data} WHERE [uid] = :uid AND [default_langcode] = 1', [':uid' => $this->argument])->fetchField();
    }
    if (empty($title)) {
      return $this->t('No user');
    }

    return $title;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultActions($which = NULL) {
    // Disallow summary views on this argument.
    if (!$which) {
      $actions = parent::defaultActions();
      unset($actions['summary asc']);
      unset($actions['summary desc']);
      return $actions;
    }

    if ($which != 'summary asc' && $which != 'summary desc') {
      return parent::defaultActions($which);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    // Use the table definition to correctly add this user ID condition.
    if ($this->table != 'knowledge') {
      $subselect = $this->database->select('knowledge', 'c');
      $subselect->addField('c', 'kid');
      $subselect->condition('c.uid', $this->argument);

      $entity_id = $this->definition['entity_id'];
      $entity_type = $this->definition['entity_type'];
      $subselect->where("[c].[entity_id] = [$this->tableAlias].[$entity_id]");
      $subselect->condition('c.entity_type', $entity_type);

      /** @var \Drupal\views\Plugin\views\query\Sql $view_query */
      $view_query = $this->view->query;
      $condition = ($view_query->getConnection()->condition('OR'))
        ->condition("$this->tableAlias.uid", $this->argument, '=')
        ->exists($subselect);

      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;
      $query->addWhere(0, $condition);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSortName() {
    return $this->t('Numerical', [], ['context' => 'Sort order']);
  }

}
