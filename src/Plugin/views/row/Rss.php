<?php

namespace Drupal\knowledge\Plugin\views\row;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\row\RssPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin which formats the knowledge as RSS items.
 *
 * @ViewsRow(
 *   id = "knowledge_rss",
 *   title = @Translation("Knowledge"),
 *   help = @Translation("Display the knowledge as RSS."),
 *   theme = "views_view_row_rss",
 *   register_theme = FALSE,
 *   base = {"knowledge"},
 *   display_types = {"feed"}
 * )
 */
class Rss extends RssPluginBase {

  /**
   * ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a RssPluginBase  object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_display_repository);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container
      ->get('entity_type.manager'), $container
      ->get('entity_display.repository'), $container
      ->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  // phpcs:ignore
  protected $base_table = 'knowledge';

  /**
   * {@inheritdoc}
   */
  // phpcs:ignore
  public string $base_field = 'kid';

  /**
   * The field alias.
   */
  // phpcs:ignore
  public string $field_alias;

  /**
   * List of linked knowledge.
   *
   * @var \Drupal\knowledge\KnowledgeInterface[]
   */
  protected $knowledge;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'knowledge';

  /**
   * {@inheritdoc}
   */
  public function preRender($result) {
    $kids = [];

    foreach ($result as $row) {
      $kids[] = $row->kid;
    }

    $this->knowledge = $this->entityTypeManager->getStorage('knowledge')->loadMultiple($kids);
  }

  /**
   * {@inheritdoc}
   */
  // phpcs:ignore
  public function buildOptionsForm_summary_options() {
    $options = parent::buildOptionsForm_summary_options();
    $options['title'] = $this->t('Title only');
    $options['default'] = $this->t('Use site default RSS settings');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    global $base_url;

    $kid = $row->{$this->field_alias};
    if (!is_numeric($kid)) {
      return;
    }

    $view_mode = $this->options['view_mode'];
    if ($view_mode == 'default') {
      $view_mode = $this->configFactory
        ->get('system.rss')
        ->get('items.view_mode');
    }

    // Load the specified knowledge and its associated node:
    /** @var \Drupal\knowledge\KnowledgeInterface $know */
    $know = $this->knowledge[$kid];
    if (is_null($know)) {
      return;
    }

    $know->rss_namespaces = [];
    $know->rss_elements = [
      [
        'key' => 'pubDate',
        'value' => gmdate('r', $know->getCreatedTime()),
      ],
      [
        'key' => 'dc:creator',
        'value' => $know->getAuthorName(),
      ],
      [
        'key' => 'guid',
        'value' => 'knowledge ' . $know->id() . ' at ' . $base_url,
        'attributes' => ['isPermaLink' => 'false'],
      ],
    ];

    // The knowledge gets built and modules add to or modify
    // $know->rss_elements and $know->rss_namespaces.
    $build = $this->entityTypeManager->getViewBuilder('knowledge')->view($know, 'rss');
    unset($build['#theme']);

    if (!empty($know->rss_namespaces)) {
      /** @var \Drupal\views\Plugin\views\style\Rss $style_plugin */
      $style_plugin = $this->view->style_plugin;
      $style_plugin->namespaces = array_merge($style_plugin->namespaces, $know->rss_namespaces);
    }

    $item = new \stdClass();
    if ($view_mode != 'title') {
      // We render knowledge contents.
      $item->description = $build;
    }
    $item->title = $know->label();
    $item->link = $know->toUrl('canonical', ['absolute' => TRUE])->toString();
    // Provide a reference so that the render call in
    // template_preprocess_views_view_row_rss() can still access it.
    $item->elements = &$know->rss_elements;
    $item->kid = $know->id();

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#row' => $item,
    ];
    return $build;
  }

}
