<?php

namespace Drupal\knowledge\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Date;

/**
 * Sort handler for the newer of last knowledge / entity updated.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("knowledge_weekly")
 */
class KnowledgeWeekly extends Date {

  /**
   * The field alias.
   *
   * @var string
   */
  // phpcs:ignore
  protected $field_alias;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $params = $this->options['group_type'] != 'group' ? ['function' => $this->options['group_type']] : [];
    $this->field_alias = $query->addField(NULL, "DATE_FORMAT(FROM_UNIXTIME(created), '%Y%U')", 'knowledge_weekly', $params);
  }

}
