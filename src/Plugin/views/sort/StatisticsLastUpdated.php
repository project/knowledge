<?php

namespace Drupal\knowledge\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Date;

/**
 * Sort handler for the newer of last knowledge / entity updated.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("knowledge_ces_last_updated")
 */
class StatisticsLastUpdated extends Date {

  /**
   * The node table.
   *
   * @var string
   */
  // phpcs:ignore
  protected $node_table;

  /**
   * The field alias.
   *
   * @var string
   */
  // phpcs:ignore
  protected $field_alias;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $this->node_table = $query->ensureTable('node', $this->relationship);
    $this->field_alias = $query->addOrderBy(NULL, "GREATEST(" . $this->node_table . ".changed, " . $this->tableAlias . ".last_knowledge_timestamp)", $this->options['order'], $this->tableAlias . '_' . $this->field);
  }

}
