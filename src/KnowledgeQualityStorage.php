<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\knowledge\Entity\KnowledgeQualityInterface;

/**
 * Defines the storage handler class for Quality entities.
 *
 * This extends the base storage class, adding required special handling for
 * Quality entities.
 *
 * @ingroup knowledge
 */
class KnowledgeQualityStorage extends SqlContentEntityStorage implements KnowledgeQualityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(KnowledgeQualityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {knowledge_quality_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {knowledge_quality_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
