<?php

namespace Drupal\knowledge\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('system.admin_reports')) {
      $permissions = $route->getRequirement('_permission');
      $permissions .= '+access knowledge reports';
      $route->setRequirement('_permission', $permissions);
    }
  }

}
