<?php

declare(strict_types=1);

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\knowledge\KnowledgeWaveInterface;

/**
 * Defines the wave entity class.
 *
 * @ContentEntityType(
 *   id = "knowledge_wave",
 *   label = @Translation("Wave"),
 *   label_collection = @Translation("Waves"),
 *   label_singular = @Translation("wave"),
 *   label_plural = @Translation("waves"),
 *   label_count = @PluralTranslation(
 *     singular = "@count waves",
 *     plural = "@count waves",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\knowledge\WaveListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\knowledge\Form\WaveForm",
 *       "edit" = "Drupal\knowledge\Form\WaveForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "knowledge_wave",
 *   admin_permission = "administer knowledge_wave",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/structure/knowledge/wave",
 *     "add-form" = "/admin/structure/knowledge/wave/add",
 *     "canonical" = "/admin/structure/knowledge/wave/{knowledge_wave}",
 *     "edit-form" = "/admin/structure/knowledge/wave/{knowledge_wave}/edit",
 *     "delete-form" = "/admin/structure/knowledge/wave/{knowledge_wave}/delete",
 *     "delete-multiple-form" = "/admin/structure/knowledge/wave/delete-multiple",
 *   },
 *   field_ui_base_route  = "knowledge.wave.settings",
 * )
 */
final class Wave extends ContentEntityBase implements KnowledgeWaveInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['start'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Start'))
      ->setDescription(t('The time that the wave was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the wave was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the wave was last edited.'));

    return $fields;
  }

}
