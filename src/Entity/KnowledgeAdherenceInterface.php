<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Adherence entities.
 *
 * @ingroup knowledge
 */
interface KnowledgeAdherenceInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Adherence name.
   *
   * @return string
   *   Name of the Adherence.
   */
  public function getName();

  /**
   * Sets the Adherence name.
   *
   * @param string $name
   *   The Adherence name.
   *
   * @return \Drupal\knowledge\Entity\KnowledgeAdherenceInterface
   *   The called Adherence entity.
   */
  public function setName($name);

  /**
   * Gets the Adherence creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Adherence.
   */
  public function getCreatedTime();

  /**
   * Sets the Adherence creation timestamp.
   *
   * @param int $timestamp
   *   The Adherence creation timestamp.
   *
   * @return \Drupal\knowledge\Entity\KnowledgeAdherenceInterface
   *   The called Adherence entity.
   */
  public function setCreatedTime($timestamp);

}
