<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\knowledge\KnowledgeTypeInterface;

/**
 * Defines the knowledge type entity.
 *
 * @ConfigEntityType(
 *   id = "knowledge_type",
 *   label = @Translation("Knowledge type"),
 *   label_singular = @Translation("knowledge type"),
 *   label_plural = @Translation("knowledge types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count knowledge type",
 *     plural = "@count knowledge types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\knowledge\KnowledgeTypeForm",
 *       "add" = "Drupal\knowledge\KnowledgeTypeForm",
 *       "edit" = "Drupal\knowledge\KnowledgeTypeForm",
 *       "delete" = "Drupal\knowledge\Form\KnowledgeTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "permissions" = "Drupal\user\Entity\EntityPermissionsRouteProviderWithCheck",
 *     },
 *     "list_builder" = "Drupal\knowledge\KnowledgeTypeListBuilder"
 *   },
 *   admin_permission = "administer knowledge types",
 *   config_prefix = "type",
 *   bundle_of = "knowledge",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/knowledge/link/manage/{knowledge_type}/delete",
 *     "edit-form" = "/admin/structure/knowledge/link/manage/{knowledge_type}",
 *     "add-form" = "/admin/structure/knowledge/link/types/add",
 *     "entity-permissions-form" = "/admin/structure/knowledge/link/manage/{knowledge_type}/permissions",
 *     "collection" = "/admin/structure/knowledge/link",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "target_entity_type_id",
 *     "incident_entity_type_id",
 *     "description",
 *   }
 * )
 */
class KnowledgeType extends ConfigEntityBundleBase implements KnowledgeTypeInterface {

  /**
   * The knowledge type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The knowledge type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the knowledge type.
   *
   * @var string
   */
  protected $description;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $target_entity_type_id;

  /**
   * The incident entity type.
   *
   * @var string
   */
  protected $incident_entity_type_id;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getIncidentEntityTypeId() {
    return $this->incident_entity_type_id;
  }

}
