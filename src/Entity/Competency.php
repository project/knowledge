<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityConstraintViolationList;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\knowledge\KnowledgeCompetencyInterface;
use Drupal\knowledge_field\Helper\CompetencyField;
use Drupal\user\UserInterface;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Defines the Competency entity.
 *
 * @ingroup knowledge
 *
 * @ContentEntityType(
 *   id = "knowledge_competency",
 *   label = @Translation("Competency"),
 *   handlers = {
 *     "storage" = "Drupal\knowledge\CompetencyStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\knowledge\CompetencyListBuilder",
 *     "views_data" = "Drupal\knowledge\Entity\CompetencyViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\knowledge\Form\CompetencyForm",
 *       "add" = "Drupal\knowledge\Form\CompetencyForm",
 *       "edit" = "Drupal\knowledge\Form\CompetencyForm",
 *       "delete" = "Drupal\knowledge\Form\CompetencyDeleteForm",
 *       "approve" = "Drupal\knowledge\Form\CompetencyApproveForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\knowledge\CompetencyHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\knowledge\CompetencyAccessControlHandler",
 *   },
 *   base_table = "knowledge_competency",
 *   revision_table = "knowledge_competency_revision",
 *   revision_data_table = "knowledge_competency_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = FALSE,
 *   admin_permission = "administer knowledge_competency",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/knowledge/competency/{knowledge_competency}",
 *     "add-form" = "/admin/content/knowledge/competency/add",
 *     "edit-form" = "/admin/content/knowledge/competency/{knowledge_competency}/edit",
 *     "delete-form" = "/admin/content/knowledge/competency/{knowledge_competency}/delete",
 *     "version-history" = "/admin/content/knowledge/competency/{knowledge_competency}/revisions",
 *     "revision" = "/admin/content/knowledge/competency/{knowledge_competency}/revisions/{knowledge_competency_revision}/view",
 *     "revision_revert" = "/admin/content/knowledge/competency/{knowledge_competency}/revisions/{knowledge_competency_revision}/revert",
 *     "revision_delete" = "/admin/content/knowledge/competency/{knowledge_competency}/revisions/{knowledge_competency_revision}/delete",
 *     "collection" = "/admin/content/knowledge/competency",
 *   },
 *   field_ui_base_route = "knowledge_competency.settings"
 * )
 */
class Competency extends ContentEntityBase implements EntityChangedInterface, RevisionLogInterface, KnowledgeCompetencyInterface {

  use EntityChangedTrait;
  use RevisionLogEntityTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->ensureRoles();
    parent::preSave($storage);
    $this->setNewRevision();
    // If no revision author has been set explicitly,
    // make the knowledge_competency owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }

    $roles = $this->get('roles');
    $total = 0;
    $correct = 0;
    foreach ($roles as $role) {
      $total += $role->total;
      $correct += $role->correct;
    }

    $this->set('correct', $correct);
    $this->set('total', $total);

    $completed = NULL;
    if ($correct == $total) {
      $completed = time();
    }
    $this->set('completed', $completed);

    $this->setLegacyFields();

    $roles = $this->get('roles');
    $orginal_roles = $this->original?->get('roles')->getValue() ?? [];
    \Drupal::service('knowledge.competency')->doPreSave($roles, $orginal_roles);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Get the orginals values.
    $orginal_roles = $this->original?->get('roles')->getValue() ?? [];
    $roles = $this->get('roles')->getValue();
    $owner = $this->getOwner();
    \Drupal::service('knowledge.competency')->doPostSave($owner, $roles, $orginal_roles);

  }

  /**
   * Ensure all roles are present.
   */
  private function ensureRoles() {
    $roles = $this->get('roles');
    $definitions = $this->getFieldDefinitions();
    $role_fields = CompetencyField::roleFields($definitions);

    foreach ($roles as $role) {
      unset($role_fields[$role->role]);
    }

    foreach ($role_fields as $role => $field) {
      $roles->appendItem([
        'role' => $role,
        'correct' => 0,
        'total' => 0,
        'proposed' => NULL,
        'approved' => NULL,
        'proposer' => NULL,
        'approver' => NULL,
      ]);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    // Add the published field.
    $fields += static::revisionLogBaseFieldDefinitions($entity_type);
    /** @var \Drupal\Core\Field\BaseFieldDefinition $fields['revision_log'] */
    $fields['revision_log']
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', FALSE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Learner'))
      ->setDescription(t('The user ID of Competency entity.'))
      ->setRevisionable(FALSE)
      ->setReadOnly(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['roles'] = BaseFieldDefinition::create('knowledge_competency_role')
      ->setLabel(t('Roles'))
      ->setDescription(t('The roles of the user.'))
      ->setRevisionable(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'knowledge_competency_role',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'knowledge_competency_role',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['contributor_coach'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Coach'))
      ->setDescription(t('The user who suggests promotion to contributor.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['contributor_leader'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Leader'))
      ->setDescription(t('The user who promotes the user to contributor.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['publisher_coach'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Coach'))
      ->setDescription(t('The user ID of Competency entity.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['publisher_leader'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Leader'))
      ->setDescription(t('The user who approves the promotion.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['contributor_proposed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Proposed'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['contributor_approved'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Approved'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['publisher_proposed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Proposed'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['publisher_approved'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Approved'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['candidate_correct'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Correct candidate competencies'))
      ->setDescription(t('The number of correct candidate items.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['candidate_total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total candidate competencies'))
      ->setDescription(t('The total candidate items.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['contributor_correct'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Correct contributor competencies'))
      ->setDescription(t('The number of correct contributor items.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['contributor_total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total contributor competencies'))
      ->setDescription(t('The total contributor items.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['publisher_correct'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Correct publisher competencies'))
      ->setDescription(t('The number of correct publisher items.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['publisher_total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total publisher competencies'))
      ->setDescription(t('The total publisher items.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['correct'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Correct competencies'))
      ->setDescription(t('The number of correct items.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total competencies'))
      ->setDescription(t('The total competencies items.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Completed'))
      ->setDescription(t('The time that correct = total.'))
      ->setRevisionable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {

    $query = $this->entityTypeManager()
      ->getStorage('knowledge_competency')
      ->getQuery();
    $query->condition('user_id', $this->getOwnerId());
    if ($this->id()) {
      $query->condition('id', $this->id(), '<>');
    }

    $result = $query->accessCheck(FALSE)->execute();

    $this->validated = TRUE;
    $violations = $this
      ->getTypedData()
      ->validate();
    if (count($result)) {
      $violations[] = new ConstraintViolation('Competency for user already exists', '', [], '', 'user_id', 'duplicate');
    }

    return new EntityConstraintViolationList($this, iterator_to_array($violations));
  }

  /**
   * {@inheritdoc}
   */
  public function isPending(): ?string {
    $roles = $this->get('roles');
    foreach ($roles as $role) {
      if ($role->isPending()) {
        return $role->role;
      }
    }

    return FALSE;
  }

  /**
   * Set legacy fields.
   */
  private function setLegacyFields() {
    $legacy_roles = [
      'knowledge_candidate',
      'knowledge_contributor',
      'knowledge_publisher',
    ];
    $roles = $this->get('roles');
    foreach ($roles as $role) {
      $name = $role->role;
      if (!in_array($name, $legacy_roles)) {
        continue;
      }
      $name = str_replace('knowledge_', '', $name);

      $this->set($name . '_correct', $role->correct);
      $this->set($name . '_total', $role->total);

      if ($name == 'candidate') {
        continue;
      }

      $this->set($name . '_proposed', $role->proposed);
      $this->set($name . '_approved', $role->approved);
      $this->set($name . '_coach', $role->proposer);
      $this->set($name . '_leader', $role->approver);
    }
  }

}
