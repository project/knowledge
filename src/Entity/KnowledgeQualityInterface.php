<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Quality entities.
 *
 * @ingroup knowledge
 */
interface KnowledgeQualityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Quality creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Quality.
   */
  public function getCreatedTime();

  /**
   * Sets the Quality creation timestamp.
   *
   * @param int $timestamp
   *   The Quality creation timestamp.
   *
   * @return \Drupal\knowledge\Entity\KnowledgeQualityInterface
   *   The called Quality entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Quality revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Quality revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\knowledge\Entity\KnowledgeQualityInterface
   *   The called Quality entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Quality revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Quality revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\knowledge\Entity\KnowledgeQualityInterface
   *   The called Quality entity.
   */
  public function setRevisionUserId($uid);

}
