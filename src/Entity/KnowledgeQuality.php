<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Quality entity.
 *
 * @ingroup knowledge
 *
 * @ContentEntityType(
 *   id = "knowledge_quality",
 *   label = @Translation("Quality"),
 *   handlers = {
 *     "storage" = "Drupal\knowledge\KnowledgeQualityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\knowledge\KnowledgeQualityListBuilder",
 *     "views_data" = "Drupal\knowledge\Entity\KnowledgeQualityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\knowledge\Form\KnowledgeQualityForm",
 *       "add" = "Drupal\knowledge\Form\KnowledgeQualityForm",
 *       "edit" = "Drupal\knowledge\Form\KnowledgeQualityForm",
 *       "delete" = "Drupal\knowledge\Form\KnowledgeQualityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\knowledge\KnowledgeQualityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\knowledge\KnowledgeQualityAccessControlHandler",
 *   },
 *   base_table = "knowledge_quality",
 *   revision_table = "knowledge_quality_revision",
 *   revision_data_table = "knowledge_quality_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer quality entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/knowledge/quality/{knowledge_quality}",
 *     "add-form" = "/admin/content/knowledge/quality/add",
 *     "edit-form" = "/admin/content/knowledge/quality/{knowledge_quality}/edit",
 *     "delete-form" = "/admin/content/knowledge/quality/{knowledge_quality}/delete",
 *     "version-history" = "/admin/content/knowledge/quality/{knowledge_quality}/revisions",
 *     "revision" = "/admin/content/knowledge/quality/{knowledge_quality}/revisions/{knowledge_quality_revision}/view",
 *     "revision_revert" = "/admin/content/knowledge/quality/{knowledge_quality}/revisions/{knowledge_quality_revision}/revert",
 *     "revision_delete" = "/admin/content/knowledge/quality/{knowledge_quality}/revisions/{knowledge_quality_revision}/delete",
 *     "collection" = "/admin/content/knowledge/quality",
 *   },
 *   field_ui_base_route = "knowledge_quality.settings"
 * )
 */
class KnowledgeQuality extends EditorialContentEntityBase implements KnowledgeQualityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->setNewRevision();
    // If no revision author has been set explicitly,
    // make the knowledge_quality owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('knowledge_quality', 'knowledge_quality');

    $total = 0;
    $correct = 0;
    foreach ($fields as $name => $field) {
      if (get_class($field) == 'Drupal\field\Entity\FieldConfig') {
        if ($field->getType() == 'boolean') {
          $total += 1;
          $correct += $this->get($name)->value;
        }
      }
    }
    $this->set('total', $total);
    $this->set('correct', $correct);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->get('entity_id')->entity->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotal() {
    return $this->get('total')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCorrect() {
    return $this->get('correct')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getScore() {
    $total = $this->getTotal();
    if ($total == 0) {
      return 0;
    }
    $correct = $this->getCorrect();
    $diff = $total - $correct;

    return round(100 * (($correct - $diff) / $total));
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    /** @var \Drupal\Core\Field\BaseFieldDefinition $revision_log_message */
    $revision_log_message = $fields['revision_log_message'];
    $revision_log_message->setDisplayOptions('form', []);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setRequired(TRUE)
      ->setDescription(t('The entity type to which this knowledge is attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity ID'))
      ->setRequired(TRUE)
      ->setDescription(t('The ID of the entity of which this knowledge is a reply.'));

    $fields['entity_vid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity Revision ID'))
      ->setDescription(t('The Revision ID of the entity of which this knowledge is a reply.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Quality entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['correct'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Correct'))
      ->setDescription(t('Number of correct questions.'))
      ->setRevisionable(TRUE);

    $fields['total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total'))
      ->setDescription(t('Total number of questions.'))
      ->setRevisionable(TRUE);

    $status = $fields['status'];
    /** @var \Drupal\Core\Field\BaseFieldDefinition $status */
    $status->setDescription(t('A boolean indicating whether the Quality is published.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
