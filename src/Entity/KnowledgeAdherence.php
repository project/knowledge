<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Adherence entity.
 *
 * @ingroup knowledge
 *
 * @ContentEntityType(
 *   id = "knowledge_adherence",
 *   label = @Translation("Adherence"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\knowledge\KnowledgeAdherenceListBuilder",
 *     "views_data" = "Drupal\knowledge\Entity\KnowledgeAdherenceViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\knowledge\Form\KnowledgeAdherenceForm",
 *       "add" = "Drupal\knowledge\Form\KnowledgeAdherenceForm",
 *       "edit" = "Drupal\knowledge\Form\KnowledgeAdherenceForm",
 *       "delete" = "Drupal\knowledge\Form\KnowledgeAdherenceDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\knowledge\KnowledgeAdherenceHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\knowledge\KnowledgeAdherenceAccessControlHandler",
 *   },
 *   base_table = "knowledge_adherence",
 *   translatable = FALSE,
 *   admin_permission = "administer adherence entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/knowledge/adherence/{knowledge_adherence}",
 *     "add-form" = "/admin/content/knowledge/adherence/add",
 *     "edit-form" = "/admin/content/knowledge/adherence/{knowledge_adherence}/edit",
 *     "delete-form" = "/admin/content/knowledge/adherence/{knowledge_adherence}/delete",
 *     "collection" = "/admin/content/knowledge/adherence",
 *   },
 *   field_ui_base_route = "knowledge_adherence.settings"
 * )
 */
class KnowledgeAdherence extends ContentEntityBase implements KnowledgeAdherenceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished() {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnpublished() {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Adherence entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['learner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Learner'))
      ->setDescription(t('The user ID of the incident owner.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['incident_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Incident Id'))
      ->setDescription(t('The ID of the support case.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['incident_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Incident type'))
      ->setRequired(TRUE)
      ->setDescription(t('The entity type to which this knowledge is attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['article_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Article type'))
      ->setRequired(FALSE)
      ->setDescription(t('The entity type to which this knowledge is attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['knowledge_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Knowledge'))
      ->setDescription(t('The ID of the knowledge entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'knowledge')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['linked'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Article Linked to case?'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'nullable_bool',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['reused'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Was Article Created or Reused?'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'nullable_bool',
        'settings' => [
          'display_label' => TRUE,
          'yes' => 'Reused',
          'no' => 'Created',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['accurate'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Was linked Article accurate?'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'nullable_bool',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['improve'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Should the article be improved?'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'nullable_bool',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['required'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Was an Article required?'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'nullable_bool',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['found'] = BaseFieldDefinition::create('integer')
      ->setSetting('size', 'tiny')
      ->setLabel(t('Can you find an Article?'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'nullable_bool',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['disposition'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Disposition'))
      ->setDescription(t('The result or disposition of the incident.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Adherence entity.'))
      ->setSettings([
        'max_length' => 250,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['article_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Article'))
      ->setDescription(t('The article that should have been linked.'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 100,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
