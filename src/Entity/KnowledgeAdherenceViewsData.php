<?php

namespace Drupal\knowledge\Entity;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\knowledge\KnowledgeLinkRelationshipInterface;
use Drupal\views\EntityViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Views data for Adherence entities.
 */
class KnowledgeAdherenceViewsData extends EntityViewsData {

  /**
   * The knowledge link relationship service.
   *
   * @var \Drupal\knowledge\KnowledgeLinkRelationshipInterface
   */
  protected $knowledgeLinkRelationship;

  /**
   * Constructs a KnowledgeAdherenceViewsData object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to provide views integration for.
   * @param \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $storage_controller
   *   The storage handler used for this entity type.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The translation manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\knowledge\KnowledgeLinkRelationshipInterface $knowledge_link_relationship
   *   The knowledge link relationship service.
   */
  public function __construct(EntityTypeInterface $entity_type, SqlEntityStorageInterface $storage_controller, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, TranslationInterface $translation_manager, EntityFieldManagerInterface $entity_field_manager, KnowledgeLinkRelationshipInterface $knowledge_link_relationship) {
    parent::__construct($entity_type, $storage_controller, $entity_type_manager, $module_handler, $translation_manager, $entity_field_manager);

    $this->knowledgeLinkRelationship = $knowledge_link_relationship;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('string_translation'),
      $container->get('entity_field.manager'),
      $container->get('knowledge.link_relationship'));
  }

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $incident_entity_types = $this->knowledgeLinkRelationship->getIncidentEntityTypes();
    foreach ($incident_entity_types as $type => $entity_type) {
      $data['knowledge_adherence']['incident_' . $type] = [
        'relationship' => [
          'title' => $entity_type->getLabel(),
          'help' => $this->t('The @entity_type to which the entity is linked.', ['@entity_type' => $entity_type->getLabel()]),
          'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
          'base field' => $entity_type->getKey('id'),
          'relationship field' => 'incident_id',
          'id' => 'standard',
          'label' => $entity_type->getLabel(),
          'extra' => [
            [
              'field' => 'incident_type',
              'value' => $type,
              'table' => 'knowledge_adherence',
            ],
          ],
        ],
      ];
    }

    return $data;
  }

}
