<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a knowledge entity.
 */
interface KnowledgeInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Knowledge is awaiting approval.
   */
  const NOT_PUBLISHED = 0;

  /**
   * Knowledge is published.
   */
  const PUBLISHED = 1;

  /**
   * Anonymous posters cannot enter their contact information.
   */
  const ANONYMOUS_MAYNOT_CONTACT = 0;

  /**
   * Anonymous posters may leave their contact information.
   */
  const ANONYMOUS_MAY_CONTACT = 1;

  /**
   * Anonymous posters are required to leave their contact information.
   */
  const ANONYMOUS_MUST_CONTACT = 2;

  /**
   * Returns the entity to which the knowledge is attached.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|null
   *   The entity on which the knowledge is attached or NULL if the knowledge is
   *   an orphan.
   */
  public function getKnowledgeedEntity();

  /**
   * Returns the incident to which the knowledge is attached.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|null
   *   The entity on which the knowledge is attached or NULL if the knowledge is
   *   an orphan.
   */
  public function getIncidentEntity();

  /**
   * Returns the ID of the entity to which the knowledge is attached.
   *
   * @return int
   *   The ID of the entity to which the knowledge is attached.
   */
  public function getKnowledgeedEntityId();

  /**
   * Returns the ID of the incident to which the knowledge is attached.
   *
   * @return int
   *   The ID of the entity to which the knowledge is attached.
   */
  public function getIncidentEntityId();

  /**
   * Returns the type of the entity to which the knowledge is attached.
   *
   * @return string
   *   An entity type.
   */
  public function getKnowledgeedEntityTypeId();

  /**
   * Returns the type of the incident to which the knowledge is attached.
   *
   * @return string
   *   An entity type.
   */
  public function getIncidentEntityTypeId();

  /**
   * Sets the field ID for which this knowledge is attached.
   *
   * @param string $field_name
   *   The field name through which the knowledge was added.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function setFieldName($field_name);

  /**
   * Returns the name of the field the knowledge is attached to.
   *
   * @return string
   *   The name of the field the knowledge is attached to.
   */
  public function getFieldName();

  /**
   * Returns the subject of the knowledge.
   *
   * @return string
   *   The subject of the knowledge.
   */
  public function getSubject();

  /**
   * Sets the subject of the knowledge.
   *
   * @param string $subject
   *   The subject of the knowledge.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function setSubject($subject);

  /**
   * Returns the knowledge author's name.
   *
   * For anonymous authors, this is the value as typed in the knowledge form.
   *
   * @return string
   *   The name of the knowledge author.
   */
  public function getAuthorName();

  /**
   * Returns the time that the knowledge was created.
   *
   * @return int
   *   The timestamp of when the knowledge was created.
   */
  public function getCreatedTime();

  /**
   * Sets the creation date of the knowledge.
   *
   * @param int $created
   *   The timestamp of when the knowledge was created.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function setCreatedTime($created);

  /**
   * Returns the permalink URL for this knowledge.
   *
   * @return \Drupal\Core\Url
   *   The url of the permanent link.
   */
  public function permalink();

  /**
   * Get the knowledge type id for this knowledge.
   *
   * @return string
   *   The id of the knowledge type.
   */
  public function getTypeId();

}
