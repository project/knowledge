<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Competency entities.
 *
 * @ingroup knowledge
 */
class CompetencyListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Competency ID');
    $header['user'] = $this->t('User');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\knowledge\Entity\KnowledgeCompetency $entity */
    $row['id'] = $entity->id();
    $row['user'] = Link::createFromRoute(
      $entity->getOwner()->label(),
      'entity.knowledge_competency.edit_form',
      ['knowledge_competency' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
