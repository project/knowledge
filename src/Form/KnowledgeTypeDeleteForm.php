<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\knowledge\KnowledgeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form for deleting a knowledge type entity.
 *
 * @internal
 */
class KnowledgeTypeDeleteForm extends EntityDeleteForm {

  /**
   * The knowledge manager service.
   *
   * @var \Drupal\knowledge\KnowledgeManagerInterface
   */
  protected $knowledgeManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\knowledge\KnowledgeTypeInterface
   */
  protected $entity;

  /**
   * Constructs a query factory object.
   *
   * @param \Drupal\knowledge\KnowledgeManagerInterface $knowledge_manager
   *   The knowledge manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(KnowledgeManagerInterface $knowledge_manager, LoggerInterface $logger) {
    $this->knowledgeManager = $knowledge_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('knowledge.manager'),
      $container->get('logger.factory')->get('knowledge')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $total_count = $this->entityTypeManager->getStorage('knowledge')->getQuery()
      ->accessCheck(FALSE)
      ->condition('knowledge_type', $this->entity->id())
      ->count()
      ->execute();
    $entity_type = $this->entity->getTargetEntityTypeId();
    $caption = '';
    foreach (array_keys($this->knowledgeManager->getFields($entity_type)) as $field_name) {
      /** @var \Drupal\field\FieldStorageConfigInterface $field_storage */
      if (($field_storage = FieldStorageConfig::loadByName($entity_type, $field_name)) && $field_storage->getSetting('knowledge_type') == $this->entity->id() && !$field_storage->isDeleted()) {
        $caption .= '<p>' . $this->t('%label is used by the %field field on your site. You can not remove this knowledge type until you have removed the field.', [
          '%label' => $this->entity->label(),
          '%field' => $field_storage->label(),
        ]) . '</p>';
      }
    }

    if ($total_count) {
      $caption .= '<p>' . $this->formatPlural($total_count, '%label is used by 1 knowledge on your site. You can not remove this knowledge type until you have removed all of the %label knowledge.', '%label is used by @count knowledge on your site. You may not remove %label until you have removed all of the %label knowledge.', ['%label' => $this->entity->label()]) . '</p>';
    }
    if ($caption) {
      $form['description'] = ['#markup' => $caption];
      return $form;
    }
    else {
      return parent::buildForm($form, $form_state);
    }
  }

}
