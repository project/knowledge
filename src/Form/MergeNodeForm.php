<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class MergeNodeForm extends FormBase {

  /**
   * The knowledge link storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $knowledgeStorage;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $formatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->knowledgeStorage = $container->get('entity_type.manager')->getStorage('knowledge');
    $instance->formatter = $container->get('date.formatter');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "knowledge_merge_node_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NodeInterface $original = NULL, ?NodeInterface $duplicate = NULL) {

    $form_state->set('original_node', $original);
    $form_state->set('duplicate_node', $duplicate);

    $form['#title'] = $this->t('Merge "@duplicate" into "@node"', [
      '@node' => $original->label(),
      '@duplicate' => $duplicate->label(),
    ]);
    $form['list'] = [
      '#theme' => 'item_list',
      '#items' => [
        $this->t('The duplicate article will be archived.'),
        $this->t('The links on the duplicate article will be moved to the original.'),
      ],
    ];
    $form['compare'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field'),
        $this->t('Original'),
        $this->t('Duplicate'),
      ],
      '#rows' => [
        [
          $this->t('Title'),
          $original->label(),
          $duplicate->label(),
        ],
        [
          $this->t('Type'),
          $original->getType(),
          $duplicate->getType(),
        ],
        [
          $this->t('Product'),
          $original->field_product_ref->entity->label(),
          $duplicate->field_product_ref->entity->label(),
        ],
        [
          $this->t('Moderation state'),
          $original->moderation_state->value,
          $duplicate->moderation_state->value,
        ],
        [
          $this->t('Author'),
          $original->getOwner()->label(),
          $duplicate->getOwner()->label(),
        ],
        [
          $this->t('Created'),
          $this->formatter->format($original->getCreatedTime()),
          $this->formatter->format($duplicate->getCreatedTime()),
        ],
        [
          $this->t('Links'),
          $this->getLinkCount($original->id()),
          $this->getLinkCount($duplicate->id()),
        ],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Merge'),
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $original = $form_state->get('original_node');
    $duplicate = $form_state->get('duplicate_node');
    $message = 'Merged into ' . $original->label() . ' [' . $original->id() . ']';

    $duplicate_link_ids = $this->knowledgeStorage->getQuery()
      ->condition('entity_type', 'node')
      ->condition('entity_id', $duplicate->id())
      ->accessCheck(FALSE)
      ->execute();

    $duplicate_links = $this->knowledgeStorage->loadMultiple($duplicate_link_ids);
    foreach ($duplicate_links as $duplicate_link) {
      $duplicate_link->entity_id = $original->id();
      $duplicate_link->setNewRevision();
      $duplicate_link->revision_log_message->value = $message;
      $duplicate_link->save();
    }

    $duplicate->moderation_state = 'archived';
    $duplicate->revision_log->value = $message;
    $duplicate->save();

  }

  /**
   * The total links for the node.
   */
  protected function getLinkCount($nid) {
    $count = $this->knowledgeStorage->getQuery()
      ->condition('entity_type', 'node')
      ->condition('entity_id', $nid)
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    return $count;
  }

}
