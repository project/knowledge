<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings for the KnowledgeQuality entity.
 *
 * @ingroup knowledge
 */
class KnowledgeQualitySettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'knowledge_quality_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'knowledge.quality.settings',
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted entry.
    $this->config('knowledge.quality.settings')
      ->set('categories', $form_state->getValue('categories'))
      ->save();
  }

  /**
   * Defines the settings form for Quality entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $categories = $this->config('knowledge.quality.settings')->get('categories');

    $form['categories'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Categories'),
      '#description' => $this->t('Use like the select field.'),
      '#default_value' => $categories,
    ];

    return $form;
  }

}
