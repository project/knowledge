<?php

namespace Drupal\knowledge\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\knowledge\KnowledgeCompetencyServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the knowledge delete confirmation form.
 *
 * @internal
 */
class CompetencyApproveForm extends ConfirmFormBase {

  /**
   * The knowledge competency.
   *
   * @var \Drupal\knowledge\KnowledgeCompetencyInterface
   */
  protected $competency;

  /**
   * The user to be promoted.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $learner;

  /**
   * The role to ne promoted to.
   *
   * @var \Drupal\user\RoleInterface
   */
  protected $role;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The competency storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $competencyStorage;

  /**
   * The competency service.
   *
   * @var \Drupal\knowledge\KnowledgeCompetencyServiceInterface
   */
  protected $competencyService;

  /**
   * The user role storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userRoleStorage;

  /**
   * Constructs a new CompetencyApproveForm object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $competency_storage
   *   The competency storage service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $user_role_storage
   *   The user role storage service.
   * @param \Drupal\knowledge\KnowledgeCompetencyServiceInterface $competency_service
   *   The competency service.
   */
  public function __construct(
    AccountProxyInterface $account,
    TimeInterface $time,
    EntityStorageInterface $competency_storage,
    EntityStorageInterface $user_role_storage,
    KnowledgeCompetencyServiceInterface $competency_service,
  ) {
    $this->account = $account;
    $this->time = $time;
    $this->competencyStorage = $competency_storage;
    $this->userRoleStorage = $user_role_storage;
    $this->competencyService = $competency_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')->getStorage('knowledge_competency'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('knowledge.competency'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_knowledge_competency_approve_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = '') {
    $competency = $this->competencyService->getUserCompetency($user->id());
    $this->setCompetency($competency);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Promote');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.user.knowledge_competency', [
      'user' => $this->competency->getOwnerId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Do you want to promote the user?');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {

    $username = $this->learner ? $this->learner->label() : '<unknown>';
    $role = $this->role ? $this->role->label() : '<unknown>';

    $p = [
      '@username' => $username,
      '@role' => $role,
    ];
    return $this->t('Do you want to promote @username to a @role?', $p);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $competency = $this->competency;
    $learner = $this->learner;
    $role = $this->role->id();

    if (!$this->competencyService->hasRoleOrBetter($role, $learner->getRoles())) {
      $learner->addRole($role);
      $learner->save();
      $this->messenger()->addMessage($this->t('@user was promoted to @role.', [
        '@role' => $this->role->label(),
        '@user' => $learner->getDisplayName(),
      ]));
    }

    $pending_role = $competency->isPending();
    if ($pending_role == FALSE) {
      return;
    }
    foreach ($competency->get('roles') as $role) {
      if ($role->role == $pending_role) {
        $role->approved = $this->time->getRequestTime();
        $role->approver = $this->account->id();
      }
    }
    $competency->save();

    $form_state->setRedirect('entity.user.knowledge_competency', [
      'user' => $this->competency->getOwnerId(),
    ]);
  }

  /**
   * Sets the competency pending approval.
   */
  public function setCompetency($competency) {
    $this->competency = $competency;
    $this->learner = $competency->getOwner();

    $role_id = $competency->isPending();
    if (!$role_id) {
      return;
    }

    $promotion_role = $this->competencyService->getPromotionRole($role_id);
    if (!$promotion_role) {
      return;
    }
    $this->role = $this->userRoleStorage->load($promotion_role);
  }

}
