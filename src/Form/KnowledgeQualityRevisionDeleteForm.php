<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Quality revision.
 *
 * @ingroup knowledge
 */
class KnowledgeQualityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Quality revision.
   *
   * @var \Drupal\knowledge\Entity\KnowledgeQualityInterface
   */
  protected $revision;

  /**
   * The Quality storage.
   *
   * @var \Drupal\knowledge\KnowledgeQualityStorageInterface
   */
  protected $knowledgeQualityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->knowledgeQualityStorage = $container->get('entity_type.manager')->getStorage('knowledge_quality');
    $instance->connection = $container->get('database');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'knowledge_quality_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.knowledge_quality.version_history', ['knowledge_quality' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $knowledge_quality_revision = NULL) {
    $this->revision = $this->knowledgeQualityStorage->loadRevision($knowledge_quality_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->knowledgeQualityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Quality: deleted %title revision %revision.', [
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()->addMessage($this->t('Revision from %revision-date of Quality %title has been deleted.', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));
    $form_state->setRedirect(
      'entity.knowledge_quality.canonical',
       ['knowledge_quality' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {knowledge_quality_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.knowledge_quality.version_history',
         ['knowledge_quality' => $this->revision->id()]
      );
    }
  }

}
