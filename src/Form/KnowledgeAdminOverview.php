<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\knowledge\KnowledgeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the knowledge overview administration form.
 *
 * @internal
 */
class KnowledgeAdminOverview extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The knowledge storage.
   *
   * @var \Drupal\knowledge\KnowledgeStorageInterface
   */
  protected $knowledgeStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Creates a KnowledgeAdminOverview form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, ModuleHandlerInterface $module_handler, PrivateTempStoreFactory $temp_store_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->knowledgeStorage = $entity_type_manager->getStorage('knowledge');
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('module_handler'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'knowledge_default_overview';
  }

  /**
   * Form constructor for the knowledge overview administration form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $type
   *   The type of the overview form ('approval' or 'new').
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $type = 'new') {

    // Build an 'Update options' form.
    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Update options'),
      '#open' => TRUE,
      '#attributes' => ['class' => ['container-inline']],
    ];

    if ($type == 'approval') {
      $options['publish'] = $this->t('Publish the selected knowledge');
    }
    else {
      $options['unpublish'] = $this->t('Unpublish the selected knowledge');
    }
    $options['delete'] = $this->t('Delete the selected knowledge');

    $form['options']['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#default_value' => 'publish',
    ];
    $form['options']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];

    // Load the knowledge that need to be displayed.
    $status = ($type == 'approval') ? KnowledgeInterface::NOT_PUBLISHED : KnowledgeInterface::PUBLISHED;
    $header = [
      'posted_in' => [
        'data' => $this->t('Posted in'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'subject' => [
        'data' => $this->t('Incident'),
        'specifier' => 'subject',
      ],
      'author' => [
        'data' => $this->t('Author'),
        'specifier' => 'name',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'changed' => [
        'data' => $this->t('Updated'),
        'specifier' => 'changed',
        'sort' => 'desc',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'operations' => $this->t('Operations'),
    ];
    $kids = $this->knowledgeStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', $status)
      ->tableSort($header)
      ->pager(50)
      ->execute();

    /** @var \Drupal\knowledge\KnowledgeInterface[] $knowledge_links */
    $knowledge_links = $this->knowledgeStorage->loadMultiple($kids);

    // Build a table listing the appropriate knowledge.
    $options = [];
    $destination = $this->getDestinationArray();

    $linked_entity_ids = [];
    $linked_entities = [];

    $incident_entity_ids = [];
    $incident_entities = [];

    foreach ($knowledge_links as $knowledge) {
      $linked_entity_ids[$knowledge->getKnowledgeedEntityTypeId()][] = $knowledge->getKnowledgeedEntityId();
      $incident_entity_ids[$knowledge->getIncidentEntityTypeId()][] = $knowledge->getIncidentEntityId();
    }

    foreach ($linked_entity_ids as $entity_type => $ids) {
      $linked_entities[$entity_type] = $this->entityTypeManager
        ->getStorage($entity_type)
        ->loadMultiple($ids);
    }

    foreach ($incident_entity_ids as $entity_type => $ids) {
      $incident_entities[$entity_type] = $this->entityTypeManager
        ->getStorage($entity_type)
        ->loadMultiple($ids);
    }

    foreach ($knowledge_links as $knowledge) {
      /** @var \Drupal\Core\Entity\EntityInterface $linked_entity */
      $linked_entity = $linked_entities[$knowledge->getKnowledgeedEntityTypeId()][$knowledge->getKnowledgeedEntityId()];
      $incident_entity = $incident_entities[$knowledge->getIncidentEntityTypeId()][$knowledge->getIncidentEntityId()];

      $options[$knowledge->id()] = [
        'title' => ['data' => ['#title' => $knowledge->id()]],
        'posted_in' => [
          'data' => [
            '#type' => 'link',
            '#title' => $linked_entity->label(),
            '#access' => $linked_entity->access('view'),
            '#url' => $linked_entity->toUrl(),
          ],
        ],
        'subject' => [
          'data' => [
            '#type' => 'link',
            '#title' => $incident_entity->label(),
            '#access' => $incident_entity->access('view'),
            '#url' => $incident_entity->toUrl(),
          ],
        ],
        'author' => [
          'data' => [
            '#theme' => 'username',
            '#account' => $knowledge->getOwner(),
          ],
        ],

        'changed' => $this->dateFormatter->format($knowledge->getChangedTimeAcrossTranslations(), 'short'),
      ];
      $knowledge_uri_options = $knowledge->toUrl()->getOptions() + ['query' => $destination];
      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url' => $knowledge->toUrl('edit-form', $knowledge_uri_options),
      ];
      if ($this->moduleHandler->moduleExists('content_translation') && $this->moduleHandler->invoke('content_translation', 'translate_access', [$knowledge])->isAllowed()) {
        $links['translate'] = [
          'title' => $this->t('Translate'),
          'url' => $knowledge->toUrl('drupal:content-translation-overview', $knowledge_uri_options),
        ];
      }
      $options[$knowledge->id()]['operations']['data'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    $form['knowledge'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => $this->t('No knowledge available.'),
    ];

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('knowledge', array_diff($form_state->getValue('knowledge'), [0]));
    // We can't execute any 'Update options' if no knowledge was selected.
    if (count($form_state->getValue('knowledge')) == 0) {
      $form_state->setErrorByName('', $this->t('Select one or more knowledge links to perform the update on.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operation = $form_state->getValue('operation');
    $kids = $form_state->getValue('knowledge');
    /** @var \Drupal\knowledge\KnowledgeInterface[] $knowledge_links */
    $knowledge_links = $this->knowledgeStorage->loadMultiple($kids);
    if ($operation != 'delete') {
      foreach ($knowledge_links as $knowledge) {
        if ($operation == 'unpublish') {
          $knowledge->setUnpublished();
        }
        elseif ($operation == 'publish') {
          $knowledge->setPublished();
        }
        $knowledge->save();
      }
      $this->messenger()->addStatus($this->t('The update has been performed.'));
      $form_state->setRedirect('knowledge.admin');
    }
    else {
      $info = [];
      $this->tempStoreFactory
        ->get('entity_delete_multiple_confirm')
        ->set($this->currentUser()->id() . ':knowledge', $info);
      $form_state->setRedirect('entity.knowledge.delete_multiple_form');
    }
  }

}
