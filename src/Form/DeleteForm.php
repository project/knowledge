<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides the knowledge delete confirmation form.
 *
 * @internal
 */
class DeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // Point to the entity of which this knowledge is a reply.
    return $this->entity->get('entity_id')->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Any replies to this knowledge will be lost. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage() {
    return $this->t('The knowledge and all its replies have been deleted.');
  }

  /**
   * {@inheritdoc}
   */
  public function logDeletionMessage() {
    $this->logger('knowledge')->notice('Deleted knowledge @kid and its replies.', ['@kid' => $this->entity->id()]);
  }

}
