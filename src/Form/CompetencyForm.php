<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\knowledge_field\Helper\CompetencyField;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Competency edit forms.
 *
 * @ingroup knowledge
 */
class CompetencyForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The knowledge settings config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The user role storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * The field group info array.
   *
   * @var array
   */
  protected $groups;

  /**
   * The Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $render;

  /**
   * The field definitions for the knowledge competency entity.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $definitions;

  /**
   * The competency service.
   *
   * @var \Drupal\knowledge\KnowledgeCompetencyServiceInterface
   */
  protected $competencyService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->roleStorage = $container->get('entity_type.manager')->getStorage('user_role');
    $instance->settings = $container->get('config.factory')->get('knowledge.competency.settings');
    $instance->competencyService = $container->get('knowledge.competency');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->time = $container->get('datetime.time');
    $instance->render = $container->get('renderer');
    $instance->definitions = $container->get('entity_field.manager')->getFieldDefinitions('knowledge_competency', 'knowledge_competency');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $this->addRoleTabs($form);
    $this->buildCompetency($form);

    // In several places within this function, we vary $form on:
    // - The current user's permissions.
    // - Whether the current user is authenticated or anonymous.
    // - The 'user.settings' configuration.
    // - The knowledge field's definition.
    $form['#cache']['contexts'][] = 'user';
    $form['#cache']['contexts'][] = 'user.roles:authenticated';

    $form['user_id']['#type'] = 'hidden';

    $form['#attached']['library'][] = 'knowledge/competency_form';

    return $form;
  }

  /**
   * Adds the role tabs.
   *
   * @param array $form
   *   The form.
   */
  private function addRoleTabs(array &$form) {
    $roles_weight = $form['roles']['widget'][0]['#weight'];
    $form['skills'] = [
      "#type" => "vertical_tabs",
      "#weight" => $roles_weight,
      "#default_tab" => "edit-knowledge-publisher",
    ];
    $competency_roles = $this->competencyService->getRoleIds();
    $roles = $this->roleStorage->loadMultiple($competency_roles);
    foreach ($competency_roles as $role) {
      $form[$role] = [
        '#type' => 'details',
        '#title' => $roles[$role]?->label(),
        '#group' => 'skills',
      ];
    }
  }

  /**
   * Builds the competency.
   *
   * @param array $form
   *   The form.
   */
  protected function buildCompetency(array &$form) {
    $roles = [];
    $role_fields = CompetencyField::roleFields($this->definitions);

    foreach ($role_fields as $role => $fields) {
      $roles[$role] = [
        'correct' => 0,
        'total' => 0,
      ];
      foreach ($fields as $field_name) {
        $roles[$role]['total'] += 1;
        if ($this->entity->get($field_name)?->value) {
          $roles[$role]['correct'] += 1;
        }
        $form[$role][$field_name] = &$form[$field_name];
        unset($form[$field_name]);
      }
    }

    $setting = $this->competencyService->get();
    $default_tab = 'edit-' . str_replace('_', '-', $setting[0]['role']);
    foreach ($setting as $index => $r) {

      $role = $r['role'];
      if ($roles[$role]['total'] != $roles[$role]['correct']) {
        break;
      }

      $next = $setting[$index + 1] ?? NULL;
      if (is_null($next)) {
        break;
      }

      $next_role = $next['role'];
      $default_tab = 'edit-' . str_replace('_', '-', $next_role);
    }

    $form['skills']['#default_tab'] = $default_tab;
  }

  /**
   * Sets the default tab.
   *
   * @param array $form
   *   The form.
   */
  private function setDefaultTab(&$form) {

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $contributor_proposal = $form_state->getValue('contributor_proposal');
    $publisher_proposal = $form_state->getValue('publisher_proposal');
    /** @var \Drupal\knowledge\KnowledgeCompetencyInterface $competency */
    $competency = $this->entity;
    if (empty($competency->contributor_proposed->value) && $contributor_proposal) {
      $competency->contributor_proposed->value = $this->time->getCurrentTime();
      $competency->contributor_coach->target_id = $this->account->id();
    }
    elseif (!empty($competency->contributor_proposed->value) && !$contributor_proposal) {
      $competency->contributor_proposed->value = NULL;
      $competency->contributor_coach->target_id = NULL;
      $competency->contributor_leader->target_id = NULL;
    }
    if (empty($competency->publisher_proposed->value) && $publisher_proposal) {
      $competency->publisher_proposed->value = $this->time->getCurrentTime();
      $competency->publisher_coach->target_id = $this->account->id();
    }
    elseif (!empty($competency->publisher_proposed->value) && !$publisher_proposal) {
      $competency->publisher_proposed->value = NULL;
      $competency->publisher_coach->target_id = NULL;
      $competency->publisher_leader->target_id = NULL;
    }
    $roles = $competency->get('roles');
    foreach ($roles as $index => $role) {

      if ($role->proposer) {
        $roles[$index]->proposer = $role->proposer->id();
      }
      if ($role->approver) {
        $roles[$index]->approver = $role->approver->id();
      }

    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t("Created %user's Competency.", [
          '%user' => $competency->getOwner()->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t("Saved %user's Competency.", [
          '%user' => $competency->getOwner()->label(),
        ]));
    }

    return $status;
  }

}
