<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the knowledge unlink confirmation form.
 *
 * @internal
 */
class UnlinkForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = $this->getEntity();
    // Make new revision.
    $knowledge->setNewRevision(TRUE);

    return $this
      ->t('Are you sure you want to unlink @incident-type @incident-id from "%label"?', [
        '@incident-type' => $knowledge->getIncidentEntity()->bundle(),
        '@incident-id' => $knowledge->getIncidentEntityId(),
        '%label' => $knowledge->getKnowledgeedEntity()->label() ?? $knowledge->id(),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // Point to the entity of which this knowledge is a reply.
    return $this->entity->get('entity_id')->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Unlink knowledge link. This action can be undone.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage() {
    return $this->t('The knowledge has been unlink');
  }

  /**
   * {@inheritdoc}
   */
  public function logDeletionMessage() {
    $this->logger('knowledge')->notice('Unlink knowledge @kid.', ['@kid' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\knowledge\KnowledgeInterface $entity */
    $entity = $this->getEntity();
    $entity->status = FALSE;
    $entity->save();
    $this
      ->messenger()
      ->addStatus($this
        ->getDeletionMessage());
    $form_state
      ->setRedirectUrl($this->getCancelUrl());
    $this
      ->logDeletionMessage();
  }

}
