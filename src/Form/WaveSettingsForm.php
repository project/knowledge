<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings for the knowledge module.
 */
class WaveSettingsForm extends ConfigFormBase {

  /**
   * The node type storage service.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * The cache tag invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheInvalidator;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_invalidator
   *   The cache tag invalidation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, CacheTagsInvalidatorInterface $cache_invalidator) {
    $this->setConfigFactory($config_factory);
    $this->nodeTypeStorage = $entity_type_manager->getStorage('node_type');
    $this->cacheInvalidator = $cache_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['knowledge.wave.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "knowledge_wave_settings_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['knowledgequality_settings']['#markup'] = 'Settings form for Wave entities. Manage field settings here.';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node_types = $form_state->getValue('node_types');
    $node_types = array_filter($node_types);

    $config = $this->config('knowledge.settings');
    $original_node_types = $config->get('node_types');

    $config
      ->set('audience.public.label', $form_state->getValue('public_label'))
      ->set('audience.public.enabled', $form_state->getValue('public_enabled'))

      ->set('audience.customer.label', $form_state->getValue('customer_label'))
      ->set('audience.customer.enabled', $form_state->getValue('customer_enabled'))

      ->set('audience.partner.label', $form_state->getValue('partner_label'))
      ->set('audience.partner.enabled', $form_state->getValue('partner_enabled'))

      ->set('audience.internal.label', $form_state->getValue('internal_label'))
      ->set('audience.internal.enabled', $form_state->getValue('internal_enabled'))

      ->set('node_types', $node_types)
      ->set('leader_coach', $form_state->getValue('leader_coach'))
      ->save();

    if ($original_node_types != $node_types) {
      $this->cacheInvalidator->invalidateTags([
        'config:knowledge.settings.node_types',
      ]);
    }
  }

}
