<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm as EntityDeleteMultipleForm;
use Drupal\Core\Url;

/**
 * Provides the knowledge multiple delete confirmation form.
 *
 * @internal
 */
class ConfirmDeleteMultiple extends EntityDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->selection), 'Are you sure you want to delete this knowledge and all its children?', 'Are you sure you want to delete these knowledge and all their children?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('knowledge.admin');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletedMessage($count) {
    return $this->formatPlural($count, 'Deleted @count knowledge.', 'Deleted @count knowledge.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getInaccessibleMessage($count) {
    return $this->formatPlural($count, "@count knowledge has not been deleted because you do not have the necessary permissions.", "@count knowledge have not been deleted because you do not have the necessary permissions.");
  }

}
