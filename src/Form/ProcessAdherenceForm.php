<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class ProcessAdherenceForm extends FormBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The knowledge storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $knowledgeStorage;

  /**
   * The system time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->nodeStorage = $container->get('entity_type.manager')->getStorage('node');
    $instance->knowledgeStorage = $container->get('entity_type.manager')->getStorage('knowledge');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->database = $container->get('database');
    $instance->time = $container->get('datetime.time');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $parameters = $this->routeMatch->getParameters()->all();
    $incident_id = $this->getRequest()->query->get('incident_id');

    $pT = array_keys($parameters);
    $pI = array_values($parameters);

    $incident_type = current($pT);
    $incident_id = current($pI);

    $kids = $this->knowledgeStorage->getQuery()
      ->condition('incident_type', $incident_type)
      ->condition('incident_id', $incident_id)
      ->accessCheck(FALSE)
      ->execute();
    $knowledge = $this->knowledgeStorage->loadMultiple($kids);

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load(15441);
    /** @var \Drupal\Core\Entity\ContentEntityBase $incident */
    $incident = $this->entityTypeManager->getStorage($incident_type)->load($incident_id);

    $query = $this->database->query("SELECT incident_type, incident_id, uid, coach, linked, reused, accurate, improve, required, found FROM {knowledge_adherence} WHERE incident_type = :incident_type AND incident_id = :incident_id", [
      ':incident_type' => $incident_type,
      ':incident_id' => $incident_id,
    ]);
    $result = $query->fetchAll();

    $default = current($result);

    $form = [];
    $form['linked'] = [
      '#type' => 'radios',
      '#title' => $this->t('Article Linked to case?'),
      '#default_value' => (string) ((int) !empty($knowledge)),
      '#options' => [
        $this->t('No'),
        $this->t('Yes'),
      ],
      '#disabled' => TRUE,
    ];
    $form['reused'] = [
      '#type' => 'radios',
      '#title' => $this->t('Was Article Created or Reused?'),
      '#default_value' => $default->reused ?? FALSE,
      '#options' => [
        $this->t('Created'),
        $this->t('Reused'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="linked"]' => ['value' => 1],
        ],
      ],
    ];
    $form['accurate'] = [
      '#type' => 'radios',
      '#title' => $this->t('Was linked Article accurate?'),
      '#default_value' => $default->accurate ?? FALSE,
      '#options' => [
        $this->t('No'),
        $this->t('Yes'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="linked"]' => ['value' => 1],
          ':input[name="reused"]' => ['value' => 1],
        ],
      ],
    ];
    $form['improve'] = [
      '#type' => 'radios',
      '#title' => $this->t('Should the article be improved?'),
      '#default_value' => $default->improve ?? FALSE,
      '#options' => [
        $this->t('No'),
        $this->t('Yes'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="linked"]' => ['value' => 1],
          ':input[name="reused"]' => ['value' => 1],
          ':input[name="accurate"]' => ['value' => 1],
        ],
      ],
    ];
    $form['required'] = [
      '#type' => 'radios',
      '#title' => $this->t('Was an Article required?'),
      '#default_value' => $default->required ?? FALSE,
      '#options' => [
        $this->t('No'),
        $this->t('Yes'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="linked"]' => ['value' => 0],
        ],
        'required' => [
          ':input[name="linked"]' => ['value' => 0],
        ],
      ],
    ];
    $form['found'] = [
      '#type' => 'radios',
      '#title' => $this->t('Can you find an Article?'),
      '#default_value' => $default->found ?? FALSE,
      '#options' => [
        $this->t('No'),
        $this->t('Yes'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="linked"]' => ['value' => 0],
          ':input[name="required"]' => ['value' => 1],
        ],
        'required' => [
          ':input[name="required"]' => ['value' => 1],
        ],
      ],
    ];

    $form['advanced'] = [
      "#type" => "vertical_tabs",
      "#weight" => 99,
      "#attributes" => [
        "class" => [
          "entity-meta",
        ],
      ],
    ];

    $form['meta'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
    ];
    $form['meta']['title'] = [
      '#type' => 'item',
      '#markup' => $node->label(),
      '#access' => !$node->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $node->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$node->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['created'] = [
      '#type' => 'item',
      '#title' => $this->t('Created'),
      '#markup' => !$node->isNew() ? $this->dateFormatter->format($node->getCreatedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$node->isNew() ? $this->dateFormatter->format($node->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $node->getOwner()->getAccountName(),
      '#wrapper_attributes' => ['class' => ['entity-meta__author']],
    ];

    $form['incident'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -20,
      '#title' => $this->t('Incident'),
      '#attributes' => ['class' => ['incident-meta__header']],
      '#tree' => TRUE,
    ];
    $form['incident']['$disposition'] = [
      '#type' => 'item',
      '#markup' => 'Pending',
      '#wrapper_attributes' => ['class' => ['incident-meta__title']],
    ];
    $form['incident']['id'] = [
      '#type' => 'item',
      '#title' => $this->t('Id'),
      '#markup' => $incident->id(),
      '#wrapper_attributes' => ['class' => ['incident-meta__id']],
    ];
    $form['incident']['published'] = [
      '#type' => 'item',
      '#title' => $this->t('Type'),
      '#markup' => $incident->get('status')->value,
      '#wrapper_attributes' => ['class' => ['incident-meta__type']],
    ];
    $form['incident']['type'] = [
      '#type' => 'item',
      '#title' => $this->t('Type'),
      '#markup' => $incident->get('type')->value,
      '#wrapper_attributes' => ['class' => ['incident-meta__type']],
    ];
    $form['incident']['company'] = [
      '#type' => 'item',
      '#title' => $this->t('Company'),
      '#markup' => $incident->get('company')->entity ? $incident->get('company')->entity->label() : '',
      '#wrapper_attributes' => ['class' => ['incident-meta__company']],
    ];
    $form['incident']['created'] = [
      '#type' => 'item',
      '#title' => $this->t('Created'),
      '#markup' => !$incident->isNew() ? $this->dateFormatter->format($incident->get('created')->value, 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['incident-meta__created']],
    ];
    $form['incident']['created'] = [
      '#type' => 'item',
      '#title' => $this->t('Created'),
      '#markup' => !$incident->isNew() ? $this->dateFormatter->format($incident->get('created')->value, 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['incident-meta__created']],
    ];
    $form['incident']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last updated'),
      '#markup' => !$incident->isNew() ? $this->dateFormatter->format($incident->get('changed')->value, 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['incident-meta__last-saved']],
    ];
    $form['incident']['owner'] = [
      '#type' => 'item',
      '#title' => $this->t('Assigned To'),
      '#markup' => $incident->get('assigned_to')->entity ? $incident->get('assigned_to')->entity->label() : '',
      '#wrapper_attributes' => ['class' => ['incident-meta__owner']],
    ];
    $form['incident']['provider_group'] = [
      '#type' => 'item',
      '#title' => $this->t('Provider Group'),
      '#markup' => $incident->get('provider_group')->entity ? $incident->get('provider_group')->entity->label() : '',
      '#wrapper_attributes' => ['class' => ['incident-meta__provider_group']],
    ];
    $form['incident']['product'] = [
      '#type' => 'item',
      '#title' => $this->t('Product'),
      '#markup' => $incident->get('product')->entity ? $incident->get('product')->entity->label() : '',
      '#wrapper_attributes' => ['class' => ['incident-meta__product']],
    ];

    $form['#title'] = $incident->label();

    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    $form['status']['#group'] = 'footer';

    // Node author information for administrators.
    $form['author'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['node-form-author'],
      ],
      '#attached' => [
        'library' => ['node/drupal.node'],
      ],
      '#weight' => 90,
      '#optional' => TRUE,
    ];

    $form['#theme'] = ['knowledge_adherence_form'];

    $form['advanced']['#type'] = 'container';
    $form['advanced']['#accordion'] = TRUE;
    $form['meta']['#type'] = 'container';
    $form['meta']['#access'] = TRUE;
    $form['meta']['changed']['#wrapper_attributes']['class'][] = 'container-inline';
    $form['meta']['author']['#wrapper_attributes']['class'][] = 'container-inline';

    $form['revision_information']['#type'] = 'container';
    $form['revision_information']['#group'] = 'meta';

    $form['#attached']['library'][] = 'node/form';
    $form['#attached']['library'][] = 'knowledge/adherence_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $parameters = $this->routeMatch->getParameters()->all();

    $pT = array_keys($parameters);
    $pI = array_values($parameters);

    $incident_type = current($pT);
    $incident_id = current($pI);

    $uid = 76;
    $coach = 1;
    $linked = $form_state->getValue('linked');
    $reused = $form_state->getValue('reused');
    $accurate = $form_state->getValue('accurate');
    $improve = $form_state->getValue('improve');
    $required = $form_state->getValue('required');
    $found = $form_state->getValue('found');
    $disposition = 'Pending';
    if ($linked == '1') {
      if ($reused == '1') {
        if ($accurate == '1') {
          if ($improve == '1') {
            $disposition = 'Improve loss';
          }
          elseif ($improve == '0') {
            $disposition = 'Reuse';
          }
        }
        elseif ($accurate == '0') {
          $disposition = 'Inaccurate Reuse';
          $improve = NULL;
        }
      }
      elseif ($reused == '0') {
        $disposition = 'Create';
        $accurate = NULL;
        $improve = NULL;
      }
    }
    elseif ($linked == '0') {
      if ($required == '1') {
        if ($found == '1') {
          $disposition = 'Reuse Loss';
        }
        elseif ($found == '0') {
          $disposition = 'Create Loss';
        }
      }
      elseif ($required == '0') {
        $disposition = 'No Link Required';
        $found = NULL;
      }
    }

    $this->database->merge('knowledge_adherence')
      ->key(
        ['incident_type', 'incident_id'],
        [$incident_type, $incident_id])
      ->fields([
        'uid' => $uid,
        'coach' => $coach,
        'linked' => $linked,
        'reused' => $reused,
        'accurate' => $accurate,
        'improve' => $improve,
        'required' => $required,
        'found' => $found,
        'disposition' => $disposition,
        'article_type' => 'node',
        'article_id' => 15441,
        'timestamp' => $this->time->getRequestTime(),
      ])
      ->execute();

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "knowledge_process_adherence";
  }

}
