<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Adherence edit forms.
 *
 * @ingroup knowledge
 */
class KnowledgeAdherenceForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The knowledge storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $knowledgeStorage;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->nodeStorage = $container->get('entity_type.manager')->getStorage('node');
    $instance->knowledgeStorage = $container->get('entity_type.manager')->getStorage('knowledge');
    $instance->dateFormatter = $container->get('date.formatter');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $query = $this->getRequest()->query;
    $incident_id = $this->entity
      ->get('incident_id')
      ->target_id ?? $query->get('incident_id');
    $incident_type = $this->entity
      ->get('incident_type')
      ->value ?? $query->get('incident_type');

    $incident = $this->entityTypeManager->getStorage($incident_type)->load($incident_id);
    $form_state->set('incident', $incident);

    $kids = $this->knowledgeStorage->getQuery()
      ->condition('incident_type', $incident_type)
      ->condition('incident_id', $incident_id)
      ->accessCheck(FALSE)
      ->execute();

    $form = parent::buildForm($form, $form_state);
    $form['incident_type']['#default_value'] = $incident_type;
    $form['incident_id']['#default_value'] = $incident_id;

    $count_link = count($kids);
    $knowledge_id = $this->entity
      ->get('knowledge_id')->value ?? array_pop($kids);

    $form['knowledge_id']['#default_value'] = $knowledge_id;

    $form['linked']['widget'][0]['value']['#default_value'] = intval($count_link > 0);
    $form['linked']['widget'][0]['value']['#disabled'] = TRUE;

    if ($count_link) {
      /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
      $knowledge = $this->knowledgeStorage->load($knowledge_id);
      /** @var \Drupal\node\NodeInterface $node */
      $node = $knowledge->getKnowledgeedEntity();
      $field = $knowledge->getFieldName();
      $first_knowledge = $node->$field[0]->first_knowledge;
      $linker_uid = $knowledge->getOwnerId();
      $author_uid = $node->getOwnerId();
      $is_first_use = $knowledge_id != $first_knowledge;
      $not_author = $linker_uid != $author_uid;
      $default_value = intval($is_first_use || $not_author);

      $form['reused']['widget'][0]['value']['#default_value'] = $default_value;
      $form['reused']['widget'][0]['value']['#disabled'] = TRUE;

      $node = $knowledge->getKnowledgeedEntity();

      $entity_type = $knowledge->getKnowledgeedEntityTypeId();
      $view_mode = 'printable';
      $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
      $node_view = $view_builder->view($node, $view_mode);

      $form['entity'] = [
        '#type' => 'container',
        '#group' => 'advanced',
        '#weight' => 20,
        '#title' => $this->t('Entity'),
        '#attributes' => ['class' => ['incident-meta__header']],
        '#tree' => TRUE,
      ];
      $form['entity']['node'] = $node_view;
      $form['entity']['node']['#weight'] = 500;
    }

    $form['incident'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -20,
      '#title' => $this->t('Incident'),
      '#attributes' => ['class' => ['incident-meta__header']],
      '#tree' => TRUE,
    ];
    $disposition = $this->entity
      && $this->entity->get('disposition')
      && $this->entity->get('disposition')->value
      ? $this->entity->get('disposition')->value : 'pending';

    $form['advanced'] = [
      "#type" => "vertical_tabs",
      "#weight" => 99,
      "#attributes" => [
        "class" => [
          "entity-meta",
        ],
      ],
    ];

    $form['incident']['disposition'] = [
      '#type' => 'item',
      '#markup' => ucwords($disposition),
      '#wrapper_attributes' => ['class' => ['incident-meta__title']],
    ];

    $form['#attached']['library'][] = 'node/form';
    $form['#attached']['library'][] = 'knowledge/adherence_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['article_type'] = [
      '#type' => 'hidden',
      '#default_value' => $this->entity->get('article_type')->value,
    ];
    $form['incident_type'] = [
      '#type' => 'hidden',
      '#default_value' => $this->entity->get('incident_type')->value,
    ];
    $form['incident_id'] = [
      '#type' => 'hidden',
      '#default_value' => $this->entity->get('incident_id')->value,
    ];
    $form['knowledge_id'] = [
      '#type' => 'hidden',
      '#default_value' => $this->entity->get('knowledge_id')->target_id ?? NULL,
    ];
    $form['disposition'] = [
      '#type' => 'hidden',
      '#default_value' => $this->entity->get('disposition')->value,
    ];

    $form = parent::form($form, $form_state);

    $form['reused']['widget'][0]['value']['#states'] = [
      'visible' => [
        ':input[name="linked[0][value]"]' => ['value' => 1],
      ],
    ];
    $form['accurate']['widget'][0]['value']['#states'] = [
      'visible' => [
        ':input[name="reused[0][value]"]' => ['value' => 1],
      ],
    ];
    $form['improve']['widget'][0]['value']['#states'] = [
      'visible' => [
        ':input[name="accurate[0][value]"]' => ['value' => 1],
      ],
    ];
    $form['required']['widget'][0]['value']['#states'] = [
      'visible' => [
        ':input[name="linked[0][value]"]' => ['value' => 0],
      ],
    ];
    $form['found']['widget'][0]['value']['#states'] = [
      'visible' => [
        ':input[name="required[0][value]"]' => ['value' => 1],
      ],
    ];
    $form['article_id']['widget'][0]['target_id']['#states'] = [
      'visible' => [
        ':input[name="found[0][value]"]' => ['value' => 1],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    if (is_null($form_state->getValue('article_id'))) {
      $form_state->setValue('article_type', NULL);
    }
    $incident_id = $form_state->getValue('incident_id');
    $kid = $form_state->getValue('knowledge_id');
    $article_id = $form_state->getValue('article_id')[0]['target_id'];
    $article_type = $article_id > 0 ? 'node' : NULL;
    $entity->set('article_type', $article_type);

    $name = $incident_id . ' ';
    if ($kid) {
      /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
      $knowledge = $this->knowledgeStorage->load($kid);
      $article_id = $knowledge->get('entity_id')->target_id;
      $name .= 'kid:' . $kid . ' ';
    }
    if ($article_id > 0) {
      $node = $this->nodeStorage->load($article_id);
      $name .= 'on ' . $node->label();
    }
    $name = substr($name, 0, 250);
    $entity->set('name', $name);

    if (empty($kid)) {
      $entity->set('knowledge_id', NULL);
    }

    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Adherence.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Adherence.', [
          '%label' => $entity->label(),
        ]));
    }

    $form_state->setRedirect('entity.knowledge_adherence.canonical', ['knowledge_adherence' => $entity->id()]);

    return $status;
  }

}
