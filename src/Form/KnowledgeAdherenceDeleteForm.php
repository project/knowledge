<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Adherence entities.
 *
 * @ingroup knowledge
 */
class KnowledgeAdherenceDeleteForm extends ContentEntityDeleteForm {


}
