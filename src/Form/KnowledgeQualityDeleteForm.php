<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Quality entities.
 *
 * @ingroup knowledge
 */
class KnowledgeQualityDeleteForm extends ContentEntityDeleteForm {


}
