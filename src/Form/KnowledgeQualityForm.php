<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\knowledge\Helper\QualityField;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Form controller for Quality edit forms.
 *
 * @ingroup knowledge
 */
class KnowledgeQualityForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The field definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * The knowledge settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The Quality form.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account.
   * @param array $definitions
   *   The field definitions.
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   The quality settings.
   */
  public function __construct(AccountProxyInterface $current_user, array $definitions, ImmutableConfig $settings) {
    $this->account = $current_user;
    $this->definitions = $definitions;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_field.manager')
        ->getFieldDefinitions('knowledge_quality', 'knowledge_quality'),
      $container->get('config.factory')->get('knowledge.quality.settings'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    unset($form['actions']['delete']);

    $this->addFieldSets($form);

    $this->addFields($form);

    return $form;
  }

  /**
   * Add fieldsets to the form.
   *
   * @param array $form
   *   The form array.
   */
  protected function addFieldSets(array &$form) {

    $quality_categories = $this->settings->get('categories');
    $options_pipe = explode(PHP_EOL, $quality_categories);

    $options = [];
    foreach ($options_pipe as $option) {
      $option = explode('|', $option);
      $options[$option[0]] = trim($option[1] ?? $option[0]);
    }

    foreach ($options as $category => $title) {
      $form[$category] = [
        '#type' => 'fieldset',
        '#title' => $title,
      ];
    }

    return $form;
  }

  /**
   * Add fields to the fieldsets.
   *
   * @param array $form
   *   The form array.
   */
  protected function addFields(array &$form) {
    $category_fields = QualityField::categoryFields($this->definitions);

    foreach ($category_fields as $category => $fields) {
      foreach ($fields as $field_name) {
        if (!isset($form[$field_name])) {
          continue;
        }

        $form[$category][$field_name] = $form[$field_name];
        unset($form[$field_name]);
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Quality.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Quality.', [
          '%label' => $entity->label(),
        ]));
    }

    return $status;
  }

}
