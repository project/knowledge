<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Provides a form for deleting Competency entities.
 *
 * @ingroup knowledge
 */
class CompetencyDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    $entity = $this->getEntity();
    $owner = $entity->getOwner();

    return Url::fromRoute('entity.user.knowledge_competency', ['user' => $owner->id()]);

  }

}
