<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class LearnerForm extends FormBase {

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->userStorage = $container->get('entity_type.manager')->getStorage('user');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->state = $container->get('state');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "knowledge_learners";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $coach = $this->routeMatch->getParameter('user');
    $query = $this->userStorage->getQuery();
    $leaner_uids = $query
      ->condition('knowledge_coach', $coach)
      ->accessCheck(FALSE)
      ->execute();
    $leaners = $this->userStorage->loadMultiple($leaner_uids);

    $form['#tree'] = TRUE;
    $form['learners'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Your learners'),
    ];

    $i = 0;
    foreach ($leaners as $leaner) {
      $form['learners'][$i]['uid'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'user',
        '#selection_settings' => ['include_anonymous' => FALSE],
        '#default_value' => $leaner,
      ];
      $i += 1;
    }
    $form['learners'][$i]['uid'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#selection_settings' => ['include_anonymous' => FALSE],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('learners');
    $coach = $this->routeMatch->getParameter('user');
    $n = 0;

    $allow_leader_coach = $this->config('knowledge.settings')->get('leader_coach');
    foreach ($values as $value) {
      if (empty($value['uid'])) {
        continue;
      }
      if ($coach == ($value['uid'])) {
        $form_state->setErrorByName('learners][' . $n,
          $this->t('You cannot coach yourself.'));
      }

      $is_direct_report = ($coach == $this->userStorage->load($value['uid'])->get('knowledge_leader')->target_id);
      if ($is_direct_report && !$allow_leader_coach) {
        $form_state->setErrorByName('learners][' . $n,
          $this->t('You cannot coach a direct report. It is unethical.'));
      }
      $n += 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('learners');
    $coach = $this->routeMatch->getParameter('user');
    $user_ids = [];
    $default_wave = $this->state->get('knowledge.wave_default', 1);
    foreach ($values as $value) {
      if (empty($value['uid'])) {
        continue;
      }
      $user_ids[] = $value['uid'];
    }
    $user_ids = array_unique($user_ids);

    /** @var \Drupal\user\UserInterface[] $learners */
    $learners = $this->userStorage->loadMultiple($user_ids);
    foreach ($learners as $learner) {
      $update = FALSE;
      if ($learner->knowledge_coach->target_id != $coach) {
        $learner->knowledge_coach->target_id = $coach;
        $update = TRUE;
      }
      if (empty($learner->knowledge_wave->target_id)) {
        $learner->knowledge_wave->target_id = $default_wave;
        $update = TRUE;
      }
      if ($update) {
        $learner->save();
      }
    }
    $query = $this->userStorage->getQuery();
    $query->condition('knowledge_coach', $coach);
    if (!empty($user_ids)) {
      $query->condition('uid', $user_ids, 'NOT IN');
    }
    $remove_uids = $query->accessCheck(FALSE)->execute();

    /** @var \Drupal\user\UserInterface[] $old_learners */
    $old_learners = $this->userStorage->loadMultiple($remove_uids);
    foreach ($old_learners as $learner) {
      $learner->knowledge_coach->target_id = NULL;
      $learner->save();
    }
  }

}
