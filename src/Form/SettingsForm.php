<?php

namespace Drupal\knowledge\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings for the knowledge module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The node type storage service.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * The cache tag invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheInvalidator;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_invalidator
   *   The cache tag invalidation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, CacheTagsInvalidatorInterface $cache_invalidator) {
    $this->setConfigFactory($config_factory);
    $this->nodeTypeStorage = $entity_type_manager->getStorage('node_type');
    $this->cacheInvalidator = $cache_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['knowledge.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "knowledge_settings_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('knowledge.settings');
    $node_types = $this->nodeTypeStorage->loadMultiple();
    $options = [];
    foreach ($node_types as $machine => $type) {
      $options[$machine] = $type->label();
    }
    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#options' => $options,
      '#default_value' => $config->get('node_types'),
    ];

    $form['audience'] = [
      '#title' => $this->t('Audience'),
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-public',
    ];

    $form['audience']['public'] = [
      '#type' => 'details',
      '#title' => $this->t('Public'),
      '#group' => 'audience',
    ];
    $form['audience']['public']['public_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config->get('audience.public.label'),
    ];
    $form['audience']['public']['public_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('audience.public.enabled'),
    ];
    $form['audience']['customer'] = [
      '#type' => 'details',
      '#title' => $this->t('Customer'),
      '#group' => 'audience',
    ];
    $form['audience']['customer']['customer_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config->get('audience.customer.label'),
    ];
    $form['audience']['customer']['customer_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('audience.customer.enabled'),
    ];
    $form['audience']['partner'] = [
      '#type' => 'details',
      '#title' => $this->t('Partner'),
      '#group' => 'audience',
    ];
    $form['audience']['partner']['partner_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config->get('audience.partner.label'),
    ];
    $form['audience']['partner']['partner_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('audience.partner.enabled'),
    ];
    $form['audience']['internal'] = [
      '#type' => 'details',
      '#title' => $this->t('Internal'),
      '#group' => 'audience',
    ];
    $form['audience']['internal']['internal_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config->get('audience.internal.label'),
    ];
    $form['audience']['internal']['internal_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('audience.internal.enabled'),
    ];

    $form['leader_coach'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow leader to coach direct reports.'),
      '#description' => $this->t('It is <strong>highly</strong> discouraged to let leaders coach their direct reports.'),
      '#default_value' => $config->get('leader_coach'),
    ];

    $form['auto_actions'] = [
      '#title' => $this->t('Actions'),
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-public',
    ];

    $form['auto_actions']['work_in_progress__not_validated'] = [
      '#type' => 'details',
      '#title' => $this->t('From Work In Progress to Not Validated'),
      '#group' => 'auto_actions',
    ];

    $form['auto_actions']['work_in_progress__not_validated']['work_in_progress__not_validated__action'] = [
      '#title' => $this->t('Action'),
      '#type' => 'select',
      '#options' => [
        'none' => $this->t('Do nothing'),
        'message' => $this->t('Show message to user'),
        'promote' => $this->t('Promote the confidence'),
      ],
      '#default_value' => $config->get('actions.work_in_progress__not_validated.action'),
    ];

    $form['auto_actions']['work_in_progress__not_validated']['work_in_progress__not_validated__message'] = [
      '#title' => $this->t('Message'),
      '#type' => 'textarea',
      '#default_value' => $config->get('actions.work_in_progress__not_validated.message'),
      '#states' => [
        'visible' => [':input[name="work_in_progress__not_validated__action"]' => ['value' => 'message']],
      ],
    ];

    $form['auto_actions']['work_in_progress__not_validated']['work_in_progress__not_validated__count'] = [
      '#title' => $this->t('Count'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $config->get('actions.work_in_progress__not_validated.count') ?? 1,
    ];

    $form['auto_actions']['work_in_progress__not_validated']['work_in_progress__not_validated__frequency'] = [
      '#title' => $this->t('Frequency'),
      '#type' => 'radios',
      '#options' => [
        'every' => $this->t('Every X links'),
        'greater_than' => $this->t('Greater than X links'),
      ],
      '#default_value' => $config->get('actions.work_in_progress__not_validated.frequency') ?? 'every',
    ];

    $form['auto_actions']['not_validated__validated'] = [
      '#type' => 'details',
      '#title' => $this->t('From Not Validated to Validated'),
      '#group' => 'auto_actions',
    ];
    $form['auto_actions']['not_validated__validated']['not_validated__validated__action'] = [
      '#title' => $this->t('Action'),
      '#type' => 'select',
      '#options' => [
        'none' => $this->t('Do nothing'),
        'message' => $this->t('Show message to user'),
        'promote' => $this->t('Promote the confidence'),
      ],
      '#default_value' => $config->get('actions.not_validated__validated.action'),
    ];

    $form['auto_actions']['not_validated__validated']['not_validated__validated__message'] = [
      '#title' => $this->t('Message'),
      '#type' => 'textarea',
      '#default_value' => $config->get('actions.not_validated__validated.message'),
      '#states' => [
        'visible' => [':input[name="not_validated__validated__action"]' => ['value' => 'message']],
      ],
    ];

    $form['auto_actions']['not_validated__validated']['not_validated__validated__count'] = [
      '#title' => $this->t('Count'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $config->get('actions.not_validated__validated.count') ?? 1,
    ];

    $form['auto_actions']['not_validated__validated']['not_validated__validated__frequency'] = [
      '#title' => $this->t('Frequency'),
      '#type' => 'radios',
      '#options' => [
        'every' => $this->t('Every X links'),
        'greater_than' => $this->t('Greater than X links'),
      ],
      '#default_value' => $config->get('actions.not_validated__validated.frequency') ?? 'every',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $node_types = $form_state->getValue('node_types');
    $node_types = array_filter($node_types);

    $config = $this->config('knowledge.settings');
    $original_node_types = $config->get('node_types');

    $config
      ->set('audience.public.label', $form_state->getValue('public_label'))
      ->set('audience.public.enabled', $form_state->getValue('public_enabled'))

      ->set('audience.customer.label', $form_state->getValue('customer_label'))
      ->set('audience.customer.enabled', $form_state->getValue('customer_enabled'))

      ->set('audience.partner.label', $form_state->getValue('partner_label'))
      ->set('audience.partner.enabled', $form_state->getValue('partner_enabled'))

      ->set('audience.internal.label', $form_state->getValue('internal_label'))
      ->set('audience.internal.enabled', $form_state->getValue('internal_enabled'))

      ->set('actions.work_in_progress__not_validated.action', $form_state->getValue('work_in_progress__not_validated__action'))
      ->set('actions.work_in_progress__not_validated.count', $form_state->getValue('work_in_progress__not_validated__count'))
      ->set('actions.work_in_progress__not_validated.message', $form_state->getValue('work_in_progress__not_validated__message'))
      ->set('actions.work_in_progress__not_validated.frequency', $form_state->getValue('work_in_progress__not_validated__frequency'))

      ->set('actions.not_validated__validated.action', $form_state->getValue('not_validated__validated__action'))
      ->set('actions.not_validated__validated.count', $form_state->getValue('not_validated__validated__count'))
      ->set('actions.not_validated__validated.message', $form_state->getValue('not_validated__validated__message'))
      ->set('actions.not_validated__validated.frequency', $form_state->getValue('not_validated__validated__frequency'))

      ->set('node_types', $node_types)
      ->set('leader_coach', $form_state->getValue('leader_coach'))
      ->save();

    if ($original_node_types != $node_types) {
      $this->cacheInvalidator->invalidateTags([
        'config:knowledge.settings.node_types',
      ]);
    }
  }

}
