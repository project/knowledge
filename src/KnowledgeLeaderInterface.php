<?php

namespace Drupal\knowledge;

use Drupal\user\UserInterface;

/**
 * Defines an interface for managing the knowledge_leader role on a entity.
 */
interface KnowledgeLeaderInterface {

  /**
   * Adds the knowledge_leader role if there are direct reports.
   *
   * @param Drupal\user\UserInterface $leader
   *   The user being looked at.
   */
  public function addKnowledgeLeader(UserInterface &$leader);

  /**
   * Removes the knowledge_leader role if there are direct reports.
   *
   * @param Drupal\user\UserInterface $leader
   *   The user being looked at.
   */
  public function removeKnowledgeLeader(UserInterface &$leader);

  /**
   * Removes the knowledge_leader role if there are direct reports.
   *
   * @param Drupal\user\UserInterface $user
   *   The user that was updated.
   * @param Drupal\user\UserInterface $leader
   *   The leader being looked at.
   */
  public function updateKnowledgeLeader(&$user, &$leader);

}
