<?php

namespace Drupal\knowledge;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Adherence entities.
 *
 * @ingroup knowledge
 */
class KnowledgeAdherenceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Adherence ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\knowledge\Entity\KnowledgeAdherence $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.knowledge_adherence.edit_form',
      ['knowledge_adherence' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
