<?php

/**
 * @file
 * Provide views data for knowledge.module.
 */

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Implements hook_views_data_alter().
 */
function knowledge_views_data_alter(&$data) {

  $data['node_field_data']['knowledge_node_type'] = [
    'title' => t('Knowledge Content Types'),
    'group' => t('Content'),
    'filter' => [
      'title' => t('Knowledge Content Types'),
      'help' => t('Limit to "knowledge" content types.'),
      'field' => 'type',
      'id' => 'knowledge_node_type',
    ],
  ];

  $data['node_field_data']['knowledge_merge'] = [
    'field' => [
      'title' => t('Merge link'),
      'help' => t('Provide a simple link to merge duplicate articles.'),
      'id' => 'knowledge_merge_link',
    ],
  ];

  $data['knowledge_adherence']['knowledge_disposition'] = [
    'title' => t('Disposition'),
    'group' => t('Adherence'),
    'filter' => [
      'title' => t('Knowledge Adherence Disposition'),
      'help' => t('Filters by disposition.'),
      'field' => 'disposition',
      'id' => 'knowledge_disposition',
    ],
  ];

  $data['users_field_data']['knowledge_learners'] = [
    'title' => t('Knowledge Learners'),
    'group' => t('User'),
    'filter' => [
      'title' => t('Knowledge Learners'),
      'help' => t('Limit a coaches learners.'),
      'field' => 'uid',
      'id' => 'knowledge_learners',
    ],
  ];
  $data['users_field_data']['knowledge_leader_coach'] = [
    'title' => t('Knowledge Leader Coach'),
    'help' => t("The user's coach is also their leader."),
    'filter' => [
      'id' => 'knowledge_leader_coach',
    ],
  ];

  // New knowledge is only supported for node table because it requires the
  // history table.
  $data['node']['new_knowledge'] = [
    'title' => t('New knowledge'),
    'help' => t('The amount of new knowledge on the node.'),
    'field' => [
      'id' => 'node_new_knowledge',
      'no group by' => TRUE,
    ],
  ];

  // Provides an integration for each entity type except knowledge.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if ($entity_type_id == 'knowledge' || !$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
      continue;
    }
    $fields = \Drupal::service('knowledge.manager')->getFields($entity_type_id);
    $base_table = $entity_type->getDataTable() ?: $entity_type->getBaseTable();
    $args = ['@entity_type' => $entity_type_id];

    if ($fields) {
      $data[$base_table]['knowledge_link'] = [
        'field' => [
          'title' => t('Add knowledge link'),
          'help' => t('Display the standard add knowledge link used on regular @entity_type, which will only display if the viewing user has access to add a knowledge.', $args),
          'id' => 'knowledge_entity_link',
        ],
      ];

      // Multilingual properties are stored in data table.
      if (!($table = $entity_type->getDataTable())) {
        $table = $entity_type->getBaseTable();
      }
      $data[$table]['uid_touch'] = [
        'title' => t('User posted or knowledge'),
        'help' => t('Display nodes only if a user posted the @entity_type or knowledge on the @entity_type.', $args),
        'argument' => [
          'field' => 'uid',
          'name table' => 'users_field_data',
          'name field' => 'name',
          'id' => 'argument_knowledge_user_uid',
          'no group by' => TRUE,
          'entity_type' => $entity_type_id,
          'entity_id' => $entity_type->getKey('id'),
        ],
        'filter' => [
          'field' => 'uid',
          'name table' => 'users_field_data',
          'name field' => 'name',
          'id' => 'knowledge_user_uid',
          'entity_type' => $entity_type_id,
          'entity_id' => $entity_type->getKey('id'),
        ],
      ];

      foreach ($fields as $field_name => $field) {
        $data[$base_table][$field_name . '_kid'] = [
          'title' => t('Knowledge of the @entity_type using field: @field_name', $args + ['@field_name' => $field_name]),
          'help' => t('Relate all knowledge on the @entity_type. This will create 1 duplicate record for every knowledge. Usually if you need this it is better to create a knowledge view.', $args),
          'relationship' => [
            'group' => t('Knowledge'),
            'label' => t('Knowledge'),
            'base' => 'knowledge',
            'base field' => 'entity_id',
            'relationship field' => $entity_type->getKey('id'),
            'id' => 'standard',
            'extra' => [
              [
                'field' => 'entity_type',
                'value' => $entity_type_id,
              ],
              [
                'field' => 'field_name',
                'value' => $field_name,
              ],
            ],
          ],
        ];
      }
    }
  }

  $data['users_field_data']['knowledge_approve'] = [
    'title' => t('Knowledge Competency Approval'),
    'help' => t("Link to approve user's promotion."),
    'field' => [
      'id' => 'knowledge_competency_approve',
    ],

  ];

  $data['knowledge_competency']['progress'] = [
    'title' => t("Progress"),
    'help' => t('The progress of mastery.'),
    'field' => [
      'id' => 'knowledge_competency_percentage',
      'no group by' => TRUE,
    ],
    'sort' => [
      'id' => 'knowledge_competency_percentage',
      'no group by' => TRUE,
    ],
  ];

}
