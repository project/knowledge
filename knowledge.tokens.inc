<?php

/**
 * @file
 * Builds placeholder replacement tokens for knowledge-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function knowledge_token_info() {
  $type = [
    'name' => t('Knowledge'),
    'description' => t('Tokens for knowledge posted on the site.'),
    'needs-data' => 'knowledge',
  ];

  $tokens = [];
  // Provides an integration for each entity type except knowledge.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if ($entity_type_id == 'knowledge' || !$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }

    if (\Drupal::service('knowledge.manager')->getFields($entity_type_id)) {
      // Get the correct token type.
      $token_type = ($entity_type_id == 'taxonomy_term') ? 'term' : $entity_type_id;

      // @todo Make this work per field. See https://www.drupal.org/node/2031903.
      $tokens[$token_type]['knowledge-count'] = [
        'name' => t("Knowledge count"),
        'description' => t("The number of knowledge posted on an entity."),
      ];
      $tokens[$token_type]['knowledge-count-new'] = [
        'name' => t("New knowledge count"),
        'description' => t("The number of knowledge posted on an entity since the reader last viewed it."),
      ];
    }
  }

  // Core knowledge tokens.
  $knowledge['kid'] = [
    'name' => t("Knowledge ID"),
    'description' => t("The unique ID of the knowledge."),
  ];
  $knowledge['title'] = [
    'name' => t("Title"),
    'description' => t("The title of the knowledge."),
  ];
  $knowledge['langcode'] = [
    'name' => t('Language code'),
    'description' => t('The language code of the language the knowledge is written in.'),
  ];
  $knowledge['url'] = [
    'name' => t("URL"),
    'description' => t("The URL of the knowledge."),
  ];
  $knowledge['edit-url'] = [
    'name' => t("Edit URL"),
    'description' => t("The URL of the knowledge's edit page."),
  ];

  // Chained tokens for knowledge.
  $knowledge['created'] = [
    'name' => t("Date created"),
    'description' => t("The date the knowledge was posted."),
    'type' => 'date',
  ];
  $knowledge['changed'] = [
    'name' => t("Date changed"),
    'description' => t("The date the knowledge was most recently updated."),
    'type' => 'date',
  ];

  $knowledge['entity'] = [
    'name' => t("Entity"),
    'description' => t("The entity the knowledge was posted to."),
    'type' => 'entity',
  ];
  $knowledge['author'] = [
    'name' => t("Author"),
    'description' => t("The author name of the knowledge."),
    'type' => 'user',
  ];

  return [
    'types' => ['knowledge' => $type],
    'tokens' => [
      'knowledge' => $knowledge,
    ] + $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function knowledge_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $replacements = [];

  if ($type == 'knowledge' && !empty($data['knowledge'])) {
    /** @var \Drupal\knowledge\KnowledgeInterface $knowledge */
    $knowledge = $data['knowledge'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the knowledge.
        case 'kid':
          $replacements[$original] = $knowledge->id();
          break;

        case 'title':
          $replacements[$original] = $knowledge->label();
          break;

        case 'langcode':
          $replacements[$original] = $knowledge->language()->getId();
          break;

        // Knowledge related URLs.
        case 'url':
          $url_options['fragment'] = 'knowledge-' . $knowledge->id();
          $replacements[$original] = $knowledge->toUrl('canonical', $url_options)->toString();
          break;

        case 'edit-url':
          $url_options['fragment'] = NULL;
          $replacements[$original] = $knowledge->toUrl('edit-form', $url_options)->toString();
          break;

        case 'author':
          $name = $knowledge->getAuthorName();
          // Add the user cacheability metadata in case the author of the
          // knowledge is not the anonymous user.
          if ($knowledge->getOwnerId()) {
            $bubbleable_metadata->addCacheableDependency($knowledge->getOwner());
          }
          $replacements[$original] = $name;
          break;

        case 'created':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')->format($knowledge->getCreatedTime(), 'medium', '', NULL, $langcode);
          break;

        case 'changed':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')->format($knowledge->getChangedTime(), 'medium', '', NULL, $langcode);
          break;

        case 'entity':
          $entity = $knowledge->getKnowledgeedEntity();
          $bubbleable_metadata->addCacheableDependency($entity);
          $title = $entity->label();
          $replacements[$original] = $title;
          break;
      }
    }

    // Chained token relationships.
    if ($entity_tokens = $token_service->findwithPrefix($tokens, 'entity')) {
      $entity = $knowledge->getKnowledgeedEntity();
      $replacements += $token_service->generate($knowledge->getKnowledgeedEntityTypeId(), $entity_tokens, [$knowledge->getKnowledgeedEntityTypeId() => $entity], $options, $bubbleable_metadata);
    }

    if ($date_tokens = $token_service->findwithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate('date', $date_tokens, ['date' => $knowledge->getCreatedTime()], $options, $bubbleable_metadata);
    }

    if ($date_tokens = $token_service->findwithPrefix($tokens, 'changed')) {
      $replacements += $token_service->generate('date', $date_tokens, ['date' => $knowledge->getChangedTime()], $options, $bubbleable_metadata);
    }

    if (($author_tokens = $token_service->findwithPrefix($tokens, 'author')) && $account = $knowledge->getOwner()) {
      $replacements += $token_service->generate('user', $author_tokens, ['user' => $account], $options, $bubbleable_metadata);
    }
  }
  // Replacement tokens for any content entities that have knowledge field.
  elseif (!empty($data[$type]) && $data[$type] instanceof FieldableEntityInterface) {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $data[$type];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'knowledge-count':
          $count = 0;
          $fields = array_keys(\Drupal::service('knowledge.manager')->getFields($entity->getEntityTypeId()));
          $definitions = array_keys($entity->getFieldDefinitions());
          $valid_fields = array_intersect($fields, $definitions);
          foreach ($valid_fields as $field_name) {
            /** @var \Drupal\knowledge\Plugin\Field\FieldType\KnowledgeItemInterface $field */
            $field = $entity->get($field_name);
            $count += $field->total_count;
          }
          $replacements[$original] = $count;
          break;

        case 'knowledge-count-new':
          $replacements[$original] = \Drupal::service('knowledge.manager')->getCountNewKnowledges($entity);
          break;
      }
    }
  }

  return $replacements;
}
