<?php

/**
 * @file
 * Post update functions for the knowledge module.
 */

/**
 * Implements hook_removed_post_updates().
 */
function knowledge_removed_post_updates() {
  return [
    'knowledge_post_update_enable_knowledge_admin_view' => '9.0.0',
    'knowledge_post_update_add_ip_address_setting' => '9.0.0',
  ];
}
